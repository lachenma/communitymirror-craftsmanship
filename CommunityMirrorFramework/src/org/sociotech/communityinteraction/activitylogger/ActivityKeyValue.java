package org.sociotech.communityinteraction.activitylogger;

/**
 * A simple key value pair for activity logging.
 * 
 * @author Peter Lachenmaier
 * 
 * @param <T> The type of the value
 */
public class ActivityKeyValue<T> {
	/**
	 * The key
	 */
	private final String key;
	
	/**
	 * The value
	 */
	private final T value;
	
	/**
	 * Initializes the key value pair with the given parameters.
	 * 
	 * @param key Key 
	 * @param value Value
	 */
	public ActivityKeyValue(String key, T value) {
		this.key = key;
		this.value = value;
	}

	/**
	 * Returns the key used in this key value pair
	 * 
	 * @return The key used in this key value pair
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Returns the value used in this key value pair
	 * 
	 * @return The value used in this key value pair
	 */
	public T getValue() {
		return value;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return key + ":\t" + value;
	}
}
