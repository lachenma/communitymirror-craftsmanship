package org.sociotech.communityinteraction.sample;

import java.util.LinkedList;
import java.util.List;

import javafx.scene.Group;

import org.sociotech.communityinteraction.behaviors.InteractionBehavior;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.PositionEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragDraggedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragStartedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationRotatedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationStartedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomStartedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomZoomedEvent;
import org.sociotech.communityinteraction.helper.CIEventConsumerHelper;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interactionhandling.HandleDrag;
import org.sociotech.communityinteraction.interactionhandling.HandleRotate;
import org.sociotech.communityinteraction.interactionhandling.HandleZoom;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;
import org.sociotech.communityinteraction.interfaces.ManageInteractionBehaviors;
import org.sociotech.communityinteraction.interfaces.PositionedEventConsumer;

/**
 * Simple abstract component as base for components that react on CommunityInteraction events.
 * 
 * @author Peter Lachenmaier
 */
public abstract class CIEventsConsumerComponent extends Group implements PositionedEventConsumer, ManageInteractionBehaviors, HandleRotate, HandleZoom, HandleDrag {

	/**
	 * The list of interaction behaviors. The behaviors are used to visualize the resulting
	 * Event and not only the initial one. All interaction behaviors are possible event consumers. 
	 */
	private List<InteractionBehavior<?>> interactionBehaviors;
	
	/**
	 * The counter for ids
	 */
	private static int idCounter = 0;
	
	/**
	 * The id of this event consumer.
	 */
	private String id;
	
	public CIEventsConsumerComponent() {
		// create new list for interaction behaviors
		interactionBehaviors = new LinkedList<>();
		// create id
		id = "" + idCounter++;
	}
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#getConsumers(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public List<CIEventConsumer> getConsumers(CIEvent event) {
		// TODO check if this restriction is necessary or intended
		if(!(event instanceof PositionEvent)) {
			// allow only position events
			return CIListHelper.emptyUnmodifiableEventConsumerList();
		}
		
		// use behaviors
		return CIEventConsumerHelper.collectConsumersFromBehaviors(interactionBehaviors, event);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#handle(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public void handle(CIEvent event) {
		// nothing to do in general handle method
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.ManageInteractionBehaviors#addInteractionBehavior(org.sociotech.communityinteraction.behaviors.InteractionBehavior)
	 */
	@Override
	public void addInteractionBehavior(InteractionBehavior<?> interactionBehavior) {
		interactionBehaviors.add(interactionBehavior);
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.ManageInteractionBehaviors#getInteractionBehaviors()
	 */
	@Override
	public List<InteractionBehavior<?>> getInteractionBehaviors(){
		return this.interactionBehaviors;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communityinteraction.interfaces.ManageInteractionBehaviors
	 * #removeInteractionBehavior
	 * (org.sociotech.communityinteraction.behaviors.InteractionBehavior)
	 */
	@Override
	public void removeInteractionBehavior(
			InteractionBehavior<?> interactionBehavior) {
		interactionBehaviors.remove(interactionBehavior);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.PositionedEventConsumer#isOnPosition(double, double)
	 */
	@Override
	public boolean isOnPosition(double sceneX, double sceneY) {
		// TODO check if one of the components in this group contains the point
		if(this.contains(this.sceneToLocal(sceneX, sceneY))) {
			return true;
		}
		
		return false;
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#getID()
	 */
	@Override
	public String getID() {
		return id;
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.PositionedEventConsumer#setID(java.lang.String)
	 */
	@Override
	public void setID(String id) {
		this.id = id;
	}
	
	// implementation of HandleRotate
	
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interactionhandling.HandleRotate#onRotationStarted(org.sociotech.communityinteraction.events.gesture.rotation.RotationStartedEvent)
	 */
	@Override
	public void onRotationStarted(RotationStartedEvent rotationEvent) {
		// currently do nothing
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interactionhandling.HandleRotate#onRotationRotated(org.sociotech.communityinteraction.events.gesture.rotation.RotationRotatedEvent)
	 */
	@Override
	public void onRotationRotated(RotationRotatedEvent rotationEvent) {
		// rotate
		this.setRotate(this.getRotate() + rotationEvent.getAngle());
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interactionhandling.HandleRotate#onRotationFinished(org.sociotech.communityinteraction.events.gesture.rotation.RotationFinishedEvent)
	 */
	@Override
	public void onRotationFinished(RotationFinishedEvent rotationEvent) {
		// currently do nothing
	}
	
	// implementation of HandleZoom
	
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interactionhandling.HandleZoom#onZoomStarted(org.sociotech.communityinteraction.events.gesture.zoom.ZoomStartedEvent)
	 */
	@Override
	public void onZoomStarted(ZoomStartedEvent zoomEvent) {
		// currently do nothing
	}
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interactionhandling.HandleZoom#onZoomZoomed(org.sociotech.communityinteraction.events.gesture.zoom.ZoomZoomedEvent)
	 */
	@Override
	public void onZoomZoomed(ZoomZoomedEvent zoomEvent) {
		// scale in both directions
		this.setScaleX(this.getScaleX() * zoomEvent.getZoomFactor());
		this.setScaleY(this.getScaleY() * zoomEvent.getZoomFactor());
	}
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interactionhandling.HandleZoom#onZoomFinished(org.sociotech.communityinteraction.events.gesture.zoom.ZoomFinishedEvent)
	 */
	@Override
	public void onZoomFinished(ZoomFinishedEvent zoomEvent) {
		// currently do nothing
	}
		
	// implementation of HandleDrag
	
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interactionhandling.HandleDrag#onDragStarted(org.sociotech.communityinteraction.events.gesture.drag.DragStartedEvent)
	 */
	@Override
	public void onDragStarted(DragStartedEvent dragEvent) {
		// currently do nothing
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interactionhandling.HandleDrag#onDragRotated(org.sociotech.communityinteraction.events.gesture.drag.DragDraggedEvent)
	 */
	@Override
	public void onDragDragged(DragDraggedEvent dragEvent) {
		// move in both direction
		this.setLayoutX(this.getLayoutX() + dragEvent.getDeltaX());
		this.setLayoutY(this.getLayoutY() + dragEvent.getDeltaY());
		
		// consume event
		dragEvent.consume();
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interactionhandling.HandleDrag#onDragFinished(org.sociotech.communityinteraction.events.gesture.drag.DragFinishedEvent)
	 */
	@Override
	public void onDragFinished(DragFinishedEvent dragEvent) {
		// currently do nothing
	}
	
}
