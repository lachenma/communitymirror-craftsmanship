package org.sociotech.communityinteraction.helper;

import java.util.LinkedList;
import java.util.List;

import org.sociotech.communityinteraction.behaviors.InteractionBehavior;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

/**
 * Helper class that provides methods for easier handling of event consumers.
 * 
 * @author Peter Lachenmaier
 */
public class CIEventConsumerHelper {
	
	/**
	 * Asks every consumer for its event consumers for the given event and collects them.
	 * 
	 * @param eventConsumers Collection of event consumers.
	 * @param event Event to find consumers for.
	 * @return A list of possible consumers for the given event.
	 */
	public static List<CIEventConsumer> collectConsumers(List<CIEventConsumer> eventConsumers, CIEvent event) {
		List<CIEventConsumer> result = new LinkedList<>();
		
		if(eventConsumers != null && event != null) {
			List<CIEventConsumer> tmpConsumerList;
			// look if consumers return consumers
			for(CIEventConsumer consumer : eventConsumers) {
				
				// stop if event is filtered
				if(event.isFiltered()) {
					break;
				}
				
				try {
					tmpConsumerList = consumer.getConsumers(event);
				} catch (Exception e) {
					// continue with next behavior
					continue;
				}
				
				if(tmpConsumerList != null) {
					// add all to result
					result.addAll(tmpConsumerList);
				}
			}
		}
		
		// return the collection
		return result;
	}
	
	/**
	 * Asks every behavior for its event consumers for the given event and collects them.
	 * 
	 * @param behaviors Collection of event consuming behaviors.
	 * @param event Event to find consumers for.
	 * @return A list of possible consumers for the given event.
	 */
	public static List<CIEventConsumer> collectConsumersFromBehaviors(List<InteractionBehavior<?>> behaviors, CIEvent event) {
		
		// TODO check
		// doubled implementation for type safety
		
		List<CIEventConsumer> result = CIListHelper.createConsumerList();
		
		if(behaviors != null && event != null) {
			List<CIEventConsumer> tmpConsumerList;
			// look if behaviors return consumers
			for(CIEventConsumer behavior : behaviors) {
				
				// stop if event is consumed
				if(event.isConsumed()) {
					break;
				}
				
				try {
					tmpConsumerList = behavior.getConsumers(event);
				} catch (Exception e) {
					// continue with next behavior
					continue;
				}
				
				if(tmpConsumerList != null) {
					// add all to result
					result.addAll(tmpConsumerList);
				}
			}
		}
		
		// return the collection
		return result;
	} 
}
