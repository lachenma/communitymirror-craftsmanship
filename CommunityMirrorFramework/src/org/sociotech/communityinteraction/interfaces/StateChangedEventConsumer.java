package org.sociotech.communityinteraction.interfaces;

import org.sociotech.communityinteraction.events.stateChange.State;
import org.sociotech.communityinteraction.events.stateChange.StateChangeableComponent;

/**
 * Interface that must be implemented by components that consume state changed
 * events.
 * 
 * @param <T>
 *            The concrete state class that has changed.
 * @param <U>
 *            The concrete component class that has changed.
 * 
 * @author evalosch
 *
 */
public interface StateChangedEventConsumer<T extends State, U extends StateChangeableComponent<T>> extends
		CIEventConsumer {

}
