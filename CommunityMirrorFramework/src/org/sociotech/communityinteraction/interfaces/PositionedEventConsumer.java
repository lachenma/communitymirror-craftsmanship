package org.sociotech.communityinteraction.interfaces;



/**
 * Interface that must be implemented by components consuming events only
 * on certain positions.
 * 
 * @author Peter Lachenmaier
 */
public interface PositionedEventConsumer extends CIEventConsumer {
	
	
	/**
	 * Returns whether this consumer is on the given scene position or not.
	 * 
	 * @param sceneX Scene x position
	 * @param sceneY Scene y position
	 * 
	 * @return Whether this consumer is on the given scene position or not.
	 */
	public boolean isOnPosition(double sceneX, double sceneY);
	
	/**
	 * Returns the z-index of this component
	 * @return The z-index
	 */
	public int getZIndex();
	
	/**
	 * Returns true if this event consumer matches as target for the given target.
	 * 
	 * @param target Target to match.
	 * 
	 * @return True on match, false otherwise.
	 */
	public boolean isTarget(Object target);
	
	/**
	 * Returns the id of this event consumer as string.
	 * 
	 * @return The id of this event consumer as string.
	 */
	public String getID();		
	
	/**
	 * Sets the id of the event consumer to the given value.
	 * 
	 * @param id Id to use for this event consumer.
	 */
	public void setID(String id);
}
