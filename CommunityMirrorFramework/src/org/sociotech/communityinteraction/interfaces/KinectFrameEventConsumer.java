package org.sociotech.communityinteraction.interfaces;

/**
 * Interface that must be implemented by components that consume Kinect frame events.
 *  
 * @author evalosch
 *
 */
public interface KinectFrameEventConsumer extends CIEventConsumer {

}
