package org.sociotech.communityinteraction.interfaces;



/**
 * Interface that must be implemented by components consuming intersection events.
 * 
 * @author Eva Loesch
 */
public interface IntersectionEventConsumer extends CIEventConsumer {
	
	/**
	 * Returns the z-index of this component
	 * @return The z-index
	 */
	public int getZIndex();
	
	/**
	 * Returns the id of this event consumer as string.
	 * 
	 * @return The id of this event consumer as string.
	 */
	public String getID();		
	
	/**
	 * Sets the id of the event consumer to the given value.
	 * 
	 * @param id Id to use for this event consumer.
	 */
	public void setID(String id);
}
