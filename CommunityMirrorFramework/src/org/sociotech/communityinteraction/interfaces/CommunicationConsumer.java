package org.sociotech.communityinteraction.interfaces;



/**
 * Interface that must be implemented by components consuming communication events.
 * 
 * @author Eva Lösch
 */
public interface CommunicationConsumer extends CIEventConsumer {

}
