package org.sociotech.communityinteraction.interfaces;

import java.util.List;

import org.sociotech.communityinteraction.events.CIEvent;

/**
 * Interface that must be implemented to consume CommunityIteraction events.
 * 
 * @author Peter Lachenmaier
 *
 */
public interface CIEventConsumer{
	
	/**
	 * Returns the possible consumers for the given event. The event
	 * can be handled to child objects or not if this element should be
	 * filtered out. Call {@link CIEvent#filter()} on the given event to
	 * filter it.
	 * 
	 * @param event Event to return possible consumers for.
	 * 
	 * @return A list of possible consumers for the given event.
	 */
	public List<CIEventConsumer> getConsumers(CIEvent event);
	
	/**
	 * Handles the given event. The event can be consumed by calling 
	 * {@link CIEvent#consume()}.
	 * 
	 * @param event Event to handle.
	 */
	public void handle(CIEvent event);
}
