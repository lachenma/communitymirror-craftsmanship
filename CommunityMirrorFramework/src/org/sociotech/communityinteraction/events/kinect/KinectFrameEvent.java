package org.sociotech.communityinteraction.events.kinect;

import org.sociotech.communityinteraction.events.SensingEvent;

/**
 * Abstract super class of all sensing events triggered by kinect frames.
 * 
 * @author evalosch
 *
 */
public abstract class KinectFrameEvent extends SensingEvent {

}
