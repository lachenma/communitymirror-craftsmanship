package org.sociotech.communityinteraction.events.kinect;

import java.util.List;

import edu.ufl.digitalworlds.j4k.Skeleton;

/**
 * A kinect frame event that is triggered if a skeleton frame is received by the kinect.
 * 
 * @author evalosch
 *
 */
public class SkeletonFrameEvent extends KinectFrameEvent {

	private List<Skeleton> skeletons;

	public SkeletonFrameEvent(List<Skeleton> skeletons) {
		this.skeletons = skeletons;
	}
	
	/**
	 * @return the skeletons
	 */
	public List<Skeleton> getSkeletons() {
		return skeletons;
	}

	/**
	 * @param skeletons the skeletons to set
	 */
	public void setSkeletons(List<Skeleton> skeletons) {
		this.skeletons = skeletons;
	}
}
