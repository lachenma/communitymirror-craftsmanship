package org.sociotech.communityinteraction.events;

/**
 * Event representing a user interaction.
 * 
 * @author Peter Lachenmaier
 */
public abstract class InteractionEvent extends CIEvent {
	/**
	 * Whether the keyboard shift button is pressed or not.
	 */
	private boolean   shiftDown = false;
	
	/**
	 * Whether the keyboard control button is pressed or not.
	 */
	private boolean controlDown = false;
	
	/**
	 * Whether the keyboard alt button is pressed or not.
	 */
	private boolean    altDown = false;
	
	/**
	 * Returns whether the keyboard shift button is pressed or not.
	 * @return Whether the keyboard shift  button is pressed or not.
	 */
	public boolean isShiftDown() {
		return shiftDown;
	}

	/**
	 * Sets the down value for the shift button.
	 * 
	 * @param down True if keyboard button is down.
	 */
	public void setShiftDown(boolean shiftDown) {
		this.shiftDown = shiftDown;
	}

	/**
	 * Returns whether the keyboard control button is pressed or not.
	 * @return Whether the keyboard control  button is pressed or not.
	 */
	public boolean isControlDown() {
		return controlDown;
	}

	/**
	 * Sets the down value for the control button.
	 * 
	 * @param down True if keyboard button is down.
	 */
	public void setControlDown(boolean controlDown) {
		this.controlDown = controlDown;
	}

	/**
	 * Returns whether the keyboard control button is pressed or not.
	 * @return Whether the keyboard control  button is pressed or not.
	 */
	public boolean isAltDown() {
		return altDown;
	}

	/**
	 * Sets the down value for the alt button.
	 * 
	 * @param down True if keyboard button is down.
	 */
	public void setAltDown(boolean altDown) {
		this.altDown = altDown;
	}
}
