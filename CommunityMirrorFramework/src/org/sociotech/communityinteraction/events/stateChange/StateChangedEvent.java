package org.sociotech.communityinteraction.events.stateChange;

import org.sociotech.communityinteraction.events.InteractionEvent;

/**
 * Event that is thrown if the state of a state changeable component has changed.
 * 
 * @author evalosch
 *
 * @param <T>
 *            The type of the state that has changed.
 * 
 * @param <U>
 *            The type of the component that has changed.
 */
public class StateChangedEvent<T extends State, U extends StateChangeableComponent<T>>
		extends InteractionEvent {

	/**
	 * The component of which the state has changed.
	 */
	private U component;

	/**
	 * The state before the change.
	 */
	private T oldState;

	/**
	 * The state after the change.
	 */
	private T newState;

	public StateChangedEvent(U component, T oldState,
			T newState) {
		this.component = component;
		this.oldState = oldState;
		this.newState = newState;
	}

	/**
	 * @return the component
	 */
	public U getComponent() {
		return component;
	}

	/**
	 * @return the oldState
	 */
	public T getOldState() {
		return oldState;
	}

	/**
	 * @return the newState
	 */
	public T getNewState() {
		return newState;
	}

	/**
	 * @param component
	 *            the component to set
	 */
	public void setComponent(U component) {
		this.component = component;
	}

	/**
	 * @param oldState
	 *            the oldState to set
	 */
	public void setOldState(T oldState) {
		this.oldState = oldState;
	}

	/**
	 * @param newState
	 *            the newState to set
	 */
	public void setNewState(T newState) {
		this.newState = newState;
	}

}
