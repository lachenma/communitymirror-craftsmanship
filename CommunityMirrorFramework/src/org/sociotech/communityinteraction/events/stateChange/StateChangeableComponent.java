package org.sociotech.communityinteraction.events.stateChange;

/**
 * Abstract super calls of all components with changable state.
 * 
 * 
 * @param <T> the type of the changeable state of this component
 * 
 * @author evalosch
 *
 */
public abstract class StateChangeableComponent<T extends State> {
	
	private T state;

	public StateChangeableComponent(T state) {
		this.state = state;
	}
	
	public T getState(){
		return this.state;
	}
	
	public void setState(T state){
		this.state = state;
	}

}
