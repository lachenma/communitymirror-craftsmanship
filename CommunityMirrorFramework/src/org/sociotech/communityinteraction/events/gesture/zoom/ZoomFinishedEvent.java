package org.sociotech.communityinteraction.events.gesture.zoom;

/**
 * Representation of a finished zoom.
 * 
 * @author Peter Lachenmaier
 */
public class ZoomFinishedEvent extends ZoomEvent {

	/**
	 * Constructs a zoom finished event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public ZoomFinishedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
