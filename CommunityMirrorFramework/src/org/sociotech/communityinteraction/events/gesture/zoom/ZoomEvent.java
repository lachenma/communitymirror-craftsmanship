package org.sociotech.communityinteraction.events.gesture.zoom;

import org.sociotech.communityinteraction.events.gesture.GestureEvent;

/**
 * Abstract super class of all zoom events.
 * 
 * @author Peter Lachenmaier
 */
public abstract class ZoomEvent extends GestureEvent {

	/**
	 * The current factor of a zoom. 
	 */
	private double zoomFactor;

	/**
	 * Constructs a zoom event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public ZoomEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

	/**
	 * Returns the zoom factor.
	 * 
	 * @return The zoom factor.
	 */
	public double getZoomFactor() {
		return zoomFactor;
	}

	/**
	 * Sets the zoom factor.
	 * 
	 * @param factor Zoom factor.
	 */
	public void setZoomFactor(double factor) {
		this.zoomFactor = factor;
	}
}
