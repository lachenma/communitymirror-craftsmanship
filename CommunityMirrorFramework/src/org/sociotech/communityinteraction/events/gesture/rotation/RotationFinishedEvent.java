package org.sociotech.communityinteraction.events.gesture.rotation;

/**
 * Representation of a finished rotation.
 * 
 * @author Peter Lachenmaier
 */
public class RotationFinishedEvent extends RotationEvent {

	/**
	 * Constructs a rotate finished event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public RotationFinishedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
