package org.sociotech.communityinteraction.events.gesture.scroll;

/**
 * Representation of a starting scroll.
 * 
 * @author Peter Lachenmaier
 */
public class ScrollStartedEvent extends ScrollEvent {

	/**
	 * Constructs a scroll started event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public ScrollStartedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
