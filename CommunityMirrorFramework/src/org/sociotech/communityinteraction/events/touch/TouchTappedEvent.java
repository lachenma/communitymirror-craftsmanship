package org.sociotech.communityinteraction.events.touch;

/**
 * Representation of a touch tap.
 * 
 * @author Peter Lachenmaier
 */
public class TouchTappedEvent extends TouchEvent {

	/**
	 * Constructs a touch tapped event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public TouchTappedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
