package org.sociotech.communityinteraction.events.touch;

/**
 * Representation of a touch and hold.
 * 
 * @author Peter Lachenmaier
 */
public class TouchHoldEvent extends TouchEvent {

	/**
	 * Constructs a touch hold event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public TouchHoldEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
