package org.sociotech.communityinteraction.events;

/**
 * An event that occurs while communicating (e.g. with other cmf-applications).
 * 
 * @author Eva Lösch
 *
 */
public abstract class CommunicationEvent extends CIEvent {

}
