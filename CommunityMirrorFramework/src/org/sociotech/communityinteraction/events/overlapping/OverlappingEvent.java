package org.sociotech.communityinteraction.events.overlapping;

import java.util.LinkedList;
import java.util.List;

import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

public abstract class OverlappingEvent extends IntersectionEvent {

	public OverlappingEvent(Intersection intersection) {
		super(intersection);
		// set the first and second component of this event as target consumers of this overlapping event
		List<CIEventConsumer> consumers = new LinkedList<CIEventConsumer>();
		consumers.add(intersection.getFirstComponent());
		consumers.add(intersection.getSecondComponent());
		this.setTargetConsumers(consumers);
	}
}
