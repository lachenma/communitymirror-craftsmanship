package org.sociotech.communityinteraction.events.overlapping;

import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

public class MagneticRepulsionFinishedEvent extends MagneticRepulsionEvent {

	public MagneticRepulsionFinishedEvent(Intersection intersection,  CIEventConsumer targetConsumer) {
		super(intersection, targetConsumer);
	}

}
