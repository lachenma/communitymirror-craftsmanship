package org.sociotech.communityinteraction.events.overlapping;

import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

public abstract class MagneticAttractionEvent extends MagneticEffectEvent {

	public MagneticAttractionEvent(Intersection intersection, CIEventConsumer targetConsumer) {
		super(intersection, targetConsumer);
	}

}
