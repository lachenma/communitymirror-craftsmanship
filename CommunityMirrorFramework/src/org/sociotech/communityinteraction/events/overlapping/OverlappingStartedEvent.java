package org.sociotech.communityinteraction.events.overlapping;


public class OverlappingStartedEvent extends OverlappingEvent {

	public OverlappingStartedEvent(Intersection overlapping) {
		super(overlapping);
	}

}
