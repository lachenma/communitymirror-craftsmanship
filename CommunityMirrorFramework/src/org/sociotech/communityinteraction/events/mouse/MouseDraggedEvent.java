package org.sociotech.communityinteraction.events.mouse;

/**
 * Representation of a mouse drag.
 * 
 * @author Peter Lachenmaier
 */
public class MouseDraggedEvent extends MouseEvent {

	/**
	 * Constructs a mouse drag event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public MouseDraggedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
