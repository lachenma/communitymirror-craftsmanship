package org.sociotech.communityinteraction.events.mouse;

import org.sociotech.communityinteraction.events.PositionEvent;

/**
 * Abstract super class of all mouse events.
 * 
 * @author Peter Lachenmaier
 */
public abstract class MouseEvent extends PositionEvent {

	/**
	 * The primary button 
	 */
	public static int PRIMARY_BUTTON = 0;
	
	/**
	 * The primary button 
	 */
	public static int SECONDARY_BUTTON = 1;
	
	/**
	 * The primary button 
	 */
	public static int MIDDLE_BUTTON = 2;
	
	/**
	 * Indicates no button usage.
	 */
	public static int NONE_BUTTON = 3;
	
	/**
	 * The used button
	 */
	public int button = NONE_BUTTON;
	
	/**
	 * Whether the primary button is pressed or not.
	 */
	private boolean   primaryButtonDown = false;
	
	/**
	 * Whether the secondary button is pressed or not.
	 */
	private boolean secondaryButtonDown = false;
	
	/**
	 * Whether the middle button is pressed or not.
	 */
	private boolean    middleButtonDown = false;
	
	/**
	 * Constructs a mouse event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public MouseEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

	/**
	 * Returns whether the primary button is pressed or not.
	 * @return Whether the secondary button is pressed or not.
	 */
	public boolean isPrimaryButtonDown() {
		return primaryButtonDown;
	}
	
	/**
	 * Sets the down value for the primary button.
	 * 
	 * @param down True if button is down.
	 */
	public void setPrimaryButtonDown(boolean down) {
		this.primaryButtonDown = down;
	}

	/**
	 * Returns whether the secondary button is pressed or not.
	 * @return Whether the secondary button is pressed or not.
	 */
	public boolean isSecondaryButtonDown() {
		return secondaryButtonDown;
	}
	
	/**
	 * Sets the down value for the secondary button.
	 * 
	 * @param down True if button is down.
	 */
	public void setSecondaryButtonDown(boolean down) {
		this.secondaryButtonDown = down;
	}

	/**
	 * Returns whether the middle button is pressed or not.
	 * @return Whether the middle button is pressed or not.
	 */
	public boolean isMiddleButtonDown() {
		return middleButtonDown;
	}
	
	/**
	 * Sets the down value for the middle button.
	 * 
	 * @param down True if button is down.
	 */
	public void setMiddleButtonDown(boolean down) {
		this.middleButtonDown = down;
	}

	/**
	 * Sets the button used for this mouse event.
	 * 
	 * @param button Mouse Button
	 */
	public void setButton(int button) {
		this.button = button;
		if(button < PRIMARY_BUTTON || button > NONE_BUTTON) {
			this.button = NONE_BUTTON;
		}
	}
	
	/**
	 * Return the used button.
	 * 
	 * @return The used button.
	 */
	public int getButton() {
		return button;
	}
}
