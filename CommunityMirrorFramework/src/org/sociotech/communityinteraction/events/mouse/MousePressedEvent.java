package org.sociotech.communityinteraction.events.mouse;

/**
 * Representation of a mouse button pressed event.
 * 
 * @author Peter Lachenmaier
 */
public class MousePressedEvent extends MouseEvent {

	/**
	 * Constructs a mouse pressed event at the given position.
	 * 
	 * @param sceneX The scene x
	 * @param sceneY The scene y
	 */
	public MousePressedEvent(double sceneX, double sceneY) {
		super(sceneX, sceneY);
	}

}
