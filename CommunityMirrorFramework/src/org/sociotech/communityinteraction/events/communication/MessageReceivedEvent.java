package org.sociotech.communityinteraction.events.communication;

import org.sociotech.communityinteraction.events.CommunicationEvent;

/**
 * @author Eva Lösch
 *
 */
public class MessageReceivedEvent extends CommunicationEvent{
	
	private String message;

	public MessageReceivedEvent(String message){
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
