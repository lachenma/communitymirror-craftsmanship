package org.sociotech.communityinteraction.visualizer.visualizations;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import org.sociotech.communityinteraction.events.touch.TouchEvent;

/**
 * Visualization of a touch point
 * 
 * @author Peter Lachenmaier
 */
public class TouchVisualization extends CIEventVisualization {
	
	private Circle innerCircle;
	private Circle borderCircle;
	
	private static final double innerSizeRadius = 10;
	private static final double outerSizeRadius = 18;
	private static final Color color = Color.DARKBLUE;
	private static final double opacity = 0.8;
	
	/**
	 * Initializes the visualization for the given touch event.
	 *  
	 * @param touchEvent Touch event to visualize.
	 */
	public TouchVisualization(TouchEvent touchEvent) {
		// create circles
		innerCircle = new Circle();
		borderCircle = new Circle();
		
		// set sizes
		innerCircle.setRadius(innerSizeRadius);
		borderCircle.setRadius(outerSizeRadius);
		
		// set fill
		innerCircle.setFill(color);
		innerCircle.setOpacity(opacity);
		
		borderCircle.setStroke(color);
		borderCircle.setFill(Color.TRANSPARENT);
		
		// set position
		this.setPositionX(touchEvent.getSceneX());
		this.setPositionY(touchEvent.getSceneY());
		
		// add it to this group
		this.getChildren().add(innerCircle);
		this.getChildren().add(borderCircle);
	}
	
	/**
	 * Sets the x position of this touch visualization.
	 * 
	 * @param x The x position.
	 */
	public void setPositionX(double x) {
		this.setLayoutX(x);
	}
	
	/**
	 * Sets the y position of this touch visualization.
	 * 
	 * @param y The y position.
	 */
	public void setPositionY(double y) {
		this.setLayoutY(y);
	}
}
