package org.sociotech.communityinteraction.visualizer.visualizations;

import javafx.scene.Group;

/**
 * Abstract super class of all visualizations of CommunityInteraction Events
 *  
 * @author Peter Lachenmaier
 *
 */
public abstract class CIEventVisualization extends Group {

}
