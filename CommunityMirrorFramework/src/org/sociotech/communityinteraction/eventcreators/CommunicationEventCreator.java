package org.sociotech.communityinteraction.eventcreators;

import org.sociotech.communityinteraction.events.communication.MessageReceivedEvent;

/**
 * Event creator that creates communication events.
 * 
 * @author Eva Lösch
 *
 */
public class CommunicationEventCreator extends CIEventCreator {

	/**
	 * Creates a MessageReceivedEvent and publishes it.
	 * 
	 * @param message
	 *              the message that has been received
	 * 
	 */
	public void createAndPublishMessageReceivedEvent(String message) {
		// create the event
		MessageReceivedEvent event = new MessageReceivedEvent(message);
		// publish the event
		this.publish(event);
	}
}
