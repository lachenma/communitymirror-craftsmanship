package org.sociotech.communityinteraction.eventcreators;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communityinteraction.events.kinect.ColorFrameEvent;
import org.sociotech.communityinteraction.events.kinect.DepthFrameEvent;
import org.sociotech.communityinteraction.events.kinect.SkeletonFrameEvent;

import edu.ufl.digitalworlds.gui.DWApp;
import edu.ufl.digitalworlds.j4k.Skeleton;


/**
 * Event creator that creates kinect frame events.
 * 
 * @author evalosch
 *
 */
public class KinectFrameEventCreator extends CIEventCreator {
	
	/**
	 * The kinect from which this kinect frame event creator receives kinect frames.
	 */
	private Kinect kinect;
	
	private final Logger logger = 
			LogManager.getLogger(KinectFrameEventCreator.class.getName());

	public KinectFrameEventCreator() {
		kinect = this.initializeAndStartKinect();
		this.setKinect(kinect);
	}
	
	private Kinect initializeAndStartKinect(){
		Kinect kinect = new Kinect(this);
		boolean kinectStarted = kinect.start(Kinect.DEPTH| Kinect.COLOR |Kinect.SKELETON |Kinect.XYZ|Kinect.PLAYER_INDEX);
		//boolean kinectStarted = kinect.start(J4KSDK.SKELETON);
		if(!kinectStarted)
		{
			DWApp.showErrorDialog("ERROR", "<html><center><br>ERROR: The Kinect device could not be initialized.<br><br>1. Check if the Microsoft's Kinect SDK was succesfully installed on this computer.<br> 2. Check if the Kinect is plugged into a power outlet.<br>3. Check if the Kinect is connected to a USB port of this computer.</center>");
			logger.warn("ERROR: kinect could not be started!");
		}
		else{
			logger.info("kinect started!");
		}
		return kinect;
	}
	
	/**
	 * Creates a new skeleton frame event and publishes it.
	 *
	 * @param skeleton_tracked
	 * @param joint_positions
	 * @param joint_orientations
	 * @param joint_state
	 */
	public void createAndPublishSkeletonFrameEvent(List<Skeleton> skeletons){
		// create new skeleton frame event containing the list of skeletons
		SkeletonFrameEvent event = new SkeletonFrameEvent(skeletons);
		// publish the new skeleton frame event
		this.publish(event);
		
		logger.debug("published skeleton frame event: " + event);
	}
	
	/**
	 * Creates a new color frame event and publishes it.
	 *
	 * @param color_data The color data of the received kienct frame.
	 */
	public void createAndPublishColorFrameEvent(byte[] color_data){
		ColorFrameEvent event = new ColorFrameEvent(color_data);
		this.publish(event);
		
		logger.debug("published color frame event: " + event);
	}
	
	/**
	 * Creates a new depth frame event and publishes it.
	 * @param depth_frame
	 * @param player_index
	 * @param xyz
	 * @param uv
	 */
	public void createAndPublishDepthFrameEvent(short[] depth_frame, byte[] player_index, 
			float[] xyz, float[] uv){
		DepthFrameEvent event = new DepthFrameEvent(depth_frame, player_index, uv, uv);
		this.publish(event);
	}

	/**
	 * @return the kinect
	 */
	public Kinect getKinect() {
		return kinect;
	}

	/**
	 * @param kinect the kinect to set
	 */
	public void setKinect(Kinect kinect) {
		this.kinect = kinect;
	}
}
