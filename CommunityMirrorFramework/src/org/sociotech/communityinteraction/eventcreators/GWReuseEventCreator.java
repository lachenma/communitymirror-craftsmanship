package org.sociotech.communityinteraction.eventcreators;

import java.util.LinkedList;
import java.util.Queue;

import javafx.scene.Scene;

import org.sociotech.communityinteraction.interfaces.PositionedEventConsumer;

/**
 * Event creator that produces CommunityInteraction events from GestureWorks events. Special implementation
 * that reuses ids to less often call register and deregister on GestureWorks.
 * 
 * @author Peter Lachenmaier
 */
public class GWReuseEventCreator extends GWEventCreator {
	
    /**
     * A stack with ids to reuse
     */
    private Queue<String> reusableIds;
    
	/**
	 * Initializes the GestureWorks events creator.
	 * 
	 * @param scene JavaFX scene to determine window size. 
	 * @param windowName Name used for main window.
	 * @param gmlPath Path to gesture definition.
	 * @param gwDllPath Path to the gestureworks dll.
	 */
	public GWReuseEventCreator(Scene scene, String windowName, String gmlPath, String gwDllPath) {
		super(scene, windowName, gmlPath, gwDllPath);
		reusableIds = new LinkedList<String>();
	}


	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.eventcreators.GWEventCreator#registerConsumerAtGestureworks(org.sociotech.communityinteraction.interfaces.PositionedEventConsumer)
	 */
	@Override
	protected void registerConsumerAtGestureworks(PositionedEventConsumer consumer) {
		
		// try to get an id to reuse
		String reuseID = reusableIds.poll();
		
		if(reuseID != null) {
			// set it at component
			consumer.setID(reuseID);
			// keep mapping from id to consumer
			consumerIDMapping.put(reuseID, consumer);
			// no need to register the id again
			return;
		}
		
		// normal process if no reusable ids are available
		super.registerConsumerAtGestureworks(consumer);
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.eventcreators.GWEventCreator#unregisterEventConsumerAtGestureWorks(org.sociotech.communityinteraction.interfaces.PositionedEventConsumer)
	 */
	@Override
	protected void unregisterEventConsumerAtGestureWorks(PositionedEventConsumer consumer) {
		String unusedID = consumer.getID();
		
		// remove from consumer id mapping
		consumerIDMapping.remove(unusedID);
			
		// keep unused id for later reuse
		reusableIds.add(unusedID);
		
		// and do not really deregister at gestureworks
	}

}
