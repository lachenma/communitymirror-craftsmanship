
package org.sociotech.communityinteraction.eventcreators;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections15.map.HashedMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communityinteraction.events.gesture.drag.DragDraggedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragStartedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationRotatedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationStartedEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollScrolledEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollStartedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomStartedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomZoomedEvent;
import org.sociotech.communityinteraction.events.mouse.MouseClickedEvent;
import org.sociotech.communityinteraction.events.mouse.MouseDraggedEvent;
import org.sociotech.communityinteraction.events.mouse.MousePressedEvent;
import org.sociotech.communityinteraction.events.mouse.MouseReleasedEvent;
import org.sociotech.communityinteraction.events.touch.TouchHoldEvent;
import org.sociotech.communityinteraction.events.touch.TouchMovedEvent;
import org.sociotech.communityinteraction.events.touch.TouchPressedEvent;
import org.sociotech.communityinteraction.events.touch.TouchReleasedEvent;
import org.sociotech.communityinteraction.events.touch.TouchTappedEvent;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;
import org.sociotech.communityinteraction.interfaces.PositionedEventConsumer;

import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.scene.Scene;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.RotateEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.input.TouchPoint;
import javafx.scene.input.ZoomEvent;

/**
 * Event creator that produces CommunityInteraction events from JavaFX and
 * TUIOFX events.
 *
 * @author Peter Lachenmaier, Michael Koch
 */
public class TUIOFXEventCreator extends CIEventCreator {

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(TUIOFXEventCreator.class);

    /**
     * Reference to the used JavaFX scene.
     */
    private Scene scene;

    /**
     * Maximum time between touch press and release to be a tap
     */
    public static long MAX_TIME_TO_TAP_MS = 500;

    /**
     * Maximum time between touch press and release to be a tap
     */
    public static long MIN_TIME_TO_HOLD = 2000;

    /**
     * Keeps the dates for touch presses to create tap events
     */
    private Map<String, Date> touchPressedTimes = new HashedMap<>(10);

    /**
     * Creates a fx creator that maps events from the given scene.
     *
     * @param scene
     *            JavaFX scene.
     */
    public TUIOFXEventCreator(Scene scene) {
        this.scene = scene;

        this.addTouchFilter();
        this.addMouseFilter();
        this.addRotationFilter();
        this.addZoomFilter();
        this.addDragFilter();
        this.addScrollFilter();

        // TODO
        // - Swipe
    }

    /**
     * Adds filters to get all rotation events of the scene and map them.
     */
    private void addRotationFilter() {

        // Rotation started
        this.scene.addEventFilter(RotateEvent.ROTATION_STARTED, new EventHandler<RotateEvent>() {

            @Override
            public void handle(RotateEvent rotateEvent) {
                // create new ci event
                RotationStartedEvent ciEvent = new RotationStartedEvent(rotateEvent.getSceneX(),
                        rotateEvent.getSceneY());

                // process the newly created rotation event
                TUIOFXEventCreator.this.processNewRotationEvent(rotateEvent, ciEvent);
            }
        });

        // Rotation rotated
        this.scene.addEventFilter(RotateEvent.ROTATE, new EventHandler<RotateEvent>() {

            @Override
            public void handle(RotateEvent rotateEvent) {
                // create new ci event
                RotationRotatedEvent ciEvent = new RotationRotatedEvent(rotateEvent.getSceneX(),
                        rotateEvent.getSceneY());

                // process the newly created rotation event
                TUIOFXEventCreator.this.processNewRotationEvent(rotateEvent, ciEvent);
            }
        });

        // Rotation finished
        this.scene.addEventFilter(RotateEvent.ROTATION_FINISHED, new EventHandler<RotateEvent>() {

            @Override
            public void handle(RotateEvent rotateEvent) {
                // create new ci event
                RotationFinishedEvent ciEvent = new RotationFinishedEvent(rotateEvent.getSceneX(),
                        rotateEvent.getSceneY());

                // process the newly created rotation event
                TUIOFXEventCreator.this.processNewRotationEvent(rotateEvent, ciEvent);
            }
        });
    }

    /**
     * Adds filters to get all zoom events of the scene and map them.
     */
    private void addZoomFilter() {

        // Zoom started
        this.scene.addEventFilter(ZoomEvent.ZOOM_STARTED, new EventHandler<ZoomEvent>() {

            @Override
            public void handle(ZoomEvent zoomEvent) {
                // create new ci event
                ZoomStartedEvent ciEvent = new ZoomStartedEvent(zoomEvent.getSceneX(), zoomEvent.getSceneY());

                // process the newly created zoom event
                TUIOFXEventCreator.this.processNewZoomEvent(zoomEvent, ciEvent);
            }
        });

        // Zoom finished
        this.scene.addEventFilter(ZoomEvent.ZOOM_FINISHED, new EventHandler<ZoomEvent>() {

            @Override
            public void handle(ZoomEvent zoomEvent) {
                // create new ci event
                ZoomFinishedEvent ciEvent = new ZoomFinishedEvent(zoomEvent.getSceneX(), zoomEvent.getSceneY());

                // process the newly created zoom event
                TUIOFXEventCreator.this.processNewZoomEvent(zoomEvent, ciEvent);
            }
        });

        // Zoom zoomed
        this.scene.addEventFilter(ZoomEvent.ZOOM, new EventHandler<ZoomEvent>() {

            @Override
            public void handle(ZoomEvent zoomEvent) {
                // create new ci event
                ZoomZoomedEvent ciEvent = new ZoomZoomedEvent(zoomEvent.getSceneX(), zoomEvent.getSceneY());

                // process the newly created zoom event
                TUIOFXEventCreator.this.processNewZoomEvent(zoomEvent, ciEvent);
            }
        });
    }

    /**
     * Adds filters to get all scroll events of the scene and map them.
     */
    private void addScrollFilter() {

        // Scroll started
        this.scene.addEventFilter(ScrollEvent.SCROLL_STARTED, new EventHandler<ScrollEvent>() {

            @Override
            public void handle(ScrollEvent scrollEvent) {
                // create new ci event
                ScrollStartedEvent ciEvent = new ScrollStartedEvent(scrollEvent.getSceneX(), scrollEvent.getSceneY());

                // process the newly created scroll event
                TUIOFXEventCreator.this.processNewScrollEvent(scrollEvent, ciEvent);
            }

        });

        // Scroll finished
        this.scene.addEventFilter(ScrollEvent.SCROLL_FINISHED, new EventHandler<ScrollEvent>() {

            @Override
            public void handle(ScrollEvent scrollEvent) {
                // create new ci event
                ScrollFinishedEvent ciEvent = new ScrollFinishedEvent(scrollEvent.getSceneX(), scrollEvent.getSceneY());

                // process the newly created scroll event
                TUIOFXEventCreator.this.processNewScrollEvent(scrollEvent, ciEvent);
            }

        });

        // Scroll scrolled
        this.scene.addEventFilter(ScrollEvent.SCROLL, new EventHandler<ScrollEvent>() {

            @Override
            public void handle(ScrollEvent scrollEvent) {
                // create new ci event
                ScrollScrolledEvent ciEvent = new ScrollScrolledEvent(scrollEvent.getSceneX(), scrollEvent.getSceneY());

                // process the newly created scroll event
                TUIOFXEventCreator.this.processNewScrollEvent(scrollEvent, ciEvent);
            }

        });

    }

    /**
     * Adds filters to get all scroll events of the scene and maps them to drag
     * events.
     */
    private void addDragFilter() {

        // FX Scrolls are the same as drags

        // Drag started
        this.scene.addEventFilter(ScrollEvent.SCROLL_STARTED, new EventHandler<ScrollEvent>() {

            @Override
            public void handle(ScrollEvent scrollEvent) {
                // create new ci event
                DragStartedEvent ciEvent = new DragStartedEvent(scrollEvent.getSceneX(), scrollEvent.getSceneY());

                // process the newly created drag event
                TUIOFXEventCreator.this.processNewDragEvent(scrollEvent, ciEvent);
            }
        });

        // Drag draged
        this.scene.addEventFilter(ScrollEvent.SCROLL, new EventHandler<ScrollEvent>() {

            @Override
            public void handle(ScrollEvent scrollEvent) {
                // create new ci event
                DragDraggedEvent ciEvent = new DragDraggedEvent(scrollEvent.getSceneX(), scrollEvent.getSceneY());

                // process the newly created drag event
                TUIOFXEventCreator.this.processNewDragEvent(scrollEvent, ciEvent);
            }
        });

        // Drag finished
        this.scene.addEventFilter(ScrollEvent.SCROLL_FINISHED, new EventHandler<ScrollEvent>() {

            @Override
            public void handle(ScrollEvent scrollEvent) {
                // create new ci event
                DragFinishedEvent ciEvent = new DragFinishedEvent(scrollEvent.getSceneX(), scrollEvent.getSceneY());

                // process the newly created drag event
                TUIOFXEventCreator.this.processNewDragEvent(scrollEvent, ciEvent);
            }
        });
    }

    /**
     * Adds filters to get all touch events of the scene and map them.
     */
    private void addTouchFilter() {

        // filter touch presses
        this.scene.addEventFilter(TouchEvent.TOUCH_PRESSED, new EventHandler<TouchEvent>() {

            @Override
            public void handle(TouchEvent event) {
                // create new ci event
                TouchPressedEvent touchEvent = new TouchPressedEvent(event.getTouchPoint().getSceneX(),
                        event.getTouchPoint().getSceneY());

                TUIOFXEventCreator.this.setPressTime(event.getTouchPoint());

                // process the newly created touch event
                TUIOFXEventCreator.this.processNewTouchEvent(event, touchEvent);
            }

        });

        // filter touch releases
        this.scene.addEventFilter(TouchEvent.TOUCH_RELEASED, new EventHandler<TouchEvent>() {

            @Override
            public void handle(TouchEvent event) {
                if (TUIOFXEventCreator.this.isTap(event.getTouchPoint())) {
                    // create a tap event first
                    TouchTappedEvent touchEvent = new TouchTappedEvent(event.getTouchPoint().getSceneX(),
                            event.getTouchPoint().getSceneY());

                    // process the newly created touch event
                    TUIOFXEventCreator.this.processNewTouchEvent(event, touchEvent);
                }

                // create new ci event
                TouchReleasedEvent touchEvent = new TouchReleasedEvent(event.getTouchPoint().getSceneX(),
                        event.getTouchPoint().getSceneY());

                // process the newly created touch event
                TUIOFXEventCreator.this.processNewTouchEvent(event, touchEvent);
            }
        });

        // filter touch moves
        this.scene.addEventFilter(TouchEvent.TOUCH_MOVED, new EventHandler<TouchEvent>() {

            @Override
            public void handle(TouchEvent event) {
                // create new ci event
                TouchMovedEvent touchEvent = new TouchMovedEvent(event.getTouchPoint().getSceneX(),
                        event.getTouchPoint().getSceneY());

                // process the newly created touch event
                TUIOFXEventCreator.this.processNewTouchEvent(event, touchEvent);
            }
        });

        // filter touch moves
        this.scene.addEventFilter(TouchEvent.TOUCH_STATIONARY, new EventHandler<TouchEvent>() {

            @Override
            public void handle(TouchEvent event) {
                // check time
                if (TUIOFXEventCreator.this.isHold(event.getTouchPoint())) {

                    // create new ci event
                    TouchHoldEvent touchEvent = new TouchHoldEvent(event.getTouchPoint().getSceneX(),
                            event.getTouchPoint().getSceneY());

                    // process the newly created touch event
                    TUIOFXEventCreator.this.processNewTouchEvent(event, touchEvent);

                    TUIOFXEventCreator.this.removePressTime(event.getTouchPoint());
                }
            }
        });

    }

    /**
     * Keeps the time for the given touch point.
     *
     * @param touchPoint
     *            Touch point to keep time for.
     */
    private void setPressTime(TouchPoint touchPoint) {
        if (touchPoint == null) {
            return;
        }

        this.touchPressedTimes.put(touchPoint.getId() + "", new Date());
    }

    /**
     * Removes a touch point from the time array
     *
     * @param touchPoint
     *            Touch point to remove.
     */
    private void removePressTime(TouchPoint touchPoint) {
        if (touchPoint == null) {
            return;
        }
        this.touchPressedTimes.remove(touchPoint.getId() + "");
    }

    /**
     * Checks it time between press and now is short enough to be a tap.
     *
     * @param touchPoint
     *            Point for release
     * @return True if it is a tap
     */
    private boolean isTap(TouchPoint touchPoint) {
        if (touchPoint != null && this.touchPressedTimes.containsKey(touchPoint.getId() + "")
                && (new Date().getTime() - this.touchPressedTimes.get(touchPoint.getId() + "")
                        .getTime()) < TUIOFXEventCreator.MAX_TIME_TO_TAP_MS) {
            return true;
        }

        return false;
    }

    /**
     * Checks it time between press and now is long enough to be a hold.
     *
     * @param touchPoint
     *            Point for release
     * @return True if it is a hold
     */
    private boolean isHold(TouchPoint touchPoint) {
        if (touchPoint != null && this.touchPressedTimes.containsKey(touchPoint.getId() + "")
                && (new Date().getTime() - this.touchPressedTimes.get(touchPoint.getId() + "")
                        .getTime()) > TUIOFXEventCreator.MIN_TIME_TO_HOLD) {
            return true;
        }

        return false;
    }

    /**
     * Adds filters to get all mouse events of the scene and map them.
     */
    private void addMouseFilter() {

        // filter mouse clicks
        this.scene.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                // create new ci event
                MouseClickedEvent ciEvent = new MouseClickedEvent(event.getSceneX(), event.getSceneY());

                // processes the newly created mouse event
                TUIOFXEventCreator.this.processNewMouseEvent(event, ciEvent);
            }
        });

        // filter mouse button press
        this.scene.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                // create new ci event
                MousePressedEvent ciEvent = new MousePressedEvent(event.getSceneX(), event.getSceneY());

                // processes the newly created mouse event
                TUIOFXEventCreator.this.processNewMouseEvent(event, ciEvent);
            }
        });

        // filter mouse button releases
        this.scene.addEventFilter(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                // create new ci event
                MouseReleasedEvent ciEvent = new MouseReleasedEvent(event.getSceneX(), event.getSceneY());

                // processes the newly created mouse event
                TUIOFXEventCreator.this.processNewMouseEvent(event, ciEvent);
            }
        });

        // TODO: currently deactivated mouse moved events for performance
        // reasons
        // // filter mouse move events
        // scene.addEventFilter(MouseEvent.MOUSE_MOVED, new
        // EventHandler<MouseEvent>() {
        //
        // @Override
        // public void handle(MouseEvent event) {
        // // create new ci event
        // MouseMovedEvent ciEvent = new MouseMovedEvent(event.getSceneX(),
        // event.getSceneY());
        //
        // // processes the newly created mouse event
        // processNewMouseEvent(event, ciEvent);
        // }
        // });

        // filter mouse drag events
        this.scene.addEventFilter(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                // create new ci event
                MouseDraggedEvent ciEvent = new MouseDraggedEvent(event.getSceneX(), event.getSceneY());

                // processes the newly created mouse event
                TUIOFXEventCreator.this.processNewMouseEvent(event, ciEvent);
            }
        });

        // TODO
        // open mouse
        // -entered
        // -exited
        // -drag detected
    }

    /**
     * Processes the given ci event that must be created from the given fx mouse
     * event. This method
     * maps base attribute values, consumes the fx event and publishes the new
     * ci event.
     *
     * @param fxMouseEvent
     *            The fx mouse event with the base information
     * @param ciMouseEvent
     *            Ci event matching the fx event
     */
    private void processNewMouseEvent(MouseEvent fxMouseEvent,
            org.sociotech.communityinteraction.events.mouse.MouseEvent ciMouseEvent) {
        // map attributes
        if (fxMouseEvent.isPrimaryButtonDown()) {
            ciMouseEvent.setPrimaryButtonDown(true);
        }
        if (fxMouseEvent.isSecondaryButtonDown()) {
            ciMouseEvent.setSecondaryButtonDown(true);
        }
        if (fxMouseEvent.isMiddleButtonDown()) {
            ciMouseEvent.setMiddleButtonDown(true);
        }

        if (fxMouseEvent.getButton() == MouseButton.PRIMARY) {
            ciMouseEvent.setButton(org.sociotech.communityinteraction.events.mouse.MouseEvent.PRIMARY_BUTTON);
        } else if (fxMouseEvent.getButton() == MouseButton.SECONDARY) {
            ciMouseEvent.setButton(org.sociotech.communityinteraction.events.mouse.MouseEvent.SECONDARY_BUTTON);
        } else if (fxMouseEvent.getButton() == MouseButton.MIDDLE) {
            ciMouseEvent.setButton(org.sociotech.communityinteraction.events.mouse.MouseEvent.MIDDLE_BUTTON);
        } else if (fxMouseEvent.getButton() == MouseButton.NONE) {
            ciMouseEvent.setButton(org.sociotech.communityinteraction.events.mouse.MouseEvent.NONE_BUTTON);
        }

        // find target if possible
        ciMouseEvent.setTargetConsumers(this.findTargetConsumer(fxMouseEvent.getTarget()));

        // set parallel keyboard press
        ciMouseEvent.setAltDown(fxMouseEvent.isAltDown());
        ciMouseEvent.setShiftDown(fxMouseEvent.isShiftDown());
        ciMouseEvent.setControlDown(fxMouseEvent.isControlDown());

        // TODO

        // consume the java fx event
        // fxMouseEvent.consume();
        // do not consume - might be needed in FX components like WebView

        // and publish the new one
        this.publish(ciMouseEvent);
    }

    /**
     * Looks if there is an direct registered target (in positioned consumers)
     * that matches the given target.
     *
     * @param target
     *            FX Target to find matching event consumer for.
     * @return The matching event consumer or null if not found.
     */
    private CIEventConsumer findTargetConsumer(EventTarget target) {
        if (target == null) {
            return null;
        }

        // look for positioned consumers as target
        List<PositionedEventConsumer> positionedConsumers = this.getPositionedConsumers();

        for (PositionedEventConsumer consumer : positionedConsumers) {
            if (consumer.isTarget(target)) {
                return consumer;
            }
        }

        // null if no direct target found
        return null;
    }

    /**
     * Processes the given ci event that must be created from the given fx touch
     * event. This method
     * maps base attribute values, consumes the fx event and publishes the new
     * ci event.
     *
     * @param fxTouchEvent
     *            The fx touch event with the base information
     * @param ciTouchEvent
     *            Ci event matching the fx event
     */
    private void processNewTouchEvent(TouchEvent fxTouchEvent,
            org.sociotech.communityinteraction.events.touch.TouchEvent ciTouchEvent) {

        this.logger.debug("touch event: " + fxTouchEvent.toString());

        // map attributes
        // set id
        ciTouchEvent.setTouchID(fxTouchEvent.getTouchPoint().getId());

        // set parallel keyboard press
        ciTouchEvent.setAltDown(fxTouchEvent.isAltDown());
        ciTouchEvent.setShiftDown(fxTouchEvent.isShiftDown());
        ciTouchEvent.setControlDown(fxTouchEvent.isControlDown());

        // TODO
        // map other touch points

        // find target if possible
        ciTouchEvent.setTargetConsumers(this.findTargetConsumer(fxTouchEvent.getTarget()));

        // consume the java fx event
        // fxTouchEvent.consume();
        // do not consume - might be needed in FX components like WebView

        // and publish the new one
        this.publish(ciTouchEvent);
    }

    /**
     * Processes the given ci event that must be created from the given fx touch
     * event. This method
     * maps base attribute values, consumes the fx event and publishes the new
     * ci event.
     *
     * @param fxTouchEvent
     *            The fx rotate event with the base information
     * @param ciTouchEvent
     *            Ci event matching the fx event
     */
    private void processNewRotationEvent(RotateEvent fxRotateEvent, RotationEvent ciRotationEvent) {

        this.logger.debug("rotation event: " + fxRotateEvent.toString());

        // set the angle
        ciRotationEvent.setAngle(fxRotateEvent.getAngle());

        // set parallel keyboard press
        ciRotationEvent.setAltDown(fxRotateEvent.isAltDown());
        ciRotationEvent.setShiftDown(fxRotateEvent.isShiftDown());
        ciRotationEvent.setControlDown(fxRotateEvent.isControlDown());

        // TODO
        // map other attributes

        // map inertia
        ciRotationEvent.setInertia(fxRotateEvent.isInertia());

        // find target if possible
        ciRotationEvent.setTargetConsumers(this.findTargetConsumer(fxRotateEvent.getTarget()));

        // consume the java fx event
        fxRotateEvent.consume();

        // and publish the new one
        this.publish(ciRotationEvent);
    }

    /**
     * Processes the given ci event that must be created from the given fx touch
     * event. This method
     * maps base attribute values, consumes the fx event and publishes the new
     * ci event.
     *
     * @param fxZoomEvent
     *            The fx zoom event with the base information
     * @param ciZoomEvent
     *            Ci event matching the fx event
     */
    private void processNewZoomEvent(ZoomEvent fxZoomEvent,
            org.sociotech.communityinteraction.events.gesture.zoom.ZoomEvent ciZoomEvent) {

        this.logger.debug("zoom event: " + fxZoomEvent.toString());

        // set the zoom factor
        ciZoomEvent.setZoomFactor(fxZoomEvent.getZoomFactor());

        // set parallel keyboard press
        ciZoomEvent.setAltDown(fxZoomEvent.isAltDown());
        ciZoomEvent.setShiftDown(fxZoomEvent.isShiftDown());
        ciZoomEvent.setControlDown(fxZoomEvent.isControlDown());

        // TODO
        // map other attributes

        // map inertia
        ciZoomEvent.setInertia(fxZoomEvent.isInertia());

        // find target if possible
        ciZoomEvent.setTargetConsumers(this.findTargetConsumer(fxZoomEvent.getTarget()));

        // consume the java fx event
        fxZoomEvent.consume();

        // and publish the new one
        this.publish(ciZoomEvent);
    }

    /**
     * Processes the given ci event that must be created from the given fx touch
     * event. This method
     * maps base attribute values, consumes the fx event and publishes the new
     * ci event.
     *
     * @param fxScrollEvent
     *            The fx scroll event with the base information
     * @param ciDragEvent
     *            Ci event matching the fx event
     */
    private void processNewDragEvent(ScrollEvent fxScrollEvent, DragEvent ciDragEvent) {

        this.logger.debug("drag event: " + fxScrollEvent.toString());

        // set the delta
        ciDragEvent.setDeltaX(fxScrollEvent.getDeltaX());
        ciDragEvent.setDeltaY(fxScrollEvent.getDeltaY());

        // set parallel keyboard press
        ciDragEvent.setAltDown(fxScrollEvent.isAltDown());
        ciDragEvent.setShiftDown(fxScrollEvent.isShiftDown());
        ciDragEvent.setControlDown(fxScrollEvent.isControlDown());

        // TODO
        // map other attributes

        // map inertia
        ciDragEvent.setInertia(fxScrollEvent.isInertia());

        // find target if possible
        ciDragEvent.setTargetConsumers(this.findTargetConsumer(fxScrollEvent.getTarget()));

        // do not consume the java fx event cause this one will also be mapped
        // to a scroll event
        // fxScrollEvent.consume();

        // and publish the new one
        this.publish(ciDragEvent);
    }

    /**
     * Processes the given ci event that must be created from the given fx touch
     * event. This method
     * maps base attribute values, consumes the fx event and publishes the new
     * ci event.
     *
     * @param fxScrollEvent
     *            The fx scroll event with the base information
     * @param ciScrollEvent
     *            Ci event matching the fx event
     */
    private void processNewScrollEvent(ScrollEvent fxScrollEvent,
            org.sociotech.communityinteraction.events.gesture.scroll.ScrollEvent ciScrollEvent) {

        this.logger.debug("scroll event: " + fxScrollEvent.toString());

        // set the delta
        ciScrollEvent.setDeltaX(fxScrollEvent.getDeltaX());
        ciScrollEvent.setDeltaY(fxScrollEvent.getDeltaY());

        // set parallel keyboard press
        ciScrollEvent.setAltDown(fxScrollEvent.isAltDown());
        ciScrollEvent.setShiftDown(fxScrollEvent.isShiftDown());
        ciScrollEvent.setControlDown(fxScrollEvent.isControlDown());

        // TODO
        // map other attributes

        // map inertia
        ciScrollEvent.setInertia(fxScrollEvent.isInertia());

        // find target if possible
        ciScrollEvent.setTargetConsumers(this.findTargetConsumer(fxScrollEvent.getTarget()));

        // consume the java fx event
        // fxScrollEvent.consume();
        // do not consume - might be needed in FX components like WebView

        // and publish the new one
        this.publish(ciScrollEvent);
    }
}
