package org.sociotech.communityinteraction.eventcreators;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javafx.geometry.Bounds;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import org.sociotech.communityinteraction.behaviors.InteractionBehavior;
import org.sociotech.communityinteraction.behaviors.MagneticEffectBehavior;
import org.sociotech.communityinteraction.behaviors.MagneticField;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.overlapping.Intersection;
import org.sociotech.communityinteraction.events.overlapping.MagneticAttractionFinishedEvent;
import org.sociotech.communityinteraction.events.overlapping.MagneticAttractionStartedEvent;
import org.sociotech.communityinteraction.events.overlapping.MagneticRepulsionFinishedEvent;
import org.sociotech.communityinteraction.events.overlapping.MagneticRepulsionStartedEvent;
import org.sociotech.communityinteraction.events.overlapping.OverlappingFinishedEvent;
import org.sociotech.communityinteraction.events.overlapping.OverlappingStartedEvent;
import org.sociotech.communitymirror.view.FXView;
import org.sociotech.communitymirror.visualitems.VisualComponent;

/**
 * Event creator that creates intersection events.
 * @author evalosch
 *
 */
public class IntersectionEventCreator extends CIEventCreator {

	private FXView view;

	private Rectangle rect;

	/**
	 * A HashMap containing a list of intersection objects that belong to
	 * overlapping events as keys and booleans that tell if the intersection is
	 * still valid (true) or already finished (false) as values.
	 */
	private ConcurrentHashMap<Intersection, Boolean> overlappings;

	/**
	 * A HashMap containing a list of intersection objects that belong to
	 * magnetic attraction events as keys and booleans that tell if the
	 * intersection is still valid (true) or already finished (false) as values.
	 */
	private ConcurrentHashMap<Intersection, Boolean> magneticAttractions;

	/**
	 * A HashMap containing a list of intersection objects that belong to
	 * magnetic repulsion events as keys and booleans that tell if the
	 * intersection is still valid (true) or already finished (false) as values.
	 */
	private ConcurrentHashMap<Intersection, Boolean> magneticRepulsions;

	public IntersectionEventCreator(FXView view) {
		this.view = view;
		this.overlappings = new ConcurrentHashMap<Intersection, Boolean>();
		this.magneticAttractions = new ConcurrentHashMap<Intersection, Boolean>();
		this.magneticRepulsions = new ConcurrentHashMap<Intersection, Boolean>();

		rect = new Rectangle(0, 0, 0, 0);
		rect.setStroke(Color.RED);
		rect.setFill(Color.TRANSPARENT);
		this.view.addNode(rect);
	}

	public void createIntersectionEvents(
			List<VisualComponent> firstSetOfComponents,
			List<VisualComponent> secondSetOfComponents) {
		// (first!) create started events
		this.createIntersectionStartedEvents(firstSetOfComponents,
				secondSetOfComponents);
		// (second!) create finished events
		this.createIntersectionFinishedEvents();
	}

	private void createIntersectionFinishedEvents() {
		this.createIntersectionFinishedEvents(this.overlappings,
				"OverlappingFinished");
		this.createIntersectionFinishedEvents(this.magneticAttractions,
				"MagneticAttractionFinished");
		this.createIntersectionFinishedEvents(this.magneticRepulsions,
				"MagneticRepulsionFinished");
	}

	private void createIntersectionFinishedEvents(
			ConcurrentHashMap<Intersection, Boolean> intersections,
			String eventType) {
		// go through the list of intersections
		Iterator<Intersection> iterator = intersections.keySet().iterator();
		while (iterator.hasNext()) {
			Intersection intersection = iterator.next();
			if (!intersections.get(intersection)) {
				// if the intersection is not marked as still valid
				// then create a new intersection finished event
				this.createAndPublishIntersectionFinishedEvent(intersection,
						eventType);
				// and remove intersection from the list of intersections
				intersections.remove(intersection);
			}
		}
	}

	private void createAndPublishIntersectionFinishedEvent(
			Intersection intersection, String eventType) {
		CIEvent event = null;
		VisualComponent firstComponent = intersection.getFirstComponent();
		MagneticField fieldOfFirstComponent = this
				.getMagneticFieldOfVisualComponent(firstComponent);
		VisualComponent secondComponent = intersection.getSecondComponent();
		MagneticField fieldOfSecondComponent = this
				.getMagneticFieldOfVisualComponent(secondComponent);
		VisualComponent magneticTargetComponent = null;
		if (fieldOfFirstComponent != null && fieldOfSecondComponent != null) {
			magneticTargetComponent = (fieldOfFirstComponent.getForce() > fieldOfSecondComponent
					.getForce()) ? secondComponent : firstComponent;
		}

		if (eventType.equals("OverlappingFinished")) {
			event = new OverlappingFinishedEvent(intersection);
		} else if (eventType.equals("MagneticAttractionFinished")) {
			event = new MagneticAttractionFinishedEvent(intersection,
					magneticTargetComponent);
		} else if (eventType.equals("MagneticRepulsionFinished")) {
			event = new MagneticRepulsionFinishedEvent(intersection,
					magneticTargetComponent);
		}

		// publish the event
		if (event != null) {
			this.publish(event);
		}
	}

	private void createIntersectionStartedEvents(
			List<VisualComponent> firstSetOfComponents,
			List<VisualComponent> secondSetOfComponents) {
		// mark all saved intersections as finished
		setIntersectionValuesTo(false);
		// go through the first set of visual components
		for (VisualComponent firstComponent : firstSetOfComponents) {
			// component should not be null
			if(firstComponent == null){
				continue;
			}
			// get magnetic field of first component
			MagneticField fieldOfFirstComponent = this
					.getMagneticFieldOfVisualComponent(firstComponent);
			// go through the second set of visual components
			for (VisualComponent secondComponent : secondSetOfComponents) {
				// component should not be null
				if(secondComponent == null){
					continue;
				}
				// first and second component need to be different
				if ((firstComponent == secondComponent)) {
					continue;
				}
				// check for an overlapping
				checkForOverlapping(firstComponent, secondComponent);
				// get magnetic field of second component
				MagneticField fieldOfSecondComponent = this
						.getMagneticFieldOfVisualComponent(secondComponent);
				// check for an magnetic effect
				checkForMagneticEffect(firstComponent, secondComponent,
						fieldOfFirstComponent, fieldOfSecondComponent);
			}
		}
	}

	private void setIntersectionValuesTo(boolean value) {
		this.overlappings.replaceAll((k, v) -> value);
		this.magneticAttractions.replaceAll((k, v) -> value);
		this.magneticRepulsions.replaceAll((k, v) -> value);
	}

	private MagneticField getMagneticFieldOfVisualComponent(
			VisualComponent component) {

		InteractionBehavior<?> interactionBehavior = component
				.getFirstInteractionBehavior(MagneticEffectBehavior.class);
		if (interactionBehavior != null) {
			MagneticEffectBehavior magneticEffectBehavior = (MagneticEffectBehavior) interactionBehavior;
			if (magneticEffectBehavior != null) {
				return magneticEffectBehavior.getMagneticField();
			}
		}
		return null;
	}

	private void checkForOverlapping(VisualComponent firstComponent,
			VisualComponent secondComponent) {
		// if there is an intersection between components
		if (secondComponent.getBoundsInParent().intersects(
				firstComponent.getBoundsInParent())) {
			// create new intersection
			Intersection intersection = new Intersection(firstComponent,
					secondComponent);
			// add to list of overlappings
			if (this.addIntersection(intersection, this.overlappings)) {
				// if is new overlapping create started event
				OverlappingStartedEvent event = new OverlappingStartedEvent(
						intersection);
				this.publish(event);
			}
		}
	}

	private boolean checkForMagneticEffect(VisualComponent firstComponent,
			VisualComponent secondComponent,
			MagneticField fieldOfFirstComponent,
			MagneticField fieldOfSecondComponent) {
		// both components need to have a magnetic field
		if (fieldOfFirstComponent == null || fieldOfSecondComponent == null) {
			return false;
		}

		// get component with greater and with smaller magnetic force
		boolean firstGreaterThanSecond = (fieldOfFirstComponent.getForce() > fieldOfSecondComponent
				.getForce());
		VisualComponent componentWithGreaterForce = firstGreaterThanSecond ? firstComponent
				: secondComponent;
		VisualComponent componentWithSmallerForce = firstGreaterThanSecond ? secondComponent
				: firstComponent;
		MagneticField fieldWithGreaterForce = firstGreaterThanSecond ? fieldOfFirstComponent
				: fieldOfSecondComponent;

		// calculate bounds of magnetic field
		Bounds boundsOfComponentWithGreaterForce = componentWithGreaterForce
				.getBoundsInParent();
		double range = fieldWithGreaterForce.getRange();
		double minX = boundsOfComponentWithGreaterForce.getMinX() - range;
		double minY = boundsOfComponentWithGreaterForce.getMinY() - range;
		double width = boundsOfComponentWithGreaterForce.getWidth() + 2 * range;
		double height = boundsOfComponentWithGreaterForce.getHeight() + 2
				* range;
		// visualize magnetic field
		this.view.removeNode(rect);
		rect.setX(minX);
		rect.setY(minY);
		rect.setWidth(width);
		rect.setHeight(height);
		this.view.addNode(rect);

		// if there is an intersection with the stronger magnetic field
		if (componentWithSmallerForce.getBoundsInParent().intersects(minX,
				minY, width, height)) {
			// create new intersection
			Intersection intersection = new Intersection(
					componentWithSmallerForce, componentWithGreaterForce);
			// if the two components have the same active poles
			if (fieldOfFirstComponent.getPole() == fieldOfSecondComponent
					.getPole()) {
				// add to list of magnetic repulsions
				if (this.addIntersection(intersection, this.magneticRepulsions)) {
					// if is new intersection create started event
					MagneticRepulsionStartedEvent event = new MagneticRepulsionStartedEvent(
							intersection, componentWithSmallerForce);
					this.publish(event);
				}
			}
			// else if the two components have different active poles
			else {
				// add to list of magnetic attractions
				if (this.addIntersection(intersection, this.magneticAttractions)) {
					// if is new intersection create started event
					MagneticAttractionStartedEvent event = new MagneticAttractionStartedEvent(
							intersection, componentWithSmallerForce);
					this.publish(event);
				}
			}
			return true;
		}

		// else return false
		return false;
	}

	/**
	 * adds a given intersection object to the given hashmap of intersections if
	 * the hashmap does not contains the overlapping already or sets the value
	 * of the key intersection to true
	 * 
	 * @param intersection
	 *            the intersection object to add to the hashmap
	 * @return false if the hashmap already contains the intersection else true
	 */
	private boolean addIntersection(Intersection intersection,
			ConcurrentHashMap<Intersection, Boolean> intersections) {
		Intersection existingIntersection = getIntersectionKey(intersection,
				intersections);
		if (existingIntersection != null) {
			intersections.put(existingIntersection, true);
			return false;
		}
		intersections.put(intersection, true);
		return true;
	}

	/**
	 * helper method that gets a given intersection object that is already saved
	 * within the given hashmap intersections of this intersection event creator
	 * (the default contains-method called on the hashmaps keySet does not use
	 * the overrided equals method of the overlapping class)
	 * 
	 * @param intersection
	 *            the intersection object that has to be looked for in the
	 *            hashmap
	 * @param intersections
	 *            the hashmap of intersections to looked in
	 * @return the intersection object that is saved as key within the hashmap
	 *         or null if it is not in the hashmap
	 */
	private Intersection getIntersectionKey(Intersection intersection,
			ConcurrentHashMap<Intersection, Boolean> intersections) {
		Iterator<Intersection> iterator = intersections.keySet().iterator();
		while (iterator.hasNext()) {
			Intersection key = iterator.next();
			if (key.equals(intersection)) {
				return key;
			}
		}
		return null;
	}
}
