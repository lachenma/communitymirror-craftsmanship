package org.sociotech.communityinteraction.eventcreators;

import java.util.Comparator;

import org.sociotech.communityinteraction.interfaces.PositionedEventConsumer;

public class PositionedEventConsumerComparator implements Comparator<PositionedEventConsumer> {

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(PositionedEventConsumer o1, PositionedEventConsumer o2) {
		return o2.getZIndex() - o1.getZIndex();
	}
}
