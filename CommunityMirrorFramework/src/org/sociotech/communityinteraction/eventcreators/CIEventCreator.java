
package org.sociotech.communityinteraction.eventcreators;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.PositionEvent;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;
import org.sociotech.communityinteraction.interfaces.PositionedEventConsumer;

import javafx.application.Platform;

/**
 * Abstract super class for all creators of CommunityInteraction events.
 *
 * @author Peter Lachenmaier
 */
public abstract class CIEventCreator {

    /**
     * List containing all registered event consumers.
     */
    private List<CIEventConsumer> consumers;

    /**
     * List containing all registered event consumers which are restricted
     * to positions.
     */
    private List<PositionedEventConsumer> positionedConsumers;

    /**
     * Initializes the list of possible event consumers.
     */
    public CIEventCreator() {
        this.consumers = new LinkedList<>();
        this.positionedConsumers = new LinkedList<>();
    }

    /**
     * Returns all registered consumers.
     *
     * @return Returns all registered consumers.
     */
    public List<CIEventConsumer> getConsumers() {
        return this.consumers;
    }

    /**
     * Returns all positioned consumers.
     *
     * @return All positioned consumers.
     */
    public List<PositionedEventConsumer> getPositionedConsumers() {

        // TODO: think of a high-performance way of keeping list sorted
        // sort list of consumers by zIndex
        LinkedList<PositionedEventConsumer> sortedConsumers = new LinkedList<>(this.positionedConsumers);
        Collections.sort(sortedConsumers, new PositionedEventConsumerComparator());

        // return sorted list of consumers
        return sortedConsumers;
    }

    /**
     * Registers an event consumer for events created by this creator.
     *
     * @param consumer
     *            Consumer to register.
     */
    public void registerEventConsumer(CIEventConsumer consumer) {
        if (consumer != null && !this.consumers.contains(consumer)) {
            // add it to the list of registered consumers
            this.consumers.add(consumer);
            if (consumer instanceof PositionedEventConsumer) {
                // add to positioned event consumers
                this.addPositionedEventConsumer((PositionedEventConsumer) consumer);
            }
        }
    }

    /**
     * Unregisters an event consumer from events created by this creator.
     *
     * @param consumer
     *            Consumer to unregister
     */
    public void unregisterEventConsumer(CIEventConsumer consumer) {
        if (consumer != null && this.consumers.contains(consumer)) {
            // remove it from the list of registered consumers
            this.consumers.remove(consumer);
            if (consumer instanceof PositionedEventConsumer) {
                // remove from positioned event consumers
                this.removePositionedEventConsumer((PositionedEventConsumer) consumer);
            }
        }
    }

    /**
     * Adds a positioned event consumer as subscriber for events.
     *
     * @param consumer
     *            Positioned consumer
     */
    protected void addPositionedEventConsumer(PositionedEventConsumer consumer) {
        this.positionedConsumers.add(consumer);
    }

    /**
     * Removes a positioned event consumer from subscribers for events.
     *
     * @param consumer
     *            Positioned consumer
     */
    protected void removePositionedEventConsumer(PositionedEventConsumer consumer) {
        this.positionedConsumers.remove(consumer);
    }

    /**
     * Publishes the given event to the registered consumers until it is
     * filtered
     * or consumed.
     *
     * @param event
     *            Event to publish.
     */
    protected void publish(CIEvent event) {
        if (event == null) {
            return;
        }

        // if(event.getTargetConsumer() != null) {
        // System.out.println(event.getTargetConsumer());
        // } else {
        // System.out.println("no target consumer");
        // }

        List<CIEventConsumer> possibleConsumers = CIListHelper.createConsumerList();
        List<CIEventConsumer> derivedConsumers = null;
        List<CIEventConsumer> targetConsumers = event.getTargetConsumers();

        if (targetConsumers != null && targetConsumers.size() > 0) {
            // add direct targets first to possible consumers
            for (CIEventConsumer targetConsumer : targetConsumers) {
                if (targetConsumer != null) {
                    targetConsumer.getConsumers(event);
                    possibleConsumers.addAll(targetConsumer.getConsumers(event));
                }
            }

        } else if (event instanceof PositionEvent) {
            // find consumers on position
            for (PositionedEventConsumer consumer : this.getPositionedConsumers()) {
                // positioned consumers are ordered by z-Index, so find first on
                // position
                if (consumer.isOnPosition(((PositionEvent) event).getSceneX(), ((PositionEvent) event).getSceneY())) {
                    // add consumers
                    possibleConsumers.addAll(consumer.getConsumers(event));
                    // skip all others
                    break;
                }
            }
        }

        // find possible consumers by asking all registered consumers
        for (CIEventConsumer consumer : this.consumers) {

            if (consumer instanceof PositionedEventConsumer && event instanceof PositionEvent) {
                // skip, handled befor in correct z order
                continue;
            }

            // get possible consumers for the event
            derivedConsumers = consumer.getConsumers(event);
            if (derivedConsumers == null || derivedConsumers.isEmpty()) {
                // no consumers
                continue;
            }

            // add consumers
            possibleConsumers.addAll(derivedConsumers);
        }

        if (possibleConsumers == null) {
            // no possible consumers, so nothing to do
            return;
        }

        // copy and reverse list to let handle in reverse order
        possibleConsumers = new LinkedList<>(possibleConsumers);
        Collections.reverse(possibleConsumers);

        // let consumers handle the event
        for (CIEventConsumer consumer : possibleConsumers) {
            consumer.handle(event);
            if (event.isConsumed()) {
                // stop if event is consumed
                break;
            }
        }

    }

    /**
     * Publishes the given event in the java fx main thread using
     * {@link CIEventCreator#publish(CIEvent)}.
     *
     * @param event
     *            Event to publish
     */
    protected void publishLater(final CIEvent event) {
        Platform.runLater(new Runnable() {

            /*
             * (non-Javadoc)
             *
             * @see java.lang.Runnable#run()
             */
            @Override
            public void run() {
                // publish event
                CIEventCreator.this.publish(event);
            }
        });
    }

}
