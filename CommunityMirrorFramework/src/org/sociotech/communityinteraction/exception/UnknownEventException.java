package org.sociotech.communityinteraction.exception;

import org.sociotech.communityinteraction.events.CIEvent;

/**
 * Exception to state an unknown interaction event.
 * 
 * @author Peter Lachenmaier
 */
public class UnknownEventException extends RuntimeException {

	/**
	 * Generated serial version uid
	 */
	private static final long serialVersionUID = 817406108763374330L;
	
	/**
	 * Then unknown event. 
	 */
	private final CIEvent unknownEvent;

	/**
	 * Constructs the exception for the given unknown event.
	 *  
	 * @param unknownEvent Unknown event.
	 */
	public UnknownEventException(CIEvent unknownEvent) {
		super("Unknown event " + unknownEvent);
		this.unknownEvent = unknownEvent;
	}

	/**
	 * Returns the unknown event.
	 * 
	 * @return The unknown event.
	 */
	public CIEvent getUnknownEvent() {
		return unknownEvent;
	}
}
