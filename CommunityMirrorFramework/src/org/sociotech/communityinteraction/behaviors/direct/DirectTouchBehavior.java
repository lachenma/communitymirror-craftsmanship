package org.sociotech.communityinteraction.behaviors.direct;

import java.util.List;

import org.sociotech.communityinteraction.behaviors.TouchBehavior;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.touch.TouchEvent;
import org.sociotech.communityinteraction.helper.CIHandleEventHelper;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interactionhandling.HandleTouch;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;


/**
 * A behavior that directly applies received touch events.
 *  
 * @author Peter Lachenmaier
 */
public class DirectTouchBehavior extends TouchBehavior {

	/**
	 * Constructs the touch behavior for the given handler.
	 * 
	 * @param handler Handler that handles the touch interaction.
	 */
	public DirectTouchBehavior(HandleTouch handler) {
		super(handler);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#getConsumers(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public List<CIEventConsumer> getConsumers(CIEvent event) {
		if(!(event instanceof TouchEvent) || !this.isEnabled()) {
			return null;
		}
		return CIListHelper.listFromEventConsumer(this);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#handle(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public void handle(CIEvent event) {
		if(!(event instanceof TouchEvent)) {
			return;
		}
		// let the event be handled by the handler
		CIHandleEventHelper.handleTouchEventWith((TouchEvent) event, this.getInteractionHandler());
	}

}
