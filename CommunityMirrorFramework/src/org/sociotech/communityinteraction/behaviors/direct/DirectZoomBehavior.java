package org.sociotech.communityinteraction.behaviors.direct;

import java.util.List;

import org.sociotech.communityinteraction.behaviors.ZoomBehavior;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomEvent;
import org.sociotech.communityinteraction.helper.CIHandleEventHelper;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interactionhandling.HandleZoom;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;


/**
 * A behavior that directly applies received zoom events.
 *  
 * @author Peter Lachenmaier
 */
public class DirectZoomBehavior extends ZoomBehavior {

	/**
	 * Constructs the zoom behavior for the given handler.
	 * 
	 * @param handler Handler that handles the zoom interaction.
	 */
	public DirectZoomBehavior(HandleZoom handler) {
		super(handler);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#getConsumers(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public List<CIEventConsumer> getConsumers(CIEvent event) {
		if(!(event instanceof ZoomEvent) || !this.isEnabled()) {
			return null;
		}
		return CIListHelper.listFromEventConsumer(this);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#handle(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public void handle(CIEvent event) {
		if(!(event instanceof ZoomEvent)) {
			return;
		}
		// let the event be handled by the handler
		CIHandleEventHelper.handleZoomEventWith((ZoomEvent) event, this.getInteractionHandler());
	}

}
