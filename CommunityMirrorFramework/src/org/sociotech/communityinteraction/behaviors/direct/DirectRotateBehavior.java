package org.sociotech.communityinteraction.behaviors.direct;

import java.util.List;

import org.sociotech.communityinteraction.behaviors.manipulation.RotateBehavior;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationEvent;
import org.sociotech.communityinteraction.helper.CIHandleEventHelper;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interactionhandling.HandleRotate;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

/**
 * A interaction behavior that directly reacts on rotation events and let them be handled
 * by the assigned handler.
 * 
 * @author Peter Lachenmaier
 */
public class DirectRotateBehavior extends RotateBehavior{

	/**
	 * Constructs the direct rotation behavior with the given handler for rotation events.
	 * 
	 * @param handler Handler for rotation events.
	 */
	public DirectRotateBehavior(HandleRotate handler) {
		super(handler);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#getConsumers(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public List<CIEventConsumer> getConsumers(CIEvent event) {
		// use only rotation events
		if(!(event instanceof RotationEvent) || !this.isEnabled()) {
			return null;
		}
		
		return CIListHelper.listFromEventConsumer(this);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#handle(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public void handle(CIEvent event) {
		if(!(event instanceof RotationEvent)) {
			return;
		}
		// let the event be handled by the handler
		CIHandleEventHelper.handleRotationEventWith((RotationEvent) event, this.getInteractionHandler());
	}

}
