package org.sociotech.communityinteraction.behaviors.direct;

import java.util.List;

import org.sociotech.communityinteraction.behaviors.ScrollBehavior;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollEvent;
import org.sociotech.communityinteraction.helper.CIHandleEventHelper;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interactionhandling.HandleScroll;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;


/**
 * A behavior that directly applies received scroll events.
 *  
 * @author Peter Lachenmaier
 */
public class DirectScrollBehavior extends ScrollBehavior {

	/**
	 * Constructs the scroll behavior for the given handler.
	 * 
	 * @param handler Handler that handles the scroll interaction.
	 */
	public DirectScrollBehavior(HandleScroll handler) {
		super(handler);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#getConsumers(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public List<CIEventConsumer> getConsumers(CIEvent event) {
		if(!(event instanceof ScrollEvent) || !this.isEnabled()) {
			return null;
		}
		return CIListHelper.listFromEventConsumer(this);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#handle(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public void handle(CIEvent event) {
		if(!(event instanceof ScrollEvent)) {
			return;
		}
		// let the event be handled by the handler
		CIHandleEventHelper.handleScrollEventWith((ScrollEvent) event, this.getInteractionHandler());
	}

}
