package org.sociotech.communityinteraction.behaviors;

import org.sociotech.communityinteraction.interactionhandling.HandleTouch;

/**
 * Behavior to add touch interaction to a component.
 * 
 * @author Peter Lachenmaier
 */
public abstract class TouchBehavior extends InteractionBehavior<HandleTouch> {

	/**
	 * Creates the touch behavior for the given handler.
	 * 
	 * @param handler Handler for touch events.
	 */
	public TouchBehavior(HandleTouch handler) {
		super(handler);
	}

}
