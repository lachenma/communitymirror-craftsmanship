package org.sociotech.communityinteraction.behaviors.manipulation;

import org.sociotech.communityinteraction.behaviors.InteractionBehavior;
import org.sociotech.communityinteraction.interactionhandling.HandleRotate;

/**
 * Behavior to add rotate manipulation to a component.
 * 
 * @author Peter Lachenmaier
 */
public abstract class RotateBehavior extends InteractionBehavior<HandleRotate> {

	/**
	 * Creates the rotate behavior for the given handler.
	 * 
	 * @param handler Handler for rotate events.
	 */
	public RotateBehavior(HandleRotate handler) {
		super(handler);
	}

}
