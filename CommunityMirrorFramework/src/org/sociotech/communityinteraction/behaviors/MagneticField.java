package org.sociotech.communityinteraction.behaviors;


public class MagneticField {
	
	private int force;
	
	private double range;
	
	private MagneticPole pole;

	public MagneticField(int force, double range, MagneticPole pole) {
		this.force = force;
		this.range = range;
		this.pole = pole;
	}
	
	/**
	 * @return the force
	 */
	public int getForce() {
		return force;
	}


	/**
	 * @return the range
	 */
	public double getRange() {
		return range;
	}


	/**
	 * @return the pole
	 */
	public MagneticPole getPole() {
		return pole;
	}


	/**
	 * @param force the force to set
	 */
	public void setForce(int force) {
		this.force = force;
	}


	/**
	 * @param range the range to set
	 */
	public void setRange(double range) {
		this.range = range;
	}


	/**
	 * @param pole the pole to set
	 */
	public void setPole(MagneticPole pole) {
		this.pole = pole;
	}

}
