package org.sociotech.communityinteraction.behaviors;

import java.util.List;

import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.CommunicationEvent;
import org.sociotech.communityinteraction.helper.CIHandleEventHelper;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interactionhandling.HandleCommunication;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

/**
 * Behavior to add a communication behavior to a component.
 * 
 * @author Eva Lösch
 */
public class CommunicationBehavior extends InteractionBehavior<HandleCommunication> {

	/**
	 * Creates the communication behavior for the given handler.
	 * 
	 * @param handler Handler for communication events.
	 */
	public CommunicationBehavior(HandleCommunication handler) {
		super(handler);
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#getConsumers(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public List<CIEventConsumer> getConsumers(CIEvent event) {
		if(!(event instanceof CommunicationEvent) || !this.isEnabled()) {
			return null;
		}
		return CIListHelper.listFromEventConsumer(this);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.interfaces.CIEventConsumer#handle(org.sociotech.communityinteraction.events.CIEvent)
	 */
	@Override
	public void handle(CIEvent event) {
		if(!(event instanceof CommunicationEvent)){
			return;
		}
		CIHandleEventHelper.handleCommunicationEventWith((CommunicationEvent)event, this.getInteractionHandler());
	}

}
