package org.sociotech.communityinteraction.behaviors;

import org.sociotech.communityinteraction.interactionhandling.HandleScroll;

/**
 * Behavior to add scroll interaction to a component.
 * 
 * @author Peter Lachenmaier
 */
public abstract class ScrollBehavior extends InteractionBehavior<HandleScroll> {

	/**
	 * Creates the scroll behavior for the given handler.
	 * 
	 * @param handler Handler for scroll events.
	 */
	public ScrollBehavior(HandleScroll handler) {
		super(handler);
	}

}
