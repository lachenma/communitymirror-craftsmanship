package org.sociotech.communityinteraction.behaviors;

public enum MagneticPole {
	
	NORTH, SOUTH

}
