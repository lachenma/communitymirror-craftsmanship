package org.sociotech.communityinteraction.behaviors;

import org.sociotech.communityinteraction.interactionhandling.HandleInteraction;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

/**
 * The abstract super class of all interaction behaviors.
 * 
 * @author Peter Lachenmaier
 *
 * @param <T> The type of the interaction handler
 */
public abstract class InteractionBehavior<T extends HandleInteraction> implements CIEventConsumer {
	
	/**
	 * The real handler of the interaction 
	 */
	private T interactionHandler;
	
	/**
	 * Field storing the enabled state
	 */
	private boolean enabled = true;
	
		/**
	 * Stores a reference to the given handler.
	 * 
	 * @param handler Interaction handler.
	 */
	public InteractionBehavior(T handler) {
		this.setInteractionHandler(handler);
	}

	/**
	 * Returns the interaction handler belonging to this behavior.
	 * 
	 * @return Interaction handler of this behavior.
	 */
	public T getInteractionHandler() {
		return interactionHandler;
	}

	/**
	 * Sets the interaction handler for this behavior.
	 * 
	 * @param interactionHandler Interaction handler for this behavior.
	 */
	protected void setInteractionHandler(T interactionHandler) {
		this.interactionHandler = interactionHandler;
	}

	/**
	 * Returns whether this behavior is enabled or not.
	 * 
	 * @return Whether this behavior is enabled or not.
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Sets the enabled state of this behavior.
	 * 
	 * @param enabled New enabled state.
	 */
	protected void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	/**
	 * Enables this behavior
	 */
	public void enable() {
		this.setEnabled(true);
	}
	
	/**
	 * Disables this behavior
	 */
	public void disable() {
		this.setEnabled(true);
	}
}
