package org.sociotech.communityinteraction.behaviors;

import org.sociotech.communityinteraction.interactionhandling.HandleDrag;

/**
 * Behavior to add drag interaction to a component.
 * 
 * @author Peter Lachenmaier
 */
public abstract class DragBehavior extends InteractionBehavior<HandleDrag> {

	/**
	 * Creates the drag behavior for the given handler.
	 * 
	 * @param handler Handler for drag events.
	 */
	public DragBehavior(HandleDrag handler) {
		super(handler);
	}

}
