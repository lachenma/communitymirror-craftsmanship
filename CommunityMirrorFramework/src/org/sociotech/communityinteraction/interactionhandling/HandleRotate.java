package org.sociotech.communityinteraction.interactionhandling;

import org.sociotech.communityinteraction.events.gesture.rotation.RotationFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationRotatedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationStartedEvent;

/**
 * Interface that must be implemented to handle rotates.
 * 
 * @author Peter Lachenmaier
 */
public interface HandleRotate extends HandleInteraction {

	/**
	 * Will be called when a rotation is started on this component.
	 * 
	 * @param rotationEvent Rotation started event.
	 */
	public void onRotationStarted(RotationStartedEvent rotationEvent);

	/**
	 * Will be called when a rotation is performed on this component.
	 * 
	 * @param rotationEvent Rotation performed event.
	 */
	public void onRotationRotated(RotationRotatedEvent rotationEvent);

	/**
	 * Will be called when a rotation is finished on this component.
	 * 
	 * @param rotationEvent Rotation finished event.
	 */
	public void onRotationFinished(RotationFinishedEvent rotationEvent);

}
