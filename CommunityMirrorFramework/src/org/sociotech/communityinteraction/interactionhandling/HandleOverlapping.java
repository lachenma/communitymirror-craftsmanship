package org.sociotech.communityinteraction.interactionhandling;

import org.sociotech.communityinteraction.events.overlapping.OverlappingFinishedEvent;
import org.sociotech.communityinteraction.events.overlapping.OverlappingStartedEvent;

/**
 * Interface that must be implemented by components that handle overlapping
 * events.
 * 
 * @author evalosch
 *
 */
public interface HandleOverlapping extends HandleInteraction {

	/**
	 * Will be called if an overlapping started event occurs.
	 * 
	 * @param event
	 *            The overlapping started event to handle.
	 */
	public void onOverlappingStarted(OverlappingStartedEvent event);

	/**
	 * Will be called if an overlapping finished event occurs.
	 * 
	 * @param event
	 *            The overlapping finished event to handle.
	 */
	public void onOverlappingFinished(OverlappingFinishedEvent event);

}
