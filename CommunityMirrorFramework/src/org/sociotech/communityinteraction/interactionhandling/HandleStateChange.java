package org.sociotech.communityinteraction.interactionhandling;

import org.sociotech.communityinteraction.events.stateChange.State;
import org.sociotech.communityinteraction.events.stateChange.StateChangeableComponent;
import org.sociotech.communityinteraction.events.stateChange.StateChangedEvent;

/**
 * Interface that must be implemented by components that handle state
 * changes.
 * 
 * @param <T> The concrete state class that has changed.
 * 
 * @author evalosch
 *
 */
public interface HandleStateChange<T extends State, U extends StateChangeableComponent<T>> extends HandleInteraction {

	/**
	 * Will be called if a state changed event occurs.
	 * 
	 * @param event
	 *            The state changed event to handle.
	 */
	/**
	 * @param event
	 */
	public void onStateChanged(StateChangedEvent<T, U> event);

}
