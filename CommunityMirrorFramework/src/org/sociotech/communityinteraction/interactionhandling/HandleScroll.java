package org.sociotech.communityinteraction.interactionhandling;

import org.sociotech.communityinteraction.events.gesture.scroll.ScrollFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollScrolledEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollStartedEvent;

/**
 * Interface that must be implemented to handle scroll interaction.
 * 
 * @author Peter Lachenmaier
 */
public interface HandleScroll extends HandleInteraction {

	/**
	 * Will be called when a scroll is started on this component.
	 * 
	 * @param ScrollEvent Scroll started event.
	 */
	public void onScrollStarted(ScrollStartedEvent scrollEvent);

	/**
	 * Will be called when a scroll is performed on this component.
	 * 
	 * @param ScrollEvent Scroll performed event.
	 */
	public void onScrollScrolled(ScrollScrolledEvent scrollEvent);

	/**
	 * Will be called when a scroll is finished on this component.
	 * 
	 * @param ScrollEvent Scroll finished event.
	 */
	public void onScrollFinished(ScrollFinishedEvent scrollEvent);

}
