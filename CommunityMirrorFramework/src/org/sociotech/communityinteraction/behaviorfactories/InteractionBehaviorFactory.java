package org.sociotech.communityinteraction.behaviorfactories;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communityinteraction.behaviors.DragBehavior;
import org.sociotech.communityinteraction.behaviors.ScrollBehavior;
import org.sociotech.communityinteraction.behaviors.TouchBehavior;
import org.sociotech.communityinteraction.behaviors.ZoomBehavior;
import org.sociotech.communityinteraction.behaviors.manipulation.RotateBehavior;
import org.sociotech.communityinteraction.interactionhandling.HandleDrag;
import org.sociotech.communityinteraction.interactionhandling.HandleRotate;
import org.sociotech.communityinteraction.interactionhandling.HandleScroll;
import org.sociotech.communityinteraction.interactionhandling.HandleTouch;
import org.sociotech.communityinteraction.interactionhandling.HandleZoom;

/**
 * The abstract super class of all interaction behavior factories. All methods simply log
 * unsupported behavior warnings.
 *  
 * @author Peter Lachenmaier
 */
public abstract class InteractionBehaviorFactory {

	 /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(InteractionBehaviorFactory.class.getName());

	/**
	 * Creates a touch behavior with the given handler assigned.
	 * 
	 * @param handler Touch handler.
	 * 
	 * @return New touch behavior or null if the behavior is not supported by this factory.
	 */
	public TouchBehavior createTouchBehavior(HandleTouch handler) {
		logger.warn("Touch behaviours are not supported!");
		return null;
	}
	
	/**
	 * Creates a rotate behavior with the given handler assigned.
	 * 
	 * @param handler Rotation handler.
	 * 
	 * @return New rotate behavior or null if the behavior is not supported by this factory.
	 */
	public RotateBehavior createRotateBehavior(HandleRotate handler) {
		logger.warn("Rotate behaviours are not supported!");
		return null;
	}
	
	/**
	 * Creates a zoom behavior with the given handler assigned.
	 * 
	 * @param handler Zoom handler.
	 * 
	 * @return New zoom behavior or null if the behavior is not supported by this factory.
	 */
	public ZoomBehavior createZoomBehavior(HandleZoom handler) {
		logger.warn("Zoom behaviours are not supported!");
		return null;
	}
	
	/**
	 * Creates a drag behavior with the given handler assigned.
	 * 
	 * @param handler Drag handler.
	 * 
	 * @return New drag behavior or null if the behavior is not supported by this factory.
	 */
	public DragBehavior createDragBehavior(HandleDrag handler) {
		logger.warn("Drag behaviours are not supported!");
		return null;
	}
	
	/**
	 * Creates a scroll behavior with the given handler assigned.
	 * 
	 * @param handler Scroll handler.
	 * 
	 * @return New scroll behavior or null if the behavior is not supported by this factory.
	 */
	public ScrollBehavior createScrollBehavior(HandleScroll handler) {
		logger.warn("Drag behaviours are not supported!");
		return null;
	}
}
