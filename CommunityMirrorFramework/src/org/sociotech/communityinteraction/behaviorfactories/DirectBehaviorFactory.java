package org.sociotech.communityinteraction.behaviorfactories;

import org.sociotech.communityinteraction.behaviors.DragBehavior;
import org.sociotech.communityinteraction.behaviors.ScrollBehavior;
import org.sociotech.communityinteraction.behaviors.TouchBehavior;
import org.sociotech.communityinteraction.behaviors.ZoomBehavior;
import org.sociotech.communityinteraction.behaviors.direct.DirectDragBehavior;
import org.sociotech.communityinteraction.behaviors.direct.DirectRotateBehavior;
import org.sociotech.communityinteraction.behaviors.direct.DirectScrollBehavior;
import org.sociotech.communityinteraction.behaviors.direct.DirectTouchBehavior;
import org.sociotech.communityinteraction.behaviors.direct.DirectZoomBehavior;
import org.sociotech.communityinteraction.behaviors.manipulation.RotateBehavior;
import org.sociotech.communityinteraction.interactionhandling.HandleDrag;
import org.sociotech.communityinteraction.interactionhandling.HandleRotate;
import org.sociotech.communityinteraction.interactionhandling.HandleScroll;
import org.sociotech.communityinteraction.interactionhandling.HandleTouch;
import org.sociotech.communityinteraction.interactionhandling.HandleZoom;

/**
 * Factory to create behaviors which are mapping directly to ci events.
 * 
 * @author Peter Lachenmaier
 */
public class DirectBehaviorFactory extends InteractionBehaviorFactory {

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.behaviorfactories.InteractionBehaviorFactory#createTouchBehaviour(org.sociotech.communityinteraction.interactionhandling.HandleTouch)
	 */
	@Override
	public TouchBehavior createTouchBehavior(HandleTouch handler) {
		return new DirectTouchBehavior(handler);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.behaviorfactories.InteractionBehaviorFactory#createRotateBehaviour(org.sociotech.communityinteraction.interactionhandling.HandleRotate)
	 */
	@Override
	public RotateBehavior createRotateBehavior(HandleRotate handler) {
		return new DirectRotateBehavior(handler);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.behaviorfactories.InteractionBehaviorFactory#createZoomBehaviour(org.sociotech.communityinteraction.interactionhandling.HandleRotate)
	 */
	@Override
	public ZoomBehavior createZoomBehavior(HandleZoom handler) {
		return new DirectZoomBehavior(handler);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.behaviorfactories.InteractionBehaviorFactory#createDragBehavior(org.sociotech.communityinteraction.interactionhandling.HandleZoom)
	 */
	@Override
	public DragBehavior createDragBehavior(HandleDrag handler) {
		return new DirectDragBehavior(handler);
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communityinteraction.behaviorfactories.InteractionBehaviorFactory#createScrollBehavior(org.sociotech.communityinteraction.interactionhandling.HandleScroll)
	 */
	@Override
	public ScrollBehavior createScrollBehavior(HandleScroll handler) {
		return new DirectScrollBehavior(handler);
	}
}
