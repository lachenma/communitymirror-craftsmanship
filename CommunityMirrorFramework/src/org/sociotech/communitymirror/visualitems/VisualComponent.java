
package org.sociotech.communitymirror.visualitems;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javafx.animation.Animation;
import javafx.animation.Animation.Status;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

import org.apache.commons.configuration2.Configuration;
import org.sociotech.communityinteraction.activitylogger.UserActivityLogger;
import org.sociotech.communityinteraction.behaviors.InteractionBehavior;
import org.sociotech.communityinteraction.behaviors.MagneticEffectBehavior;
import org.sociotech.communityinteraction.behaviors.MagneticField;
import org.sociotech.communityinteraction.behaviors.OverlappingBehavior;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.PositionEvent;
import org.sociotech.communityinteraction.events.communication.MessageReceivedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragDraggedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragStartedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationRotatedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationStartedEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollScrolledEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollStartedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomStartedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomZoomedEvent;
import org.sociotech.communityinteraction.events.overlapping.IntersectionEvent;
import org.sociotech.communityinteraction.events.overlapping.MagneticAttractionFinishedEvent;
import org.sociotech.communityinteraction.events.overlapping.MagneticAttractionStartedEvent;
import org.sociotech.communityinteraction.events.overlapping.MagneticRepulsionFinishedEvent;
import org.sociotech.communityinteraction.events.overlapping.MagneticRepulsionStartedEvent;
import org.sociotech.communityinteraction.events.overlapping.OverlappingFinishedEvent;
import org.sociotech.communityinteraction.events.overlapping.OverlappingStartedEvent;
import org.sociotech.communityinteraction.events.touch.TouchHoldEvent;
import org.sociotech.communityinteraction.events.touch.TouchMovedEvent;
import org.sociotech.communityinteraction.events.touch.TouchPressedEvent;
import org.sociotech.communityinteraction.events.touch.TouchReleasedEvent;
import org.sociotech.communityinteraction.events.touch.TouchTappedEvent;
import org.sociotech.communityinteraction.helper.CIEventConsumerHelper;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interactionhandling.HandleDrag;
import org.sociotech.communityinteraction.interactionhandling.HandleMagneticEffect;
import org.sociotech.communityinteraction.interactionhandling.HandleOverlapping;
import org.sociotech.communityinteraction.interactionhandling.HandleRotate;
import org.sociotech.communityinteraction.interactionhandling.HandleScroll;
import org.sociotech.communityinteraction.interactionhandling.HandleTouch;
import org.sociotech.communityinteraction.interactionhandling.HandleZoom;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;
import org.sociotech.communityinteraction.interfaces.CommunicationConsumer;
import org.sociotech.communityinteraction.interfaces.IntersectionEventConsumer;
import org.sociotech.communityinteraction.interfaces.ManageInteractionBehaviors;
import org.sociotech.communityinteraction.interfaces.PositionedEventConsumer;
import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.configuration.ThemeResourceLoader;
import org.sociotech.communitymirror.physics.PhysicsApplicable;
import org.sociotech.communitymirror.physics.behaviors.DriftBehavior;
import org.sociotech.communitymirror.physics.behaviors.MagneticAttractionBehavior;
import org.sociotech.communitymirror.physics.behaviors.MagneticRepulsionBehavior;
import org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior;
import org.sociotech.communitymirror.physics.behaviors.StopMoveOnInteractionBehavior;
import org.sociotech.communitymirror.view.View;
import org.sociotech.communitymirror.view.flow.components.VisualComponentEvent;
import org.sociotech.communitymirror.view.flow.components.VisualComponentListener;
import org.sociotech.communitymirror.visualitems.components.spring.SpringConnectable;
import org.sociotech.communitymirror.visualstates.ComponentState;

/**
 * The abstract super class for all visual components. This class is a direct
 * subclass of a JavaFX {@link Group}, so it can contain all other JavaFX
 * components to build up a complex visual component.
 *
 * @author Peter Lachenmaier, Martin Burkhard
 */
public abstract class VisualComponent extends Group implements SpringConnectable, PhysicsApplicable,
        PositionedEventConsumer, IntersectionEventConsumer, ManageInteractionBehaviors, HandleDrag, HandleScroll,
        HandleRotate, HandleZoom, HandleTouch, HandleOverlapping, HandleMagneticEffect {
    /**
     * Keeps the current state of the Component.
     */
    private ComponentState componentState = ComponentState.DEFAULT;
    private ComponentState componentStateBeforePause = null;

    /**
     * Reference to the ThemeResourceLoader. Optional, may be null if the client
     * never sets one.
     */
    protected ThemeResourceLoader themeResources = null;

    /**
     * List of all visual component listeners that should be notified about
     * events on this visual component.
     */
    private List<VisualComponentListener> visualComponentListeners = new LinkedList<>();

    /**
     * The property for the time (in millis) since the last interaction
     */
    private final LongProperty timeSinceLastInteractionProperty = new SimpleLongProperty(Long.MAX_VALUE);

    /**
     * The system time in millis of the last interaction
     */
    private long timeOfLastInteractionInMillis = 0;

    /**
     * The counter for ids
     */
    private static int idCounter = 0;

    /**
     * The id of this event consumer.
     */
    private String id = "" + VisualComponent.idCounter++;

    /**
     * The list of physics behaviors.
     */
    private List<PhysicsBehavior> physicsBehaviors;

    /**
     * Indicates a change in the physics behavior list of this view to update
     * iteration copy.
     */
    private boolean physicsBehaviorsListChanged = true;

    /**
     * List of all physics behaviors for iteration in update loop. Will be
     * updated when physics behavior list has changed since last update.
     */
    private List<PhysicsBehavior> physicsBehaviorsListCopy;

    /**
     * The list of interaction behaviors. The behaviors are used to visualize
     * the resulting Event and not only the initial one. All interaction
     * behaviors are possible event consumers.
     */
    protected List<InteractionBehavior<?>> interactionBehaviors;

    /**
     * Local reference to the visual group this component is contained in.
     */
    private VisualGroup parentGroup = null;

    /**
     * Temporary value that contains the step in x direction in during the next
     * update.
     */
    private double moveX = 0.0;

    /**
     * Temporary value that contains the step in y direction in during the next
     * update.
     */
    private double moveY = 0.0;

    /**
     * Temporary value that contains the rotation step during next update
     */
    private double rotationAngle;

    /**
     * List that keeps all animation that needs to be managed.
     */
    protected List<Animation> managedAnimations;

    /**
     * The z-index of this component (this is for information only - the z index
     * cannot be manipulated setting this value, but only moving the component
     * in its group or moving it to another group.
     */
    private int zIndex = -1;

    /**
     * Reference to the global activity logger.
     */
    protected final UserActivityLogger activityLogger = CommunityMirror.getUserActivityLogger();

    /**
     * xPosition of this Visual item set on dragStarted
     */
    protected double dragStartedSceneX;

    /**
     * @return the dragStartedSceneY
     */
    public double getDragStartedSceneY() {
        return this.dragStartedSceneY;
    }

    /**
     * @return the dragStartedSceneX
     */
    public double getDragStartedSceneX() {
        return this.dragStartedSceneX;
    }

    /**
     * yPosition of this Visual item set on dragStarted
     */
    protected double dragStartedSceneY;

    /**
     * Initializes this visual component. Initialization is only possible if the
     * component state ({@link VisualComponent#getComponentState()} is
     * {@link ComponentState#INIT}.
     */
    public void init() {

        if (this.getComponentState() != ComponentState.INIT) {
            return;
        }

        // globalInitDestroyCount++;
        // System.out.println("Init: " + globalInitDestroyCount);

        this.onInit();
        // calculate z index
        this.updateZIndex();
        this.setComponentState(ComponentState.UPDATE);
    }

    /**
     * Path where the position of this component has moved
     */
    private Path positionPath;

    /**
     * Whether to add a new point for current position to the path
     */
    private boolean newPositionForPath = false;

    /**
     * Whether to create the position path of this component
     */
    private boolean createPositionPath = false;

    /**
     * Indicates if this component receives interaction events (is registered)
     * or not.
     */
    private boolean receiveInteractionEvents = false;

    /**
     * Reference to the configuration.
     */
    protected Configuration configuration = null;

    /**
     * Updates this visual component. This component needs to be in the update
     * state to execute the update.
     *
     * @param framesPerSecond
     *            Current frames per second.
     * @param nanoTime
     *            Nano time of the update step
     */
    public void update(double framesPerSecond, long nanoTime) {

        // Only call update in UPDATE_INT-State
        if (this.getComponentState() != ComponentState.UPDATE) {
            return;
        }

        this.onUpdate(framesPerSecond, nanoTime);
    }

    public void pause() {
        this.setComponentStateBeforePause(this.getComponentState());
        this.setComponentState(ComponentState.PAUSE);

        if (this.managedAnimations != null) {
            for (Animation animation : this.managedAnimations) {
                if (animation.getStatus() == Status.RUNNING) {
                    animation.pause();
                }
            }
        }
    }

    public void resume() {
        this.setComponentState(this.getComponentStateBeforePause());

        // manage pause state of animations
        if (this.managedAnimations != null) {
            for (Animation animation : this.managedAnimations) {
                if (animation.getStatus() == Status.PAUSED) {
                    animation.play();
                }
            }
        }
    }

    /**
     * Destroys this component.
     */
    @Override
    public void destroy() {

        // First, set die state
        this.setComponentState(ComponentState.DIE);

        // globalInitDestroyCount--;
        // System.out.println("Destroy: " + globalInitDestroyCount);

        // Second, perform destruction
        this.onDestroy();

        // Third, set destroy state
        this.setComponentState(ComponentState.DESTROYED);
    }

    /**
     * Adds the given node as child to this visual component.
     */
    public void addNode(Node node) {
        if (!this.getChildren().contains(node)) {
            this.getChildren().add(node);
        }
    }

    /**
     * Removes the given node from this visual component.
     */
    public void removeNode(Node node) {
        this.getChildren().remove(node);
    }

    public void setThemeResourceLoader(ThemeResourceLoader newThemeResourceLoader) {
        this.themeResources = newThemeResourceLoader;
    }

    public ComponentState getComponentState() {
        return this.componentState;
    }

    public ComponentState getComponentStateBeforePause() {
        return this.componentStateBeforePause;
    }

    private void setComponentState(ComponentState componentState) {
        if (componentState == null) {
            this.componentState = ComponentState.DEFAULT;
        } else {
            this.componentState = componentState;
        }
    }

    private void setComponentStateBeforePause(ComponentState componentState) {
        if (componentState == null) {
            this.componentStateBeforePause = ComponentState.DEFAULT;
        } else {
            this.componentStateBeforePause = componentState;
        }
    }

    /**
     * Whether to create a path with the positions of this component or not.
     *
     * @param createPath
     *            True to create the position path. False to switch it off.
     */
    public void createPositionPath(boolean createPath) {
        if (this.positionPath != null) {
            // clear all elements
            this.positionPath.getElements().clear();
        }

        if (createPath && !this.createPositionPath) {
            // switched on

            // init path
            if (this.positionPath == null) {
                // never used before
                this.positionPath = new Path();

                // create listener
                ChangeListener<Number> positionChangeListener = new ChangeListener<Number>() {

                    @Override
                    public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {
                        // indicate a new position
                        VisualComponent.this.newPositionForPath = true;
                    }
                };

                // add listener to x and y to observe position changes
                this.getPositionXProperty().addListener(positionChangeListener);
                this.getPositionYProperty().addListener(positionChangeListener);
            }

            // start at current position
            this.positionPath.getElements()
                    .add(new MoveTo(this.getPositionXProperty().get(), this.getPositionYProperty().get()));

            // set visual properties
            this.positionPath.setFill(null);
            this.positionPath.setStroke(Color.RED);
            this.positionPath.setStrokeWidth(2);
        }

        // keep setting
        this.createPositionPath = createPath;
    }

    /**
     * Returns the path with the position of this component since the position
     * path was switched on with
     * {@link VisualComponent#createPositionPath(true)}. If it was never
     * switched on, this method will return null.
     *
     * @return The path with the positions of this component.
     */
    public Path getPositionPath() {
        return this.positionPath;
    }

    /**
     * Will be called while initialization of this component. Execute your
     * initialization code in this method. Make sure to always call
     * super.onInit() .
     */
    protected void onInit() {
        // set a random unique id for this VisualComponent
        this.setId(UUID.randomUUID().toString());
        this.setZIndex(0);

    }

    /**
     * Will be called in every update step. Overwrite this method in concrete
     * subclasses to perform your updates. Make sure to always call
     * super.onUpdate() .
     *
     * @param framesPerSecond
     *            The current frames per second.
     * @param nanoTime
     *            Nano time when this update step has started
     */
    protected void onUpdate(double framesPerSecond, long nanoTime) {
        if (this.timeOfLastInteractionInMillis != 0) {
            // calculate time since last interaction
            this.timeSinceLastInteractionProperty.set(System.currentTimeMillis() - this.timeOfLastInteractionInMillis);
        }

        if (this.physicsBehaviors != null) {
            // update all physics behaviors
            for (PhysicsBehavior behavior : this.physicsBehaviors) {
                behavior.update(framesPerSecond, nanoTime);
            }
            if (this.physicsBehaviorsListChanged) {
                // update copy
                this.physicsBehaviorsListCopy = new LinkedList<>(this.physicsBehaviors);
                this.physicsBehaviorsListChanged = false;
            }
            // and apply them to this visual component
            for (PhysicsBehavior behavior : this.physicsBehaviorsListCopy) {
                behavior.applyTo(this);
            }
        }

        // apply changes made to move and rotate
        this.applyChanges();

        // check if a new position must be added to the path
        if (this.createPositionPath && this.newPositionForPath) {
            this.positionPath.getElements()
                    .add(new LineTo(this.getPositionXProperty().get(), this.getPositionYProperty().get()));
            this.newPositionForPath = false;
        }
    }

    /**
     * Will be called when this visual component gets destroyed. Overwrite this
     * method in concrete subclasses to execute destruction code. Make sure to
     * always call super.onDestroy() .
     */
    protected void onDestroy() {
        // clear managed animation
        if (this.managedAnimations != null) {
            this.managedAnimations.clear();
        }

        if (this.receiveInteractionEvents) {
            // unregister from interaction events
            CommunityMirror.getInteractionEventCreator().unregisterEventConsumer(this);
        }

        // clear interaction behaviors
        if (this.interactionBehaviors != null) {
            this.interactionBehaviors.clear();
        }

        // clear physics behaviors
        if (this.physicsBehaviors != null) {
            this.physicsBehaviors.clear();
        }
        // and copy
        if (this.physicsBehaviorsListCopy != null) {
            this.physicsBehaviorsListCopy.clear();
        }

        // TODO remove this after remove of listeners
        if (this.visualComponentListeners != null) {
            this.visualComponentListeners.clear();
        }

        // remove all nodes
        this.getChildren().clear();

        // remove from parent
        this.setParentVisualGroup(null);
    }

    /**
     * Add the given animation to the managed animations. Currently only the
     * pause state will be managed.
     *
     * @param animation
     *            Animation to manage.
     */
    protected void manageAnimation(Animation animation) {
        if (animation == null) {
            return;
        }

        if (this.managedAnimations == null) {
            // create list on first use
            this.managedAnimations = new LinkedList<>();
        }

        if (!this.managedAnimations.contains(animation)) {
            // add it to the managed animations
            this.managedAnimations.add(animation);
        }
    }

    /**
     * Removes the given animation from the managed animations.
     *
     * @param animation
     *            Animation to stop managing for.
     */
    protected void removeManagedAnimation(Animation animation) {
        if (animation == null) {
            return;
        }

        if (this.managedAnimations == null) {
            return;
        }

        if (this.managedAnimations.contains(animation)) {
            // remove it from the managed animations
            this.managedAnimations.remove(animation);
        }
    }

    /**
     * Adds the given physics behavior to this visual component.
     *
     * @param behavior
     *            Physics behavior to add to this component.
     */
    public void addPhysicsBehavior(PhysicsBehavior behavior) {
        if (this.physicsBehaviors == null) {
            // create behavior list on first usage
            this.physicsBehaviors = new LinkedList<>();
        }

        // add it
        this.physicsBehaviors.add(behavior);
        // indicate change
        this.physicsBehaviorsListChanged = true;
    }

    /**
     * Returns all physics behaviors added to this visual component.
     *
     */
    public List<PhysicsBehavior> getPhysicsBehaviors() {

        return this.physicsBehaviors;

    }

    /**
     * Checks if this VisualComponent has a special PhysicsBehavior attached.
     *
     * @param behavior
     *            : the behavior that should be checked
     * @return true if behavior is attached else false
     */
    public boolean hasPhysicsBehavior(Class<? extends PhysicsBehavior> physicsBehaviorClass) {
        List<PhysicsBehavior> attachedBehaviors = this.getPhysicsBehaviors();

        for (PhysicsBehavior attachedBehavior : attachedBehaviors) {
            if (attachedBehavior.getClass().equals(physicsBehaviorClass)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the first instance of a given special PhysicsBehavior in the list
     * of physics behaviors attached to this VisualComponent.
     *
     * @param behavior
     *            : the behavior that should be returned
     * @return the first instance of the special PhysicsBehavior in the list or
     *         null if no instance is found
     */
    public PhysicsBehavior getFirstPhysicsBehavior(Class<? extends PhysicsBehavior> physicsBehaviorClass) {
        List<PhysicsBehavior> attachedBehaviors = this.getPhysicsBehaviors();

        for (PhysicsBehavior attachedBehavior : attachedBehaviors) {
            if (attachedBehavior.getClass().equals(physicsBehaviorClass)) {
                return attachedBehavior;
            }
        }
        return null;
    }

    /**
     * Returns the first instance of a given special InteractionBehavior in the
     * list of interaction behaviors attached to this VisualComponent.
     *
     * @param behavior
     *            : the behavior that should be returned
     * @return the first instance of the special InteractionBehavior in the list
     *         or null if no instance is found
     */
    public InteractionBehavior<?> getFirstInteractionBehavior(
            Class<? extends InteractionBehavior<?>> interactionBehaviorClass) {
        List<InteractionBehavior<?>> attachedBehaviors = this.getInteractionBehaviors();

        if (attachedBehaviors != null) {
            for (InteractionBehavior<?> attachedBehavior : attachedBehaviors) {
                if (attachedBehavior.getClass().equals(interactionBehaviorClass)) {
                    return attachedBehavior;
                }
            }
        }
        return null;
    }

    /**
     * Removes the given physics behavior from this visual component.
     *
     * @param behavior
     *            Behavior to remove
     */
    public void removePhysicsBehavior(PhysicsBehavior behavior) {
        this.physicsBehaviors.remove(behavior);
        // indicate change
        this.physicsBehaviorsListChanged = true;
    }

    /**
     * Removes all physics behaviors from this visual component.
     */
    public void clearPhysicsBehaviors() {
        if (this.physicsBehaviors != null) {
            this.physicsBehaviors.clear();
            // indicate change
            this.physicsBehaviorsListChanged = true;
        }
    }

    /**
     * Applies the changes that are made by calls to move, rotate ..
     */
    private void applyChanges() {
        // move in x direction
        if (this.moveX != 0.0) {
            this.getPositionXProperty().set(this.getPositionXProperty().get() + this.moveX);
        }

        // move in y direction
        if (this.moveY != 0.0) {
            this.getPositionYProperty().set(this.getPositionYProperty().get() + this.moveY);
        }

        // reset move values
        this.resetMove();

        // rotate
        if (this.rotationAngle != 0.0) {
            this.rotateProperty().set(this.rotateProperty().get() + this.rotationAngle);
        }

        // reset rotation angle
        this.resetRotation();
    }

    // implementation of SpringConnectable interface

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.visualitems.components.spring.
     * SpringConnectable
     * #getSpringConnectorXProperty()
     */
    @Override
    public DoubleProperty getSpringConnectorXProperty() {
        // use the position property for spring connection
        return this.getPositionXProperty();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.visualitems.components.spring.
     * SpringConnectable
     * #getSpringConnectorYProperty()
     */
    @Override
    public DoubleProperty getSpringConnectorYProperty() {
        // use the position property for spring connection
        return this.getPositionYProperty();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.visualitems.components.spring.
     * SpringConnectable
     * #isFixed()
     */
    @Override
    public boolean isFixed() {
        return false;
    }

    // implementation of PhysicsApplicable interface

    @Override
    public double getWeight() {
        return 1.0;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.physics.PhysicsApplicable#move(double,
     * double)
     */
    @Override
    public void move(double diffX, double diffY) {
        // move in next update step
        this.moveX += diffX;
        this.moveY += diffY;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.physics.PhysicsApplicable#rotate(double)
     */
    @Override
    public void rotate(double angle) {
        this.rotationAngle += angle;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.physics.PhysicsApplicable#getPositionBounds
     * ()
     */
    @Override
    public Bounds getPositionBounds() {
        return this.getBoundsInParent();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.physics.PhysicsApplicable#
     * getOpacityProperty
     * ()
     */
    @Override
    public DoubleProperty getOpacityProperty() {
        return this.opacityProperty();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.physics.PhysicsApplicable#
     * getTimeSinceLastInteractionProperty()
     */
    @Override
    public LongProperty getTimeSinceLastInteractionProperty() {
        return this.timeSinceLastInteractionProperty;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.physics.PhysicsApplicable#resetMove()
     */
    @Override
    public void resetMove() {
        this.moveX = 0;
        this.moveY = 0;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.physics.PhysicsApplicable#resetRotation()
     */
    @Override
    public void resetRotation() {
        this.rotationAngle = 0.0;
    }

    /**
     * Returns the property that is used for x positioning of this component.
     *
     * @return The property that is used for x positioning of this component.
     */
    @Override
    public DoubleProperty getPositionXProperty() {
        return this.layoutXProperty();
    }

    /**
     * Returns the value of the position x property.
     *
     * @return The value of the position x property.
     */
    public double getPositionX() {
        return this.getPositionXProperty().get();
    }

    /**
     * Sets the x position
     *
     * @param x
     *            The new x positon
     */
    public void setPositionX(double x) {
        this.getPositionXProperty().set(x);
    }

    /**
     * Returns the property that is used for y positioning of this component.
     *
     * @return The property that is used for y positioning of this component.
     */
    @Override
    public DoubleProperty getPositionYProperty() {
        return this.layoutYProperty();
    }

    /**
     * Returns the value of the position x property.
     *
     * @return The value of the position x property.
     */
    public double getPositionY() {
        return this.getPositionYProperty().get();
    }

    /**
     * Sets the y position
     *
     * @param y
     *            The new y positon
     */
    public void setPositionY(double y) {
        this.getPositionYProperty().set(y);
    }

    /**
     * Put this component to top in the containing group
     */
    public void putToTop() {
        if (this.parentGroup != null) {
            // put to top in group
            this.parentGroup.putToTop(this);
        } else {
            // just on top in the visual tree
            this.toFront();
        }
    }

    /**
     * Put this component to back in the containing group
     */
    public void putToBack() {
        if (this.parentGroup != null) {
            // put to back in group
            this.parentGroup.putToBack(this);
        } else {
            // just to back in the visual tree
            this.toBack();
        }
    }

    // implementation of Handle Drag

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleDrag#
     * onDragStarted
     * (org.sociotech.communityinteraction.events.gesture.drag.DragStartedEvent)
     */
    @Override
    public void onDragStarted(DragStartedEvent dragEvent) {

        VisualComponentEvent vcEvent = new VisualComponentEvent(this, dragEvent);
        this.notifyListeners(vcEvent);
        if (!vcEvent.getSourceEvent().isConsumed()) {
            this.dragStartedSceneX = dragEvent.getSceneX();
            this.dragStartedSceneY = dragEvent.getSceneY();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleDrag#
     * onDragDragged
     * (org.sociotech.communityinteraction.events.gesture.drag.DragDraggedEvent)
     */
    @Override
    public void onDragDragged(DragDraggedEvent dragEvent) {
        VisualComponentEvent vcEvent = new VisualComponentEvent(this, dragEvent);
        this.notifyListeners(vcEvent);

        if (!vcEvent.getSourceEvent().isConsumed()) {
            // move in both direction
            this.setPositionX(this.getPositionX() + dragEvent.getDeltaX());
            this.setPositionY(this.getPositionY() + dragEvent.getDeltaY());

            // consume event
            dragEvent.consume();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleDrag#
     * onDragFinished
     * (org.sociotech.communityinteraction.events.gesture.drag.DragFinishedEvent
     * )
     */
    @Override
    public void onDragFinished(DragFinishedEvent dragEvent) {
        // notify listeners
        this.notifyListeners(new VisualComponentEvent(this, dragEvent));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleTouch#
     * onTouchPressed
     * (org.sociotech.communityinteraction.events.touch.TouchPressedEvent)
     */
    @Override
    public void onTouchPressed(TouchPressedEvent event) {
        // reset interaction time on simple touch events
        this.resetInteractionTime();

        // put this component on top in the group
        this.putToTop();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleTouch#
     * onTouchReleased
     * (org.sociotech.communityinteraction.events.touch.TouchReleasedEvent)
     */
    @Override
    public void onTouchReleased(TouchReleasedEvent event) {
        // reset interaction time on simple touch events
        this.resetInteractionTime();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleTouch#
     * onTouchMoved
     * (org.sociotech.communityinteraction.events.touch.TouchMovedEvent)
     */
    @Override
    public void onTouchMoved(TouchMovedEvent event) {
        // reset interaction time on simple touch events
        this.resetInteractionTime();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleTouch#
     * onTouchTapped
     * (org.sociotech.communityinteraction.events.touch.TouchTappedEvent)
     */
    @Override
    public void onTouchTapped(TouchTappedEvent event) {
        // reset interaction time on simple touch events
        this.resetInteractionTime();

        // notify listeners
        this.notifyListeners(new VisualComponentEvent(this, event));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleTouch#
     * onTouchHold
     * (org.sociotech.communityinteraction.events.touch.TouchHoldEvent)
     */
    @Override
    public void onTouchHold(TouchHoldEvent event) {
        // do nothing by default
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleZoom#
     * onZoomStarted
     * (org.sociotech.communityinteraction.events.gesture.zoom.ZoomStartedEvent)
     */
    @Override
    public void onZoomStarted(ZoomStartedEvent zoomEvent) {
        // reset interaction time on zoom started events
        this.resetInteractionTime();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleZoom#
     * onZoomZoomed
     * (org.sociotech.communityinteraction.events.gesture.zoom.ZoomZoomedEvent)
     */
    @Override
    public void onZoomZoomed(ZoomZoomedEvent zoomEvent) {
        // reset interaction time on zoom started events
        this.resetInteractionTime();

        // scale in both directions
        this.setScaleX(this.getScaleX() * zoomEvent.getZoomFactor());
        this.setScaleY(this.getScaleY() * zoomEvent.getZoomFactor());

        // and consume event
        zoomEvent.consume();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleZoom#
     * onZoomFinished
     * (org.sociotech.communityinteraction.events.gesture.zoom.ZoomFinishedEvent
     * )
     */
    @Override
    public void onZoomFinished(ZoomFinishedEvent zoomEvent) {
        // notify listeners
        this.notifyListeners(new VisualComponentEvent(this, zoomEvent));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleRotate#
     * onRotationStarted
     * (org.sociotech.communityinteraction.events.gesture.rotation
     * .RotationStartedEvent)
     */
    @Override
    public void onRotationStarted(RotationStartedEvent rotationEvent) {
        // do nothing by default
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleRotate#
     * onRotationRotated
     * (org.sociotech.communityinteraction.events.gesture.rotation
     * .RotationRotatedEvent)
     */
    @Override
    public void onRotationRotated(RotationRotatedEvent rotationEvent) {
        // rotate
        this.setRotate(this.getRotate() + rotationEvent.getAngle());

        // and consume event
        rotationEvent.consume();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleRotate#
     * onRotationFinished
     * (org.sociotech.communityinteraction.events.gesture.rotation
     * .RotationFinishedEvent)
     */
    @Override
    public void onRotationFinished(RotationFinishedEvent rotationEvent) {
        // notify listeners
        this.notifyListeners(new VisualComponentEvent(this, rotationEvent));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleScroll#
     * onScrollStarted
     * (org.sociotech.communityinteraction.events.gesture.scroll.
     * ScrollStartedEvent )
     */
    @Override
    public void onScrollStarted(ScrollStartedEvent scrollEvent) {
        // do nothing by default
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleScroll#
     * onScrollScrolled
     * (org.sociotech.communityinteraction.events.gesture.scroll.
     * ScrollScrolledEvent)
     */
    @Override
    public void onScrollScrolled(ScrollScrolledEvent scrollEvent) {
        // do nothing by default
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleScroll#
     * onScrollFinished
     * (org.sociotech.communityinteraction.events.gesture.scroll.
     * ScrollFinishedEvent)
     */
    @Override
    public void onScrollFinished(ScrollFinishedEvent scrollEvent) {
        // do nothing by default
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.
     * HandleMagneticAttraction
     * #onMagneticAttractionStarted(org.sociotech.communityinteraction
     * .events.overlapping.MagneticAttractionStartedEvent)
     */
    @Override
    public void onMagneticAttractionStarted(MagneticAttractionStartedEvent magneticAttractionStartedEvent) {

        // get the other component of attraction event
        VisualComponent otherComponent = magneticAttractionStartedEvent.getIntersection().getOppenent(this);
        if (otherComponent == null) {
            return;
        }

        // get old drift behavior
        DriftBehavior driftBehavior = (DriftBehavior) this.getFirstPhysicsBehavior(new DriftBehavior(0, 0).getClass());
        if (driftBehavior == null) {
            // or create a new drift behavior
            driftBehavior = new DriftBehavior(0, 0, 0, 0.1, 1.0);
        }
        // remove old drift behavior
        this.removePhysicsBehavior(driftBehavior);

        // get stop move on interaction behavior
        StopMoveOnInteractionBehavior stopMoveOnInteractionBehavior = (StopMoveOnInteractionBehavior) this
                .getFirstPhysicsBehavior(StopMoveOnInteractionBehavior.class);
        // remove stop move on interaction behavior
        if (stopMoveOnInteractionBehavior != null) {
            this.removePhysicsBehavior(stopMoveOnInteractionBehavior);
        }

        // create new drift towards target behavior
        MagneticAttractionBehavior driftTowardsTargetBoundsBehavior = new MagneticAttractionBehavior(
                driftBehavior.velocityProperty().get(), driftBehavior.accelerationProperty().get(),
                driftBehavior.targetVelocityProperty().get(), otherComponent);
        // add new drift behavior
        this.addPhysicsBehavior(driftTowardsTargetBoundsBehavior);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.
     * HandleMagneticAttraction
     * #onMagneticAttractionFinished(org.sociotech.communityinteraction
     * .events.overlapping.MagneticAttractionFinishedEvent)
     */
    @Override
    public void onMagneticAttractionFinished(MagneticAttractionFinishedEvent magneticAttractionFinishedEvent) {

        // get magneticAttractionBehavior
        MagneticAttractionBehavior magneticAttractionBehavior = (MagneticAttractionBehavior) this
                .getFirstPhysicsBehavior(MagneticAttractionBehavior.class);
        if (magneticAttractionBehavior != null) {
            // remove magneticAttractionBehavior
            this.removePhysicsBehavior(magneticAttractionBehavior);

            // add normal drift behavior
            DriftBehavior driftBehavior = new DriftBehavior(1, 0, magneticAttractionBehavior.velocityProperty().get(),
                    magneticAttractionBehavior.accelerationProperty().get(),
                    magneticAttractionBehavior.targetVelocityProperty().get());
            this.addPhysicsBehavior(driftBehavior);

            // add stop move on interaction behavior
            // ....???
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.
     * HandleMagneticRepulsion
     * #onMagneticRepulsionStarted(org.sociotech.communityinteraction
     * .events.overlapping.MagneticRepulsionStartedEvent)
     */
    @Override
    public void onMagneticRepulsionStarted(MagneticRepulsionStartedEvent magneticRepulsionStartedEvent) {

        // get the other component of repulsion event
        VisualComponent otherComponent = magneticRepulsionStartedEvent.getIntersection().getOppenent(this);
        if (otherComponent == null) {
            return;
        }

        // get drift behavior
        DriftBehavior driftBehavior = (DriftBehavior) this.getFirstPhysicsBehavior(new DriftBehavior(0, 0).getClass());
        if (driftBehavior == null) {
            // or create a new drift behavior
            driftBehavior = new DriftBehavior(0, 0);
            this.addPhysicsBehavior(driftBehavior);
        }

        // get stop move on interaction behavior
        StopMoveOnInteractionBehavior stopMoveOnInteractionBehavior = (StopMoveOnInteractionBehavior) this
                .getFirstPhysicsBehavior(StopMoveOnInteractionBehavior.class);
        // remove stop move on interaction behavior
        if (stopMoveOnInteractionBehavior != null) {
            this.removePhysicsBehavior(stopMoveOnInteractionBehavior);
        }

        // create new magnetic repulsion behavior
        MagneticRepulsionBehavior magneticRepulsionBehavior = new MagneticRepulsionBehavior(driftBehavior,
                otherComponent);
        // add magnetic repulsion behavior
        this.addPhysicsBehavior(magneticRepulsionBehavior);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.
     * HandleMagneticRepulsion
     * #onMagneticRepulsionFinished(org.sociotech.communityinteraction
     * .events.overlapping.MagneticRepulsionFinishedEvent)
     */
    @Override
    public void onMagneticRepulsionFinished(MagneticRepulsionFinishedEvent magneticRepulsionFinishedEvent) {

        // remove magneticRepulsionBehavior
        MagneticRepulsionBehavior magneticRepulsionBehavior = (MagneticRepulsionBehavior) this
                .getFirstPhysicsBehavior(MagneticRepulsionBehavior.class);
        if (magneticRepulsionBehavior != null) {
            this.removePhysicsBehavior(magneticRepulsionBehavior);

            // add stop move on interaction behavior
            // ....???
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communityinteraction.interactionhandling.HandleOverlapping
     * #onOverlappingStarted
     * (org.sociotech.communityinteraction.events.overlapping
     * .OverlappingStartedEvent)
     */
    @Override
    public void onOverlappingStarted(OverlappingStartedEvent overlappingStartedEvent) {
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communityinteraction.interactionhandling.HandleOverlapping
     * #onOverlappingFinished
     * (org.sociotech.communityinteraction.events.overlapping
     * .OverlappingFinishedEvent)
     */
    @Override
    public void onOverlappingFinished(OverlappingFinishedEvent overlappingFinishedEvent) {
    }

    // TODO handle other events

    // methods to make components interactive

    /**
     * Makes this component react to overlapping events by adding a overlapping
     * behavior.
     *
     * @author Eva Lösch
     */
    public void makeReactToOverlappingEvents() {
        // create a overlapping behavior and add it to this component
        this.addInteractionBehavior(new OverlappingBehavior(this));
    }

    /**
     * Makes this component magnatic by adding a magnetic effect behavior.
     *
     * @author Eva Lösch
     */
    public void makeMagnetic(MagneticField magneticField) {
        List<InteractionBehavior<?>> interactionBehavior = this.getInteractionBehaviors();
        if (interactionBehavior != null) {
            Iterator<InteractionBehavior<?>> iterator = this.getInteractionBehaviors().iterator();
            while (iterator.hasNext()) {
                InteractionBehavior<?> nextBehavior = iterator.next();
                if (nextBehavior instanceof MagneticEffectBehavior) {
                    this.getInteractionBehaviors().remove(nextBehavior);
                    break;
                }
            }
        }
        // create a magnetic repulsion behavior and add it to this component
        this.addInteractionBehavior(new MagneticEffectBehavior(this, magneticField));
    }

    /**
     * Makes this component scrollable by adding a scroll behavior.
     */
    public void makeTouchable() {
        // create a touch behavior and add it to this component
        this.addInteractionBehavior(CommunityMirror.getInteractionBehaviorFactory().createTouchBehavior(this));

        // register this component for interaction events
        this.receiveInteractionEvents();
    }

    /**
     * Makes this component dragable by adding a drag behavior.
     */
    public void makeDragable() {
        // create a drag behavior and add it to this component
        this.addInteractionBehavior(CommunityMirror.getInteractionBehaviorFactory().createDragBehavior(this));

        // register this component for interaction events
        this.receiveInteractionEvents();
    }

    /**
     * Makes this component zoomable by adding a zoom behavior.
     */
    public void makeZoomable() {
        // create a zoom behavior and add it to this component
        this.addInteractionBehavior(CommunityMirror.getInteractionBehaviorFactory().createZoomBehavior(this));

        // register this component for interaction events
        this.receiveInteractionEvents();
    }

    /**
     * Makes this component rotatable by adding a rotate behavior.
     */
    public void makeRotatable() {
        // create a rotate behavior and add it to this component
        this.addInteractionBehavior(CommunityMirror.getInteractionBehaviorFactory().createRotateBehavior(this));

        // register this component for interaction events
        this.receiveInteractionEvents();
    }

    /**
     * Makes this component scrollable by adding a scroll behavior.
     */
    public void makeScrollable() {
        // create a scroll behavior and add it to this component
        this.addInteractionBehavior(CommunityMirror.getInteractionBehaviorFactory().createScrollBehavior(this));

        // register this component for interaction events
        this.receiveInteractionEvents();
    }

    // implementation of CIEventConsumer

    /**
     * Registers this component to receive interaction events.
     */
    protected void receiveInteractionEvents() {
        if (this.receiveInteractionEvents) {
            // already registerd
            return;
        }

        // keep registration state
        this.receiveInteractionEvents = true;

        // register on global event creator
        CommunityMirror.getInteractionEventCreator().registerEventConsumer(this);
    }

    /**
     * Collects the possible consumers in direction to the root of the tree.
     *
     * @param event
     *            Event to collect consumers for
     * @return The list of possible consumers
     */
    public List<CIEventConsumer> getConsumersToTop(CIEvent event) {

        // create new list for result
        List<CIEventConsumer> consumerList = CIListHelper.createConsumerList();

        // first: mine
        // use behaviors
        consumerList.addAll(CIEventConsumerHelper.collectConsumersFromBehaviors(this.interactionBehaviors, event));

        // second: parent
        if (this.parentGroup != null) {
            consumerList.addAll(this.parentGroup.getConsumersToTop(event));
        }

        return consumerList;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communityinteraction.interfaces.CIEventConsumer#
     * getConsumers
     * (org.sociotech.communityinteraction.events.CIEvent)
     */
    @Override
    public List<CIEventConsumer> getConsumers(CIEvent event) {

        if (this.getComponentState() == ComponentState.PAUSE) {
            // no interaction during pause
            return CIListHelper.emptyUnmodifiableEventConsumerList();
        }

        if (this instanceof PositionedEventConsumer) {
            // consume position events
            if (event instanceof PositionEvent) {
                if (event.getTargetConsumers() != null && event.getTargetConsumers().contains(this) || this
                        .isOnPosition(((PositionEvent) event).getSceneX(), ((PositionEvent) event).getSceneY())) {
                    return this.getConsumersToTop(event);
                }
            }
        }

        if (this instanceof IntersectionEventConsumer) {
            // consume interaction events
            if (event instanceof IntersectionEvent) {
                return (this.getConsumersToTop(event));
            }
        }

        if (this instanceof CommunicationConsumer) {
            // consume communication events
            if (event instanceof MessageReceivedEvent) {
                return (this.getConsumersToTop(event));
            }
        }

        return CIListHelper.emptyUnmodifiableEventConsumerList();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communityinteraction.interfaces.CIEventConsumer#handle(
     * org.sociotech.communityinteraction.events.CIEvent)
     */
    @Override
    public void handle(CIEvent event) {
        // do nothing in default implementation
    }

    // implementation of PositionedEventConsumer

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communityinteraction.interfaces.PositionedEventConsumer
     * #isOnPosition(double, double)
     */
    @Override
    public boolean isOnPosition(double sceneX, double sceneY) {
        if (this.getComponentState() == ComponentState.PAUSE) {
            // no interaction during pause
            return false;
        }

        // check if one of the children contains the point
        for (Node child : this.getChildren()) {
            if (child.contains(child.sceneToLocal(sceneX, sceneY))) {
                return true;
            }
        }

        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communityinteraction.interfaces.PositionedEventConsumer
     * #isTarget(java.lang.Object)
     */
    @Override
    public boolean isTarget(Object target) {
        if (target == null) {
            return false;
        }

        if (this.getComponentState() == ComponentState.PAUSE) {
            // no interaction during pause
            return false;
        }

        if (target == this) {
            return true;
        }

        // step up in hierarchy
        if (target instanceof Node) {
            return this.isTarget(((Node) target).getParent());
        }

        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communityinteraction.interfaces.PositionedEventConsumer
     * #getID()
     */
    @Override
    public String getID() {
        return this.id;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communityinteraction.interfaces.PositionedEventConsumer
     * #setID(java.lang.String)
     */
    @Override
    public void setID(String id) {
        this.id = id;
    }

    // implementation of ManageInteractionBehaviors

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communityinteraction.interfaces.ManageInteractionBehaviors
     * #addInteractionBehavior
     * (org.sociotech.communityinteraction.behaviors.InteractionBehavior)
     */
    @Override
    public void addInteractionBehavior(InteractionBehavior<?> interactionBehavior) {
        if (this.interactionBehaviors == null) {
            // create new list for interaction behaviors
            this.interactionBehaviors = new LinkedList<>();
        }

        // add only once
        if (!this.interactionBehaviors.contains(interactionBehavior)) {
            this.interactionBehaviors.add(interactionBehavior);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communityinteraction.interfaces.ManageInteractionBehaviors
     * #removeInteractionBehavior
     * (org.sociotech.communityinteraction.behaviors.InteractionBehavior)
     */
    @Override
    public void removeInteractionBehavior(InteractionBehavior<?> interactionBehavior) {
        if (this.interactionBehaviors != null) {
            this.interactionBehaviors.remove(interactionBehavior);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communityinteraction.interfaces.ManageInteractionBehaviors
     * #getInteractionBehaviors
     * (org.sociotech.communityinteraction.behaviors.InteractionBehavior)
     */
    @Override
    public List<InteractionBehavior<?>> getInteractionBehaviors() {
        return this.interactionBehaviors;
    }

    /**
     * Call this method to indicate a interaction on this component and to reset
     * the time of the last interaction.
     */
    public void resetInteractionTime() {
        this.timeOfLastInteractionInMillis = System.currentTimeMillis();

        // call also resetInteractionTime on parent group
        VisualGroup parentGroup = this.getParentVisualGroup();
        if (parentGroup != null) {
            parentGroup.resetInteractionTime();
        }

    }

    /**
     * Returns the view parent group of this visual component, or null there is
     * no view in the parent path.
     *
     * @return The parent group of this visual component, or null if not set.
     */
    public View getParentView() {
        VisualGroup visualGroup = this.parentGroup;
        while (visualGroup != null) {
            if (visualGroup instanceof View) {
                break;
            }
            visualGroup = visualGroup.getParentVisualGroup();
        }
        return (View) visualGroup;
    }

    /**
     * Returns the parent group of this visual component, or null if not set.
     *
     * @return The parent group of this visual component, or null if not set.
     */
    public VisualGroup getParentVisualGroup() {
        return this.parentGroup;
    }

    /**
     * Sets the parent visual group of this visual component.
     *
     * @param parentGroup
     */
    public void setParentVisualGroup(VisualGroup parentGroup) {
        if (this.parentGroup == parentGroup) {
            // nothing to do
            return;
        }

        if (this.parentGroup != null) {
            // remove from previous group
            this.parentGroup.removeVisualComponent(this);
        }

        // set group
        this.parentGroup = parentGroup;

        // may change the z index, so update
        this.updateZIndex();
    }

    /**
     * Updates the z index
     */
    protected void updateZIndex() {
        if (this.parentGroup != null) {
            // let parent update the z index
            this.parentGroup.updateZIndex();
        } else {
            // start with 0
            this.calculateZIndex(0);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communityinteraction.interfaces.PositionedEventConsumer
     * #getZIndex()
     */
    @Override
    public int getZIndex() {
        if (this.zIndex < 0) {
            // never calculated before
            this.updateZIndex();
        }

        return this.zIndex;
    }

    /**
     * Calculates the z index of this component that will be higher than the
     * given highest z index.
     *
     * @param highestZIndex
     *            Currently highest z index
     * @return The new highest z index
     */
    protected int calculateZIndex(int highestZIndex) {
        // simple component so just increase by one
        this.setZIndex(highestZIndex + 1);

        // return the new z index
        return this.getZIndex();
    }

    /**
     * Set the z index to the given value. The z index cannot be manipulated
     * using this method but only by adding the component to groups or by moving
     * the component in a group.
     *
     * @param zIndex
     *            New z index
     */
    private void setZIndex(int zIndex) {
        this.zIndex = zIndex;
    }

    // TODO code below in not framework philosophy, remove as soon as possible

    /**
     * Adds a visual component listener to the list of visual component
     * listeners of this visual component.
     *
     * @param visualComponentListener
     *            : the visual component listener that should added to the list
     *            of visual component listeners of this visual component
     * @author Eva Loesch
     */
    public void addVisualComponentListener(VisualComponentListener visualComponentListener) {
        this.visualComponentListeners.add(visualComponentListener);
    }

    /**
     * Removes a visual component listener from the list of visual component
     * listeners of this visual component.
     *
     * @param visualComponentListener
     *            : the visual component listener that should be removed from
     *            the list of visual component listeners of this visual
     *            component
     * @author Eva Loesch
     */
    public void removeVisualComponentListener(VisualComponentListener visualComponentListener) {
        this.visualComponentListeners.remove(visualComponentListener);
    }

    /**
     * Notifies all visual component listeners about an event on this visual
     * component.
     *
     * @param visualComponentEvent
     *            : the visual component event the listeners should be notified
     *            about
     * @author Eva Loesch
     */
    public synchronized void notifyListeners(VisualComponentEvent event) {
        List<VisualComponentListener> visualComponentListenersCopy = new LinkedList<>();
        for (VisualComponentListener listener : this.visualComponentListeners) {
            visualComponentListenersCopy.add(listener);
        }

        // call right method only on listeners that exist when this method is
        // called and not on listeners that are created by calling onTouchTapped
        for (VisualComponentListener listener : visualComponentListenersCopy) {
            CIEvent sourceEvent = event.getSourceEvent();
            listener.onObservedVisualComponentEvent(this, sourceEvent);
        }
    }

    /**
     * Removes this visual component from its parent group and adds a given
     * component to it.
     *
     * @param componentToAdd
     *            The component to add.
     *
     * @author Eva Loesch
     */
    public void substituteVisualComponent(VisualComponent componentToAdd) {
        // get parent group of this visual component
        VisualGroup parentVisualGroup = this.getParentVisualGroup();
        if (parentVisualGroup == null) {
            return;
        }
        // remove this visual component form its parent group
        parentVisualGroup.removeVisualComponent(this, true);
        // add the visual component that should be added to the parent group
        parentVisualGroup.addVisualComponent(componentToAdd, true);
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * Configure visual component using the given configuration and
     * configuration prefix.
     *
     * This method can be overwritten if a particular visual component wants to
     * use this possibility. Currently, it is only used / implemented when
     * sticky components are created.
     *
     * @author Michael Koch
     */
    public void configure(Configuration configuration, String confPrefix) {
        // NOP on default
    }

}
