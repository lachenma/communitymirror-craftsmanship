package org.sociotech.communitymirror.visualitems.factory;

import java.util.HashMap;
import java.util.Map;

import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.visualitems.VisualItem;
import org.sociotech.communitymirror.visualitems.renderer.VisualItemRenderer;
import org.sociotech.communitymirror.visualstates.VisualState;

/**
 * Generic visual item factory that simply takes renderer for different visual states to render
 * the visual items.
 *  
 * @author Peter Lachenmaier
 *
 */
public class GenericItemFactory<T extends Item, V extends VisualItem<T>> extends VisualItemFactory<T, V> {

	/**
	 * The default renderer.
	 */
	VisualItemRenderer<T, V> defaultRenderer = null;
	
	/**
	 * Map containing the different renderer for specific states.
	 */
	protected Map<VisualState, VisualItemRenderer<T,V>> stateRendererMapping = new HashMap<VisualState, VisualItemRenderer<T,V>>();
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.factory.VisualItemFactory#getRenderer(org.sociotech.communitymashup.data.Item, org.sociotech.communitymirror.visualstates.VisualState)
	 */
	@Override
	protected VisualItemRenderer<T, V> getRenderer(T item, VisualState state) {
		return stateRendererMapping.get(state);
	}
	
	/**
	 * Sets the renderer for the given state. Provide null to remove the renderer for the state. 
	 *
	 * @param renderer Renderer to set for the given state.
	 * @param state State to set the renderer for.
	 */
	public void setRendererForVisualState(VisualItemRenderer<T, V> renderer, VisualState state) {
		if(state == null) {
			return;
		}
		if(renderer == null) {
			// remove renderer for state.
			stateRendererMapping.remove(state);
		}
		// set renderer for state
		stateRendererMapping.put(state, renderer);
	}
}
