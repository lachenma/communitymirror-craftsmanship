
package org.sociotech.communitymirror.visualitems.factory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.configuration.ThemeResourceLoader;
import org.sociotech.communitymirror.visualitems.VisualItem;
import org.sociotech.communitymirror.visualitems.renderer.VisualItemRenderer;
import org.sociotech.communitymirror.visualstates.VisualState;

/**
 * The abstract superclass of all visual item factories.
 *
 * @author Peter Lachenmaier
 *
 * @param <T>
 *            The type of visual items that can be produced by the concrete
 *            Implementation of this item factory.
 */
public abstract class VisualItemFactory<T extends Item, V extends VisualItem<T>> {

    /**
     * The default renderer if no other configured renderer is available
     */
    private VisualItemRenderer<T, V> defaultRenderer = null;

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(VisualItemFactory.class.getName());

    /**
     * Sets the default renderer to the given one.
     *
     * @param defaultRenderer
     *            The default renderer to be used if no other renderer matches.
     */
    public void setDefaultRenderer(VisualItemRenderer<T, V> defaultRenderer) {
        this.defaultRenderer = defaultRenderer;
    }

    /**
     * Produces a visual item from the given data item in the given state.
     *
     * @param item
     *            The data item to produce the visual item for.
     * @param state
     *            The visual state for the item to be produce.
     * @return The produced visual item.
     */
    public V produceVisualItem(T item, VisualState state, ThemeResourceLoader themeResourceLoader) {
        VisualItemRenderer<T, V> renderer = this.getRenderer(item, state);

        if (renderer == null) {
            // fallback to default renderer
            renderer = this.defaultRenderer;
        }
        if (renderer == null) {
            // no renderer defined
            return null;
        }

        // Render VisualItem
        V visualItem = renderer.renderItem(item, state);

        // TODO look for a better place to init
        // Init VisualItem
        if (visualItem != null) {
            visualItem.setThemeResourceLoader(themeResourceLoader);
            visualItem.init();
        } else {
            this.logger.debug("visual item could not be produced - renderer=" + renderer.getClass().getName());
        }

        return visualItem;
    }

    // TODO: We need asynchronous production methods to avoid ui lacks when
    // production needs a lot of time (e.g. for loading images from internet)

    /**
     * Returns the renderer that will be used to render a visual item for the
     * given visual state.
     *
     * @param item
     *            Item to render
     * @param state
     *            VisualState
     *
     * @return The renderer for the given visual state and item.
     */
    protected abstract VisualItemRenderer<T, V> getRenderer(T item, VisualState state);
}
