package org.sociotech.communitymirror.visualitems.components.map;

import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

import org.sociotech.communitymirror.visualitems.VisualComponent;

import com.google.common.io.Resources;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

// the mapcomponent that finally renders the fxml file to give the icon its look

public class MapVisualComponent extends VisualComponent {

	private final Logger logger = LogManager.getLogger(MapVisualComponent.class
			.getName());

	public MapVisualComponent() {

	};

	/*
	 * (non-Javadoc)
	 *
	 * @see org.sociotech.communitymirror.visualitems.VisualGroup#onInit()
	 */
	@Override
	protected void onInit() {
		super.onInit();
		render();

	}

	protected void render() {
		try {
			// Get FXML for map item
			FXMLLoader loader = new FXMLLoader();
			URL location;
			String fxmlPath = "fxml/inPreview/map.fxml";
	        if(this.themeResources != null) {
	        	location = this.themeResources.getResourceURL(fxmlPath);
	        } else {
	        	location = Resources.getResource("flow/" + fxmlPath);
	        	logger.warn(this.getClass().getName() + " using deprecated FXML path.");
	        }
			loader.setLocation(location);
			AnchorPane mapPane = (AnchorPane) loader
					.load(location.openStream());

			this.addNode(mapPane);

			// add Node to Scene
			// prevent ConcurrentModificationException
			int size = mapPane.getChildren().size();
			for (int i = 0; i < size; i++) {
				addNode(mapPane.getChildren().get(0));

			}
			// addNode(comment);

		} catch (IOException e) {

			logger.error("Problems loading fxml-file for MapItem"
					+ e.getStackTrace());
		}

	}

}