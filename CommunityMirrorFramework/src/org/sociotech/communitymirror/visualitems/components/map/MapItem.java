package org.sociotech.communitymirror.visualitems.components.map;

import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymashup.data.MetaTag;
import org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem;

import com.google.common.io.Resources;

// OLD!! First Version of Mapitem that is rendered by the FlowMEtaTagRenderer

public class MapItem extends FlowVisualItem<MetaTag, MapItemController> {

	/**
	 * The controller of this time item
	 */
	@SuppressWarnings("unused")
	private MapItemController controller;

	/**
	 * The log4j logger reference.
	 */
	private final Logger logger = LogManager.getLogger(MapItem.class.getName());

	/**
	 * The data item representing the metatag
	 */
	@SuppressWarnings("unused")
	private MetaTag dataItem;

	public MapItem(MetaTag dataItem) {
		super(dataItem);
		this.dataItem = dataItem;
	}


	@Override
	protected void onInit() {
		render();

		super.onInit();
	}

	protected void render() {
		try {
			// Get FXML for map item
			FXMLLoader loader = new FXMLLoader();
			URL location;
			String fxmlPath = "fxml/inPreview/map.fxml";
	        if(this.themeResources != null) {
	        	location = this.themeResources.getResourceURL(fxmlPath);
	        } else {
	        	location = Resources.getResource("flow/" + fxmlPath);
	        	logger.warn(this.getClass().getName() + " using deprecated FXML path.");
	        }
			loader.setLocation(location);
			AnchorPane mapPane = (AnchorPane) loader
					.load(location.openStream());
			controller = loader.getController();

			// add Node to Scene
			// prevent ConcurrentModificationException
			int size = mapPane.getChildren().size();
			for (int i = 0; i < size; i++) {
				addNode(mapPane.getChildren().get(0));
			}
			// addNode(comment);

		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Problems loading fxml-file for MapItem"
					+ e.getStackTrace());
		}

	}

}
