/*
 * Copyright (c) 2014. Martin Burkhard, CSCM Cooperation Systems Center Munich.
 * Institute for Software Technology at Universitaet der Bundeswehr Muenchen.
 *
 * This part of the program is made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.sociotech.communitymirror.visualitems.components;

import java.awt.Toolkit;

import org.sociotech.communitymirror.visualitems.VisualComponent;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;

/**
 * Created by i21bmabu on 17.02.14.
 */
public class BackgroundImageComponent extends VisualComponent {

    public enum BackgroundImageMode {
        NONE, STRETCH, COVER, CONTAIN
    }

    private final String imageUrl;
    private final double sWidth;
    private final double sHeight;
    private final BackgroundImageMode mode;

    @Deprecated
    public BackgroundImageComponent(String imageUrl) {
        this.imageUrl = imageUrl;
        // The following method to determine the screen size returns raw pixels,
        // thus nonstandard DPI settings (e.g. on HiDPI displays) may cause
        // incorrect background sizing. If at all possible, avoid using this
        // constructor.
        this.sWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
        this.sHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
        this.mode = BackgroundImageMode.STRETCH;
    }

    public BackgroundImageComponent(String imageUrl, double scene_width, double scene_height,
            BackgroundImageMode mode) {
        this.imageUrl = imageUrl;
        this.sWidth = scene_width;
        this.sHeight = scene_height;
        this.mode = mode;
    }

    @Override
    protected void onInit() {
        // Create background image
        Image background = null;
        if (this.mode == BackgroundImageMode.CONTAIN || this.mode == BackgroundImageMode.COVER) {
            Image background_raw = new Image(this.imageUrl);
            double rawWidth = background_raw.getWidth();
            double rawHeight = background_raw.getHeight();
            double scaleFactor;
            if (this.mode == BackgroundImageMode.CONTAIN) {
                scaleFactor = Math.min(this.sWidth / rawWidth, this.sHeight / rawHeight);
            } else { // COVER
                scaleFactor = Math.max(this.sWidth / rawWidth, this.sHeight / rawHeight);
            }
            Image background_scaled = new Image(this.imageUrl, scaleFactor * rawWidth, scaleFactor * rawHeight, true,
                    true);
            WritableImage background_new = new WritableImage((int) Math.round(this.sWidth),
                    (int) Math.round(this.sHeight));
            int xOffset = (int) Math.round((background_new.getWidth() - background_scaled.getWidth()) / 2);
            int yOffset = (int) Math.round((background_new.getHeight() - background_scaled.getHeight()) / 2);
            if (this.mode == BackgroundImageMode.CONTAIN) {
                background_new.getPixelWriter().setPixels(xOffset, yOffset,
                        (int) Math.round(background_scaled.getWidth()), (int) Math.round(background_scaled.getHeight()),
                        background_scaled.getPixelReader(), 0, 0);
                background = background_new;
            } else { // COVER
                try {
                    background_new.getPixelWriter().setPixels(0, 0, (int) Math.round(background_new.getWidth()),
                            (int) Math.round(background_new.getHeight()), background_scaled.getPixelReader(), -xOffset,
                            -yOffset);
                    background = background_new;
                } catch (Exception e) {
                    // java.lang.IndexOutOfBoundsException is raised if screen
                    // size is too small (if perspective resize resulted in a
                    // still too large image) ...
                    background = new Image(this.imageUrl, this.sWidth, this.sHeight, false, true);
                }
            }
        } else if (this.mode == BackgroundImageMode.STRETCH) {
            // do not preserve ratio of source image, do use smooth scaling
            background = new Image(this.imageUrl, this.sWidth, this.sHeight, false, true);
        } else if (this.mode == BackgroundImageMode.NONE) {
            // load source image as is, no resizing at all
            background = new Image(this.imageUrl);
        }

        // Create background image view
        ImageView ivBackground = new ImageView(background);

        // Add background image to your JavaFX scene Graph
        this.addNode(ivBackground);
    }
}
