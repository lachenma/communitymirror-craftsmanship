
package org.sociotech.communitymirror.visualitems.components;

import org.sociotech.communitymirror.visualitems.VisualComponent;

import com.google.common.io.Resources;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class MapVisualComponent extends VisualComponent {

    private WebView mapWebView;
    protected boolean mapWebViewAvailable;

    private double width;
    private double height;

    private double latitude;
    private double longitude;
    private int zoom;

    public MapVisualComponent() {
        this.mapWebViewAvailable = false;

        this.latitude = 11.6333;
        this.longitude = 48.0812;
        this.zoom = 16;

        this.width = 512;
        this.height = 512;
    };

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.visualitems.VisualGroup#onInit()
     */
    @Override
    protected void onInit() {
        super.onInit();
        this.render();
    }

    protected void setSize(double width, double height) {
        this.width = width;
        this.height = height;
        if (this.mapWebViewAvailable) {
            this.mapWebView.setPrefWidth(width);
            this.mapWebView.setPrefHeight(height);
        }
    }

    protected void refresh() {
        if (this.mapWebViewAvailable) {
            this.mapWebView.setPrefWidth(this.width);
            this.mapWebView.setPrefHeight(this.height);
            WebEngine webEngine = this.mapWebView.getEngine();
            String script = "jumpTo(" + this.latitude + ", " + this.longitude + ", " + this.zoom + ")";
            webEngine.executeScript(script);
        }
    }

    public void jumpTo(double latitude, double longitude, int zoom) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.zoom = zoom;
    }

    protected void render() {
        this.mapWebView = new WebView();
        WebEngine webEngine = this.mapWebView.getEngine();
        webEngine.load(Resources.getResource("map/osm.html").toExternalForm());

        class MyStateListener implements ChangeListener<State> {
            @Override
            public void changed(ObservableValue<? extends State> paramObservableValue, State from, State to) {
                if (to == State.SUCCEEDED) {
                    MapVisualComponent.this.mapWebViewAvailable = true;
                    MapVisualComponent.this.refresh();
                }
            }
        }

        webEngine.getLoadWorker().stateProperty().addListener(new MyStateListener());

        this.addNode(this.mapWebView);
    }

}
