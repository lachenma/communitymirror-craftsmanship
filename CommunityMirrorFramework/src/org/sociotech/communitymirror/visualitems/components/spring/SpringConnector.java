package org.sociotech.communitymirror.visualitems.components.spring;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Node;

/**
 * A component to add a spring connector coupled to the layout position of its container.
 * 
 * @author Peter Lachenmaier
 */
public class SpringConnector implements SpringConnectable {

	/**
	 * The container this spring connector is bound to. 
	 */
	protected Node container;
	
	/**
	 * The x position of the spring connector
	 */
	protected DoubleProperty xProperty;
	
	/**
	 * The y position of the spring connector
	 */
	protected DoubleProperty yProperty;

	/**
	 * Synchronization variable for updates to avoid infinite loops. 
	 */
	private boolean inUpdate = false;

	/**
	 * Listener for changes of the origin position
	 */
	protected ChangeListener<Number> originChangeListener;
	
	/**
	 * Listener for changes of the spring connector position
	 */
	private ChangeListener<Number> connectorChangeListener;
	
	/**
	 * Offset in x direction
	 */
	private double offsetX;

	/**
	 * Offset in y direction
	 */
	private double offsetY;
	
	/**
	 * Creates a spring connector bound to the given container with the given
	 * offset to the layout position of the container.
	 * 
	 * @param container Container to bind the spring connector to.
	 * @param layoutXOffset X offset to the layout position of the container.
	 * @param layoutYOffset Y offset to the layout position of the container.
	 */
	public SpringConnector(final Node container, double layoutXOffset, double layoutYOffset) {
		
		this.offsetX = layoutXOffset;
		this.offsetY = layoutYOffset;
		
		this.container = container;
		
		// create position properties
		xProperty = new SimpleDoubleProperty();
		yProperty = new SimpleDoubleProperty();
		
		// set initial values
		propagateOriginPositionChange();
		
		// bind local properties with offset to layout
		
		// create a listener for position changes of the origin / bound position
		originChangeListener =  new ChangeListener<Number>() {

				@Override
				public void changed(ObservableValue<? extends Number> arg0,
						Number oldVal, Number newVal) {
					if(inUpdate) {
						// do nothing
						return;
					}
					
					// indicate update to avoid infinite loops
					inUpdate = true;
					try {
						propagateOriginPositionChange();
					} catch (Exception e) {
						// do nothing
					} finally {
						inUpdate = false;
					}
				}
	    	};
	    	
		// add the listener to x and y position of the origin
	    getPositionBindXProperty().addListener(originChangeListener);
		getPositionBindYProperty().addListener(originChangeListener);
		container.rotateProperty().addListener(originChangeListener);
		container.scaleXProperty().addListener(originChangeListener);
		container.scaleYProperty().addListener(originChangeListener);
		container.scaleZProperty().addListener(originChangeListener);
		
		// create a listener for position changes of the spring connector
		connectorChangeListener =  new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> changedProperty,
					Number oldVal, Number newVal) {
				if(inUpdate) {
					// do nothing
					return;
				}
				
				// indicate update to avoid infinite loops
				inUpdate = true;
				try {
					// will be called for x and y separately
					DoubleProperty prop;
					if(changedProperty == xProperty) {
						prop = getPositionBindXProperty();
					}
					else if(changedProperty == yProperty) {
						prop = getPositionBindYProperty();
					}
					else {
						// do nothing
						return;
					}
					
					// propagate change difference
					prop.set(prop.get() - (oldVal.doubleValue() - newVal.doubleValue())); 
					
				} catch (Exception e) {
					// do nothing
				} finally {
					inUpdate = false;
				}
			}
    	};
    	
    	// and add it to the x and y position (simple point, so no rotation or zoom)
    	xProperty.addListener(connectorChangeListener);
    	yProperty.addListener(connectorChangeListener);
	}
	
	/**
	 * Calculates and sets the position of the spring connector based on the origin position. 
	 */
	protected void propagateOriginPositionChange() {
		
		// connector is maintained in scene coordinates
		Point2D newP = container.localToScene(new Point2D(offsetX, offsetY));
		
		xProperty.set(newP.getX());
		yProperty.set(newP.getY());
	}
	
	/**
	 * Returns the x property this spring connector is bound to with offset.
	 * You can overwrite this method in subclasses to bind the spring connector
	 * to another property.
	 * 
	 * @return The x property this spring connector is bound to with offset.
	 */
	protected DoubleProperty getPositionBindXProperty() {
		return container.layoutXProperty();
	}
	
	/**
	 * Returns the < property this spring connector is bound to with offset.
	 * You can overwrite this method in subclasses to bind the spring connector
	 * to another property.
	 * 
	 * @return The y property this spring connector is bound to with offset.
	 */
	protected DoubleProperty getPositionBindYProperty() {
		return container.layoutYProperty();
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.components.spring.SpringConnectable#getSpringConnectorXProperty()
	 */
	@Override
	public DoubleProperty getSpringConnectorXProperty() {
		return xProperty;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.components.spring.SpringConnectable#getSpringConnectorYProperty()
	 */
	@Override
	public DoubleProperty getSpringConnectorYProperty() {
		return yProperty;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.components.spring.SpringConnectable#isFixed()
	 */
	@Override
	public boolean isFixed() {
		return false;
	}

	/**
	 * Destroys this spring connector and releases all of its bindings.
	 */
	public void destroy() {
		// remove listeners from spring connector position
    	xProperty.removeListener(connectorChangeListener);
    	yProperty.removeListener(connectorChangeListener);
    	
    	// remove listeners form origin position
	    getPositionBindXProperty().removeListener(originChangeListener);
		getPositionBindYProperty().removeListener(originChangeListener);
		container.rotateProperty().removeListener(originChangeListener);
		container.scaleXProperty().removeListener(originChangeListener);
		container.scaleYProperty().removeListener(originChangeListener);
		container.scaleZProperty().removeListener(originChangeListener);
		
	}
	
	/**
	 * Creates a spring connector on the left center of the given node.
	 * 
	 * @param container Node to create the spring connector for.
	 * @return The new spring connector.
	 */
	public static SpringConnector createLeftCenterSpringConnector(Node container) {
		Bounds containerBounds = container.getLayoutBounds();
		double xOffset = containerBounds.getMinX();
		double yOffset = (containerBounds.getMinY() + containerBounds.getMaxY()) / 2.0;
		
		return new SpringConnector(container, xOffset, yOffset);
	}
	
	/**
	 * Creates a spring connector on the right center of the given node.
	 * 
	 * @param container Node to create the spring connector for.
	 * @return The new spring connector.
	 */
	public static SpringConnector createRightCenterSpringConnector(Node container) {
		Bounds containerBounds = container.getLayoutBounds();
		double xOffset = containerBounds.getMaxX();
		double yOffset = (containerBounds.getMinY() + containerBounds.getMaxY()) / 2.0;
		
		return new SpringConnector(container, xOffset, yOffset);
	}
	
	/**
	 * Creates a spring connector on the top center of the given node.
	 * 
	 * @param container Node to create the spring connector for.
	 * @return The new spring connector.
	 */
	public static SpringConnector createTopCenterSpringConnector(Node container) {
		Bounds containerBounds = container.getLayoutBounds();
		double xOffset = (containerBounds.getMinX() + containerBounds.getMaxX()) / 2.0;
		double yOffset = containerBounds.getMinY();
		
		return new SpringConnector(container, xOffset, yOffset);
	}
	
	/**
	 * Creates a spring connector on the bottom center of the given node.
	 * 
	 * @param container Node to create the spring connector for.
	 * @return The new spring connector.
	 */
	public static SpringConnector createBottomCenterSpringConnector(Node container) {
		Bounds containerBounds = container.getLayoutBounds();
		double xOffset = (containerBounds.getMinX() + containerBounds.getMaxX()) / 2.0;
		double yOffset = containerBounds.getMaxY();
		
		return new SpringConnector(container, xOffset, yOffset);
	}
	
	/**
	 * Creates a spring connector on in the center of the node
	 * 
	 * @param container Node to create the spring connector for.
	 * @return The new spring connector.
	 */
	public static SpringConnector createCenterSpringConnector(Node container) {
		Bounds containerBounds = container.getLayoutBounds();
		double xOffset = (containerBounds.getMinX() + containerBounds.getMaxX()) / 2.0;
		double yOffset = (containerBounds.getMinY() + containerBounds.getMaxY()) / 2.0;
		
		return new SpringConnector(container, xOffset, yOffset);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.components.spring.SpringConnectable#getWeight()
	 */
	@Override
	public double getWeight() {
		if(container instanceof SpringConnectable) {
			return ((SpringConnectable) container).getWeight();
		}
		return 1.0;
	}
}
