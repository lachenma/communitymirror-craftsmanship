package org.sociotech.communitymirror.visualitems.components.spring;

import com.facebook.rebound.SpringLooper;

/**
 * Nearly empty spring looper implementation to be able to start rebound.
 * 
 * @author Peter Lachenmaier
 */
public class BasicSpringLooper extends SpringLooper{

	@Override
	public void start() {
		// nothing to do
	}

	@Override
	public void stop() {
		// nothing to do
	}

}
