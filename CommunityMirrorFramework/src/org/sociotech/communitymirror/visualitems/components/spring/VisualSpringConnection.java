package org.sociotech.communitymirror.visualitems.components.spring;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

import org.sociotech.communitymirror.visualitems.VisualComponent;

import com.facebook.rebound.BaseSpringSystem;
import com.facebook.rebound.SimpleSpringListener;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringConfig;

/**
 * A visual spring based connection between two components.
 *
 * @author Peter Lachenmaier
 */
public class VisualSpringConnection extends VisualComponent {

	/**
	 * The visual line between the two components
	 */
	private Line line;

	/**
	 * The start component
	 */
	private SpringConnectable startC;

	/**
	 * The end component
	 */
	private SpringConnectable endC;

	/**
	 * Reference to the spring system to use for spring creation
	 */
	private BaseSpringSystem springSystem;

	/**
	 * The rebound spring
	 */
	private Spring spring;

	/**
	 * Reference to the configuration of the spring
	 */
	private SpringConfig springConfiguration;

	/**
	 * Real length of the spring, determined at the beginning. It will always be
	 * tried to reach this length by applying position changes to the two components.
	 */
	private double realLength;

	/**
	 * Whether if there is currently an update of the spring. Used to avoid infinite loops
	 * with springs influencing themselves.
	 */
	private boolean inSpringUpdate = false;

	/**
	 * Hidden component on the original layout position of the end component.
	 */
	private HiddenSpringConnectable hiddenLayoutComponent;

	/**
	 * Spring to maintain the hidden layout.
	 */
	private VisualSpringConnection hiddenLayoutSpring;

	/**
	 * True if layout should be maintained.
	 */
	private boolean maintainLayout;

	/**
	 * True if the position of the start component has changed and a layout update is needed.
	 */
	private boolean needLayout = false;

	/**
	 * The difference in x direction between start and end component.
	 */
	private double layoutXDiff;

	/**
	 * The difference in y direction between start and end component.
	 */
	private double layoutYDiff;

	/**
	 * The listener to observe layout violations.
	 */
	private ChangeListener<Number> layoutChangeListener;

	/**
	 * The listener to observe position changes of connected components.
	 */
	private ChangeListener<Number> positionChangeListener;

	/**
	 * If set to true, spring forces are deactivated
	 */
	private boolean deactivated = false;

	/**
	 * Whether spring forces are deactivated or not.
	 *
	 * @return True if the forces of this spring are deactivated.
	 */
	public boolean isDeactivated() {
		return deactivated;
	}

	/**
	 * Deactivates the spring forces of this spring.
	 *
	 * @param deactivated True if spring forces should be deactivated.
	 */
	public void setDeactivated(boolean deactivated) {
		this.deactivated = deactivated;
		if(hiddenLayoutSpring != null) {
			// also deactivate layouting
			hiddenLayoutSpring.setDeactivated(deactivated);
		}

//		// TODO Debug
//		if(deactivated) line.setStroke(Color.RED);
//		else line.setStroke(Color.BLUE);
	}

	/**
	 * Creates a visual spring connection between the two given components with a
	 * spring created in the given spring system. If needed the layout (vector between
	 * the components) will be maintained automatically.
	 *
	 * @param component1 Start component
	 * @param component2 End component
	 * @param springSystem Spring system (needed for spring creation)
	 * @param maintainLayout True if layout should be maintained
	 */
	public VisualSpringConnection(SpringConnectable component1,
			SpringConnectable component2, BaseSpringSystem springSystem, boolean maintainLayout) {
		this.startC = component1;
		this.endC   = component2;
		this.springSystem = springSystem;
		this.maintainLayout = maintainLayout;
	}

	/**
	 * Replaces the end component of this spring connection.
	 *
	 * @param endComponent New end component
	 */
	public void replaceEnd(SpringConnectable endComponent) {
		if(endComponent == null || endComponent == endC) {
			// skip
			return;
		}

		// unbind old
		line.endXProperty().unbindBidirectional(endC.getSpringConnectorXProperty());
		line.endYProperty().unbindBidirectional(endC.getSpringConnectorYProperty());

		// replace reference
		endC = endComponent;

		// bind new
		line.endXProperty().bindBidirectional(endC.getSpringConnectorXProperty());
		line.endYProperty().bindBidirectional(endC.getSpringConnectorYProperty());

		if(hiddenLayoutSpring != null) {
			hiddenLayoutSpring.replaceEnd(endC);
		}
	}
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onInit()
	 */
	@Override
	protected void onInit() {
		// create line
		line = new Line();
		line.setStroke(Color.WHITE);
		line.setEffect(new GaussianBlur(3));
		line.setOpacity(0.8);

		// bind start and end position
		line.startXProperty().bindBidirectional(startC.getSpringConnectorXProperty());
		line.startYProperty().bindBidirectional(startC.getSpringConnectorYProperty());

		line.endXProperty().bindBidirectional(endC.getSpringConnectorXProperty());
		line.endYProperty().bindBidirectional(endC.getSpringConnectorYProperty());

		// add the line
		this.addNode(line);

		this.realLength = calculateSpringLength();

		// create spring
		spring = springSystem.createSpring();
	    springConfiguration = new SpringConfig(10, 8);
		spring.setSpringConfig(springConfiguration);

	    // Add a listener to observe the motion of the spring.
	    spring.addListener(new SimpleSpringListener() {

	      @Override
	      public void onSpringUpdate(Spring spring) {
	    	// correct position depending on spring
	    	updatePositions(calculateSpringLength() - realLength * spring.getCurrentValue());
	      }

	    });

	    if(maintainLayout) {
	    	// create fixed hidden component with a spring on the position of the end component
	    	// add small value to give spring room to work
	    	hiddenLayoutComponent = new HiddenSpringConnectable(endC.getSpringConnectorXProperty().get() + 0.01, endC.getSpringConnectorYProperty().get() + 0.01, true);

	    	// calc layout diff
	    	layoutXDiff = hiddenLayoutComponent.getSpringConnectorXProperty().get() - startC.getSpringConnectorXProperty().get();
	    	layoutYDiff = hiddenLayoutComponent.getSpringConnectorYProperty().get() - startC.getSpringConnectorYProperty().get();

	    	// create the spring to the hidden component
	    	hiddenLayoutSpring = VisualSpringConnection.connect(hiddenLayoutComponent, endC, springSystem);

	    	layoutChangeListener = new ChangeListener<Number>() {

				@Override
				public void changed(ObservableValue<? extends Number> arg0,
						Number oldVal, Number newVal) {
					// mark for layout update in next update step
					needLayout = true;
				}
	    	};

	    	// layout is needed if x or y changes
	    	line.startXProperty().addListener(layoutChangeListener);
	    	line.startYProperty().addListener(layoutChangeListener);
	    }

	    // listener for positon changes
	    positionChangeListener = new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> arg0,
					Number oldVal, Number newVal) {
				// update spring length on a position change
				updateSpringLength();
			}

		};

		// add listener to all position properties
		line.startXProperty().addListener(positionChangeListener);
		line.endXProperty().addListener(positionChangeListener);
		line.startYProperty().addListener(positionChangeListener);
		line.endYProperty().addListener(positionChangeListener);
	}

	/**
	 * Synchronized spring update to avoid endless loops of springs influencing themselves.
	 */
	protected synchronized void updateSpringLength() {
		if(inSpringUpdate) {
			// skip if already updating this spring
			return;
		}

		inSpringUpdate = true;
		try {
			spring.setCurrentValue(calculateSpringLength()/realLength);
			spring.setEndValue(1);
		} catch (Exception e) {
			// do nothing, will be updated on next change
		}
		finally {
			inSpringUpdate = false;
		}
	}

	/**
	 * Updates the position depending on the given difference and the
	 * current direction of the spring.
	 *
	 * @param diff Length difference of the line.
	 */
	private void updatePositions(double diff) {
		if(diff == 0.0 || Double.isNaN(diff) || deactivated) {
			return;
		}

		if(startC.isFixed() && endC.isFixed()) {
			// nothing can be changed
			return;
		}

		// calculate complete weight
		double completeWeight = startC.getWeight() + endC.getWeight();

		// multiply with end weight cause weight is counter proportional to weight
		double startDiff = (diff / completeWeight) * endC.getWeight();
		if(startC.isFixed()) {
			// do not change if fixed
			startDiff = 0.0;
		}

		double endDiff = diff - startDiff;
		if(endC.isFixed()) {
			// do not change if fixed
			endDiff = 0.0;
		}

		double dX = line.getStartX() - line.getEndX();
		double dY = line.getStartY() - line.getEndY();

		double lineLength = calculateSpringLength();
		double xPart = dX / lineLength;
		double yPart = dY / lineLength;

		// update positions
		// check on zero to avoid unnecessary changes and event storms
		double d = startDiff * xPart;
		if(d != 0) {
			line.setStartX(line.getStartX() - d);
		}

		d = startDiff * yPart;
		if(d != 0) {
			line.setStartY(line.getStartY() - d);
		}

		d = endDiff * xPart;
		if(d != 0) {
			line.setEndX(line.getEndX() + d);
		}

		d = endDiff * yPart;
		if(d != 0) {
			line.setEndY(line.getEndY() + d);
		}
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onUpdate(double, long)
	 */
	@Override
	protected void onUpdate(double framesPerSecond, long nanoTime) {
		// visualize the spring strength in the stroke width
		line.setStrokeWidth(Math.min(10.0, 4/(calculateSpringLength()/realLength)));

		// maintain layout if needed
		if(maintainLayout && needLayout && !deactivated ) {
			try {
				hiddenLayoutComponent.getSpringConnectorXProperty().set(startC.getSpringConnectorXProperty().get() + layoutXDiff);
				hiddenLayoutComponent.getSpringConnectorYProperty().set(startC.getSpringConnectorYProperty().get() + layoutYDiff);
			} catch(Exception e) {
				// do nothing -> try layout on next change again
			}
			finally {
				needLayout = false;
			}
		}
	}

	/**
	 * Creates a spring connection between the two given components without maintaining the layout.
	 *
	 * @param c1 Component 1
	 * @param c2 Component 2
	 * @param springSystem Spring system to use for the creation of springs.
	 *
	 * @return The newly created spring connection.
	 */
	public static VisualSpringConnection connect(SpringConnectable c1, SpringConnectable c2, BaseSpringSystem springSystem) {
		// create without maintaining the layout
		return connect(c1, c2, springSystem, false);
	}

	/**
	 * Creates a spring connection between the two given components.
	 *
	 * @param c1 Component 1
	 * @param c2 Component 2
	 * @param springSystem Spring system to use for the creation of springs.
	 * @param maintainLayout True to maintain the layout. Maintains position of c2 in reference to c1
	 *
	 * @return The newly created spring connection.
	 */
	public static VisualSpringConnection connect(SpringConnectable c1, SpringConnectable c2, BaseSpringSystem springSystem, boolean maintainLayout) {
		// create a spring connection
		VisualSpringConnection springConnection = new VisualSpringConnection(c1, c2, springSystem, maintainLayout);
		// init it
		springConnection.init();

		// and return it
		return springConnection;
	}
	/**
	 * Calculates the spring length based on the line length.
	 *
	 * @return The calculated spring length.
	 */
	protected double calculateSpringLength() {
		if(line.getStartX() == line.getEndX() && line.getStartY() == line.getEndY()) {
			return 0.0;
		}
		return Math.hypot(line.getStartX() - line.getEndX(), line.getStartY() - line.getEndY());
	}

	/**
	 * Releases the spring connection
	 */
	public void disconnect() {
		// disconnect possible added hidden springs
		if(hiddenLayoutSpring != null) {
			hiddenLayoutSpring.disconnect();
		}

		// disconnect position bindings
		line.startXProperty().unbindBidirectional(startC.getSpringConnectorXProperty());
		line.startYProperty().unbindBidirectional(startC.getSpringConnectorYProperty());
		line.endXProperty().unbindBidirectional(endC.getSpringConnectorXProperty());
		line.endYProperty().unbindBidirectional(endC.getSpringConnectorYProperty());

		// remove position listener
		line.startXProperty().removeListener(positionChangeListener);
		line.endXProperty().removeListener(positionChangeListener);
		line.startYProperty().removeListener(positionChangeListener);
		line.endYProperty().removeListener(positionChangeListener);

		// remove layout listener if set
		if(layoutChangeListener != null) {
			line.startXProperty().removeListener(layoutChangeListener);
	    	line.startYProperty().removeListener(layoutChangeListener);
		}

		// destroy the spring
		spring.destroy();

		// destroy the visual component
		super.destroy();
	}

	/**
	 * Returns the visual line between the two components.
	 *
	 * @return The line between the two components.
	 */
	public Line getLine() {
		return line;
	}

	/**
	 * Returns the real length (relaxed length) of the spring.
	 *
	 * @return Real length
	 */
	public double getRealLength() {
		return realLength;
	}

	/**
	 * Returns the hidden layout spring.
	 *
	 * @return The hidden layout spring
	 */
	public VisualSpringConnection getHiddenLayoutSpring() {
		return hiddenLayoutSpring;
	}

	/**
	 * Returns the spring configuration.
	 *
	 * @return The spring configuration.
	 */
	public SpringConfig getSpringConfiguration() {
		return springConfiguration;
	}

	/**
	 * Sets the configuration of the spring.
	 *
	 * @param springConfig Rebound spring configuration
	 */
	public void setSpringConfiguration(SpringConfig springConfig) {
		this.springConfiguration = springConfig;
		spring.setSpringConfig(springConfiguration);
	}

	/**
	 * Sets the new real length of the spring.
	 *
	 * @param newLength The new spring length
	 */
	public void setRealLength(double newLength) {
		if(newLength == this.realLength) {
			// skip
			return;
		}

		if(maintainLayout) {
			// update layout position
			double factor = newLength / this.realLength;
			layoutXDiff *= factor;
			layoutYDiff *= factor;
			// indicate change
			needLayout = true;
		}
		this.realLength = newLength;

		// update spring length to indicate change
		updateSpringLength();
	}

	/**
	 * Returns the spring connectable start connector.
	 *
	 */
	public SpringConnectable getStartC()
	{
		return this.startC;
	}

	/**
	 * Returns the spring connectable end connector.
	 *
	 */
	public SpringConnectable getEndC()
	{
		return this.endC;
	}


}
