/*
 * Copyright (c) 2014. Martin Burkhard, CSCM Cooperation Systems Center Munich.
 * Institute for Software Technology at Universitaet der Bundeswehr Muenchen.
 *
 * This part of the program is made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.sociotech.communitymirror.visualitems.components;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.image.ImageView;
import org.sociotech.communitymirror.visualitems.components.base.CMFToggleButton;

public class SyncedToggleButton extends CMFToggleButton {

    private SyncedToggleGroup m_syncedToggleGroup = null;

    public SyncedToggleButton(String title) {
        super(title);
        addSelectedListener();
    }

    protected SyncedToggleButton(String title, ImageView imageView) {
        super(title, imageView);
        addSelectedListener();
    }

    public void setSyncedToggleGroup(SyncedToggleGroup syncedToggleGroup) {
        m_syncedToggleGroup = syncedToggleGroup;
        syncedToggleGroup.add(this);
    }

    private void addSelectedListener() {
        getButtonBase().selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov, Boolean oldSelected, Boolean newSelected) {
                if (m_syncedToggleGroup != null) {
                    m_syncedToggleGroup.setSelected(newSelected);
                }
            }
        });
    }
}
