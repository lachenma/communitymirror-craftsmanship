package org.sociotech.communitymirror.visualitems.components.base;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.ButtonBase;
import javafx.scene.input.MouseEvent;

/**
 * Created with IntelliJ IDEA.
 * User: i21bmabu
 * Date: 31.10.13
 * Time: 09:41
 * To change this template use File | Settings | File Templates.
 */
public class CMFButton<T extends ButtonBase> extends BaseComponent {

    public CMFButton(T buttonBase) {
        setButtonBase(buttonBase);
    }

    public T getButtonBase() {
        return buttonBase;
    }

    public void setButtonBase(T buttonBase) {
        this.buttonBase = buttonBase;
    }

    private T buttonBase;

    public void setOnMousePressed(EventHandler<? super MouseEvent> eventHandler) {
        getButtonBase().setOnMousePressed(eventHandler);
    }

    // Set style
    public void setStyle(String buttonStyle) {
        getButtonBase().setStyle(buttonStyle);
    }

    public void setAlignment(Pos pos) {
        getButtonBase().setAlignment(pos);
    }

    public void setMaxWidth(double value) {
        getButtonBase().setMaxWidth(value);
    }

    public void setPrefSize(double width, double height) {
        getButtonBase().setPrefSize(width,height);
    }

    public void setDisable(boolean disable) {
        getButtonBase().setDisable(disable);
    }

    public String getText() {
        return getButtonBase().getText();
    }

    public void setText(String text) {
        getButtonBase().setText(text);
    }


}
