
package org.sociotech.communitymirror.visualitems;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.shape.Path;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;

/**
 * A visual group is a group of visual components. The visual group is a visual
 * component itself and therefore can be grouped hierarchically.
 *
 * @author Peter Lachenmaier, Martin Burkhard
 */
public class VisualGroup extends VisualComponent {

    /**
     * List containing all visual components of this group.
     */
    protected ObservableList<VisualComponent> visualComponentList;

    /**
     * Temporary list with visual component that will be removed after update.
     */
    protected List<VisualComponent> visualComponentRemoveList;

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger();

    /**
     * Indicates a change in the visual component list to update iteration copy.
     */
    private boolean componentListChanged = true;

    /**
     * List of all visual components for iteration in update loop. Will be
     * updated when visual component list has changed since last update.
     */
    private List<VisualComponent> visualComponentListCopy;

    /**
     * Initializes the new visual group.
     */
    public VisualGroup() {
        this.visualComponentList = FXCollections.observableArrayList();
        this.visualComponentRemoveList = new ArrayList<>();
    }

    /**
     * Adds the given visual component to this group but not as node to the java
     * fx component.
     *
     * @param visualComponent
     *            Component to add to this group.
     */
    public void addVisualComponent(VisualComponent visualComponent) {
        this.addVisualComponent(visualComponent, false);
    }

    /**
     * Adds the given visual component to this group and as node to the java fx
     * component when addNode is true.
     * .
     *
     * @param visualComponent
     *            Component to add to this group.
     * @param addNode
     *            Whether to add as java fx node or not.
     */
    public void addVisualComponent(VisualComponent visualComponent, boolean addNode) {

        // do not allow undefined visual components
        if (visualComponent == null) {
            return;
        }

        if (visualComponent.getParentVisualGroup() != null) {
            // remove from parent visual group if set before
            visualComponent.getParentVisualGroup().removeVisualComponent(visualComponent, addNode);
        }
        this.visualComponentList.add(visualComponent);

        visualComponent.setParentVisualGroup(this);
        this.componentListChanged = true;

        // Also add node
        if (addNode) {
            this.addNode(visualComponent);
        }

        // update the z index
        this.updateZIndex();
    }

    /**
     * Adds the given visual component to the head of this group and as node to
     * the java fx component when addNode is true.
     * .
     *
     * @param visualComponent
     *            Component to add to this group.
     * @param addNode
     *            Whether to add as java fx node or not.
     */
    public void addVisualComponentAtListHead(VisualComponent visualComponent, boolean addNode) {
        this.visualComponentList.add(0, visualComponent);

        visualComponent.setParentVisualGroup(this);
        this.componentListChanged = true;

        // Also add node
        if (addNode) {
            this.addNode(visualComponent);
        }

        // update the z index
        this.updateZIndex();
    }

    /**
     * Removes the given visual component from this group.
     *
     * @param visualComponent
     *            Component to remove.
     */
    public void removeVisualComponent(VisualComponent visualComponent) {

        this.removeVisualComponent(visualComponent, true);
    }

    /**
     * Removes the given visual component from this group.
     *
     * @param visualComponent
     *            Component to remove.
     * @param removeNode
     *            Whether to remove also java fx node or not
     */
    public void removeVisualComponent(VisualComponent visualComponent, boolean removeNode) {
        if (visualComponent == null) {
            // nothing to do
            return;
        }

        if (this.visualComponentList.contains(visualComponent)) {
            this.logger.debug("Adding visual component for removal.");
            this.visualComponentRemoveList.add(visualComponent);
        } else {
            this.logger.debug("Visual component is no child of visual component list: " + visualComponent);
        }

        // Also remove java fx node
        if (removeNode) {
            this.removeNode(visualComponent);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.visualitems.VisualComponent#
     * createPositionPath(boolean)
     */
    @Override
    public void createPositionPath(boolean createPath) {
        super.createPositionPath(createPath);
        for (VisualComponent visualComponent : this.visualComponentList) {
            visualComponent.createPositionPath(createPath);

            Path path = visualComponent.getPositionPath();
            if (path != null && createPath) {
                this.addNode(path);
                path.toFront();
            }
            if (path != null && !createPath) {
                // remove
                this.removeNode(path);
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.visualitems.VisualComponent#getConsumers(
     * org.sociotech.communityinteraction.events.CIEvent)
     */
    @Override
    public List<CIEventConsumer> getConsumers(CIEvent event) {

        // create new list for result
        List<CIEventConsumer> consumerList = CIListHelper.createConsumerList();

        List<CIEventConsumer> tmpConsumerList;
        // first: look in contained visual components
        for (VisualComponent visualComponent : this.visualComponentList) {

            // stop if event is consumed
            if (event.isConsumed()) {
                break;
            }

            try {
                tmpConsumerList = visualComponent.getConsumers(event);
            } catch (Exception e) {
                // continue with next visual component
                continue;
            }

            if (tmpConsumerList != null && !tmpConsumerList.isEmpty()) {
                // add all to result
                consumerList.addAll(tmpConsumerList);
            }
        }

        // second: mine
        // add all from super implementation
        consumerList.addAll(super.getConsumers(event));

        return consumerList;
    }

    /**
     * Returns all visual components contained in this group.
     *
     * @return A list of all visual components contained in this group.
     */
    public List<VisualComponent> getVisualComponents() {
        return this.visualComponentList;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.visualitems.VisualComponent#onInit()
     */
    @Override
    protected void onInit() {
        super.onInit();

        for (VisualComponent visualComponent : this.visualComponentList) {
            visualComponent.init();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.visualitems.VisualComponent#onUpdate(
     * double, long)
     */
    @Override
    protected void onUpdate(double framesPerSecond, long nanoTime) {
        super.onUpdate(framesPerSecond, nanoTime);

        if (this.componentListChanged) {
            // update copy
            this.visualComponentListCopy = new LinkedList<>(this.visualComponentList);
            this.componentListChanged = false;
        }

        // Update visual components
        for (VisualComponent visualComponent : this.visualComponentListCopy) {
            visualComponent.update(framesPerSecond, nanoTime);
        }

        // Remove visual components
        if (!this.visualComponentRemoveList.isEmpty()) {
            // remove all removable components
            this.visualComponentList.removeAll(this.visualComponentRemoveList);

            // Clear removal list
            this.visualComponentRemoveList.clear();
        }
    }

    @Override
    public void pause() {
        super.pause();
        for (VisualComponent visualComponent : this.visualComponentList) {
            visualComponent.pause();
        }
    }

    @Override
    public void resume() {
        super.resume();
        for (VisualComponent visualComponent : this.visualComponentList) {
            visualComponent.resume();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.visualitems.VisualComponent#onDestroy()
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        for (VisualComponent visualComponent : this.visualComponentList) {

            // Destroy visual component
            visualComponent.destroy();

            // Add visual component to removal list -> leads to remove after
            // update loop
            this.removeVisualComponent(visualComponent);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.visualitems.VisualComponent#isOnPosition(
     * double, double)
     */
    @Override
    public boolean isOnPosition(double sceneX, double sceneY) {
        if (super.isOnPosition(sceneX, sceneY)) {
            return true;
        }

        // check contained components
        for (VisualComponent visualComponent : this.visualComponentList) {
            if (visualComponent.isOnPosition(sceneX, sceneY)) {
                return true;
            }
        }

        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.visualitems.VisualComponent#isTarget(java.
     * lang.Object)
     */
    @Override
    public boolean isTarget(Object target) {
        if (super.isTarget(target)) {
            return true;
        }

        // check contained components
        for (VisualComponent visualComponent : this.visualComponentList) {
            if (visualComponent.isTarget(target)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the number of components contained in this visual group.
     *
     * @return The number of components contained in this visual group.
     */
    public int size() {
        return this.visualComponentList.size();
    }

    /**
     * Puts the given component on top position in this group.
     *
     * @param component
     *            Component that is contained in this group and should be put on
     *            top
     */
    protected void putToTop(VisualComponent component) {
        if (component == null || !this.visualComponentList.remove(component)) {
            // no element of this group
            return;
        }
        // add it at last position -> leads also to update of z index
        this.addVisualComponent(component, false);
        // put it on top of the java fx nodes
        component.toFront();
    }

    /**
     * Puts the given component on last position in this group.
     *
     * @param component
     *            Component that is contained in this group and should be put to
     *            back
     */
    protected void putToBack(VisualComponent component) {
        if (component == null || !this.visualComponentList.remove(component)) {
            // no element of this group
            return;
        }
        // add it at first position -> leads also to update of z index
        this.addVisualComponentAtListHead(component, false);
        // put it on top of the java fx nodes
        component.toBack();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.visualitems.VisualComponent#calculateZIndex
     * (int)
     */
    @Override
    protected int calculateZIndex(int highestZIndex) {
        highestZIndex = super.calculateZIndex(highestZIndex);
        if (this.visualComponentList != null) {
            // recursive calculation
            for (VisualComponent vc : this.visualComponentList) {
                highestZIndex = vc.calculateZIndex(highestZIndex);
            }
        }
        return highestZIndex;
    }
}
