
package org.sociotech.communitymirror.visualitems.fxml;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.ImageView;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * The general Item controller
 *
 * @author Andrea Nutsi
 *
 */
public abstract class FXMLController {

    /**
     * Binds a String content to a Text fxml element
     *
     * @param fxmlElement
     *            The fxml element where the content is bound to
     * @param content
     *            The content to be bound
     */
    protected void bindToFXMLElement(Text fxmlElement, String content) {
        // This test for null exists so that FXML elements can be optional. This
        // way, concrete FXMLControllers may attempt to assign a value to a
        // certain ID in the general case, but the FXML file (which can be
        // adjusted
        // per Theme) does not need to use it.
        if (fxmlElement != null) {
            fxmlElement.textProperty().bind(new SimpleStringProperty(content));
        }
    }

    /**
     * Binds an image property of an image view to an image property of an
     * ImageView fxml element
     *
     * @param fxmlImageView
     *            The fxml image view where the image property is bound to
     * @param imageViewToSetImageOf
     *            The image view which's image property should be bound
     */
    protected void bindToFXMLElement(ImageView fxmlImageView, ImageView imageViewToSetImageOf) {
        fxmlImageView.imageProperty().bind(imageViewToSetImageOf.imageProperty());
    }

    /**
     * Binds a String content up to a specified length to a Text fxml element
     *
     * @param fxmlElement
     *            The fxml element where the content is bound to
     * @param content
     *            The content to be bound
     * @param length
     *            The maximum length of the string content to be bound
     */
    protected void bindToFXMLElement(Text fxmlElement, String content, int length) {
        StringProperty contentProperty;
        if (content.length() < length) {
            contentProperty = new SimpleStringProperty(content);
        } else {
            contentProperty = new SimpleStringProperty(content.substring(0, length) + " ...");
        }
        fxmlElement.textProperty().bind(contentProperty);
    }

    /**
     * Truncates a string to the specified maximum length, including "..." at
     * the end
     *
     * @param text
     *            The line of text to be truncated
     * @param length
     *            Maximum length (in characters) of the truncated string
     *            including "..."
     * @param preferWordBoundaries
     *            If true, use a heuristic to truncate the text at a word
     *            boundary unless this would make it much shorter than the goal
     *            length
     */
    protected String truncateText(String text, int length, boolean preferWordBoundaries) {
        if (length < 0) {
            throw new IllegalArgumentException("Goal length cannot be < 0.");
        }
        if (text.length() <= length) {
            return text;
        }
        String suffix = "...";
        if (length <= suffix.length()) {
            return suffix.substring(0, length - 1);
        }
        String result = text.substring(0, length - suffix.length() - 1);
        int rightmostSpace = result.lastIndexOf(" ");
        if (rightmostSpace >= 0.75 * result.length()) {
            result = result.substring(0, rightmostSpace - 1);
        }
        result = result + suffix;

        return result;
    }

    /**
     * Resizes the font size of a text if the text is larger than the maximum
     * width
     *
     * @param fxmlElement
     *            The fxml element which content should be resized
     * @param maxWidth
     *            The maximum width of the text
     */
    protected void fitTextToMaximumWidth(Text fxmlElement, double maxWidth) {
        if (fxmlElement == null || fxmlElement.getBoundsInLocal() == null) {
            return;
        }
        double currentWidth = fxmlElement.getBoundsInLocal().getWidth();
        while (currentWidth > maxWidth) {
            Font font = fxmlElement.getFont();
            double size = font.getSize();
            if (size <= 0) {
                break;
            }
            String family = font.getFamily();
            Font newFont = new Font(family, size - 1);
            fxmlElement.setFont(newFont);
            currentWidth = fxmlElement.getBoundsInLocal().getWidth();
        }
    }

}
