package org.sociotech.communitymirror.visualitems.user;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.sociotech.communityinteraction.events.stateChange.StateChangeableComponent;
import org.sociotech.communitymirror.visualitems.user.activityLevel.ActivityLevel;
import org.sociotech.communitymirror.visualitems.user.activityLevel.UserActivityTracker;
import org.sociotech.communitymirror.visualitems.user.bodyCondition.BodyCondition;
import org.sociotech.communitymirror.visualitems.user.bodyCondition.BodyOrientation;
import org.sociotech.communitymirror.visualitems.user.bodyCondition.BodyPosition;
import org.sociotech.communitymirror.visualitems.user.interactionZone.InteractionZone;
import org.sociotech.communitymirror.visualitems.user.interactionZone.InteractionZoneFactory;
import org.sociotech.communitymirror.visualitems.user.userState.UserInFifthInteractionPhase;
import org.sociotech.communitymirror.visualitems.user.userState.UserInFirstInteractionPhase;
import org.sociotech.communitymirror.visualitems.user.userState.UserInForthInteractionPhase;
import org.sociotech.communitymirror.visualitems.user.userState.UserInSecondInteractionPhase;
import org.sociotech.communitymirror.visualitems.user.userState.UserInThirdInteractionPhase;
import org.sociotech.communitymirror.visualitems.user.userState.UserState;

/**
 * Class that represents a user within the physical interaction space around the
 * community mirror. Could be sensed by a Kinect.
 * 
 * @author evalosch
 *
 */
public class User extends StateChangeableComponent<UserState> {

	/**
	 * The identification number of this user.
	 */
	private int userId;

	/**
	 * The list of all user states this user can be in.
	 */
	private List<UserState> allPossibleUserStates;

	/**
	 * The current body condition of this user defined by body position and
	 * orientation.
	 */
	private BodyCondition bodyCondition;

	/**
	 * The user activity tracker, that tracks the recent movement of the user
	 * and calculates an activity level from that information.
	 */
	private UserActivityTracker userActivityTracker;

	/**
	 * The interaction zone this user is currently in.
	 */
	private InteractionZone currentInteractionZone;
	
	private boolean isTracked;

	public User(int userId, UserState currentUserState, BodyCondition bodyCondition, boolean isTracked,
			InteractionZone currentInteractionZone) {
		super(currentUserState);
		this.userId = userId;
		this.bodyCondition = bodyCondition;
		this.userActivityTracker = new UserActivityTracker();
		this.userActivityTracker.addBodyCondition(bodyCondition);
		this.isTracked = isTracked;
		this.currentInteractionZone = currentInteractionZone;
		this.createAllPossibleUserStates();
	}

	public User(int userId) {
		// this(userId, new UserInFifthInteractionPhase(), null, false); //pink
		// this(userId, new UserInForthInteractionPhase(), null, false); // blue
		// this(userId, new UserInThirdInteractionPhase(), null, false);
		// //yellow
		// this(userId, new UserInSecondInteractionPhase(), null, false); //
		// orange
		this(userId, null, null, false, InteractionZoneFactory.createUntrackedZone()); // green
		// set current user state to first user state in list
		this.setState(this.allPossibleUserStates.get(0));
	}

	/**
	 * creates all possible user states of this user and sets the reachable
	 * states for all user states
	 */
	private void createAllPossibleUserStates() {
		// create empty list for all possible states
		this.allPossibleUserStates = new LinkedList<UserState>();

		// create all possible states
		UserInFirstInteractionPhase first = new UserInFirstInteractionPhase();
		UserInSecondInteractionPhase second = new UserInSecondInteractionPhase();
		UserInThirdInteractionPhase third = new UserInThirdInteractionPhase();
		UserInForthInteractionPhase forth = new UserInForthInteractionPhase();
		UserInFifthInteractionPhase fifth = new UserInFifthInteractionPhase();

		// set reachable user states in all possible states
		first.setReachableUserStates(new LinkedList<UserState>(Arrays.asList(
				second, third, forth, fifth)));
//		first.setReachableUserStates(new LinkedList<UserState>());
		second.setReachableUserStates(new LinkedList<UserState>(Arrays.asList(
				third, first, first)));
		third.setReachableUserStates(new LinkedList<UserState>(Arrays.asList(
				forth, second, first)));
		forth.setReachableUserStates(new LinkedList<UserState>(Arrays.asList(
				fifth, third, first)));
		fifth.setReachableUserStates(new LinkedList<UserState>(Arrays.asList(
				first, first)));

		// add all possible states to list of all possible states of this user
		this.allPossibleUserStates.add(first);
		this.allPossibleUserStates.add(second);
		this.allPossibleUserStates.add(third);
		this.allPossibleUserStates.add(forth);
		this.allPossibleUserStates.add(fifth);
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @return the bodyCondition
	 */
	public BodyCondition getBodyCondition() {
		return bodyCondition;
	}

	/**
	 * Sets the a given body condition and updates the user activity tracker
	 * with the new body condition.
	 * 
	 * @return the bodyCondition
	 */
	public void setBodyCondition(BodyCondition bodyCondition) {
		// set the new body condition
		this.bodyCondition = bodyCondition;
		// adds the new body condition to the list of stored body conditions of
		// this user activity tracker
		this.getUserActivityTracker().addBodyCondition(bodyCondition);
	}

	/**
	 * @return the bodyPosition
	 */
	public BodyPosition getBodyPosition() {
		if (bodyCondition != null) {
			return bodyCondition.getBodyPosition();
		}
		return null;
	}

	/**
	 * @return the bodyOrientation
	 */
	public BodyOrientation getBodyOrientation() {
		return bodyCondition.getBodyOrientation();
	}

	/**
	 * @return the userActivityTracker
	 */
	public UserActivityTracker getUserActivityTracker() {
		return userActivityTracker;
	}

	/**
	 * @return the activityLevel
	 */
	public ActivityLevel getActivityLevel() {
		return this.getUserActivityTracker().getActivityLevel();
	}

	/**
	 * @return isTracked
	 */
	public synchronized boolean isTracked() {
		return this.isTracked;
	}

	/**
	 * @param isTracked
	 */
	public synchronized void setIsTracked(boolean isTracked) {
		this.isTracked = isTracked;
		this.setCurrentInteractionZone(InteractionZoneFactory.createUntrackedZone());
	}

	/**
	 * Returns the new state this user has reached, if there is a new reached
	 * state.
	 * 
	 * @return The new reached state if there is one or else null.
	 */
	public UserState getReachedUserState() {
		UserState currentUserState = this.getState();
		return currentUserState.getReachedUserState(this);
	}

	public InteractionZone getCurrentInteractionZone() {
		return currentInteractionZone;
	}

	public void setCurrentInteractionZone(
			InteractionZone currentInteractionZone) {
		this.currentInteractionZone = currentInteractionZone;
	}
}
