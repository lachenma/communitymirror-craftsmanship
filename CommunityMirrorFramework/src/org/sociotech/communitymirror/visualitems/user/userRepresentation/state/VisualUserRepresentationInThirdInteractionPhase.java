package org.sociotech.communitymirror.visualitems.user.userRepresentation.state;

import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentation;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.calculators.MirrorVisualUserRepresentationCalculatorBasedOnJointPositions;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.fxmlControllers.ThirdPhaseVisualUserRepresentationFXMLController;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.imageCreators.ThirdPhaseVisualUserRepresentationImageCreator;
import org.sociotech.communitymirror.visualitems.user.userState.InteractionPhase;

import com.google.common.io.Resources;

/**
 * State of a visual user representation that is represented like is expected
 * for users in the third interaction phase.
 * 
 * @author evalosch
 *
 */
public class VisualUserRepresentationInThirdInteractionPhase
		extends
		FXMLVisualUserRepresentationState<ThirdPhaseVisualUserRepresentationFXMLController> {

	public VisualUserRepresentationInThirdInteractionPhase(
			VisualUserRepresentation visualUserRepresentation) {
		super(
				InteractionPhase.ThirdInteractionPhase,
				Resources
						.getResource("userRepresentation/fxml/thirdInteractionPhase.fxml"),
				visualUserRepresentation, 
				//new HeadOnlyImageCreator(),
				new ThirdPhaseVisualUserRepresentationImageCreator(),
				//new AdjustedVisualUserRepresentationCalculator(),
				new MirrorVisualUserRepresentationCalculatorBasedOnJointPositions());
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.user.userRepresentation.state.FXMLVisualUserRepresentationState#setupFXMLController()
	 */
	@Override
	protected void setupFXMLController() {
		super.setupFXMLController();
		//this.fxmlController.setPersonImage(this.visualUserRepresentation.getRepresentationImage());
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communitymirror.visualitems.user.userRepresentation.state
	 * .VisualUserRepresentationState#fillListOfInteractionBehaviors()
	 */
	@Override
	protected void fillListOfInteractionBehaviors() {
		// TODO Auto-generated method stub

	}
}
