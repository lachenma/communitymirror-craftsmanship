package org.sociotech.communitymirror.visualitems.user.userRepresentation.calculators;

import org.sociotech.communitymirror.visualitems.user.User;
import org.sociotech.communitymirror.visualitems.user.bodyCondition.BodyPosition;

/**
 * A calculator for position and scale of visual user representations.
 * 
 * @author evalosch
 *
 */
public class AdjustedVisualUserRepresentationCalculator extends
VisualUserRepresentationCalculator{
	
	// determines if range x should be calculated in dependence of actual
	// position z of user
	private boolean useVariableRangeX = true;
	
	// field of view of kinect
	private double viewAngle = 43.5;

	// width and height of the projection area
	private int projectionAreaWidth;
	private int projectionAreaHeight;

	// x-pos and y-pos of left upper corner of the projection area on screen
	private int projectionAreaPosX = 0;
	private int projectionAreaPosY = 0;
	
	// length of trackable area in z direction
	private float rangeUserPositionZ = 2600;

	// smallest and biggest possible z-value of a user
	// in default depth range: between 0.8m and 4.0m (=> possible processing
	// z-values between 800 and 4000)
	// in near depth range: between 0.4m and 3.0m (=> possible processing
	// z-values between 400 an 3000)
	private float minUserPositionZ = 400;
	private float maxUserPositionZ = 3000;
	
	/**
	 * @param viewBounds
	 */
	public AdjustedVisualUserRepresentationCalculator(boolean useVariableRangeX){
		this.useVariableRangeX = useVariableRangeX;
		this.projectionAreaWidth = (int) viewBounds.getWidth();
		this.projectionAreaHeight = (int) viewBounds.getHeight();
	}
	
	/**
	 * @param viewBounds
	 */
	public AdjustedVisualUserRepresentationCalculator(){
		this(true);
	}

	/**
	 * Calculates the x position for the visual user representation.
	 * 
	 * @param user
	 *            The user of which the data is the basis for the calculation.
	 * @return The x position for the visual user representation.
	 */
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.user.userRepresentation.calculators.VisualUserRepresentationCalculator#calculatePositionX(org.sociotech.communitymirror.visualitems.user.User)
	 */
	@Override
	public double calculatePositionX(User user, double imageWidth) {
		BodyPosition bodyPosition = user.getBodyPosition();
		double positionX = 0;
		if (bodyPosition != null) {
			float userPositionX = bodyPosition.getPosX1();
			float userPositionZ = bodyPosition.getPosZ1();
			// calculate range x
			double rangeX = this.calculateRangeX(userPositionZ,
					this.useVariableRangeX);
			// calculate posX of user representation within projection area
			positionX = (userPositionX + rangeX / 2) / rangeX
					* projectionAreaWidth;
			// calculate posX of user representation on screen
			positionX = positionX + projectionAreaPosX;
			// calculate posX of left upper corner of user image
			positionX = positionX - bodyPosition.getPosXInUserImage();
		}
		return positionX;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.user.userRepresentation.calculators.VisualUserRepresentationCalculator#calculatePositionY(org.sociotech.communitymirror.visualitems.user.User)
	 */
	@Override
	public double calculatePositionY(User user, double imageHeight) {
		BodyPosition bodyPosition = user.getBodyPosition();
		double positionY = 0;
		// default implementation: use positionY of user body position
		if (bodyPosition != null) {
			float userPositionZ = bodyPosition.getPosZ1();
			// calculate posY of user representation within projection area
			positionY = projectionAreaHeight
					- (((userPositionZ - minUserPositionZ) / rangeUserPositionZ) * projectionAreaHeight);
			// calculate posY of user representation on screen
			positionY = positionY + projectionAreaPosY;
			// calculate posX of left upper corner of user image
			positionY = positionY - bodyPosition.getPosYInUserImage();
		}
		return positionY;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.user.userRepresentation.calculators.VisualUserRepresentationCalculator#calculateScale(org.sociotech.communitymirror.visualitems.user.User)
	 */
	public double calculateScale(User user) {
		return super.calculateScale(user);
	}
	
	/**
	 * Calculates the tracked range in x-direction for a given z-position of a
	 * user if useActualPosZ is set to true or else for the biggest possible
	 * z-position of a user.
	 * 
	 * Using the actual z-pos of the user for the calculation ensures that the
	 * whole projection area is reachable by the user independent of the actual
	 * z-position of the user.
	 * 
	 * @param userPositionZ
	 *            the given z-position of a user
	 * @param useActualPosZ
	 *            determines if the actual userPositionZ should be used for the
	 *            calculation
	 * @return the calculated tracked range in x-direction
	 */
	private double calculateRangeX(float userPositionZ, boolean useActualPosZ) {
		double hypthenuse = useActualPosZ ? userPositionZ : maxUserPositionZ;
		return hypthenuse * Math.sin(viewAngle / 2) * 2;
	}
}
