package org.sociotech.communitymirror.visualitems.user.userRepresentation.state;

import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentation;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.calculators.MirrorVisualUserRepresentationCalculatorBasedOnJointPositions;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.fxmlControllers.SecondPhaseVisualUserRepresentationFXMLController;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.imageCreators.SecondPhaseVisualUserRepresentationImageCreator;
import org.sociotech.communitymirror.visualitems.user.userState.InteractionPhase;

import com.google.common.io.Resources;

/**
 * State of a visual user representation that is represented like is expected
 * for users in the second interaction phase.
 * 
 * @author evalosch
 *
 */
public class VisualUserRepresentationInSecondInteractionPhase
		extends
		FXMLVisualUserRepresentationState<SecondPhaseVisualUserRepresentationFXMLController> {

	public VisualUserRepresentationInSecondInteractionPhase(
			VisualUserRepresentation visualUserRepresentation) {
		super(
				InteractionPhase.SecondInteractionPhase,
				Resources
						.getResource("userRepresentation/fxml/secondInteractionPhase.fxml"),
				visualUserRepresentation,
				//new HeadOnlyImageCreator(),
				new SecondPhaseVisualUserRepresentationImageCreator(),
				//new AdjustedVisualUserRepresentationCalculator(),
				new MirrorVisualUserRepresentationCalculatorBasedOnJointPositions());
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.user.userRepresentation.state.FXMLVisualUserRepresentationState#setupFXMLController()
	 */
	@Override
	protected void setupFXMLController() {
		super.setupFXMLController();
		//this.fxmlController.setPersonImage(this.visualUserRepresentation.getRepresentationImage());
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communitymirror.visualitems.user.userRepresentation.state
	 * .VisualUserRepresentationState#fillListOfInteractionBehaviors()
	 */
	@Override
	protected void fillListOfInteractionBehaviors() {
		// TODO Auto-generated method stub

	}

}
