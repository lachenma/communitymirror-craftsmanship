package org.sociotech.communitymirror.visualitems.user.userRepresentation;

import java.util.LinkedList;
import java.util.List;

import org.sociotech.communitymirror.visualitems.user.User;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentation;

/**
 * Factory that produces visual user representations.
 * 
 * @author evalosch
 *
 */
public class VisualUserRepresentationFactory {

	/**
	 * Creates a visual user representation for each user of a given list of
	 * users.
	 * 
	 * @param users
	 *            The list of users for which to create visual user
	 *            representations.
	 * @return The list of all created visual user representations.
	 */
	public static List<VisualUserRepresentation> createAllVisualUserRepresentationsForUsers(
			List<User> users) {
		// create an empty list for the visual user representations
		List<VisualUserRepresentation> visualUserRepresentations = new LinkedList<VisualUserRepresentation>();
		// for each user in the list of users
		for (User user : users) {
			// create a visual user representation
			VisualUserRepresentation visualUserRepresentation = new VisualUserRepresentation(
					user);
			// add the visual user representation to list
			visualUserRepresentations.add(visualUserRepresentation);
		}
		// return list of visual user representations
		return visualUserRepresentations;
	}
}
