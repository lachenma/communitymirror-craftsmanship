package org.sociotech.communitymirror.visualitems.user.userRepresentation.calculators;

import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.visualitems.user.User;

import javafx.geometry.Rectangle2D;

/**
 * Abstract super class of all calculators that calculate data for a visual user
 * representation according to the data of the user tat is represented by that
 * visual user representation.
 * 
 * @author evalosch
 *
 */
public abstract class VisualUserRepresentationCalculator {
	
	// bounds of the view in which user representations are displayed
	protected Rectangle2D viewBounds;
	
	/**
	 * @param viewBounds
	 */
	public VisualUserRepresentationCalculator(){
		this.viewBounds = CommunityMirror.getViewBounds();
	}
	
	/**
	 * Calculates the x position for the visual user representation.
	 * 
	 * @param user
	 *            The user of which the data is the basis for the calculation.
	 * @param imageWidth 
	 * 				The width of the user representation image before scale.
	 * @return The x position for the visual user representation.
	 */
	public double calculatePositionX(User user, double imageWidth) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Calculates the y position for the visual user representation.
	 * 
	 * @param user
	 *            The user of which the data is the basis for the calculation.
	 * @param imageHeight
	 * 			  The height of the user representation image before scale
	 * @return The y position for the visual user representation.
	 */
	public double calculatePositionY(User user, double imageHeight) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * Calculates the scale value for the visual user representation.
	 * 
	 * @param user
	 *            The user of which the data is the basis for the calculation.
	 * @return The scale value for the visual user representation.
	 */
	public double calculateScale(User user) {
		// default implementation: use 1.0
		return 1.0;
	}
}
