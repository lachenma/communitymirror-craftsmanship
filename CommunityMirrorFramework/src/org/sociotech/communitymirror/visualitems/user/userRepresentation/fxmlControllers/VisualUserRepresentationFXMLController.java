package org.sociotech.communitymirror.visualitems.user.userRepresentation.fxmlControllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

/**
 * Abstract super class of all fxml controllers that define the binding between
 * fxml ui elements from .fxml-files and corresponding instances of visual user
 * representations.
 * 
 * @author evalosch
 *
 */
public abstract class VisualUserRepresentationFXMLController extends
		FXMLController implements Initializable {

	// TODO: check if extended classes from
	// VisualUserRepresentationFXMLController (e.g.
	// FirstPhaseVisualUserRepresentationFXMLController) are really needed

	/**
	 * The log4j logger reference.
	 */
	private final Logger logger = LogManager
			.getLogger(VisualUserRepresentationFXMLController.class.getName());

	@FXML
	ImageView userRepresentationImage;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javafx.fxml.Initializable#initialize(java.net.URL,
	 * java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try {
			assert userRepresentationImage != null : "fx:id=\"userRepresentationImage\" was not injected: check your FXML file 'person.fxml'.";
		} catch (AssertionError e) {
			logger.error(e.getMessage());
		}
	}

	public ImageView getUserRepresentationImage() {
		return userRepresentationImage;
	}
	
	public void bindToUserRepresentationImage(ImageView userRepresentationImageView)
	{
		bindToFXMLElement(this.userRepresentationImage, userRepresentationImageView);
	}
}
