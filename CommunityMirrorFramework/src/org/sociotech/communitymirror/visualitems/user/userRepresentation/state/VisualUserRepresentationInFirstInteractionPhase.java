package org.sociotech.communitymirror.visualitems.user.userRepresentation.state;

import org.sociotech.communityinteraction.behaviors.MagneticEffectBehavior;
import org.sociotech.communityinteraction.behaviors.MagneticField;
import org.sociotech.communityinteraction.behaviors.MagneticPole;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentation;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.calculators.MirrorVisualUserRepresentationCalculatorBasedOnJointPositions;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.fxmlControllers.FirstPhaseVisualUserRepresentationFXMLController;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.imageCreators.FirstPhaseVisualUserRepresentationImageCreator;
import org.sociotech.communitymirror.visualitems.user.userState.InteractionPhase;

import com.google.common.io.Resources;

/**
 * State of a visual user representation that is represented like is expected
 * for users in the first interaction phase.
 * 
 * @author evalosch
 *
 */
public class VisualUserRepresentationInFirstInteractionPhase
		extends
		FXMLVisualUserRepresentationState<FirstPhaseVisualUserRepresentationFXMLController> {

	public VisualUserRepresentationInFirstInteractionPhase(
			VisualUserRepresentation visualUserRepresentation) {
		super(
				InteractionPhase.FirstInteractionPhase,
				Resources
						.getResource("userRepresentation/fxml/firstInteractionPhase.fxml"),
				visualUserRepresentation,
				//new HeadOnlyImageCreator(),
				new FirstPhaseVisualUserRepresentationImageCreator(),
				//new AdjustedVisualUserRepresentationCalculator(),
				new MirrorVisualUserRepresentationCalculatorBasedOnJointPositions());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communitymirror.visualitems.user.userRepresentation.state
	 * .VisualUserRepresentationState#fillListOfInteractionBehaviors()
	 */
	@Override
	protected void fillListOfInteractionBehaviors() {
		MagneticEffectBehavior behavior = new MagneticEffectBehavior(
				this.visualUserRepresentation, new MagneticField(10, 400,
						MagneticPole.SOUTH));
		this.interactionBehaviors.add(behavior);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communitymirror.visualitems.user.userRepresentation.state
	 * .FXMLVisualUserRepresentationState#setupFXMLController()
	 */
	@Override
	protected void setupFXMLController() {
		super.setupFXMLController();
		// this.fxmlController.setPersonImage(this.visualUserRepresentation.getRepresentationImage());
	}
}
