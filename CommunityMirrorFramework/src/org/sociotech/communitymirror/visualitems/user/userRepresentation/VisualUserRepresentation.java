package org.sociotech.communitymirror.visualitems.user.userRepresentation;

import java.util.LinkedList;
import java.util.List;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.user.User;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.state.VisualUserRepresentationInFifthInteractionPhase;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.state.VisualUserRepresentationInFirstInteractionPhase;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.state.VisualUserRepresentationInForthInteractionPhase;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.state.VisualUserRepresentationInSecondInteractionPhase;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.state.VisualUserRepresentationInThirdInteractionPhase;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.state.VisualUserRepresentationState;
import org.sociotech.communitymirror.visualitems.user.userState.InteractionPhase;

/**
 * Class that visually represents a user.
 * 
 * @author evalosch
 *
 */
public class VisualUserRepresentation extends VisualComponent {

	/**
	 * The user that is represented by this visual user representation.
	 */
	private User user;

	/**
	 * The current state of this visual user representation.
	 */
	private VisualUserRepresentationState currentState;

	/**
	 * List of all possible visual user representation states in which this
	 * visual user representation can be.
	 */
	private final List<VisualUserRepresentationState> allPossibleStates;

	/**
	 * The current image for representing the user on this visual user representation.
	 */
	private Image representationImage;
	private ImageView representationImageView;
	/**
	 * The next image for representing the user on this visual user representation.
	 */
	private Image nextRepresentationImage;

	public VisualUserRepresentation(User user) {
		this.user = user;
		this.allPossibleStates = createAllPossibleStates();
		this.representationImage = null;
		this.representationImageView = new ImageView();
		// call method setCurrentState here in order to have onActivation called
		// on the new set current state
		this.setCurrentState(user.getState().getInteractionPhase());
		this.setVisible(user.isTracked());
//		this.setPositionX(user.getUserId() * 100);
	}

	/**
	 * Creates all possible visual user representation states of this visual
	 * user representation and adds it to the list.
	 */
	private List<VisualUserRepresentationState> createAllPossibleStates() {
		List<VisualUserRepresentationState> possibleStates = new LinkedList<VisualUserRepresentationState>();
		// add all possible states to list
		possibleStates.add(new VisualUserRepresentationInFirstInteractionPhase(
				this));
		possibleStates
				.add(new VisualUserRepresentationInSecondInteractionPhase(this));
		possibleStates.add(new VisualUserRepresentationInThirdInteractionPhase(
				this));
		possibleStates.add(new VisualUserRepresentationInForthInteractionPhase(
				this));
		possibleStates.add(new VisualUserRepresentationInFifthInteractionPhase(
				this));
		return possibleStates;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the currentState
	 */
	public VisualUserRepresentationState getCurrentState() {
		return currentState;
	}

	/**
	 * Updates the current state with the state out of the list of all possible
	 * states of this visual user representation that matches the given
	 * interaction phase.
	 * 
	 * @param interactionPhase
	 *            The interaction phase to match.
	 */
	public void setCurrentState(InteractionPhase interactionPhase) {
		VisualUserRepresentationState oldState = this.getCurrentState();
		VisualUserRepresentationState newState = getVisualUserRepresentationStateToInteractionPhase(interactionPhase);
		this.currentState = newState;
		if (oldState != null) {
			oldState.onDeactivation();
		}
		newState.onActivation();
	}

	/**
	 * Gets the visual user representation that matches the given interaction
	 * phase out of the list of all possible states of this visual user
	 * representation
	 * 
	 * @param interactionPhase
	 *            The interaction phase to match.
	 * @return The visual user representation that matches the given interaction
	 *         phase.
	 */
	private VisualUserRepresentationState getVisualUserRepresentationStateToInteractionPhase(
			InteractionPhase interactionPhase) {
		for (VisualUserRepresentationState state : this.allPossibleStates) {
			if (state.getInteractionPhase().name() == interactionPhase.name()) {
				return state;
			}
		}
		return null;
	}

	/**
	 * @return the representationImage
	 */
	public Image getRepresentationImage() {
		return representationImage;
	}

	/**
	 * @param representationImage
	 *            the representationImage to set
	 */
	public void setRepresentationImage(Image representationImage) {
		this.representationImage = representationImage;
		this.representationImageView.setImage(representationImage);
	}

	public ImageView getRepresentationImageView() {
		return representationImageView;
	}

	public void setRepresentationImageView(ImageView representationImageView) {
		this.representationImageView = representationImageView;
	}

	public double calculatePositionX(User user, double imageWidth) {
		return this.getCurrentState().calculatePositionX(user, imageWidth);
	}

	public double calculatePositionY(User user, double imageHeight) {
		return this.getCurrentState().calculatePositionY(user, imageHeight);
	}

	public double calculateScale(User user) {
		return this.getCurrentState().calculateScale(user);
	}

	public Image createRepresentatonImage(Image userImage) {
		return this.getCurrentState().createRepresentatonImage(userImage);
	}

	public Image getNextRepresentationImage() {
		return this.nextRepresentationImage;
	}
	
	public void setNextRepresentationImage(Image nextRepresentationImage) {
		this.nextRepresentationImage = nextRepresentationImage;
	}
}
