package org.sociotech.communitymirror.visualitems.user.bodyCondition;

public class BodyPosition {

	private float[] position1;
	
	private float[] position2;
	
	private float[] positionInUserImage;

	private float[] position2InUserImage;

	public BodyPosition(float[] position1, float[] position2,float[] positionInUserImage, float[] position2InUserImage) {
		this.setPosition1(position1);
		this.setPosition2(position2);
		this.setPositionInUserImage(positionInUserImage);
		this.setPosition2InUserImage(position2InUserImage);
	}
	
	public void setPosition1(float[] position1){
		this.position1 = position1;
	}
	
	public void setPosition2(float[] position2){
		this.position2 = position2;
	}
	
	public void setPositionInUserImage(float[] positionInUserImage){
		this.positionInUserImage = positionInUserImage;
	}
	
	public void setPosition2InUserImage(float[] position2InUserImage){
		this.position2InUserImage = position2InUserImage;
	}

	/**
	 * @return the posX of first joint
	 */
	public float getPosX1() {
		if(position1.length > 0){
			return position1[0];
		}
		return 100;
	}

	/**
	 * @return the posY of first joint
	 */
	public float getPosY1() {
		if(position1.length > 0){
			return position1[1];
		}
		return 100;
	}

	/**
	 * @return the posZ of first joint
	 */
	public float getPosZ1() {
		if(position1.length > 0){
			return position1[2];
		}
		return 100;
	}
	
	/**
	 * @return the posX of second joint
	 */
	public float getPosX2() {
		if(position2.length > 0){
			return position2[0];
		}
		return 100;
	}

	/**
	 * @return the posY of second joint
	 */
	public float getPosY2() {
		if(position2.length > 0){
			return position2[1];
		}
		return 100;
	}

	/**
	 * @return the posZ of second joint
	 */
	public float getPosZ2() {
		if(position2.length > 0){
			return position2[2];
		}
		return 100;
	}
	
	/**
	 * @return the posX in user image
	 */
	public float getPosXInUserImage() {
		if(positionInUserImage.length > 0){
			return positionInUserImage[0];
		}
		return 100;
	}

	/**
	 * @return the posY in user image
	 */
	public float getPosYInUserImage() {
		if(positionInUserImage.length > 0){
			return positionInUserImage[1];
		}
		return 100;
	}

	/**
	 * @return the posZ in user image
	 */
	public float getPosZInUserImage() {
		if(positionInUserImage.length > 0){
			return positionInUserImage[2];
		}
		return 100;
	}
	
	/**
	 * @return the posX in user image
	 */
	public float getPosX2InUserImage() {
		if(position2InUserImage.length > 0){
			return position2InUserImage[0];
		}
		return 100;
	}

	/**
	 * @return the posY in user image
	 */
	public float getPosY2InUserImage() {
		if(position2InUserImage.length > 0){
			return position2InUserImage[1];
		}
		return 100;
	}

	/**
	 * @return the posZ in user image
	 */
	public float getPosZ2InUserImage() {
		if(position2InUserImage.length > 0){
			return position2InUserImage[2];
		}
		return 100;
	}
}
