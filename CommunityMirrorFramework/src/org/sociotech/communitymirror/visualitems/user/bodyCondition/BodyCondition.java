package org.sociotech.communitymirror.visualitems.user.bodyCondition;

public class BodyCondition {
	
	/**
	 * The orientation of the body described by this body condition.
	 */
	private BodyOrientation bodyOrientation;
	
	/**
	 * The position of the body described by this body condition.
	 */
	private BodyPosition bodyPosition;

	public BodyCondition(BodyOrientation bodyOrientation, BodyPosition bodyPosition) {
		this.bodyOrientation = bodyOrientation;
		this.bodyPosition = bodyPosition;
	}

	/**
	 * @return the bodyOrientation
	 */
	public BodyOrientation getBodyOrientation() {
		return bodyOrientation;
	}

	/**
	 * @return the bodyPosition
	 */
	public BodyPosition getBodyPosition() {
		return bodyPosition;
	}
	
	public void updateBodyOrientation(float[] orientation){
		this.bodyOrientation.setOrientation(orientation);
	}
	
	public void updateBodyPosition(float[] position1, float[] position2, float[] positionInUserImage, float[] position2InUserImage){
		this.bodyPosition.setPosition1(position1);
		this.bodyPosition.setPosition2(position2);
		this.bodyPosition.setPositionInUserImage(positionInUserImage);
		this.bodyPosition.setPosition2InUserImage(position2InUserImage);
	}

	/**
	 * @param bodyOrientation the bodyOrientation to set
	 */
	public void setBodyOrientation(BodyOrientation bodyOrientation) {
		this.bodyOrientation = bodyOrientation;
	}

	/**
	 * @param bodyPosition the bodyPosition to set
	 */
	public void setBodyPosition(BodyPosition bodyPosition) {
		this.bodyPosition = bodyPosition;
	}

}
