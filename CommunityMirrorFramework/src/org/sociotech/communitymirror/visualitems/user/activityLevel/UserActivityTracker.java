package org.sociotech.communitymirror.visualitems.user.activityLevel;

import java.util.LinkedList;
import java.util.List;

import org.sociotech.communitymirror.visualitems.user.bodyCondition.BodyCondition;

public class UserActivityTracker {

	/**
	 * Activity level that has been derived from the current list of body
	 * conditions of this user activity tracker.
	 */
	private ActivityLevel activityLevel;

	/**
	 * List of all saved body conditions of the user that is tracked by this
	 * user activity tracker. The body conditions are stored in a chronological
	 * order. New body conditions are append at the end of the list. The list
	 * has a maximum length of maxLengthOfBodyConditions.
	 */
	private List<BodyCondition> bodyConditions;

	/**
	 * Defines the maximum length of the list of the saved body conditions of
	 * this user activity tracker. If the list has reached a length of
	 * maxLengthOfBodyConditions the oldest entry of the list will be removed
	 * before a new body condition is appended.
	 */
	private int maxLengthOfBodyConditions;

	public UserActivityTracker(int maxLengthOfBodyConditions) {
		this.bodyConditions = new LinkedList<BodyCondition>();
		this.maxLengthOfBodyConditions = maxLengthOfBodyConditions;
	}

	public UserActivityTracker() {
		this.bodyConditions = new LinkedList<BodyCondition>();
		this.maxLengthOfBodyConditions = 100;
	}

	/**
	 * Calculates the activity level that can be derived from the current list
	 * of body conditions of this user activity tracker.
	 * 
	 * @return the activity level that has been derived from the current list of
	 *         body conditions
	 */
	private ActivityLevel calculateActivityLevel() {
		// TODO calculate the activity level
		return ActivityLevel.LOW;
	}

	/**
	 * Adds a new body condition to this user activity tracker and updates the
	 * activity level of this user activity tracker.
	 * 
	 * @param newBodyCondition
	 *            the new body condition to add
	 */
	public void addBodyCondition(BodyCondition bodyCondition) {
		if (bodyCondition != null) {
			// if the allowed maximum size of the list of body conditions is
			// reached
			if (this.bodyConditions.size() == this.maxLengthOfBodyConditions) {
				// first remove the oldest entry
				this.bodyConditions.remove(0);
			}
			// add the new body condition to the list of body conditions
			this.bodyConditions.add(bodyCondition);

			// update activity level
			this.setActivityLevel(this.calculateActivityLevel());
		}
	}

	/**
	 * @return the activityLevel
	 */
	public ActivityLevel getActivityLevel() {
		return activityLevel;
	}

	/**
	 * @param activityLevel
	 *            the activityLevel to set
	 */
	private void setActivityLevel(ActivityLevel activityLevel) {
		this.activityLevel = activityLevel;
	}

}
