package org.sociotech.communitymirror.visualitems.user.activityLevel;

public enum ActivityLevel {

	LOW, HIGH
}
