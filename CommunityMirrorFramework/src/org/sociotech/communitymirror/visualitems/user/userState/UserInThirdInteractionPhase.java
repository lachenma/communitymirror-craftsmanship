package org.sociotech.communitymirror.visualitems.user.userState;

import org.sociotech.communitymirror.visualitems.user.User;
import org.sociotech.communitymirror.visualitems.user.interactionZone.InteractionZoneType;

/**
 * A concrete user state, that says that the user is in the third interaction
 * phase.
 * 
 * @author evalosch
 *
 */
public class UserInThirdInteractionPhase extends UserState {

	public UserInThirdInteractionPhase() {
		super(InteractionPhase.ThirdInteractionPhase);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communitymirror.visualitems.user.userState.UserState#isReached
	 * (org.sociotech.communitymirror.visualitems.user.User)
	 */
	@Override
	public boolean isReached(User user) {
		// TODO define under which conditions this user state is reached by a
		// given user
		if(user.getCurrentInteractionZone().getType() == InteractionZoneType.CloserBodyZone){
			return true;
		}
		return false;
	}
}
