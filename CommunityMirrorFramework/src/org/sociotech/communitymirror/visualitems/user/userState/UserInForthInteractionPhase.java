package org.sociotech.communitymirror.visualitems.user.userState;

import org.sociotech.communitymirror.visualitems.user.User;
import org.sociotech.communitymirror.visualitems.user.interactionZone.InteractionZoneType;

/**
 * A concrete user state, that says that the user is in the forth interaction
 * phase.
 * 
 * @author evalosch
 *
 */
public class UserInForthInteractionPhase extends UserState {

	public UserInForthInteractionPhase() {
		super(InteractionPhase.ForthInteractionPhase);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communitymirror.visualitems.user.userState.UserState#isReached
	 * (org.sociotech.communitymirror.visualitems.user.User)
	 */
	@Override
	public boolean isReached(User user) {
		// TODO define under which conditions this user state is reached by a
		// given user
		if(user.getCurrentInteractionZone().getType() == InteractionZoneType.TouchZone){
			return true;
		}
		return false;
	}
}
