package org.sociotech.communitymirror.visualitems.user.userState;

import java.util.List;

import org.sociotech.communityinteraction.events.stateChange.State;
import org.sociotech.communitymirror.visualitems.user.User;

/**
 * abstract super of all user states a user can be in. A user state defines the
 * interaction phase, in which the user currently is.
 * 
 * @author evalosch
 *
 */
public abstract class UserState extends State {

	/**
	 * List of all user states the user can switch to from this state.
	 */
	private List<UserState> reachableUserStates;

	/**
	 * The interaction phase, that a user with this user state is in.
	 */
	private final InteractionPhase interactionPhase;

	public UserState(InteractionPhase interactionPhase) {
		this.interactionPhase = interactionPhase;
	}

	/**
	 * @return the reachableUserStates
	 */
	public List<UserState> getReachableUserStates() {
		return reachableUserStates;
	}

	/**
	 * @param reachableUserStates
	 */
	public void setReachableUserStates(List<UserState> reachableUserStates) {
		this.reachableUserStates = reachableUserStates;
	}

	/**
	 * @return the interactionPhase
	 */
	public InteractionPhase getInteractionPhase() {
		return interactionPhase;
	}

	/**
	 * Checks if one of the reachable states has been reached for a given user
	 * and returns the new reached state if there is one.
	 * 
	 * @param user
	 *            The user for whom the reached user state is returned.
	 * @return The reached user state or null if no new state has been reached.
	 */
	public UserState getReachedUserState(User user) {
		for (UserState reachableState : this.getReachableUserStates()) {
			if (reachableState.isReached(user)) {
				return reachableState;
			}
		}
		return null;
	}

	/**
	 * Checks if this user state is reached by a given user.
	 * 
	 * @param user
	 *            The user that is checked.
	 * @return True if this state is reached by the user else false.
	 */
	public abstract boolean isReached(User user);
}
