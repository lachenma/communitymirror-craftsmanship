package org.sociotech.communitymirror.visualitems.user.userState;

public enum InteractionPhase {
	FirstInteractionPhase, SecondInteractionPhase, ThirdInteractionPhase, ForthInteractionPhase, FifthInteractionPhase
}
