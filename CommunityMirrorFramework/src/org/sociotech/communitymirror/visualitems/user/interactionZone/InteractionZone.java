package org.sociotech.communitymirror.visualitems.user.interactionZone;

public class InteractionZone {
	
	private double internalMinZValue;
	
	private double externalMaxZValue;
	
	private InteractionZoneType type;
	
	public InteractionZone(double internalMinZValue, double externalMaxZValue, InteractionZoneType type){
		this.internalMinZValue = externalMaxZValue;
		this.externalMaxZValue = externalMaxZValue;
		this.type = type;
	}

	public double getInternalMinZValue() {
		return internalMinZValue;
	}

	public double getExternalMaxZValue() {
		return externalMaxZValue;
	}

	public InteractionZoneType getType() {
		return type;
	}
	
	public void setInternalMinZValue(double internalMinZValue) {
		this.internalMinZValue = internalMinZValue;
	}

	public void setExternalMaxZValue(double externalMaxZValue) {
		this.externalMaxZValue = externalMaxZValue;
	}

	public boolean containsPosition(double posZ){
		if(posZ >= this.internalMinZValue && posZ< this.externalMaxZValue){
			return true;
		}
		return false;
	}
}
