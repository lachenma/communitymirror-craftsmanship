package org.sociotech.communitymirror.visualitems.user;

import java.util.LinkedList;
import java.util.List;

/**
 * Factory that produces user.
 * 
 * @author evalosch
 *
 */
public class UserFactory {

	/**
	 * Creates a given number of untracked users.
	 * 
	 * @param number
	 *            The number of users to create.
	 * @return The list of created users.
	 */
	public static List<User> createAllUsers(int number) {
		// create an empty list for the users
		List<User> users = new LinkedList<User>();
		// until number is reached
		for (int i = 1; i <= number; i++) {
			users.add(new User(i));
		}
		// return the list of users
		return users;
	}
}
