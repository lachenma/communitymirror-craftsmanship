
package org.sociotech.communitymirror.visualitems;

import javafx.scene.Node;

/**
 * A simple VisualComponent class for wrapping JavaFX nodes
 *
 * @author kochm
 *
 */

public class FXVisualComponent extends VisualComponent {

    public FXVisualComponent(Node node) {
        super();
        this.getChildren().add(node);
    }

}
