package org.sociotech.communitymirror.visualstates;

/**
 * The visual state indication that the visual item is in the spotlight. 
 * 
 * @author Peter Lachenmaier
 */
public class Spotlight extends VisualState {

}
