package org.sociotech.communitymirror.visualstates;

/**
 * The visual state indication that the visual item is focused. 
 * 
 * @author Peter Lachenmaier
 */
public class Focused extends VisualState {

}
