/*
 * Copyright (c) 2014. Martin Burkhard, CSCM Cooperation Systems Center Munich.
 * Institute for Software Technology at Universitaet der Bundeswehr Muenchen.
 *
 * This part of the program is made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.sociotech.communitymirror.visualstates;

/**
 * Created by i21bmabu on 18.02.14.
 */
public class ComponentState {

    public final static int INIT_INT      = 0;
    public final static int IDLE_INT      = 1;
    public final static int UPDATE_INT    = 2;
    public final static int PAUSE_INT     = 3;
    public final static int DIE_INT       = 4;
    public final static int DESTROYED_INT = 5;

    public final static ComponentState INIT = new ComponentState(INIT_INT, "INIT");
    public final static ComponentState IDLE = new ComponentState(IDLE_INT, "IDLE");
    public final static ComponentState UPDATE = new ComponentState(UPDATE_INT, "UPDATE");
    public final static ComponentState PAUSE = new ComponentState(PAUSE_INT, "PAUSE");
    public final static ComponentState DIE = new ComponentState(DIE_INT, "DIE");
    public final static ComponentState DESTROYED = new ComponentState(DESTROYED_INT, "DESTROYED");

    public final static ComponentState DEFAULT = INIT;

    @SuppressWarnings("unused")
	private final int nState;
    @SuppressWarnings("unused")
	private final String strState;

    protected ComponentState(int state, String strState) {
        this.nState = state;
        this.strState = strState;
    }

}
