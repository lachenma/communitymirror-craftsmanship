
package org.sociotech.communitymirror.graph.structure;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.graph.creation.GraphVisualItemCreator;
import org.sociotech.communitymirror.visualitems.VisualGroup;
import org.sociotech.communitymirror.visualitems.VisualItem;
import org.sociotech.communitymirror.visualstates.VisualState;

import com.facebook.rebound.BaseSpringSystem;

import javafx.scene.Group;

/**
 * Representing a graph consisting of {@link GraphNode}s and {@link GraphEdge}s.
 *
 * @author Peter Lachenmaier
 */
public class Graph extends VisualGroup implements GraphVisualItemCreator {

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(Graph.class);

    /**
     * The edges of this graph
     */
    private Set<GraphEdge> edges;

    /**
     * The nodes of this graph
     */
    private Set<GraphNode> nodes;

    /**
     * Creator for new visual items
     */
    private GraphVisualItemCreator itemCreator;

    /**
     * The maximum number of connected nodes
     */
    private int maximumNumberOfConnectedNodes = 10;

    /**
     * The initial radius of one circle layout
     */
    private double radius;

    /**
     * The graph wide spring system.
     */
    private BaseSpringSystem springSystem;

    /**
     * The factor for the layout radius in micro state.
     */
    private double microRadiusFactor = 1.0;

    /**
     * The factor for the layout radius in preview state.
     */
    private double previewRadiusFactor = 1.0;

    /**
     * The factor for the spring length from or to components in micro state.
     */
    private double microSpringLengthFactor = 1.0;

    /**
     * The factor for the spring length from or to components in preview state.
     */
    private double previewSpringLengthFactor = 1.0;

    /**
     * The factor for the spring length from or to components in detail state.
     */
    private double detailSpringLengthFactor = 1.0;

    /**
     * The factor of the radius of the background circle relative to layout
     * radius.
     */
    private double backgroundCircleFactor = 1.0;

    /**
     * The color for the background circle
     */
    private String backgroundCircleColor = "#000000";

    /**
     * The opacity value for the backgroundcircle
     */
    private double backgroundCircleOpacity = 0.3;

    /**
     * Whether to reposition components after layouting -> leads to "animation"
     * of opening.
     */
    private boolean repositionAfterLayout = false;

    /**
     * The number of nodes that is needed to create a layout for.
     */
    private int nodesForLayout = 1;

    /**
     * The parent for all background
     */
    private final Group backgroundParent;

    /**
     * Initializes a new and empty graph
     */
    public Graph(GraphVisualItemCreator itemCreator) {
        this.edges = new LinkedHashSet<>();
        this.nodes = new LinkedHashSet<>();
        this.itemCreator = itemCreator;
        // initialize background parent
        this.backgroundParent = new Group();
        this.addNode(this.backgroundParent);
    }

    /**
     * Adds all given edges to this graph. If the connected nodes are not
     * already
     * contained in this graph, they will be added, too.
     *
     * @param edges
     *            Edges to add.
     */
    public void add(GraphEdge... edges) {
        for (GraphEdge edge : edges) {
            if (!this.edges.contains(edge)) {
                this.edges.add(edge);
                // also add the both connected nodes
                this.add(edge.getFrom());
                this.add(edge.getTo());
            }
        }
    }

    /**
     * Adds all given nodes to this graph.
     *
     * @param nodes
     *            Nodes to add.
     */
    public void add(GraphNode... nodes) {
        this.nodes.addAll(Arrays.asList(nodes));
    }

    /**
     * Creates a new node in this graph
     *
     * @return New node
     */
    public GraphNode createNode() {
        return new GraphNode(this);
    }

    /**
     * Creates a new node in this graph
     *
     * @return New node
     */
    public GraphNode createNode(VisualItem<?> viusalItem) {
        return new GraphNode(this, viusalItem);
    }

    /**
     * Creates a new node for the given data item.
     *
     * @param dataItem
     *            Data item to create node for
     * @return The new node for the data item
     */
    public GraphNode createNode(Item dataItem) {
        try {
            return new GraphNode(this, dataItem);
        } catch (Exception e) {
            this.logger.info("createNode() called with NULL dataItem");
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.view.flow.graph.GraphVisualItemCreator#
     * createVisualItem(org.sociotech.communitymashup.data.Item,
     * org.sociotech.communitymirror.visualstates.VisualState)
     */
    @Override
    public VisualItem<?> createVisualItem(Item dataItem, VisualState visualState, GraphNode node) {
        // delegate
        VisualItem<?> newVisualItem = this.itemCreator.createVisualItem(dataItem, visualState, node);

        return newVisualItem;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.view.flow.graph.GraphVisualItemCreator#
     * getRelatedItemsFor(org.sociotech.communitymashup.data.Item)
     */
    @Override
    public Set<Item> getRelatedItemsFor(Item dataItem) {
        // delegate
        return this.itemCreator.getRelatedItemsFor(dataItem);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.graph.creation.GraphVisualItemCreator#
     * getSelectedRelatedItemsToConnectFor(org.sociotech.communitymashup.data.
     * Item, java.util.Set)
     */
    @Override
    public Set<Item> getSelectedRelatedItemsToConnectFor(Item dataItem, Set<Item> connectedItems) {
        // delegate
        return this.itemCreator.getSelectedRelatedItemsToConnectFor(dataItem, connectedItems);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.view.flow.graph.GraphVisualItemCreator#
     * destroyVisualItem(org.sociotech.communitymirror.visualitems.VisualItem)
     */
    @Override
    public void destroyVisualItem(VisualItem<?> visualItem) {
        // remove from visual group
        this.removeVisualComponent(visualItem, true);

        // delegate
        this.itemCreator.destroyVisualItem(visualItem);
    }

    /**
     * Returns the maximum number of connected nodes.
     *
     * @return The maximum number of connected nodes.
     */
    public int getMaxNumberOfConnectedNodes() {
        return this.maximumNumberOfConnectedNodes;
    }

    /**
     * Sets the maximum number of connected nodes.
     *
     * @param maximumNumberOfConnectedNodes
     *            Maximum number of connected nodes.
     */
    public void setMaximumNumberOfConnectedNodes(int maximumNumberOfConnectedNodes) {
        this.maximumNumberOfConnectedNodes = maximumNumberOfConnectedNodes;
    }

    /**
     * Returns the radius of one circle layout.
     *
     * @return The radius of one circle layout.
     */
    public double getRadius() {
        return this.radius;
    }

    /**
     * The spring system for layouting.
     *
     * @return The graph wide spring system.
     */
    public BaseSpringSystem getSpringSystem() {
        return this.springSystem;
    }

    /**
     * Sets the initial radius for one circle layout.
     *
     * @param radius
     *            Radius for circle layout.
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    /**
     * Sets the spring system for the layouting.
     *
     * @param springSystem
     *            Spring system.
     */
    public void setSpringSystem(BaseSpringSystem springSystem) {
        this.springSystem = springSystem;
    }

    /**
     * Returns true if non of the contained nodes has an visual representation.
     *
     * @return True if non of the contained nodes has an visual representation.
     */
    public boolean isNonVisible() {
        for (GraphNode node : this.nodes) {
            // grap is visible if one of its nodes is visible
            if (!node.isNonVisible()) {
                return false;
            }
        }

        // no visible node
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.visualitems.VisualComponent#destroy()
     */
    @Override
    public void destroy() {
        // destroy all nodes
        Set<GraphNode> nodesCopy = new LinkedHashSet<>(this.nodes);
        for (GraphNode node : nodesCopy) {
            node.destroy();
        }

        // and all edges
        Set<GraphEdge> edgesCopy = new LinkedHashSet<>(this.edges);
        for (GraphEdge edge : edgesCopy) {
            edge.destroy();
        }
        // remove background parent
        this.removeNode(this.backgroundParent);

        super.destroy();
    }

    /**
     * Returns the factor for the layout radius in micro state.
     *
     * @return The factor for the layout radius in micro state.
     */
    public double getMicroRadiusFactor() {
        return this.microRadiusFactor;
    }

    /**
     * Sets the factor for the layout radius in micro state.
     *
     * @param microRadiusFactor
     *            The factor for the layout radius in micro state.
     */
    public void setMicroRadiusFactor(double microRadiusFactor) {
        this.microRadiusFactor = microRadiusFactor;
    }

    /**
     * Returns the factor for the layout radius in preview state.
     *
     * @return The factor for the layout radius in preview state.
     */
    public double getPreviewRadiusFactor() {
        return this.previewRadiusFactor;
    }

    /**
     * Sets the factor for the layout radius in micro state.
     *
     * @param previewRadiusFactor
     *            The factor for the layout radius in micro state.
     */
    public void setPreviewRadiusFactor(double previewRadiusFactor) {
        this.previewRadiusFactor = previewRadiusFactor;
    }

    /**
     * Returns the factor for the spring length from or to components in micro
     * state.
     *
     * @return The factor for the spring length from or to components in micro
     *         state.
     */
    public double getMicroSpringLengthFactor() {
        return this.microSpringLengthFactor;
    }

    /**
     * Sets the factor for the spring length from or to components in micro
     * state.
     *
     * @param microSpringLengthFactor
     *            The factor for the spring length from or to components in
     *            micro state.
     */
    public void setMicroSpringLengthFactor(double microSpringLengthFactor) {
        this.microSpringLengthFactor = microSpringLengthFactor;
    }

    /**
     * Returns the factor for the spring length from or to components in preview
     * state.
     *
     * @return The factor for the spring length from or to components in preview
     *         state.
     */
    public double getPreviewSpringLengthFactor() {
        return this.previewSpringLengthFactor;
    }

    /**
     * Sets the factor for the spring length from or to components in preview
     * state.
     *
     * @param previewSpringLengthFactor
     *            The factor for the spring length from or to components in
     *            preview state.
     */
    public void setPreviewSpringLengthFactor(double previewSpringLengthFactor) {
        this.previewSpringLengthFactor = previewSpringLengthFactor;
    }

    /**
     * Returns the factor for the spring length from or to components in detail
     * state.
     *
     * @return The factor for the spring length from or to components in detail
     *         state.
     */
    public double getDetailSpringLengthFactor() {
        return this.detailSpringLengthFactor;
    }

    /**
     * Sets the factor for the spring length from or to components in detail
     * state.
     *
     * @param detailSpringLengthFactor
     *            The factor for the spring length from or to components in
     *            detail state.
     */
    public void setDetailSpringLengthFactor(double detailSpringLengthFactor) {
        this.detailSpringLengthFactor = detailSpringLengthFactor;
    }

    /**
     * Returns the factor of the radius of the background circle relative to
     * layout radius.
     *
     * @return The factor of the radius of the background circle relative to
     *         layout radius
     */
    public double getBackgroundCircleFactor() {
        return this.backgroundCircleFactor;
    }

    /**
     * Sets the factor of the radius of the background circle relative to layout
     * radius
     *
     * @param backgroundCircleFactor
     *            The factor of the radius of the background circle relative to
     *            layout radius
     */
    public void setBackgroundCircleFactor(double backgroundCircleFactor) {
        this.backgroundCircleFactor = backgroundCircleFactor;
    }

    /**
     * Returns whether reposition of components after layouting is active or
     * not.
     *
     * @return True if reposition is active
     */
    public boolean isRepositionAfterLayout() {
        return this.repositionAfterLayout;
    }

    /**
     * Sets the active state for reposition of components after layouting.
     *
     * @param repositionAfterLayout
     *            True to activate, false to deactivate
     */
    public void setRepositionAfterLayout(boolean repositionAfterLayout) {
        this.repositionAfterLayout = repositionAfterLayout;
    }

    /**
     * Returns the color of the background circle
     *
     * @return The color of the background circle
     */
    public String getBackgroundCircleColor() {
        return this.backgroundCircleColor;
    }

    /**
     * Sets the color of the background circle.
     *
     * @param backgroundCircleColor
     *            The color of the background
     */
    public void setBackgroundCircleColor(String backgroundCircleColor) {
        this.backgroundCircleColor = backgroundCircleColor;
    }

    /**
     * Returns the opacity of the background circle
     *
     * @return The opacity of the background circle
     */
    public double getBackgroundCircleOpacity() {
        return this.backgroundCircleOpacity;
    }

    /**
     * Sets the opacity of the background circle.
     *
     * @param backgroundCircleOpacity
     *            The opacity of the background circle
     */
    public void setBackgroundCircleOpacity(double backgroundCircleOpacity) {
        this.backgroundCircleOpacity = backgroundCircleOpacity;
    }

    /**
     * Returns the number of nodes that is needed to create a layout for.
     *
     * @return The number of nodes that is needed to create a layout for.
     */
    public int getNodesForLayout() {
        return this.nodesForLayout;
    }

    /**
     * Set the number of nodes that is needed to create a layout for.
     *
     * @param nodesForLayout
     *            The number of nodes that is needed to create a layout for.
     */
    public void setNodesForLayout(int nodesForLayout) {
        this.nodesForLayout = nodesForLayout;
    }

    /**
     * Returns the parent for background elements.
     *
     * @return The parent for background elements.
     */
    public Group getBackgroundParent() {
        return this.backgroundParent;
    }
}
