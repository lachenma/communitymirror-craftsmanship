package org.sociotech.communitymirror.graph.structure;


/**
 * Abstract super class for all graph elements to manage
 * containment in graph.
 * 
 * @author Peter Lachenmaier
 */
public abstract class GraphElement {
	
	/**
	 * Empty constructor, use only in subclasses.
	 */
	protected GraphElement() {
		// nothing to do without graph
		created = System.currentTimeMillis();
	}
	
	/**
	 * The creation time in system milliseconds
	 */
	private final long created;
	
	/**
	 * Inititalizes the graph element with the given containing graph.
	 * 
	 * @param graph The graph that contain the will contain the element.
	 */
	public GraphElement(Graph graph) {
		this();
		this.setGraph(graph);
	}
	/**
	 * The graph in which this element is contained in. 
	 */
	protected Graph graph;

	/**
	 * Returns the graph of this element
	 * 
	 * @return The graph this element is contained in.
	 */
	public Graph getGraph() {
		return graph;
	}

	/**
	 * Sets the graph for this graph element.
	 * 
	 * @param graph Graph for the element.
	 */
	public void setGraph(Graph graph) {
		if(graph == null) {
			throw new NullPointerException("A valid graph must be provided.");
		}
		
		if(this.graph == graph) {
			// nothing to do
			return;
		}
		
		this.graph = graph;
		// not nice but easy and graph should have nothing
		// else except nodes and edges
		if(this instanceof GraphNode) {
			graph.add((GraphNode) this);
		} else if(this instanceof GraphEdge) {
			graph.add((GraphEdge) this);
		}		
	}
	
	/**
	 * Destroys this graph element and all of its visual representations.
	 */
	abstract public void destroy();

	/**
	 * Returns the creation time in system milliseconds
	 * @return The creation time in system milliseconds
	 */
	public long getCreated() {
		return created;
	}
}
