
package org.sociotech.communitymirror.graph.structure;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javafx.scene.Group;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymashup.data.Tag;
import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.graph.layout.CircleNodeSpringLayouter;
import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.VisualGroup;
import org.sociotech.communitymirror.visualitems.VisualItem;
import org.sociotech.communitymirror.visualstates.ComponentState;
import org.sociotech.communitymirror.visualstates.Detail;
import org.sociotech.communitymirror.visualstates.Micro;
import org.sociotech.communitymirror.visualstates.Preview;
import org.sociotech.communitymirror.visualstates.VisualState;

import com.facebook.rebound.BaseSpringSystem;

/**
 * Representing one node in a {@link Graph}.
 *
 * @author Peter Lachenmaier
 */
public class GraphNode extends GraphElement {
    /**
     * Logger reference
     */
    protected Logger logger = LogManager.getLogger(GraphNode.class.getName());

    /**
     * The set of edges this node is contained in.
     */
    private final Set<GraphEdge> edges;

    /**
     * The current visual state. Null indicates invisibility.
     */
    private VisualState currentVisualState = null;

    /**
     * The layouter for connected elements.
     */
    private CircleNodeSpringLayouter circleLayout;

    /**
     * Map that keeps the correlation between a visual state and the current
     * visual component
     */
    private final Map<VisualState, VisualItem<?>> visualComponentsInState;

    /**
     * Reference to the data item.
     */
    private Item dataItem;

    /**
     * Indicates that currently a transition is performed
     */
    private boolean inTransition;

    /**
     * Creates an empty node in the given graph
     *
     * @param graph
     *            Graph the node belongs to.
     */
    public GraphNode(Graph graph) {
        super(graph);
        this.edges = new LinkedHashSet<>();
        this.visualComponentsInState = new HashMap<>();
    }

    /**
     * Initialize the node within the graph and adds the visual item.
     *
     * @param graph
     *            Graph the node belongs to
     * @param visualItem
     *            One visual item
     */
    public GraphNode(Graph graph, VisualItem<? extends Item> visualItem) {
        this(graph);
        if (visualItem == null) {
            throw new IllegalArgumentException("Visual item must not be null");
        }

        // get current visual state
        this.currentVisualState = visualItem.getVisualState();
        // put first element in map
        this.visualComponentsInState.put(this.currentVisualState, visualItem);
        // get the data item from the visual item
        this.dataItem = visualItem.getDataItem();
        // put in graph
        graph.addVisualComponent(visualItem, true);
    }

    /**
     * Initialize the node within the graph and set the given data item.
     *
     * @param graph
     *            Graph the node belongs to
     * @param dataItem
     *            One data item
     */
    public GraphNode(Graph graph, Item dataItem) {
        this(graph);
        if (dataItem == null) {
            throw new IllegalArgumentException("Data item must not be null");
        }

        // set the data item
        this.dataItem = dataItem;
    }

    /**
     * Adds all given edges to this node.
     *
     * @param edges
     *            Edges to add.
     */
    protected void add(GraphEdge... edges) {
        // add all edges
        for (GraphEdge edge : edges) {
            if (edge.containsNode(this)) {
                // add only valid edges
                this.edges.add(edge);
            }
        }
    }

    /**
     * Connects this node to the given other node, or returns the existing edge
     * if
     * they are already connected.
     *
     * @param other
     *            Node to connect this to
     * @return The new edge between this to nodes
     */
    public GraphEdge connectTo(GraphNode other) {
        GraphEdge existingEdge = this.getEdgeTo(other);
        if (existingEdge != null) {
            return existingEdge;
        }

        // create edge
        // automatically adds the new edge to both edge lists and the graph
        return new GraphEdge(this, other);
    }

    /**
     * Returns the edge to the given other node, or null if it does not exist.
     *
     * @param other
     *            The other node to find the edge to
     * @return The edge between the both nodes or null if there isn't one
     */
    public GraphEdge getEdgeTo(GraphNode other) {
        for (GraphEdge edge : this.edges) {
            if (edge.containsNode(other)) {
                // found existing edge
                return edge;
            }
        }
        // no existing edge
        return null;
    }

    /**
     * Returns all directly connected nodes.
     *
     * @return All directly connected nodes.
     */
    public Set<GraphNode> getConnectedNodes() {
        // create result set
        Set<GraphNode> connectedNodes = new LinkedHashSet<>(this.edges.size());

        for (GraphEdge edge : this.edges) {
            if (edge.getTo() == this) {
                connectedNodes.add(edge.getFrom());
            } else {
                connectedNodes.add(edge.getTo());
            }
        }

        return connectedNodes;
    }

    /**
     * Returns all connected nodes until the given level (distance). Level 1 is
     * directly connected. The returned set is not modifiable.
     *
     * @param level
     *            Level/distance
     * @return Set of connected nodes
     */
    public Set<GraphNode> getConnectedNodesUntilLevel(int level) {
        if (level <= 0) {
            return Collections.emptySet();
        } else if (level == 1) {
            return Collections.unmodifiableSet(this.getConnectedNodes());
        } else {
            Set<GraphNode> connectedNodes = new LinkedHashSet<>();
            // add directly connected nodes
            connectedNodes.addAll(this.getConnectedNodes());

            for (GraphNode connectedNode : this.getConnectedNodes()) {
                // add all node in one level deeper
                connectedNodes.addAll(connectedNode.getConnectedNodesUntilLevel(level - 1));
            }
            connectedNodes.remove(this);
            return connectedNodes;
        }
    }

    /**
     * Returns all connected nodes in the given level (distance). Level 1 is
     * directly connected.
     *
     * @param level
     *            Level/distance
     * @return Set of connected nodes
     */
    public Set<GraphNode> getConnectedNodesInLevel(int level) {
        Set<GraphNode> connectedNodes = new LinkedHashSet<>();

        if (level <= 0) {
            // return empty set
            return connectedNodes;
        }

        // add all until
        connectedNodes.addAll(this.getConnectedNodesUntilLevel(level));

        // remove all nearer
        connectedNodes.removeAll(this.getConnectedNodesUntilLevel(level - 1));

        // remove self
        connectedNodes.remove(this);

        return connectedNodes;

    }

    /**
     * Returns the currently active visual component. Null if there is none.
     *
     * @return The currently active visual component.
     */
    public VisualItem<?> getCurrentVisualComponent() {
        if (this.currentVisualState == null) {
            return null;
        }

        // lookup visual component for current state
        return this.visualComponentsInState.get(this.currentVisualState);
    }

    /**
     * Returns the visual state that follows the current state.
     *
     * @return The next visual state.
     */
    protected VisualState getNextVisualState() {
        if (this.currentVisualState == null) {
            // Start with Preview
            return VisualState.PREVIEW;
        } else if (this.currentVisualState.isPreview()) {
            if (this.dataItem instanceof Tag) {
                // let tags stay in preview
                return VisualState.PREVIEW;
            }
            // Detail after Preview
            return VisualState.DETAIL;
        } else if (this.currentVisualState.isDetail()) {
            // Stay in Detail
            return VisualState.DETAIL;
        } else {
            // Default current
            return this.currentVisualState;
        }
    }

    /**
     * Returns the visual state that follows the current state.
     *
     * @return The next visual state.
     */
    protected VisualState getPreviousVisualState() {
        if (this.currentVisualState == null) {
            // no previous
            return null;
        } else if (this.currentVisualState.isPreview()) {
            // Micro before Preview
            return null;
        } else if (this.currentVisualState.isDetail()) {
            // Preview before Detail
            return VisualState.PREVIEW;
        } else {
            // Default current
            return this.currentVisualState;
        }
    }

    /**
     * Transits to next visual state.
     *
     * @return The new visual state.
     */
    public VisualState nextState() {
        VisualState nextVisualState = this.getNextVisualState();
        if (nextVisualState != this.currentVisualState) {
            this.transitTo(nextVisualState);
        } else {
            this.refreshState();
        }
        return this.currentVisualState;
    }

    /**
     * Transits to the previous visual state.
     *
     * @return The new visual state
     */
    public VisualState previousState() {

        if (this.getPreviousVisualState() != this.currentVisualState) {
            this.transitTo(this.getPreviousVisualState());
        }
        return this.currentVisualState;
    }

    /**
     * Refreshes the current visual state.
     *
     */
    private void refreshState() {
        if (this.currentVisualState instanceof Preview || this.currentVisualState instanceof Detail) {
            // reactivate nodes
            this.nextStateInAllVisualNullConnectedNodes();

            // reset interaction time in directly connected nodes
            this.resetInteractionTimeInLevel(1);
        }
    }

    /**
     * Resets the interaction time in the visual coponents of the given level.
     *
     * @param level
     *            Graph level
     */
    private void resetInteractionTimeInLevel(int level) {
        // get visual components in level
        List<VisualItem<?>> connectedComponents = this.getConnectedVisualComponentsInLevel(level);

        // reset interaction time in every connected component
        for (VisualItem<?> component : connectedComponents) {
            component.resetInteractionTime();
        }
    }

    /**
     * Resets the interaction time in all non micro visual coponents of the
     * given level.
     *
     * @param level
     *            Graph level
     */
    private void resetInteractionTimeInNonMicroInLevel(int level) {
        // get visual components in level
        List<VisualItem<?>> connectedComponents = this.getConnectedVisualComponentsInLevel(level);

        // reset interaction time in every connected component
        for (VisualItem<?> component : connectedComponents) {
            if (!(component.getVisualState() instanceof Micro)) {
                component.resetInteractionTime();
            }
        }
    }

    /**
     * Makes a transition of this node to the given visual state.
     *
     * @param newVisualState
     *            The visualState to transit to.
     */
    private void transitTo(VisualState newVisualState) {

        if (this.inTransition) {
            // only one transition
            return;
        }

        if (newVisualState == this.currentVisualState) {
            return;
        }

        try {
            // indicate ongoing transition
            this.preventFromTranisition();

            this.performTransitionTo(newVisualState);
        } catch (Exception e) {
            this.logger.warn("Could not perform state transition to " + newVisualState + ". Reason:");
            this.logger.warn("    " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length && i < 10; i++) {
                this.logger.warn("    " + e.getStackTrace()[i]);
            }
        } finally {
            // indicate end
            this.allowTranisition();
        }

    }

    /**
     * Performs the transition to the new visual state without any further
     * checks.
     *
     * @param newVisualState
     *            The visualState to transit to.
     */
    private void performTransitionTo(VisualState newVisualState) {
        VisualItem<?> newVisualItem = null;
        VisualItem<?> oldVisualItem = null;

        this.logger.debug("Transition from " + this.currentVisualState + " to " + newVisualState);

        // get old
        if (this.currentVisualState != null) {
            oldVisualItem = this.visualComponentsInState.get(this.currentVisualState);
        }

        if (newVisualState != null) {

            newVisualItem = this.visualComponentsInState.get(newVisualState);
            if (newVisualItem == null || newVisualItem.getComponentState() == ComponentState.DESTROYED) {
                // produce new visual item
                newVisualItem = this.graph.createVisualItem(this.dataItem, newVisualState, this);
                // add to map
                this.visualComponentsInState.put(newVisualState, newVisualItem);
            }

            GraphNode.transferAttributes(oldVisualItem, newVisualItem);

            // replace in own layout
            this.replaceInLayout(oldVisualItem, newVisualItem);
            // and in the layout of connected components
            this.replaceInConnectedComponentsLayout(oldVisualItem, newVisualItem);
        }

        // hide old
        if (oldVisualItem != null && newVisualItem != null) {
            this.graph.removeVisualComponent(oldVisualItem, true);
            // deactivate
            oldVisualItem.pause();
            oldVisualItem.toBack();
        }
        // show new
        if (newVisualItem != null) {
            this.graph.addVisualComponent(newVisualItem, true);
            // reactivate
            newVisualItem.resetInteractionTime();
            newVisualItem.resume();
        }

        VisualState oldVisualState = this.currentVisualState;
        // set new visual state
        this.currentVisualState = newVisualState;

        if (oldVisualState instanceof Preview && newVisualState instanceof Detail) {
            this.transitPreviewToDetail();
        } else if (oldVisualState instanceof Detail && newVisualState instanceof Preview) {
            this.transitDetailToPreview();
        } else if (oldVisualState instanceof Preview && newVisualState == null) {
            this.transitPreviewToNull();
        } else {
            this.logger.debug("No explicit implementation for state transition from " + this.currentVisualState
                    + " to " + newVisualState);
        }

        if (newVisualItem != null) {
            // put to top
            newVisualItem.putToTop();
        }

        // reset interaction time in directly connected nodes
        this.resetInteractionTimeInNonMicroInLevel(1);

        // update the layout radius
        this.updateLayoutRadius();
    }

    /**
     * Replaces the one visual component by the other in the layout of all
     * connected nodes.
     *
     * @param one
     *            Component to replace
     * @param other
     *            Replacement
     */
    private void replaceInConnectedComponentsLayout(VisualItem<?> one, VisualItem<?> other) {
        Set<GraphNode> connectedNodes = this.getConnectedNodes();

        for (GraphNode node : connectedNodes) {
            node.replaceInLayout(one, other);
        }
    }

    /**
     * Replaces the one visual component by the other in the layout of this
     * component.
     *
     * @param one
     *            Component to replace
     * @param other
     *            Replacement
     */
    private void replaceInLayout(VisualItem<?> one, VisualItem<?> other) {
        if (this.circleLayout != null) {
            this.circleLayout.substituteLayoutedComponent(one, other);
            // put new visual component to visual top
            other.putToTop();
            this.destroyLayoutIfEmpty();
        }
    }

    /**
     * Makes the transition from preview to detail
     */
    private void transitPreviewToDetail() {

        // one step back in all connected nodes
        this.previousStateInAllNonMicroConnectedNodes(1);

        // reactivate nodes
        this.nextStateInAllVisualNullConnectedNodes();

        // reset interaction time in directly connected nodes
        this.resetInteractionTimeInLevel(1);

        // prepare related items
        this.prepareAndLayoutRelatedItems();

        // one step back in third level
        this.previousStateInNonMicroWithoutBackPropagation(3);

        // one more step back in third level
        this.previousStateInNonMicroWithoutBackPropagation(3);

        // one step back in second level
        this.previousStateInNonMicroWithoutBackPropagation(2);

        // ensure to close 4th level
        this.previousStateWithoutBackPropagation(4);
    }

    /**
     * Makes the transition from preview to no more visible
     */
    private void transitPreviewToNull() {
        // one step back in all directly connected nodes
        this.previousStateInConnectedNodes(1);

        List<VisualItem<?>> nonNull = this.getConnectedVisualComponents();

        // if there are no more components delete self
        if (this.circleLayout == null || nonNull.isEmpty()) {
            // destroy all visual components
            this.destroyAllVisualComponents();

            // destroy layout
            if (this.circleLayout != null) {
                this.circleLayout.destroy();
                this.circleLayout = null;
            }

        } else {
            // keep old visual state
            this.currentVisualState = VisualState.PREVIEW;
        }
    }

    /**
     * Destroys all visual components of this node
     */
    private void destroyAllVisualComponents() {
        // duplicate collection for iteration
        Collection<VisualItem<?>> destroyList = new LinkedList<>(this.visualComponentsInState.values());
        // clear list, items will be destroy in following loop
        this.visualComponentsInState.clear();
        for (VisualItem<?> toDestroy : destroyList) {
            this.removeFromConnectedLayouts(toDestroy);
            this.removeFromLayout(toDestroy);
            // let creator destroy component
            this.graph.destroyVisualItem(toDestroy);
        }
    }

    /**
     * Prepares an layouts related items only if needed
     */
    private void prepareAndLayoutRelatedItems() {
        int neededNewRelatedItems = this.graph.getMaxNumberOfConnectedNodes() - this.edges.size();

        if (neededNewRelatedItems > 0) {

            // get selected set of related items to connect according to the
            // configuration considering already connected related items
            Set<Item> selectedRelatedItemsToConnect = this.graph.getSelectedRelatedItemsToConnectFor(this.dataItem,
                    this.getConnectedDataItems());

            // create and connect new items
            this.connectItems(selectedRelatedItemsToConnect);
        }

        // layout only if this node has enough connected nodes
        if (this.edges.size() > this.graph.getNodesForLayout() || !this.isContainedInOtherLayouts()) {
            // disconnect main component from other layouts
            this.disconnectFromConnectedLayouts(this.getCurrentVisualComponent());

            // now layout all connected nodes
            this.layoutConnectedNodes();
        }
    }

    /**
     * Returns true, if this node will currently be layouted by another node.
     *
     * @return True, if this node will currently be layouted by another node.
     */
    private boolean isContainedInOtherLayouts() {
        Set<GraphNode> connectedNodes = this.getConnectedNodes();

        VisualItem<?> currentVisualComponent = this.getCurrentVisualComponent();
        if (currentVisualComponent == null) {
            // no visualization -> so not layouted
            return false;
        }

        // disconnect in all connected nodes
        for (GraphNode node : connectedNodes) {
            if (node.doesLayout(currentVisualComponent)) {
                return true;
            }
        }

        // no other node layouts this node
        return false;
    }

    /**
     * Returns true if the given visual component will be layouted by this node.
     *
     * @param visualComponent
     *            Visual component to check
     *
     * @return True if the given visual component will be layouted by this node.
     */
    private boolean doesLayout(VisualItem<?> visualComponent) {
        if (visualComponent == null || this.circleLayout == null) {
            // no layouting
            return false;
        }

        // ask layouter
        return this.circleLayout.doesLayout(visualComponent);
    }

    /**
     * Disconnects the given visual component from the layout of all connected
     * nodes
     *
     * @param visualComponent
     *            Visual component to disconnect
     */
    private void disconnectFromConnectedLayouts(VisualItem<?> visualComponent) {
        Set<GraphNode> connectedNodes = this.getConnectedNodes();

        // disconnect in all connected nodes
        for (GraphNode node : connectedNodes) {
            node.disconnectFromLayout(visualComponent);
        }
    }

    /**
     * Disconnects the given visual component from the layout of this node
     *
     * @param component
     *            Visual component to disconnect
     */
    private void disconnectFromLayout(VisualItem<?> component) {
        if (this.circleLayout != null) {
            // deactivate the forces
            this.circleLayout.deactivateLayoutedComponent(component);
        }
    }

    /**
     * Removes the given visual component from the layout of all connected nodes
     *
     * @param visualComponent
     *            Visual component to disconnect
     */
    private void removeFromConnectedLayouts(VisualItem<?> visualComponent) {
        Set<GraphNode> connectedNodes = this.getConnectedNodes();

        // disconnect in all connected nodes
        for (GraphNode node : connectedNodes) {
            node.removeFromLayout(visualComponent);
        }
    }

    /**
     * Removes the given visual component from the layout of this node
     *
     * @param component
     *            Visual component to disconnect
     */
    private void removeFromLayout(VisualItem<?> component) {
        if (this.circleLayout != null) {
            this.circleLayout.removeLayoutedComponent(component);
            this.destroyLayoutIfEmpty();
        }
    }

    /**
     * Destroys the layout if there are no more layouted components
     *
     * @return Returns true if layout was destroyed
     */
    private boolean destroyLayoutIfEmpty() {
        if (this.circleLayout != null && this.circleLayout.isActiveLayout()) {
            return false;
        }

        if (this.circleLayout != null) {
            // no more layout so destroy
            this.circleLayout.destroy();
            this.circleLayout = null;
        }

        return true;
    }

    /**
     * Makes the transition from detail to preview
     */
    private void transitDetailToPreview() {
        // previousStateInConnectedNodes(1);
        this.previousStateInAllNonMicroConnectedNodes(1);
    }

    /**
     * Calls previous states on all connected nodes that are not in micro state
     *
     * @param level
     *            The graph level to call previous
     */
    private void previousStateInAllNonMicroConnectedNodes(int level) {
        // go to previous state in all connected nodes
        Set<GraphNode> connectedNodes = this.getConnectedNodesInLevel(level);

        for (GraphNode node : connectedNodes) {
            if (!(node.currentVisualState instanceof Micro)) {
                // go to previous state
                node.previousState();
            }
        }
    }

    /**
     * Calls previous states on all connected nodes that are not in micro state
     */
    private void nextStateInAllVisualNullConnectedNodes() {
        // go to previous state in all connected nodes
        Set<GraphNode> connectedNodes = this.getConnectedNodes();

        for (GraphNode node : connectedNodes) {
            if ((node.currentVisualState == null)) {
                // go to next state to activate
                node.nextState();
            }
        }
    }

    /**
     * Calls previous states on all connected node
     */
    private void previousStateInConnectedNodes(int level) {
        // get nodes in level
        Set<GraphNode> connectedNodes = this.getConnectedNodesInLevel(level);

        for (GraphNode node : connectedNodes) {
            // go to previous state
            node.previousState();
        }
    }

    /**
     * Calls previous state in all non micro nodes of the given level without
     * back propagation
     *
     * @param level
     *            Level to go back on step
     */
    private void previousStateInNonMicroWithoutBackPropagation(int level) {
        // avoid propagation changes in one level beyond
        this.preventLevelFromTransition(level - 1);
        try {
            // one step back in level
            this.previousStateInAllNonMicroConnectedNodes(level);
        } catch (Exception e) {
            // simply continue
        } finally {
            // ensure to always reactivate transitions
            this.allowTransitionsInLevel(level - 1);
        }
    }

    /**
     * Calls previous state in all non micro nodes of the given level without
     * back propagation
     *
     * @param level
     *            Level to go back on step
     */
    private void previousStateWithoutBackPropagation(int level) {
        // avoid propagation changes in one level beyond
        this.preventLevelFromTransition(level - 1);
        try {
            // one step back in level
            this.previousStateInConnectedNodes(level);
        } catch (Exception e) {
            // simply continue
        } finally {
            // ensure to always reactivate transitions
            this.allowTransitionsInLevel(level - 1);
        }
    }

    /**
     * Prevents all nodes in the level from transitions.
     *
     * @param level
     *            Level to prevent transitions in
     */
    private void preventLevelFromTransition(int level) {
        // valid for level one and higher
        if (level <= 0) {
            return;
        }

        // prevent in all connected nodes of level
        Set<GraphNode> connectedNodes = this.getConnectedNodesInLevel(level);

        for (GraphNode node : connectedNodes) {
            node.preventFromTranisition();
        }
    }

    /**
     * Prevents this node from any transition
     */
    private void preventFromTranisition() {
        this.inTransition = true;
    }

    /**
     * Allows transitions in all nodes of the given level.
     *
     * @param level
     *            Level to allow transitions in
     */
    private void allowTransitionsInLevel(int level) {
        // valid for level one and higher
        if (level <= 0) {
            return;
        }

        // allow in all connected nodes of level
        Set<GraphNode> connectedNodes = this.getConnectedNodesInLevel(level);

        for (GraphNode node : connectedNodes) {
            node.allowTranisition();
        }
    }

    /**
     * Allows transitions in this node.
     */
    private void allowTranisition() {
        this.inTransition = false;
    }

    /**
     * Layouts the visual components of the connected nodes
     */
    private void layoutConnectedNodes() {

        boolean oldLayout = false;

        // destroy old layout
        if (this.circleLayout != null) {
            this.circleLayout.destroy();
            oldLayout = true;
        }
        this.circleLayout = null;

        Set<GraphNode> connectedNodes = this.getConnectedNodes();

        // create new layout
        if (this.circleLayout == null && !connectedNodes.isEmpty()) {
            // calculate start angle from first connected component
            GraphNode firstNode = connectedNodes.iterator().next();
            double startAngle = this.getAngleTo(firstNode);

            if (!oldLayout) {
                // position all connected nodes to the position of this node
                this.positionAllConnectedNodes();
            }

            // create new layout
            if (CommunityMirror.getConfiguration().getString("theme.graph.layouter") != null) {
                String scheduleReaderClassName = CommunityMirror.getConfiguration().getString("theme.graph.layouter");
                try {
                    if (!scheduleReaderClassName.contains(".")) {
                        scheduleReaderClassName = "org.sociotech.communitymirror.graph.layout."
                                + scheduleReaderClassName;
                    }
                    this.circleLayout = (CircleNodeSpringLayouter) Class.forName(scheduleReaderClassName)
                            .getConstructor(List.class, VisualItem.class, double.class, BaseSpringSystem.class,
                                    VisualGroup.class, Group.class, double.class, double.class, double.class,
                                    double.class, double.class, boolean.class, String.class, double.class)
                            .newInstance(this.getConnectedVisualComponents(), this.getCurrentVisualComponent(),
                                    this.graph.getRadius(), this.graph.getSpringSystem(), this.graph,
                                    this.graph.getBackgroundParent(), startAngle,
                                    this.graph.getMicroSpringLengthFactor(), this.graph.getPreviewSpringLengthFactor(),
                                    this.graph.getDetailSpringLengthFactor(), this.graph.getBackgroundCircleFactor(),
                                    this.graph.isRepositionAfterLayout(), this.graph.getBackgroundCircleColor(),
                                    this.graph.getBackgroundCircleOpacity());
                } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                        | InvocationTargetException | NoSuchMethodException | SecurityException
                        | ClassNotFoundException e) {
                    this.logger.warn("Invalid class name given for graph layouter, falling back to default.");
                }
            }
            if (this.circleLayout == null) {
                this.circleLayout = new CircleNodeSpringLayouter(this.getConnectedVisualComponents(),
                        this.getCurrentVisualComponent(), this.graph.getRadius(), this.graph.getSpringSystem(),
                        this.graph, this.graph.getBackgroundParent(), startAngle,
                        this.graph.getMicroSpringLengthFactor(), this.graph.getPreviewSpringLengthFactor(),
                        this.graph.getDetailSpringLengthFactor(), this.graph.getBackgroundCircleFactor(),
                        this.graph.isRepositionAfterLayout(), this.graph.getBackgroundCircleColor(),
                        this.graph.getBackgroundCircleOpacity());
            }

            // and update to create animation
            this.updateLayoutRadius();
        }
    }

    /**
     * Sets the position of the current visual components of all connected nodes
     * to the position
     * of this current visual component.
     */
    private void positionAllConnectedNodes() {
        VisualItem<?> current = this.getCurrentVisualComponent();

        if (current == null) {
            // skip
            return;
        }

        List<VisualItem<?>> connectedVisualComponents = this.getConnectedVisualComponents();

        for (VisualItem<?> connected : connectedVisualComponents) {
            // set to position of this current visual component
            if (connected.getVisualState() instanceof Micro) {
                connected.setPositionX(current.getPositionX());
                connected.setPositionY(current.getPositionY());
            }
        }
    }

    /**
     * Updates radius of the layout based on visual state
     */
    private void updateLayoutRadius() {
        if (this.circleLayout != null) {
            // set radius again to have animation
            this.circleLayout.setRadius(this.calculateCurrentLayoutRadius());
        }
    }

    /**
     * Calculates the layout radius for the current visual state.
     *
     * @return The current layout radius
     */
    private double calculateCurrentLayoutRadius() {
        double radius = this.graph.getRadius();
        if (this.currentVisualState instanceof Micro) {
            radius *= this.graph.getMicroRadiusFactor();
        } else if (this.currentVisualState instanceof Preview) {
            radius *= this.graph.getPreviewRadiusFactor();
        }

        return radius;
    }

    /**
     * Returns the angle to the node
     *
     * @param node
     *            Not to get the angle to.
     *
     * @return The angle to this node.
     */
    private double getAngleTo(GraphNode node) {
        GraphEdge edgeToNode = this.getEdgeTo(node);
        if (edgeToNode == null) {
            return 0.0;
        }

        if (edgeToNode.hasAngle()) {
            return edgeToNode.getAngle(this);
        }

        double angle = this.calculateStartAngle(this.getCurrentVisualComponent(), node.getCurrentVisualComponent());
        // keep angle in edge
        edgeToNode.setAngle(angle, this);

        return angle;
    }

    /**
     * Calculates the start angle for layout between the center and the first
     * component based on the spring connector positions.
     *
     * @param center
     *            Center component
     * @param first
     *            First layout component
     * @return The start angle for layouting
     */
    private double calculateStartAngle(VisualComponent center, VisualComponent first) {
        if (first == null) {
            return 0.0;
        }

        double x = first.getPositionX();
        double y = first.getPositionY();

        if (x == 0 && y == 0) {
            // initial position, so return random angle
            return Math.random() * 2 * Math.PI;
        }

        double dX = 0.0;
        double dY = 0.0;

        try {
            dX = first.getSpringConnectorXProperty().get() - center.getSpringConnectorXProperty().get();
            dY = first.getSpringConnectorYProperty().get() - center.getSpringConnectorYProperty().get();

        } catch (Exception e) {
            // continue with default
            this.logger.warn("Could not calculate angle between " + center + " and " + first + " due to exception "
                    + e.getMessage());
            // e.printStackTrace();
        }

        double length = Math.hypot(dX, dY);

        // same position -> so no angle
        if (length == 0.0) {
            return 0.0;
        }

        // normalize
        dX /= length;

        double angleBetween = Math.acos(dX);

        if (dY > 0) {
            // 1st and 2nd quadrant
            return angleBetween;
        } else {
            // 3rd and 4th quadrant
            // 360 -
            return 2 * Math.PI - angleBetween;
        }
    }

    /**
     * Collects the visual components of all connected nodes.
     *
     * @return The set of connected visual components items
     */
    private List<VisualItem<?>> getConnectedVisualComponents() {
        // get directly connected visual components
        return this.getConnectedVisualComponentsInLevel(1);
    }

    /**
     * Collects the visual components of connected nodes in the given level.
     *
     * @param level
     *            Graph level
     *
     * @return The set of connected visual components items
     */
    private List<VisualItem<?>> getConnectedVisualComponentsInLevel(int level) {
        List<VisualItem<?>> connectedVisualComponents = new LinkedList<>();
        Set<GraphNode> connectedNodes = this.getConnectedNodesInLevel(level);

        for (GraphNode node : connectedNodes) {
            VisualItem<?> currentVisualComponent = node.getCurrentVisualComponent();
            if (currentVisualComponent != null) {
                connectedVisualComponents.add(currentVisualComponent);
            }
        }

        return connectedVisualComponents;
    }

    // /**
    // * Selects the number of needed items from the given set and connects them
    // as new nodes
    // *
    // * @param relatedItems Set of related items.
    // * @param neededNewRelatedItems Number of items to connect
    // */
    // private void connectNNewItems(Set<Item> relatedItems, int
    // neededNewRelatedItems) {
    // Iterator<Item> relatedItemsIt = relatedItems.iterator();
    //
    // while(relatedItemsIt.hasNext() && neededNewRelatedItems > 0) {
    // GraphNode newNode = graph.createNode(relatedItemsIt.next());
    // // directly transit to next state
    // newNode.nextState();
    //
    // // keep connection
    // this.connectTo(newNode);
    // neededNewRelatedItems--;
    // }
    //
    // }

    /**
     * Connects the items of the given set of related items as nodes to this
     * node.
     *
     * @param relatedItems
     *            Set of related items.
     */
    private void connectItems(Set<Item> relatedItems) {
        Iterator<Item> relatedItemsIt = relatedItems.iterator();

        while (relatedItemsIt.hasNext()) {
            GraphNode newNode = this.graph.createNode(relatedItemsIt.next());
            if (newNode == null) {
                continue;
            }
            // directly transit to next state
            newNode.nextState();

            // keep connection
            this.connectTo(newNode);
        }
    }

    /**
     * Collects the data items of all connected nodes.
     *
     * @return The set of connected data items
     */
    private Set<Item> getConnectedDataItems() {
        Set<Item> connectedDataItems = new LinkedHashSet<>(this.edges.size());
        Set<GraphNode> connectedNodes = this.getConnectedNodes();

        for (GraphNode node : connectedNodes) {
            connectedDataItems.add(node.dataItem);
        }

        return connectedDataItems;
    }

    /**
     * Creates a blank-delimited, concatenated string of the data items ident
     * (e. g. a_123) of all connected nodes.
     *
     * @return A string of connected data items ident
     */
    public String getConnectedDataItemsIdentString() {
        Set<GraphNode> connectedNodes = this.getConnectedNodes();
        String connectedDataItemsIdentString = "";

        for (GraphNode node : connectedNodes) {
            connectedDataItemsIdentString += " " + node.dataItem.getIdent();
        }

        return connectedDataItemsIdentString;
    }

    /**
     * Transfers visual attributes form one to the other.
     *
     * @param one
     *            Visual item to transfer from
     * @param other
     *            Visual item to transfer to
     */
    public static void transferAttributes(VisualComponent one, VisualComponent other) {
        if (one == null || other == null) {
            // nothing to transfer
            return;
        }

        // copy rotation
        other.setRotate(one.getRotate());

        // copy scale - do not copy! when changing to graph mode or so, visual
        // items should scale to origin again
        // other.setScaleX(one.getScaleX());
        // other.setScaleY(one.getScaleY());

        // copy position
        other.setPositionX(one.getPositionX());
        other.setPositionY(one.getPositionY());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.sociotech.communitymirror.view.flow.graph.GraphElement#destroy()
     */
    @Override
    public void destroy() {
        this.destroyAllVisualComponents();
        this.destroyLayoutIfEmpty();
    }

    /**
     * Returns true if this node is not visible.
     *
     * @return True if this node is not visible.
     */
    public boolean isNonVisible() {
        // node is nonvisible if there is no visual component
        return this.visualComponentsInState.isEmpty();
    }

    @Override
    public String toString() {
        return dataItem == null ? "empty nodde" : dataItem.toString();
    }

}
