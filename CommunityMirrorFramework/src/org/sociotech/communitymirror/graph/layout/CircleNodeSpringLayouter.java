
package org.sociotech.communitymirror.graph.layout;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.VisualGroup;
import org.sociotech.communitymirror.visualitems.VisualItem;
import org.sociotech.communitymirror.visualitems.components.spring.SpringConnectable;
import org.sociotech.communitymirror.visualitems.components.spring.VisualSpringConnection;
import org.sociotech.communitymirror.visualstates.Detail;
import org.sociotech.communitymirror.visualstates.Micro;
import org.sociotech.communitymirror.visualstates.Preview;

import com.facebook.rebound.BaseSpringSystem;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;

/**
 * A circle layouter for visual components
 *
 * @author Peter Lachenmaier
 */
public class CircleNodeSpringLayouter {

    /**
     * The list of layouted components
     */
    protected List<VisualItem<?>> layoutedComponents;

    /**
     * The center in the circle layout
     */
    protected VisualItem<?> center;

    /**
     * The radius of the circle layout
     */
    protected double radius;

    /**
     * Local reference to the spring system.
     */
    private BaseSpringSystem springSystem;

    /**
     * VisualGroup that contains all created springs of this CircleVisualGroup.
     */
    private VisualGroup springGroup = new VisualGroup();

    /**
     * The list of all created springs
     */
    private List<VisualSpringConnection> springs = new LinkedList<>();

    /**
     * The startAngle for the layout
     */
    private double startAngle;

    /**
     * Circle in the background.
     */
    private Circle backgroundCircle;

    /**
     * A listener for zoom changes of the central component
     */
    private ChangeListener<Number> zoomChangeListener;

    /**
     * A set of components that are deactivated for spring forces.
     */
    private Set<SpringConnectable> deactivatedComponents;

    /**
     * The factor for the spring length from or to components in micro state.
     */
    private final double microSpringLengthFactor;

    /**
     * The factor for the spring length from or to components in preview state.
     */
    private final double previewSpringLengthFactor;

    /**
     * The factor for the spring length from or to components in detail state.
     */
    private final double detailSpringLengthFactor;

    /**
     * The factor of the radius of the background circle relative to layout
     * radius.
     */
    private final double backgroundCircleFactor;

    /**
     * Whether to reposition components after layouting -> leads to "animation"
     * of opening.
     */
    protected final boolean repositionAfterLayout;

    /**
     * The color for the background circle
     */
    private String backgroundCircleColor;

    /**
     * The opacity value for the backgroundcircle
     */
    private double backgroundCircleOpacity;

    /**
     * The parent for the background circle
     */
    private final Group backgroundParent;

    /**
     * Creates a visual group with circular layout for the given list of
     * components and springs.
     *
     * @param components
     *            Components to align around center
     * @param center
     *            Component in the center
     * @param radius
     *            Radius of the layout circle
     * @param springSystem
     *            Reference to the spring system to be used for spring
     *            connections
     * @param parentForSprings
     *            The parent visual group for springs
     * @param parentForBackground
     *            The parent (FX) for the background
     * @param startAngle
     *            The angle to start the layout in radians
     * @param microSpringLengthFactor
     *            The factor for the spring length from or to components in
     *            micro state.
     * @param previewSpringLengthFactor
     *            The factor for the spring length from or to components in
     *            preview state.
     * @param detailSpringLengthFactor
     *            The factor for the spring length from or to components in
     *            detail state.
     * @param backgroundCircleFactor
     *            The factor for the to scale the background circle depending on
     *            radius
     * @param reposition
     *            Whether to reposition the components after layout -> leads to
     *            "animation" of opening
     * @param backgroundCircleColor
     *            The (web)-color of the background circle
     * @param backgroundCircleOpacity
     *            The opacity of the background circle
     */
    public CircleNodeSpringLayouter(List<VisualItem<?>> components, VisualItem<?> center, double radius,
            BaseSpringSystem springSystem, VisualGroup parentForSprings, Group parentForBackground, double startAngle,
            double microSpringLengthFactor, double previewSpringLengthFactor, double detailSpringLengthFactor,
            double backgroundCircleFactor, boolean reposition, String backgroundCircleColor,
            double backgroundCircleOpacity) {
        this.layoutedComponents = components;
        this.center = center;
        this.radius = radius;
        this.springSystem = springSystem;
        this.startAngle = startAngle;
        this.microSpringLengthFactor = microSpringLengthFactor;
        this.previewSpringLengthFactor = previewSpringLengthFactor;
        this.detailSpringLengthFactor = detailSpringLengthFactor;
        this.backgroundCircleFactor = backgroundCircleFactor;
        this.repositionAfterLayout = reposition;
        this.backgroundCircleColor = backgroundCircleColor;
        this.backgroundCircleOpacity = backgroundCircleOpacity;
        this.backgroundParent = parentForBackground;

        // new set for deactivated
        this.deactivatedComponents = new HashSet<>();

        // init background circle
        this.initBackgroundCircle();

        // add background to background group
        this.backgroundParent.getChildren().add(this.backgroundCircle);

        // add spring group
        parentForSprings.addVisualComponent(this.springGroup, true);
        this.springGroup.init();

        // init zoom listener
        this.initZoomListener();

        // init the layout
        this.initLayout();
    }

    /**
     * Inits the circle in the background
     */
    private void initBackgroundCircle() {

        RadialGradient backgroundPaint = new RadialGradient(0.0, 0, 0.5, 0.5, 1, true, CycleMethod.NO_CYCLE,
                new Stop(0, Color.web(this.backgroundCircleColor)), new Stop(0.9, Color.TRANSPARENT));

        // add invisible background circle to receive correct interaction events
        this.backgroundCircle = new Circle();
        this.backgroundCircle.setRadius(this.getRadius());
        // backgroundCircle.setFill(Color.web(backgroundCircleColor));
        this.backgroundCircle.setFill(backgroundPaint);
        this.backgroundCircle.setOpacity(this.backgroundCircleOpacity);
        this.backgroundCircle.setVisible(true);

        this.backgroundCircle.centerXProperty().bindBidirectional(this.center.getSpringConnectorXProperty());
        this.backgroundCircle.centerYProperty().bindBidirectional(this.center.getSpringConnectorYProperty());
        // bind also scale factor of background
        this.backgroundCircle.scaleXProperty().bindBidirectional(this.center.scaleXProperty());
        this.backgroundCircle.scaleYProperty().bindBidirectional(this.center.scaleYProperty());
        this.backgroundCircle.getStyleClass().add("cmf-graph-background-circle");

    }

    /**
     * Returns the central component of this circular visual group.
     */
    public VisualItem<?> getCentralComponent() {
        return this.center;
    }

    /**
     * Connects the given component to the center component.
     *
     * @param component
     *            Component to connect
     */
    protected VisualSpringConnection connectComponentToCenter(SpringConnectable component) {
        VisualSpringConnection springConnection = VisualSpringConnection.connect(this.center, component,
                this.springSystem, true);

        // Debug to show the position alignment springs
        // springConnection.getHiddenLayoutSpring().getLine().setStroke(Color.RED);
        // springGroup.addNode(springConnection.getHiddenLayoutSpring().getLine());

        if (this.deactivatedComponents.contains(component)) {
            // deactivate spring forces
            springConnection.setDeactivated(true);
        }

        // keep reference
        this.springs.add(springConnection);

        // bind spring opacity to opacity of connected component
        if (component instanceof VisualComponent) {
            springConnection.getOpacityProperty().bind(((VisualComponent) component).getOpacityProperty());
        }

        // and as visual component and node to springGroup
        this.springGroup.addVisualComponent(springConnection, true);

        return springConnection;
    }

    /**
     * Removes a given spring connection from this circle visual group.
     */
    private void removeSpringConnection(VisualSpringConnection spring) {
        this.springGroup.removeVisualComponent(spring, true);
        this.springs.remove(spring);
        spring.disconnect();
    }

    /**
     * Removes a given visual component from the list of layouted visual
     * components of this visual group.
     */
    public void removeLayoutedComponent(VisualItem<?> component) {

        // remove layouted component from list of layouted components
        this.layoutedComponents.remove(component);

        // remove visual spring connection to deleted layouted component
        List<VisualSpringConnection> springIterationList = new LinkedList<>(this.springs);
        for (VisualSpringConnection connection : springIterationList) {
            if (connection.getStartC().equals(component) || connection.getEndC().equals(component)) {
                this.removeSpringConnection(connection);
                break;
            }
        }

        // remove from deactivation list
        this.deactivatedComponents.remove(component);
    }

    /**
     * Deactivates the spring forces of this layouter to the given visual
     * component.
     *
     * @return True if the component was contained in the layout and could be
     *         deactivated.
     */
    public boolean deactivateLayoutedComponent(VisualItem<?> component) {

        if (component == null) {
            return false;
        }

        // add to deactivation list
        this.deactivatedComponents.add(component);

        List<VisualSpringConnection> springIterationList = new LinkedList<>(this.springs);

        // find spring connection
        for (VisualSpringConnection connection : springIterationList) {
            if (connection.getStartC().equals(component) || connection.getEndC().equals(component)) {
                connection.setDeactivated(true);
                return connection.isDeactivated();
            }
        }

        return false;
    }

    /**
     * Reactivates the spring forces of this layouter to the given visual
     * component.
     *
     * @return True if the component was contained in the layout and could be
     *         reactivated.
     */
    public boolean reactivateLayoutedComponent(VisualItem<?> component) {

        if (component == null) {
            return false;
        }

        // remove from deactivation list
        this.deactivatedComponents.remove(component);
        List<VisualSpringConnection> springIterationList = new LinkedList<>(this.springs);

        // find spring connection
        for (VisualSpringConnection connection : springIterationList) {
            if (connection.getStartC().equals(component) || connection.getEndC().equals(component)) {
                connection.setDeactivated(false);
                this.updateCalculatedRadius();
                return !connection.isDeactivated();
            }
        }

        return false;
    }

    /**
     * Adds the given component additionally to the layout.
     *
     * @param component
     *            Additional component to layout
     */
    public void addLayoutedComponent(VisualItem<?> component) {

        // add the given layouted component to the list of layouted components
        // of this circle visual group
        this.layoutedComponents.add(component);

        // repostion all elements without connection
        this.layoutCircular(false);

        double oldX = component.getSpringConnectorXProperty().get();
        double oldY = component.getSpringConnectorYProperty().get();

        // connect the new component
        this.connectComponentToCenter(component);

        // reset position to smoothly change to new position with spring force
        component.getSpringConnectorXProperty().set(oldX);
        component.getSpringConnectorYProperty().set(oldY);
    }

    /**
     * Substitutes a given old component by a given new component within the
     * layout. That means the old one is removed and the new one is added at the
     * position of the old one to the list of components.
     *
     * @param oldComponent
     *            : The component that has to be removed
     * @param newComponent
     *            : The component that has to be added instead
     */
    public void substituteLayoutedComponent(VisualItem<?> oldComponent, VisualItem<?> newComponent) {
        if (oldComponent == this.center) {
            // replaced center component
            this.replaceCenter(newComponent);
            return;
        }

        if (newComponent == null) {
            // simply delete old
            this.removeLayoutedComponent(oldComponent);
            return;
        }

        if (oldComponent != null && this.layoutedComponents.contains(oldComponent)) {
            // get position of old component within the list of components
            int index = this.layoutedComponents.indexOf(oldComponent);

            // add newComponent to the list of components at position of
            // oldComponent
            this.layoutedComponents.add(index, newComponent);

            // remove old component from list of layouted components
            this.layoutedComponents.remove(oldComponent);

            // update spring
            for (VisualSpringConnection connection : this.springs) {
                if (connection.getEndC().equals(oldComponent)) {
                    connection.replaceEnd(newComponent);
                    break;
                }
            }

            // remove from deactivation list
            if (this.deactivatedComponents.remove(oldComponent)) {
                // if old was deactivated then deactivate new
                this.deactivatedComponents.add(newComponent);
            }

            // set newComponent to screen position of oldComponent
            newComponent.getSpringConnectorXProperty().set(oldComponent.getSpringConnectorXProperty().get());
            newComponent.getSpringConnectorYProperty().set(oldComponent.getSpringConnectorYProperty().get());

        } else {
            // simply add new
            this.addLayoutedComponent(newComponent);
        }

        // put to top
        newComponent.putToTop();
    }

    /**
     * Replaces the center node by the given new center.
     *
     * @param newCenter
     *            New center
     */
    private void replaceCenter(VisualItem<?> newCenter) {
        // remove old center zoom listener
        if (this.zoomChangeListener != null) {
            this.center.scaleXProperty().removeListener(this.zoomChangeListener);
        }

        // unbind background position
        this.backgroundCircle.centerXProperty().unbindBidirectional(this.center.getSpringConnectorXProperty());
        this.backgroundCircle.centerYProperty().unbindBidirectional(this.center.getSpringConnectorYProperty());
        // unbind scale factor
        this.backgroundCircle.scaleXProperty().unbindBidirectional(this.center.scaleXProperty());
        this.backgroundCircle.scaleYProperty().unbindBidirectional(this.center.scaleYProperty());

        // set new center
        this.center = newCenter;

        // init zoom listener for new center
        this.initZoomListener();

        // remove visual spring connection to old center and create new center
        List<VisualSpringConnection> springIterationList = new LinkedList<>(this.springs);
        for (VisualSpringConnection connection : springIterationList) {
            // keep connected component
            SpringConnectable connectedComponent = connection.getEndC();
            // and the old length
            double springLength = connection.getRealLength();

            // destroy old connection
            this.removeSpringConnection(connection);

            // connect to new center
            VisualSpringConnection newSpringConnection = this.connectComponentToCenter(connectedComponent);
            // set old length
            newSpringConnection.setRealLength(springLength);
        }

        // rebind background position to new center
        this.backgroundCircle.centerXProperty().bindBidirectional(this.center.getSpringConnectorXProperty());
        this.backgroundCircle.centerYProperty().bindBidirectional(this.center.getSpringConnectorYProperty());
        // rebind scale factor
        this.backgroundCircle.scaleXProperty().bindBidirectional(this.center.scaleXProperty());
        this.backgroundCircle.scaleYProperty().bindBidirectional(this.center.scaleYProperty());

    }

    /**
     * Returns whether this layout is active or not (Any springs is applying
     * force)
     */
    public boolean isActiveLayout() {
        for (VisualSpringConnection spring : this.springs) {
            if (!spring.isDeactivated()) {
                return true;
            }
        }

        // all springs are deactivated
        return false;
    }

    /**
     * Inits the listener for the zoom state of the center component
     */
    private void initZoomListener() {
        this.zoomChangeListener = new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {
                // update radius
                CircleNodeSpringLayouter.this.updateCalculatedRadius();
                // adapt zoom factor for connected micros
                CircleNodeSpringLayouter.this.adaptZoomFactorForConnectedMicroComponents();
            }
        };

        this.center.scaleXProperty().addListener(this.zoomChangeListener);
        // this.center.scaleYProperty().addListener(zoomChangeListener);
    }

    /**
     * Adapts the zoom factor of all layouted micro elements to the center
     * component
     */
    private void adaptZoomFactorForConnectedMicroComponents() {
        this.center.getScaleX();
        for (VisualItem<?> component : this.layoutedComponents) {
            if (this.deactivatedComponents.contains(component)) {
                // skip deactivated components
                continue;
            }
            if ((component instanceof VisualItem<?>)
                    && (((VisualItem<?>) component).getVisualState() instanceof Micro)) {
                double scale = this.center.getScaleX();
                component.setScaleX(scale);
                component.setScaleY(scale);
            }
        }
    }

    /**
     * Updates the length of all springs depending on the calculated radius.
     */
    protected void updateCalculatedRadius() {

        double maxLength = 0.0;

        // change all springs
        for (VisualSpringConnection spring : this.springs) {
            // calculate length based on connected components
            // safe cast, cause we only layout visual items
            double springLenght = this.calculateDistanceBetween((VisualItem<?>) spring.getStartC(),
                    (VisualItem<?>) spring.getEndC());
            spring.setRealLength(springLenght);
            if (springLenght > maxLength) {
                maxLength = springLenght;
            }

        }

        // set radius for background to the longest spring
        this.backgroundCircle.setRadius((maxLength / this.center.getScaleX()) * this.backgroundCircleFactor);
    }

    /**
     * Calculates the layout distance between two visual items based on their
     * visual state.
     *
     * @param first
     *            First component
     * @param second
     *            second component
     * @return The layout distance between them
     */
    protected double calculateDistanceBetween(VisualItem<?> first, VisualItem<?> second) {

        double newLength = this.center.getScaleX() * this.radius;

        // multiply length with visual state specific factors
        if (first.getVisualState() instanceof Micro) {
            newLength *= this.microSpringLengthFactor;
        } else if (first.getVisualState() instanceof Preview) {
            newLength *= this.previewSpringLengthFactor;
        } else if (first.getVisualState() instanceof Detail) {
            newLength *= this.detailSpringLengthFactor;
        }

        // also for second connected component
        if (second.getVisualState() instanceof Micro) {
            newLength *= this.microSpringLengthFactor;
        } else if (second.getVisualState() instanceof Preview) {
            newLength *= this.previewSpringLengthFactor;
        } else if (second.getVisualState() instanceof Detail) {
            newLength *= this.detailSpringLengthFactor;
        }

        return newLength;
    }

    /**
     * Creates spring connections and inits the layout
     */
    private void initLayout() {
        // position all components with connection to center
        this.layoutCircular(true);

        // adapt zoom factor
        this.adaptZoomFactorForConnectedMicroComponents();

        // put all layouted components to top
        for (VisualItem<?> component : this.layoutedComponents) {
            component.putToTop();
        }
        this.center.putToTop();
    }

    /**
     * Returns the starting angle for the layout.
     *
     * @return The starting angle.
     */
    protected double getStartAngle() {
        return this.startAngle;
    }

    /**
     * Returns the angle between elements for the layout.
     *
     * @return The angle between elements.
     */
    protected double getBetweenAngle() {
        return 2 * Math.PI / this.layoutedComponents.size();
    }

    /**
     * Calculates the position and layouts all components.
     *
     * @param connectToCenter
     *            Wheter to connect layouted components to the center or not
     */
    private void layoutCircular(boolean connectToCenter) {

        // align around spring connector position
        double centerX = this.center.getSpringConnectorXProperty().get();
        double centerY = this.center.getSpringConnectorYProperty().get();

        double divAngle = this.getBetweenAngle();
        double curAngle = this.getStartAngle();

        double initialRadius = this.center.getScaleX() * this.radius;

        for (VisualItem<?> component : this.layoutedComponents) {

            initialRadius = this.calculateDistanceBetween(this.center, component);

            double xDiff = initialRadius * Math.cos(curAngle);
            double yDiff = initialRadius * Math.sin(curAngle);

            component.getSpringConnectorXProperty().get();
            component.getSpringConnectorYProperty().get();

            component.getSpringConnectorXProperty().set(centerX + xDiff);
            component.getSpringConnectorYProperty().set(centerY + yDiff);

            if (connectToCenter) {
                // connect
                this.connectComponentToCenter(component);

                if (this.repositionAfterLayout) {
                    // reset position to smoothly change to new position with
                    // spring force
                    // add some random positioning to let springs room to work
                    component.getSpringConnectorXProperty().set(centerX + xDiff + 75.0 * Math.random());
                    component.getSpringConnectorYProperty().set(centerY + yDiff + 75.0 * Math.random());
                }
            }

            curAngle += divAngle;

            if (Math.toDegrees(curAngle) > 360) {
                curAngle = Math.toRadians((Math.toDegrees(curAngle) - 360));
            }
        }
    }

    /**
     * Disconnects all springs
     */
    public void destroy() {

        // disconnect all springs
        for (VisualSpringConnection connection : this.springs) {
            connection.disconnect();
        }

        // clear deactivated
        this.deactivatedComponents.clear();

        // remove background
        this.backgroundParent.getChildren().remove(this.backgroundCircle);
    }

    /**
     * Returns the spring connections used in this group to keep the connection
     * between the components.
     *
     * @return All spring connections used in this group.
     */
    public List<VisualSpringConnection> getSprings() {
        return this.springs;
    }

    /**
     * Sets the radius of the layout. This changes the length of all springs
     * maintaining the layout.
     *
     * @param newRadius
     *            The new radius of the layout
     */
    public void setRadius(double newRadius) {
        this.radius = newRadius;
        this.updateCalculatedRadius();
    }

    /**
     * Returns the current radius of the layout.
     *
     * @return The current layout radius.
     */
    public double getRadius() {
        return this.radius;
    }

    /**
     * Gets the VisualGroup that contains all created springs of this
     * CircleVisualGroup.
     *
     * @return the springGroup
     */
    public VisualGroup getSpringGroup() {
        return this.springGroup;
    }

    /**
     * Sets the VisualGroup that contains all created springs of this
     * CircleVisualGroup.
     *
     * @param springGroup
     *            the springGroup to set
     */
    public void setSpringGroup(VisualGroup springGroup) {
        this.springGroup = springGroup;
    }

    /**
     * Returns true if the given visual component will be layouted by this
     * layouter.
     *
     * @param visualComponent
     *            Visual component to check
     *
     * @return True if the given visual component will be layouted by this
     *         layouter.
     */
    public boolean doesLayout(VisualItem<?> visualComponent) {
        return this.layoutedComponents.contains(visualComponent)
                && !this.deactivatedComponents.contains(visualComponent);
    }
}
