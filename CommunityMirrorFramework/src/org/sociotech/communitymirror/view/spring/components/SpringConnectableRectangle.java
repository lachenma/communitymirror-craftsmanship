package org.sociotech.communitymirror.view.spring.components;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.components.spring.SpringConnector;
import org.sociotech.communitymirror.visualitems.components.spring.VisualSpringConnection;

/**
 * The simple sample implementation of a visual component that can be
 * connected by a spring {@link VisualSpringConnection} by using a
 * spring connector on every side.
 *
 * @author Peter Lachenmaier
 *
 */
public class SpringConnectableRectangle extends VisualComponent {

	private double width = 60;
	private double height = 40;
	private Rectangle rect;
	private boolean fixed;

	private SpringConnector leftConnector;
	private SpringConnector rightConnector;
	private SpringConnector topConnector;
	private SpringConnector bottomConnector;

	/**
	 * Creates the component centered at the given position.
	 *
	 * @param centerX X of the center
	 * @param centerY Y of the center
	 */
	public SpringConnectableRectangle(double centerX, double centerY) {
		this(centerX, centerY, false);
	}

	/**
	 * Creates the component centered at the given position.
	 *
	 * @param centerX X of the center
	 * @param centerY Y of the center
	 * @param fixed True to fix the position
	 */
	public SpringConnectableRectangle(double centerX, double centerY, boolean fixed) {
		setCenterX(centerX);
		setCenterY(centerY);
		this.fixed = fixed;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onInit()
	 */
	@Override
	protected void onInit() {
		rect = new Rectangle();
		rect.setX(-width/2);
		rect.setY(-height/2);
		rect.setHeight(height);
		rect.setWidth(width);
		rect.setFill(Color.TRANSPARENT);
		rect.setStroke(Color.RED);

		this.addNode(rect);
	}

	/**
	 * Sets the center x.
	 *
	 * @param centerX The center x
	 */
	public void setCenterX(double centerX) {
		this.setLayoutX(centerX);
	}

	/**
	 * Sets the center y.
	 *
	 * @param centerY The center y
	 */
	public void setCenterY(double centerY) {
		this.setLayoutY(centerY);
	}


	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.springview.components.SpringConnectable#isFixed()
	 */
	@Override
	public boolean isFixed() {
		return fixed;
	}

	/**
	 * Returns the spring connector in the left center.
	 *
	 * @return The spring connector in the left center.
	 */
	public SpringConnector getLeftSpringConnector() {
		if(leftConnector == null) {
			// create only if used
			leftConnector = SpringConnector.createLeftCenterSpringConnector(this);
		}
		return leftConnector;
	}

	/**
	 * Returns the spring connector in the right center.
	 *
	 * @return The spring connector in the right center.
	 */
	public SpringConnector getRightSpringConnector() {
		if(rightConnector == null) {
			// create only if used
			rightConnector = SpringConnector.createRightCenterSpringConnector(this);
		}
		return rightConnector;
	}

	/**
	 * Returns the spring connector in the top center.
	 *
	 * @return The spring connector in the top center.
	 */
	public SpringConnector getTopSpringConnector() {
		if(topConnector == null) {
			// create only if used
			topConnector = SpringConnector.createTopCenterSpringConnector(this);
		}
		return topConnector;
	}

	/**
	 * Returns the spring connector in the bottom center.
	 *
	 * @return The spring connector in the bottom center.
	 */
	public SpringConnector getBottomSpringConnector() {
		if(bottomConnector == null) {
			// create only if used
			bottomConnector = SpringConnector.createBottomCenterSpringConnector(this);
		}
		return bottomConnector;
	}
}
