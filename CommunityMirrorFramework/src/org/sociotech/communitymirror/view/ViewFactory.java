
package org.sociotech.communitymirror.view;

import org.apache.commons.configuration2.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymashup.data.DataSet;
import org.sociotech.communitymirror.view.flow.FlowView;

import javafx.stage.Stage;

/**
 * Factory to produce views.
 *
 * @author Peter Lachenmaier, Martin Burkhard
 */
public class ViewFactory {

    private final Configuration configuration;
    private final DataSet dataSet;
    private Stage stage;

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger();

    public ViewFactory(Configuration configuration, DataSet dataSet, Stage stage) {
        this.configuration = configuration;
        this.dataSet = dataSet;
        this.stage = stage;
    }

    /**
     * Creates a new flow view.
     *
     * @return The flow view.
     */
    public FXView createFlowView() {
        return new FlowView(this.stage.getWidth(), this.stage.getHeight(), this.configuration, this.dataSet);
    }

    /**
     * Creates a new view depending on the given class name. Creates a default
     * view
     * ({@link org.sociotech.communitymirror.view.ViewFactory#createDefaultView()}
     * if no view can be created based on the
     * given class name.
     *
     * @param viewClassName
     *            Full class name of the view class.
     * @return The new view. Null in error case.
     */
    public FXView createView(String viewClassName) {
        if (viewClassName == null) {
            return null;
        }
        // if class name does not contain "." it has to be a name only - so use
        // old version of method
        if (viewClassName.indexOf(".") < 1) {
            this.logger.error(
                    "For creating views we need full class name! Using default view instead of " + viewClassName + ".");
            return this.createFlowView();
        }

        // viewClassName is a full class name ... so try to instantiate an
        // object
        try {
            // use constructor (double, double, Configuration, DataSet)
            // (stage.getWidth(), stage.getHeight(), configuration, dataSet);
            Object o = Class.forName(viewClassName)
                    .getConstructor(double.class, double.class, Configuration.class, DataSet.class)
                    .newInstance(this.stage.getWidth(), this.stage.getHeight(), this.configuration, this.dataSet);

            if (!(o instanceof FXView)) {
                this.logger.error(
                        "Instantiation of " + viewClassName + " did not return FXView object. Using default view.");
                o = this.createFlowView();
            }
            return (FXView) o;
        } catch (Exception e) {
            this.logger.error("Instantiation of " + viewClassName + " failed. Using default view.", e);
            // default
            return this.createFlowView();
        }
    }

}
