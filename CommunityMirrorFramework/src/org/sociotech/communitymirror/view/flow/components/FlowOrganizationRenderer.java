
package org.sociotech.communitymirror.view.flow.components;

import org.sociotech.communitymashup.data.Organisation;
import org.sociotech.communitymirror.view.flow.visualitems.organization.DetailOrganizationItem;
import org.sociotech.communitymirror.view.flow.visualitems.organization.MicroOrganizationItem;
import org.sociotech.communitymirror.view.flow.visualitems.organization.OrganizationItem;
import org.sociotech.communitymirror.view.flow.visualitems.organization.PreviewOrganizationItem;
import org.sociotech.communitymirror.visualitems.renderer.VisualItemRenderer;
import org.sociotech.communitymirror.visualstates.Detail;
import org.sociotech.communitymirror.visualstates.Micro;
import org.sociotech.communitymirror.visualstates.VisualState;

public class FlowOrganizationRenderer extends VisualItemRenderer<Organisation, OrganizationItem> {

    @Override
    public OrganizationItem renderItem(Organisation item, VisualState state) {

        OrganizationItem organizationItem = null;

        if (state instanceof Micro) {
            organizationItem = new MicroOrganizationItem(item);
        } else if (state instanceof Detail) {
            organizationItem = new DetailOrganizationItem(item);
        } else {
            organizationItem = new PreviewOrganizationItem(item);
        }

        organizationItem.setVisualState(state);
        return organizationItem;
    }

}
