package org.sociotech.communitymirror.view.flow.components;

import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem;
import org.sociotech.communitymirror.view.spring.components.SpringConnectableVisualComponent;
import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

import com.facebook.rebound.BaseSpringSystem;

/**
 * A workspace connection visual group that connects two data items within a flow view workspace using two spring connections and a bridging node between the two data items.
 * 
 * @author Eva Loesch
 */
public class BridgingWorkspaceConnectionVisualGroup extends WorkspaceConnectionVisualGroup{

	/**
	 * The visual component used as briding between the start node and the end node of this bridging workspace connection visual group.
	 * @author Eva Loesch 
	 */
	private VisualComponent bridgingNode;
	
	public BridgingWorkspaceConnectionVisualGroup(FlowVisualItem<? extends Item, ? extends FXMLController> startNode,
			FlowVisualItem<? extends Item, ? extends FXMLController> endNode, BaseSpringSystem springSystem) {
		
		super(startNode, endNode);
		
		this.bridgingNode = new SpringConnectableVisualComponent(0, 0);
		this.addVisualComponent(bridgingNode);
		this.addNode(bridgingNode);
	}
	
	/**
	 * Gets the visual component used as briding between the start node and the end node of this bridging workspace connection visual group.
	 * @author Eva Loesch 
	 */
	public VisualComponent getBridgingNode() {
		return bridgingNode;
	}

	/**
	 * Sets the visual component used as bridging between the start node and the end node of this bridging workspace connection visual group.
	 * 
	 * @param VisualComponent bridgingNode: the visual component that should be set as bridging node within this bridging workspace connection visual component.
	 * @author Eva Loesch 
	 */
	public void setBridgingNode(VisualComponent bridgingNode) {
		this.bridgingNode = bridgingNode;
	}
}
