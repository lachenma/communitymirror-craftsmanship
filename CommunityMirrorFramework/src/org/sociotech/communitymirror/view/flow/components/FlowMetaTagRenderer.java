
package org.sociotech.communitymirror.view.flow.components;

import org.sociotech.communitymashup.data.MetaTag;
import org.sociotech.communitymirror.view.flow.visualitems.MetaTagItem;
import org.sociotech.communitymirror.view.flow.visualitems.TimeItem;
import org.sociotech.communitymirror.visualitems.VisualItem;
import org.sociotech.communitymirror.visualitems.renderer.VisualItemRenderer;
import org.sociotech.communitymirror.visualstates.VisualState;

public class FlowMetaTagRenderer extends VisualItemRenderer<MetaTag, VisualItem<MetaTag>> {

    @Override
    public VisualItem<MetaTag> renderItem(MetaTag item, VisualState state) {
        if (item.getName().equals("about")) {
            MetaTagItem aboutItem = new MetaTagItem(item);
            aboutItem.setVisualState(state);
            return aboutItem;
        } else {
            TimeItem timeItem = new TimeItem(item);
            timeItem.setVisualState(state);
            return timeItem;
        }
    }

}
