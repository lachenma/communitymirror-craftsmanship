
package org.sociotech.communitymirror.view.flow.components;

import java.util.LinkedList;
import java.util.List;

import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.touch.TouchReleasedEvent;
import org.sociotech.communityinteraction.events.touch.TouchTappedEvent;
import org.sociotech.communitymirror.visualitems.VisualComponent;

import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;

/**
 * VisualComponent representing a Button which invokes specific Action
 * 
 * @author Eva Loesch
 *
 */
public class ButtonVisualComponent extends VisualComponent {

    /**
     * List of all button visual component listeners that should be notified
     * about events on this button visual component.
     */
    protected List<ButtonVisualComponentListener> buttonVisualComponentListeners = new LinkedList<>();

    /**
     * A JavaFX Button used to visualize the button of this
     * ButtonVisualComponent
     */
    protected ButtonBase button;

    /**
     * @return the button
     */
    public ButtonBase getButton() {
        return this.button;
    }

    /**
     * @param button
     *            the button to set
     */
    public void setButton(ButtonBase button) {
        this.button = button;
    }

    public ButtonVisualComponent(String buttonText) {
        this(new Button(buttonText));
    }

    public ButtonVisualComponent(ButtonBase button) {
        super();

        this.button = button;

        this.addNode(button);
        this.makeTouchable();
    }

    // /**
    // * Start the action that is assigned to this ButtonVisualComponent on
    // touch tapped event
    // *
    // * @see
    // org.sociotech.communityinteraction.interactionhandling.HandleTouch#onTouchTapped(org.sociotech.communityinteraction.events.touch.TouchTappedEvent)
    // *
    // * @author Eva Loesch
    // */
    // @Override
    // public void onTouchTapped(TouchTappedEvent event) {
    //
    // // notify listeners about the event on this button visual component
    // notifyListeners(new VisualComponentEvent(this, event));
    //
    // event.consume();
    // }

    /**
     * Start the action that is assigned to this ButtonVisualComponent on touch
     * tapped event
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleTouch#onTouchReleased(org.sociotech.communityinteraction.events.touch.TouchReleasedEvent)
     *
     * @author Eva Loesch
     */
    @Override
    public void onTouchReleased(TouchReleasedEvent event) {

        // notify listeners about the event on this button visual component
        this.notifyListeners(new VisualComponentEvent(this, event));

        event.consume();
    }

    /**
     * Adds a button visual component listener to the list of button visual
     * component listeners of this button visual component.
     *
     * @param componentListener:
     *            the button visual component listener that should be added to
     *            the list of button visual component listeners of this button
     *            visual component
     * @author Eva Loesch
     */
    public void addButtonVisualComponentListener(ButtonVisualComponentListener componentListener) {
        this.buttonVisualComponentListeners.add(componentListener);
    }

    /**
     * Removes a button visual component listener from the list of button visual
     * component listeners of this button visual component.
     *
     * @param componentListener:
     *            the button visual component listener that should be removed
     *            from the list of button visual component listeners of this
     *            button visual component
     * @author Eva Loesch
     */
    public void removeButtonVisualComponentListener(ButtonVisualComponentListener componentListener) {
        this.buttonVisualComponentListeners.remove(componentListener);
    }

    /**
     * Notifies all button visual component listeners about an event on this
     * button visual component.
     *
     * @param visualComponentEvent:
     *            the visual component event the listeners should be notified
     *            about
     */
    @Override
    public synchronized void notifyListeners(VisualComponentEvent visualComponentEvent) {
        // create copy of list of visual component listeners of this visual
        // component
        List<ButtonVisualComponentListener> componentListenersCopy = new LinkedList<>();
        for (ButtonVisualComponentListener componentListener : this.buttonVisualComponentListeners) {
            componentListenersCopy.add(componentListener);
        }

        // call right method only on listeners that exist when this method is
        // called and not on listeners that are created by calling onTouchTapped
        for (ButtonVisualComponentListener listener : componentListenersCopy) {
            CIEvent sourceEvent = visualComponentEvent.getSourceEvent();

            if (sourceEvent instanceof TouchTappedEvent) {
                listener.onButtonPressed(visualComponentEvent);
            }
        }
    }

}
