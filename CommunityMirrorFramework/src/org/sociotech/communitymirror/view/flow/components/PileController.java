package org.sociotech.communitymirror.view.flow.components;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.shape.Circle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

/**
 * Controller class for pile
 * @author Andrea Nutsi
 *
 */
public class PileController  extends FXMLController implements Initializable{


	@FXML
	Group labelsGroup;
	
	@FXML
	Circle touchCircle;
	
	@FXML
	Group dragPoint;
	
	/**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(PileController.class.getName());

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try{
			 assert labelsGroup != null : "fx:id=\"labelsGroup\" was not injected: check your FXML file 'pile.fxml'.";
			 assert touchCircle !=null : "fx:id=\"touchCircle\" was not injected: check your FXML file 'pile.fxml'.";
			 assert dragPoint !=null : "fx:id=\"dragPoint\" was not injected: check your FXML file 'pile.fxml'.";
		 }catch(AssertionError e){
			 logger.error(e.getMessage());
		 }
		
	}
	
	protected void setGroupVisible(){
		labelsGroup.setVisible(true);
	}
	
	public Circle getTouchCircleGroup()
	{
		return touchCircle;
	}
	
	public void setDragPointVisible(boolean visible){
		this.dragPoint.setVisible(visible);
	}

}
