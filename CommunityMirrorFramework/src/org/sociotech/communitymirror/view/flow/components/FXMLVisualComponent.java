
package org.sociotech.communitymirror.view.flow.components;

import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

/**
 * Abstract super class of all fxml based visual components
 *
 * @author Michael Koch
 *
 * @param <T>
 *            The type of the data item
 * @param <V>
 *            The type of the item controller
 */
public abstract class FXMLVisualComponent<V extends FXMLController> extends VisualComponent {

    /**
     * The component controller set in the fxml file
     */
    private V controller;

    public FXMLVisualComponent() {
        super();
    }

    /**
     * Returns the set item controller
     *
     * @return The set item controller
     */
    public V getController() {
        return this.controller;
    }

    /**
     * Sets the controller.
     *
     * @param controller
     *            Controller to use
     */
    public void setController(V controller) {
        this.controller = controller;
    }

}
