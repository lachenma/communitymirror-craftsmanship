package org.sociotech.communitymirror.view.flow.components;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;


/**
* Code is from https://github.com/IsNull/Vidada/blob/master/vidada/src/main/java/vidada/viewsFX/controls/GlyphView.java
* watch https://bitbucket.org/controlsfx/controlsfx/issue/306/glyphfont-refactoring-support-arbitary for updates
* 
* The GlyphView helps to display a GlyphFont-Icons in FXML.
* You basically only need to specify the font family and the name
* of the glyph.
*
* A Minimal FXML example to put a glyph-icon in a Button:
* <code>
* <Button>
* <graphic>
* <GlyphFontView fontFamily="FontAwesome" icon="PLUS"/>
* </graphic>
* </Button>
* </code>
*
* As long as you have loaded a Font, you can use GlyphFontView to display any character,
* even if the font or character is not directly supported by this library.
* This is possible by directly specify the unicode character.
* For example, the Star-symbol has the unicode value /uf006 in FontAwesome:
*
* <code>
* <Button>
* <graphic>
* <GlyphFontView fontFamily="FontAwesome" fontSize="20" icon="&#xf006;"/>
* </graphic>
* </Button>
* </code>
*
* In the example above, the fontSize was also directly specified.
*
*/
public class GlyphView extends Label {

    /***************************************************************************
* *
* Private fields *
* *
**************************************************************************/

    private final ObjectProperty<Object> icon = new SimpleObjectProperty<Object>();

    /***************************************************************************
* *
* Constructors *
* *
**************************************************************************/

    /**
* Empty Constructor (used by FXML)
*/
    public GlyphView(){
    	icon.addListener(x -> updateIcon());
        fontProperty().addListener(x -> updateIcon());
    }

    /**
* Creates a new GlyphFontView
* @param fontFamily
* @param unicode
*/
    public GlyphView(String fontFamily, char unicode) {
        this();
        setFontFamily(fontFamily);
        setTextUnicode(unicode);
    }

    /**
* Creates a new GlyphFontView
* @param fontFamily
* @param icon
*/
    public GlyphView(String fontFamily, Object icon) {
        this();
        setFontFamily(fontFamily);
        setIcon(icon);
    }

    /***************************************************************************
* *
* Public API *
* *
**************************************************************************/

    /**
* Sets the glyph icon font family
* @param fontFamily A font family name
* @return Returns this instance for fluent API
*/
    public GlyphView fontFamily(String fontFamily){
        setFontFamily(fontFamily);
        return this;
    }

    /**
* Sets the glyph color
* @param color
* @return Returns this instance for fluent API
*/
    public GlyphView color(Color color){
        setColor(color);
        return this;
    }

    /**
* Sets glyph size
* @param size
* @return Returns this instance for fluent API
*/
    public GlyphView size(double size) {
        setFontSize(size);
        return this;
    }

    /***************************************************************************
* *
* Properties *
* *
**************************************************************************/

    /**
* Sets the font family of this glyph
* @param family
*/
    public void setFontFamily(String family){
        if(family != getFont().getFamily()){
            Font newFont = Font.font(family, getFont().getSize());
            setFont(newFont);
        }
    }

    /**
* Gets the font family of this glyph
* @return
*/
    public String getFontFamily(){
        return getFont().getFamily();
    }

    /**
* Sets the font size of this glyph
* @param size
*/
    public void setFontSize(double size){
        Font newFont = Font.font(getFont().getFamily(), size);
        setFont(newFont);
    }

    /**
* Gets the font size of this glyph
* @return
*/
    public double getFontSize(){
        return getFont().getSize();
    }

    /**
* Set the Color of this Glyph
* @param color
*/
    public void setColor(Color color){
        setTextFill(color);
    }
    
    public Color getColor(){
    	return (Color) getTextFill();
    }

    /**
* The icon name property.
*
* This must either be a Glyph-Name (either string or enum value) known by the GlyphFontRegistry.
* Alternatively, you can directly submit a unicode character here.
*
* @return
*/
    public Object iconProperty(){
        return icon;
    }

    /**
* Set the icon to display.
* @param iconValue This can either be the Glyph-Name, Glyph-Enum Value or a unicode character representing the sign.
*/
    public void setIcon(Object iconValue){
        icon.set(iconValue);
    }

    public Object getIcon(){
        return icon.get();
    }

    /***************************************************************************
* *
* Private methods *
* *
**************************************************************************/


    /**
* This updates the text with the correct unicode value
* so that the desired icon is displayed.
*/
    private void updateIcon(){

        Object iconValue = getIcon();

        if(iconValue != null) {
            if(iconValue instanceof Character){
                setTextUnicode((Character)iconValue);
            }else {
                GlyphFont gylphFont = GlyphFontRegistry.font(getFontFamily());
                if (gylphFont != null) {
                    String name = iconValue.toString();
                    Character unicode = gylphFont.getGlyphs().get(name);
                    if (unicode != null) {
                        setTextUnicode(unicode);
                    } else {
                        // Could not find a icon with this name
                        setText(name);
                    }
                }
            }
        }
    }

    /**
* Sets the given char as text
* @param unicode
*/
    private void setTextUnicode(char unicode){
        setText(String.valueOf(unicode));
    }
}


