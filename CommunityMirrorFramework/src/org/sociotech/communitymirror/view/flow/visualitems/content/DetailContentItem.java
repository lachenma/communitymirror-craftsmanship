
package org.sociotech.communitymirror.view.flow.visualitems.content;

import org.sociotech.communityinteraction.events.gesture.drag.DragStartedEvent;
import org.sociotech.communitymashup.data.Content;

import javafx.geometry.Bounds;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;

/**
 * The visualization of a content item in the detail visual state
 *
 * @author Andrea Nutsi
 *
 */
public class DetailContentItem extends ContentItem {

    public DetailContentItem(Content dataItem) {
        super(dataItem);
    }

    @Override
    protected void render() {
        super.render();
        if (this.image != null) {
            Circle circle = new Circle(40);
            circle.setLayoutY(40);
            circle.setLayoutX(40);
            super.controller.setContentImageClip(circle);
        } else {
            Circle circle = new Circle(40);
            circle.setLayoutY(3);
            circle.setLayoutX(43);
            super.controller.setTitleClip(circle);
            super.controller.bindToTitle(this.title, 20);
        }
        if (this.authorImage != null) {
            Circle circle = new Circle(8);
            circle.setLayoutY(8);
            circle.setLayoutX(8);
            super.controller.setContentAuthorImageClip(circle);
        }
        // super.controller.adjustScrollPaneHeight();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * shouldPerformDrag()
     */
    @Override
    protected boolean shouldPerformDrag(DragStartedEvent dragEvent) {
        WebView webView = this.controller.getWebView();
        if (webView != null) {
            Bounds boundsLocal = webView.getBoundsInLocal();
            if (boundsLocal != null) {
                Bounds boundsInScene = webView.localToScene(boundsLocal);
                if (boundsInScene != null) {
                    return !boundsInScene.contains(dragEvent.getSceneX(), dragEvent.getSceneY());
                }
            }
        }
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * getInDetailCloseButton()
     */
    @Override
    protected Text getInDetailCloseButton() {
        return this.controller.getInDetailCloseButton();
    }

}
