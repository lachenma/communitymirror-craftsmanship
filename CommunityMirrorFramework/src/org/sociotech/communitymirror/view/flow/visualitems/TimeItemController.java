package org.sociotech.communitymirror.view.flow.visualitems;

import java.net.URL;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;

/**
 * The controller of the time item
 * @author Andrea Nutsi
 *
 */
public class TimeItemController extends FXMLController implements Initializable{

	@FXML
	Text time;
	
	/**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(TimeItemController.class.getName());
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		 try{
			 assert time != null : "fx:id=\"time\" was not injected: check your FXML file 'time.fxml'.";
		 }catch(AssertionError e){
			 logger.error(e.getMessage());
		 }
		
	}
	
	public Text getTime() {
		return time;
	}

	public void setTime(Text time) {
		this.time = time;
	}

}
