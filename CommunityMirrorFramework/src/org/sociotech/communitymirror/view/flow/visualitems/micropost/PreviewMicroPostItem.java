package org.sociotech.communitymirror.view.flow.visualitems.micropost;

import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymashup.data.Content;

import com.google.common.io.Resources;
/**
 * The visualization of a micropost item in the preview visual state
 * @author Andrea Nutsi
 *
 */
public class PreviewMicroPostItem extends MicroPostItem {


	/**
	 * The log4j logger reference.
	 */
	private final Logger logger = LogManager.getLogger(PreviewMicroPostItem.class.getName());

	public PreviewMicroPostItem(Content dataItem) {
		super(dataItem);
	}


	protected void render() {
		try {
			// Get FXML for micropost
			FXMLLoader loader = new FXMLLoader();

			URL location;
			String fxmlPath = "fxml/inPreview/micropost.fxml";
	        if(this.themeResources != null) {
	        	location = this.themeResources.getResourceURL(fxmlPath);
	        } else {
	        	location = Resources.getResource("flow/" + fxmlPath);
	        	logger.warn(this.getClass().getName() + " using deprecated FXML path.");
	        }
			loader.setLocation(location);
			AnchorPane microPost = (AnchorPane) loader.load(location.openStream());
			MicroPostItemController controller = loader.getController();

			if(authorImage != null){
				Circle circle = new Circle(8);
				circle.setLayoutY(8);
				circle.setLayoutX(8);
				controller.setMicropostAuthorImageClip(circle);
				controller.setMicropostAuthorImage(authorImage);
			}

			controller.bindToMicropostsCounter(microPostsCounter);

			controller.bindToMicropostDate(creationDate);

			controller.bindToMicropostAuthor(author);

			controller.bindToMicropostTags(tags);

			if(micropostImage != null){

				Circle circle = new Circle(40);
				circle.setLayoutY(40);
				circle.setLayoutX(40);
				controller.setMicropostImageClip(circle);
				controller.setMicropostImage(micropostImage);

				controller.setMicropostImageLarge(micropostImage);
			}else{

				controller.bindToMicropostContentField(microPostContent, 75);
			}


			//add Node to Scene
			//prevent ConcurrentModificationException
			int size = microPost.getChildren().size();
			for(int i = 0; i < size; i++){
				addNode(microPost.getChildren().get(0));
			}
	//		addNode(microPost);
		} catch (IOException e) {
			logger.error("Problems loading fxml-file for MicroPostItem" + e.getStackTrace());
		}
	}

}
