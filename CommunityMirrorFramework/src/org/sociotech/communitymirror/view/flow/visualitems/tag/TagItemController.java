package org.sociotech.communitymirror.view.flow.visualitems.tag;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

/**
 * The controller of a Tag item
 * @author Andrea Nutsi
 *
 */
public class TagItemController extends FXMLController implements Initializable{

	@FXML
	Text tagContent;
	
	@FXML
	Text tagsNumber;
	
	/**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(TagItemController.class.getName());

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try{
			 assert tagContent != null : "fx:id=\"tagContent\" was not injected: check your FXML file 'tag.fxml'.";
			 assert tagsNumber != null : "fx:id=\"tagsNumber\" was not injected: check your FXML file 'tag.fxml'.";
		 }catch(AssertionError e){
			 logger.error(e.getMessage());
		 }

	}
	
	protected void bindToTagContentField(String tagContent){
		bindToFXMLElement(this.tagContent, tagContent);
	}
	
	protected void bintToTagsNumber(String number){
		bindToFXMLElement(tagsNumber, number);
	}

}
