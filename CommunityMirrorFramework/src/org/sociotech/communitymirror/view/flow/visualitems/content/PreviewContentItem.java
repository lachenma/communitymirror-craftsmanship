
package org.sociotech.communitymirror.view.flow.visualitems.content;

import org.sociotech.communitymashup.data.Content;

import javafx.scene.shape.Circle;

/**
 * The visualization of a content item in the preview visual state
 *
 * @author Andrea Nutsi
 *
 */
public class PreviewContentItem extends ContentItem {

    public PreviewContentItem(Content dataItem) {
        super(dataItem);
    }

    @Override
    protected void render() {
        super.render();
        if (this.authorImage != null) {
            Circle circle = new Circle(8);
            circle.setLayoutY(8);
            circle.setLayoutX(8);
            this.controller.setContentAuthorImageClip(circle);
        }
    }
}
