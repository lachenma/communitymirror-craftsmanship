
package org.sociotech.communitymirror.view.flow.visualitems.content;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.eclipse.emf.common.util.EList;
import org.sociotech.communitymashup.data.Connection;
import org.sociotech.communitymashup.data.Content;
import org.sociotech.communitymashup.data.Event;
import org.sociotech.communitymashup.data.Image;
import org.sociotech.communitymashup.data.IndoorLocation;
import org.sociotech.communitymashup.data.InformationObject;
import org.sociotech.communitymashup.data.MetaInformation;
import org.sociotech.communitymashup.data.MetaTag;
import org.sociotech.communitymashup.data.Organisation;
import org.sociotech.communitymashup.data.Person;
import org.sociotech.communitymashup.data.StarRanking;
import org.sociotech.communitymashup.data.Tag;
import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.utils.ImageHelper;
import org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem;
import org.xml.sax.InputSource;

import com.google.common.io.Resources;

import javafx.fxml.FXMLLoader;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * The visualization of a Content item
 *
 * @author Andrea Nutsi
 *
 */
/**
 * @author Andrea Nutsi
 *
 */
public abstract class ContentItem extends FlowVisualItem<Content, ContentItemController> {

    /**
     * The data item of this content
     */
    private Content dataItem;

    /**
     * The meta tags (as Strings)
     */
    protected LinkedList<String> metaTags = new LinkedList<>();

    /**
     * The Location of this content
     */
    protected String locationText = "";
    /**
     * The title of this content
     */
    protected String title = "";
    /**
     * The description of this content
     */
    protected String description = "";

    /**
     * The start time of the content
     */
    protected String contentTime = "";

    /**
     * The year the publication was published
     */
    protected String publicationYear = "";

    /**
     * The image of this content
     */
    protected javafx.scene.image.Image image = null;

    /**
     * Square version of the image of this content
     */
    protected javafx.scene.image.Image squareImage = null;

    /**
     * Square version of the image of this content
     */
    protected javafx.scene.image.Image roundImage = null;

    /**
     * The image of the author of this content
     */
    protected javafx.scene.image.Image authorImage = null;

    /**
     * The image of a person associated with this content
     */
    protected javafx.scene.image.Image authorImageAny = null;

    /**
     * A list containing the names of the co-authors of this content
     */
    protected LinkedList<String> coAuthorNames = new LinkedList<>();

    /**
     * A list containing the images of the co-authors of this content
     */
    protected LinkedList<javafx.scene.image.Image> coAuthorImages = new LinkedList<>();

    /**
     * The value of the star ranking of this content
     */
    protected Double starRanking = 0.0;

    /**
     * The number of comments related to this content
     */
    protected String commentsCounter = "";

    /**
     * The name of the author of this content
     */
    protected String contentAuthor = "";

    /**
     * A list containing the text content of a comment
     */
    protected LinkedList<String> commentsContent = new LinkedList<>();

    private XPath xpath;

    /**
     * A list containing the creation dates of the comments
     */
    protected LinkedList<String> commentsDate = new LinkedList<>();

    protected javafx.scene.image.Image qrImage = null;

    protected String fxmlfilename = "content.fxml";

    protected String state;

    protected ContentItemController controller;

    /**
     * Creates an content item
     *
     * @param dataItem
     *            The Dataset for this item
     */
    public ContentItem(Content dataItem) {
        super(dataItem);
        String concreteClass = this.getClass().getSimpleName();
        if (concreteClass.endsWith("ContentItem")) {
            this.state = concreteClass.substring(0, concreteClass.length() - "ContentItem".length());
        }
        this.dataItem = dataItem;
    }

    @Override
    protected void onInit() {
        if (this.dataItem != null) {
            this.getContentInfo();
        }
        this.render();
        super.onInit();
    }

    /**
     * Method to render the item.
     */
    protected void render() {

        FXMLLoader loader = new FXMLLoader();
        AnchorPane contentPane = null;
        try {
            // Get FXML for content
            URL location;
            String fxmlPath = "fxml/in" + this.state + "/" + this.fxmlfilename;
            if (this.themeResources != null) {
                location = this.themeResources.getResourceURL(fxmlPath);
            } else {
                location = Resources.getResource("flow/" + fxmlPath);
                this.logger.warn(this.getClass().getName() + " using deprecated FXML path.");
            }
            loader.setLocation(location);
            contentPane = (AnchorPane) loader.load(location.openStream());
        } catch (IOException e) {
            this.logger.error("Problems loading fxml-file for ContentItem:");
            this.logger.error("    " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length && i < 10; i++) {
                this.logger.error("    " + e.getStackTrace()[i]);
            }
        }

        this.controller = loader.getController();

        // get and set category information from meta tags
        String categoryString = CommunityMirror.getConfiguration().getString("view.flow.particles.category_icons");
        String category_icons[] = new String[0];
        if (categoryString != null && categoryString.length() > 0) {
            category_icons = categoryString.split(",");
        }
        categoryString = null;
        EList<MetaTag> metaTags = this.dataItem.getMetaTags();
        for (MetaTag metaTag : metaTags) {
            for (String category : category_icons) {
                if (category.equals(metaTag.getName())) {
                    categoryString = metaTag.getName();
                    break;
                }
            }
        }
        this.setCategory(categoryString);

        /*
         * get and process template for details view - use FreeMarker template
         * engine
         */
        if (this.state.equals("Detail")) {
            Map<String, Object> templateData = new HashMap<>();
            templateData.put("category", categoryString);
            templateData.put("item", this);
            // perhaps we do not need this here ...
            templateData.put("title", this.title);
            templateData.put("description", this.description);
            this.controller.setWebContent(this.processDetailTemplate("detailContent.ftl", templateData));
        }

        this.controller.bindToTitle(this.title, 20);
        this.controller.bindToDescription(this.description);

        this.controller.setContentImage(this.image);
        this.controller.setContentImageSquare(this.squareImage);
        this.controller.setContentImageRound(this.roundImage);
        this.controller.setContentImageSmall(this.image);
        this.controller.setContentImageLarge(this.image);

        if (this.contentAuthor != "") {
            this.controller.bindToContentAuthorDetail(this.contentAuthor);
        }
        this.controller.setContentAuthorImage(this.authorImageAny);

        this.controller.setQRCode(this.qrImage);

        this.controller.bindToCommentsCounter(this.commentsCounter);
        this.controller.bindToContentTime(this.contentTime);
        this.controller.bindToTitleLong(this.title, 100);
        this.controller.bindToContentAuthor(this.contentAuthor);

        if (this.locationText == "") {
            // if no location available use publication year
            this.controller.bindToContentLocation(this.publicationYear);
        } else {
            this.controller.bindToContentLocation(this.locationText);
        }

        // show connection points for graph
        this.controller.showGraphConnections(this.getAllRelatedItems().size());

        // add Node to Scene
        // prevent ConcurrentModificationException
        int size = contentPane.getChildren().size();
        for (int i = 0; i < size; i++) {
            this.addNode(contentPane.getChildren().get(0));
        }
        // addNode(contentPane);
    }

    /**
     * Collects the necessary information for this content from the dataset
     */
    protected void getContentInfo() {

        this.title = this.dataItem.getName();
        this.xpath = XPathFactory.newInstance().newXPath();
        this.description = this.dataItem.getStringValue();

        // year of the publication
        this.publicationYear = new SimpleDateFormat("yyyy").format(this.dataItem.getCreated());

        // get meta tags (for changing the display according to special meta
        // tags)
        EList<MetaTag> mashupMetaTags = this.dataItem.getMetaTags();
        for (MetaTag mt : mashupMetaTags) {
            this.metaTags.add(mt.getName());
        }
        // first use of meta tags - if project, then add this string to the
        // title
        if (this.metaTags.contains("project")) {
            this.title = "Projekt: " + this.title;
        }
        // if agenda-item then use different fxml - this should be made
        // configurable in the future ...
        if (this.metaTags.contains("agenda_item")) {
            this.fxmlfilename = "contentagenda.fxml";
        }

        // get the location information of the content
        EList<MetaInformation> metaInformations = this.dataItem.getMetaInformations();
        if (!metaInformations.isEmpty()) {
            for (MetaInformation metaInformation : metaInformations) {
                if (metaInformation instanceof IndoorLocation) {
                    this.locationText += ((IndoorLocation) metaInformation).getName() + " ";
                } else if (metaInformation instanceof Event) {
                    // get the time: start and end
                    // just take the start time = the first event found
                    if (this.contentTime.equals("")) {
                        this.contentTime = ((Event) metaInformation).getDatePrettyInLanguage("de");
                    }
                }
            }
        }

        // get image of the content
        EList<Image> images = this.dataItem.getImages();
        String fileUrl = "";
        if (!images.isEmpty()) {
            for (int i = 0; i < images.size(); i++) {
                if (!images.get(i).hasMetaTag("qr")) {
                    fileUrl = images.get(i).getFileUrl();
                    // Set the image of this person
                    this.image = ImageHelper.createImage(fileUrl, true);
                    // if something failed try next fileUrl in dataset
                    if (this.image != null) {
                        break;
                    }
                }
            }

        } else {
            // default images?
            if (this.metaTags.contains("project")) {
                fileUrl = "themes/flow2/images/image-default-project.png";
                URL url = this.getClass().getClassLoader().getResource(fileUrl);
                try {
                    InputStream input = url.openStream();
                    this.image = new javafx.scene.image.Image(input);
                } catch (Exception e) {
                    this.logger.warn("Failed loading default image from " + fileUrl);
                }
            } else if (this.metaTags.contains("publication")) {
                fileUrl = "themes/flow2/images/image-default-publication.png";
                URL url = this.getClass().getClassLoader().getResource(fileUrl);
                try {
                    InputStream input = url.openStream();
                    this.image = new javafx.scene.image.Image(input);
                } catch (Exception e) {
                    this.logger.warn("Failed loading default image from " + fileUrl);
                }
            }
        }

        // Automatically remove transparency from logos
        double imgWidth = 256;
        double imgHeight = imgWidth;
        if (this.image != null) {
            imgWidth = this.image.getWidth();
            imgHeight = this.image.getHeight();
        }
        Canvas canvas = new Canvas(imgWidth, imgHeight);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.GRAY); // adjust here if background should
                                // be blended to black or white
                                // instead
        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        if (this.image != null) {
            gc.drawImage(this.image, 0, 0);
        }
        this.image = canvas.snapshot(null, null);

        // create square image
        int rawWidth = (int) Math.round(this.image.getWidth());
        int rawHeight = (int) Math.round(this.image.getHeight());
        int targetSize = Math.round(Math.min(rawWidth, rawHeight));
        if (targetSize > 0) {
            WritableImage newImage = new WritableImage(targetSize, targetSize);
            int xOffset = (int) Math.round((this.image.getWidth() - targetSize) / 2);
            int yOffset = (int) Math.round((this.image.getHeight() - targetSize) / 2);
            newImage.getPixelWriter().setPixels(0, 0, targetSize, targetSize, this.image.getPixelReader(), xOffset,
                    yOffset);
            this.squareImage = newImage;

            // create round image
            ImageView tempView = new ImageView();
            tempView.setImage(this.squareImage);
            Circle clip = new Circle(this.squareImage.getWidth() / 2, this.squareImage.getWidth() / 2,
                    this.squareImage.getWidth() / 2);
            tempView.setClip(clip);
            SnapshotParameters parameters = new SnapshotParameters();
            parameters.setFill(Color.TRANSPARENT);
            this.roundImage = tempView.snapshot(parameters, null);
        }

        Person contentAuthorPerson = this.dataItem.getAuthor();
        if (contentAuthorPerson != null) {
            this.contentAuthor = contentAuthorPerson.getName();
        }

        // get an image of a person somehow linked to this content
        // create Metatag list
        LinkedList<String> metaTagList = new LinkedList<>();
        metaTagList.add("profile_image_big_main");
        metaTagList.add("profile_image_big");
        this.authorImageAny = this.getContentAuthorImage(metaTagList);
        this.authorImage = this.getAuthorImage(metaTagList, contentAuthorPerson);

        // QR code image
        Image qrMashupImage = this.dataItem.getAttachedImageWithMetaTagName("fulltext_qr");
        if (qrMashupImage != null) {
            this.qrImage = ImageHelper.createImage(qrMashupImage.getFileUrl(), true);
        }

        // get information for co-authors
        EList<Person> coAuthors = this.dataItem.getContributors();
        if (!coAuthors.isEmpty()) {
            for (Person person : coAuthors) {
                this.coAuthorNames.add(person.getName());
                this.coAuthorImages.add(this.getAuthorImage(metaTagList, person));
            }
        }

        // star ranking value over all star rankings of this content
        // starRanking = dataItem.getStarRanking();
        // if(starRanking == null) starRanking = 0.0;

        // get all star rankings
        EList<StarRanking> starRankings = this.dataItem.getStarRankings();
        double sum = 0.0;
        double counter = 0;

        // get all all star rankings of this content object with metatag 'total'
        for (StarRanking starRanking : starRankings) {
            if (starRanking.hasMetaTag("total")) {
                sum += starRanking.getNormalizedValue();
                counter++;
            }
        }

        // calculate average
        double starRankingValue = 0.0;
        if (counter != 0) {
            starRankingValue = sum / counter;
        }

        this.starRanking = starRankingValue;

        this.commentsCounter = this.dataItem.getContents().size() + "";

        this.fillCommentLists();
    }

    public String xPathQuery(String query) {
        return (String) this.xPathQuery(query, XPathConstants.STRING);
    }

    public Object xPathQuery(String query, QName qname) {
        Object result = null;
        try {
            String xml = this.dataItem.getStringXML();
            if (xml != null) {
                result = this.xpath.evaluate(query, new InputSource(new StringReader(xml)), qname);
            }
        } catch (XPathExpressionException e) {
            this.logger.warn("XPathExpressionException encountered. Query was: " + query);
        }
        return result;
    }

    /**
     * Fills the two lists concerning comments with data
     */
    private void fillCommentLists() {
        EList<Content> comments = this.dataItem.getContents();
        for (Content comment : comments) {
            this.commentsContent.add(comment.getStringValue());
            this.commentsDate.add(comment.getCreatedPretty());
        }
    }

    /**
     * Returns the image of a person associated to this content
     *
     * @return the image of a person associated to this content
     */
    private javafx.scene.image.Image getContentAuthorImage(LinkedList<String> metaTagList) {
        javafx.scene.image.Image authorImage = null;
        String fileUrl = "";

        authorImage = this.getAuthorImage(metaTagList, this.dataItem.getAuthor());

        // if not successful, look for images among co-authors
        EList<Person> persons = this.dataItem.getContributors();
        if (!persons.isEmpty()) {
            for (Person person : persons) {
                for (String metaTag : metaTagList) {
                    org.sociotech.communitymashup.data.Image mashupImage = person
                            .getAttachedImageWithMetaTagName(metaTag);
                    if (mashupImage != null) {
                        fileUrl = mashupImage.getFileUrl();
                        authorImage = ImageHelper.createImage(fileUrl, true);
                        if (authorImage != null) {
                            break;
                        }
                    }
                }
            }

        }
        return authorImage;
    }

    private void setCategory(String newCategory) {
        javafx.scene.image.Image categoryIcon = null;
        if (newCategory != null) {
            String iconPath = "images/category_icon_" + newCategory + ".png";
            URL location;
            if (this.themeResources != null) {
                location = this.themeResources.getResourceURL(iconPath);
            } else {
                location = Resources.getResource("flow/" + iconPath);
                this.logger.warn(this.getClass().getName() + " using deprecated Resource path.");
            }
            // If the icon file does not exist, ImageHelper will simply return
            // null. Consequently, passing null into the controller will hide
            // the category icon.
            categoryIcon = ImageHelper.createImage(location, true);
        }
        this.controller.setCategoryIcon(categoryIcon);
    }

    private javafx.scene.image.Image getAuthorImage(LinkedList<String> metaTagList, Person author) {
        String fileUrl;
        javafx.scene.image.Image authorImage = null;
        if (author != null) {
            for (String metaTag : metaTagList) {
                org.sociotech.communitymashup.data.Image mashupImage = author.getAttachedImageWithMetaTagName(metaTag);
                if (mashupImage != null) {
                    fileUrl = mashupImage.getFileUrl();
                    authorImage = ImageHelper.createImage(fileUrl, true);
                    if (authorImage != null) {
                        break;
                    }
                }
            }
        }
        return authorImage;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * getRelatedOrganizations()
     */
    @Override
    protected List<Organisation> getRelatedOrganizations() {
        Content content = this.getDataItem();
        // get the organizations of all authors of this item ...
        List<Organisation> res = content.getOrganisations();
        // now add direct connections to organisation objects
        List<Connection> tmplist = content.getConnectedTo();
        for (Connection c : tmplist) {
            InformationObject o = c.getTo();
            if (o instanceof Organisation) {
                if (res == null) {
                    res = new LinkedList<>();
                }
                res.add((Organisation) o);
            }
        }

        return res;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * getRelatedPersons()
     */
    @Override
    protected List<Person> getRelatedPersons() {
        Content content = this.getDataItem();
        return content.getPersons();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * getRelatedContents()
     */
    @Override
    protected List<Content> getRelatedContents() {
        Content content = this.getDataItem();
        return content.getContents();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * getRelatedTags()
     */
    @Override
    protected List<Tag> getRelatedTags() {
        Content content = this.getDataItem();

        List<Tag> relatedTags = new LinkedList<>();
        for (Tag tag : content.getTags()) {
            // if(tag.getTagged().size() > 1)
            relatedTags.add(tag);
        }

        return relatedTags;
    }
}
