
package org.sociotech.communitymirror.view.flow.visualitems.organization;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.sociotech.communityinteraction.events.gesture.drag.DragStartedEvent;
import org.sociotech.communitymashup.data.Content;
import org.sociotech.communitymashup.data.MetaTag;
import org.sociotech.communitymashup.data.Organisation;
import org.sociotech.communitymashup.data.Person;
import org.sociotech.communitymashup.data.Tag;
import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.context.Location;
import org.sociotech.communitymirror.utils.ImageHelper;
import org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem;

import com.google.common.io.Resources;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;

/**
 * The visualization of an organization item
 *
 * @author Andrea Nutsi, Michael Koch
 *
 */
public abstract class OrganizationItem extends FlowVisualItem<Organisation, OrganizationItemController> {

    /**
     * The type of organization item to visualize
     */
    protected String state = null;

    /**
     * The data of this organization
     */
    private Organisation dataItem;

    /**
     * The logo image
     */
    protected Image organizationLogo;

    /**
     * Square version of the image of this content
     */
    protected Image squareImage;

    /**
     * Round version of the image of this content
     */
    protected Image roundImage;

    /**
     * The name of this organization
     */
    protected String organizationName;

    /**
     * The description of this organization
     */
    protected String description = "";

    private OrganizationItemController controller;

    public OrganizationItem(Organisation dataItem) {
        super(dataItem);
        String concreteClass = this.getClass().getSimpleName();
        if (concreteClass.endsWith("OrganizationItem")) {
            this.state = concreteClass.substring(0, concreteClass.length() - "OrganizationItem".length());
        }
        this.dataItem = dataItem;
    }

    @Override
    protected void onInit() {

        this.organizationName = this.dataItem.getName();
        this.description = this.dataItem.getStringValue();

        String fileUrl = "";
        // get Images of this person
        EList<org.sociotech.communitymashup.data.Image> images = this.dataItem.getImages();
        if (!images.isEmpty()) {
            for (int i = 0; i < images.size(); i++) {
                if (!images.get(i).hasMetaTag("takeaway_qr")) {
                    fileUrl = images.get(i).getFileUrl();
                    // Set the image of this person
                    this.organizationLogo = ImageHelper.createImage(fileUrl, true);
                    // if something failed try next fileUrl in dataset
                    if (this.organizationLogo != null) {
                        break;
                    }
                }
            }

        }

        this.render();
        super.onInit();
    }

    /**
     * Method to generate the visual representation
     */
    protected void render() {
        try {
            // Get FXML for organization
            FXMLLoader loader = new FXMLLoader();

            URL url;
            String fxmlPath = "fxml/in" + this.state + "/organization.fxml";
            if (this.themeResources != null) {
                url = this.themeResources.getResourceURL(fxmlPath);
            } else {
                url = Resources.getResource("flow/" + fxmlPath);
                this.logger.warn(this.getClass().getName() + " using deprecated FXML path.");
            }
            loader.setLocation(url);
            AnchorPane organisationPane = (AnchorPane) loader.load(url.openStream());
            this.controller = loader.getController();

            // get and set category information from meta tags
            String categoryString = CommunityMirror.getConfiguration().getString("view.flow.particles.category_icons");
            String category_icons[] = (categoryString == null) ? new String[0] : categoryString.split(",");
            categoryString = null;
            EList<MetaTag> metaTags = this.dataItem.getMetaTags();
            for (MetaTag metaTag : metaTags) {
                for (String category : category_icons) {
                    if (category.equals(metaTag.getName())) {
                        categoryString = metaTag.getName();
                        break;
                    }
                }
            }
            this.setCategory(categoryString);

            // Automatically remove transparency from logos
            double imgWidth = 256;
            double imgHeight = imgWidth;
            if (this.organizationLogo != null) {
                imgWidth = this.organizationLogo.getWidth();
                imgHeight = this.organizationLogo.getHeight();
            }
            Canvas canvas = new Canvas(imgWidth, imgHeight);
            GraphicsContext gc = canvas.getGraphicsContext2D();
            gc.setFill(Color.GRAY); // adjust here if background should be
                                    // blended to black or white instead
            gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
            if (this.organizationLogo != null) {
                gc.drawImage(this.organizationLogo, 0, 0);
            }
            this.organizationLogo = canvas.snapshot(null, null);
            // Set Logo of organization
            Circle circle = new Circle(40);
            circle.setLayoutY(40);
            circle.setLayoutX(40);
            this.controller.setOrganizationImageClip(circle);
            this.controller.setOrganizationLogo(this.organizationLogo);

            /*
             * get and process template for details view - use FreeMarker
             * template
             * engine
             */
            if (this.state.equals("Detail")) {
                Map<String, Object> templateData = new HashMap<>();
                templateData.put("category", categoryString);
                templateData.put("item", this);
                // perhaps we do not need this here ...
                templateData.put("name", this.organizationName);
                templateData.put("description", this.description);
                EList<org.sociotech.communitymashup.data.Location> loc = this.dataItem.getLocations();
                Location location = null;
                for (org.sociotech.communitymashup.data.Location l : loc) {
                    location = new Location(this.organizationName, Double.parseDouble(l.getLatitude()),
                            Double.parseDouble(l.getLongitude()));
                }
                // set a default location for testing purposes - should be
                // deleted later on
                // TODO
                if (location == null) {
                    location = new Location("", 51.183088, 6.449203);
                }
                templateData.put("locationLat", Double.toString(location.getLatitude()));
                templateData.put("locationLon", Double.toString(location.getLongitude()));
                templateData.put("mirrorLat",
                        Double.toString(CommunityMirror.getContextManager().getLocation().getLatitude()));
                templateData.put("mirrorLon",
                        Double.toString(CommunityMirror.getContextManager().getLocation().getLongitude()));
                templateData.put("locationDistance",
                        Math.ceil(CommunityMirror.getContextManager().getLocation().distanceTo(location)));
                this.controller.setWebContent(this.processDetailTemplate("detailOrganization.ftl", templateData));
            }

            // create square image
            if (this.organizationLogo != null) {
                int rawWidth = (int) Math.round(this.organizationLogo.getWidth());
                int rawHeight = (int) Math.round(this.organizationLogo.getHeight());
                int targetSize = Math.round(Math.min(rawWidth, rawHeight));
                if (targetSize > 0) {
                    WritableImage newImage = new WritableImage(targetSize, targetSize);
                    int xOffset = (int) Math.round((this.organizationLogo.getWidth() - targetSize) / 2);
                    int yOffset = (int) Math.round((this.organizationLogo.getHeight() - targetSize) / 2);
                    newImage.getPixelWriter().setPixels(0, 0, targetSize, targetSize,
                            this.organizationLogo.getPixelReader(), xOffset, yOffset);
                    this.squareImage = newImage;

                    // create round image
                    ImageView tempView = new ImageView();
                    tempView.setImage(this.squareImage);
                    Circle clip = new Circle(this.squareImage.getWidth() / 2, this.squareImage.getWidth() / 2,
                            this.squareImage.getWidth() / 2);
                    tempView.setClip(clip);
                    SnapshotParameters parameters = new SnapshotParameters();
                    parameters.setFill(Color.TRANSPARENT);
                    this.roundImage = tempView.snapshot(parameters, null);
                }
            }

            // bind organization name to organization name text
            this.controller.bindToOrganizationName(this.organizationName);

            // Set Logo of organization
            this.controller.setOrganizationLogo(this.organizationLogo);
            this.controller.setOrganizationImageSquare(this.squareImage);
            this.controller.setOrganizationImageRound(this.roundImage);

            this.controller.bindToDescription(this.description);

            // show connection points for graph
            this.controller.showGraphConnections(this.getAllRelatedItems().size());

            // add Node to Scene
            // prevent ConcurrentModificationException
            int size = organisationPane.getChildren().size();
            for (int i = 0; i < size; i++) {
                this.addNode(organisationPane.getChildren().get(0));
            }
        } catch (IOException e) {
            this.logger.error("Problems loading fxml-file for OrganizationItem:");
            this.logger.error("    " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length && i < 10; i++) {
                this.logger.error("    " + e.getStackTrace()[i]);
            }
        }
    }

    /**
     *
     * @param newCategory
     */
    private void setCategory(String newCategory) {
        Image categoryIcon = null;
        if (newCategory != null) {
            String iconPath = "images/category_icon_" + newCategory + ".png";
            URL location;
            if (this.themeResources != null) {
                location = this.themeResources.getResourceURL(iconPath);
            } else {
                location = Resources.getResource("flow/" + iconPath);
                this.logger.warn(this.getClass().getName() + " using deprecated Resource path.");
            }
            // If the icon file does not exist, ImageHelper will simply return
            // null. Consequently, passing null into the controller will hide
            // the category icon.
            categoryIcon = ImageHelper.createImage(location, true);
        }
        this.controller.setCategoryIcon(categoryIcon);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * getRelatedPersons()
     */
    @Override
    protected List<Person> getRelatedPersons() {
        Organisation organisation = this.getDataItem();
        return organisation.getParticipants();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * getRelatedContents()
     */
    @Override
    protected List<Content> getRelatedContents() {
        Organisation organisation = this.getDataItem();
        return organisation.getContents();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * getRelatedTags()
     */
    @Override
    protected List<Tag> getRelatedTags() {
        Organisation organisation = this.getDataItem();

        List<Tag> relatedTags = new LinkedList<>();
        for (Tag tag : organisation.getTags()) {
            // if(tag.getTagged().size() > 1)
            relatedTags.add(tag);
        }

        return relatedTags;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * shouldPerformDrag()
     */
    @Override
    protected boolean shouldPerformDrag(DragStartedEvent dragEvent) {
        WebView webView = this.controller.getWebView();
        if (webView != null) {
            Bounds boundsLocal = webView.getBoundsInLocal();
            if (boundsLocal != null) {
                Bounds boundsInScene = webView.localToScene(boundsLocal);
                if (boundsInScene != null) {
                    return !boundsInScene.contains(dragEvent.getSceneX(), dragEvent.getSceneY());
                }
            }
        }
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * getInDetailCloseButton()
     */
    @Override
    protected Text getInDetailCloseButton() {
        return this.controller.getInDetailCloseButton();
    }
}
