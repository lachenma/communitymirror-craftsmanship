
package org.sociotech.communitymirror.view.flow.visualitems.content;

import org.sociotech.communitymashup.data.Content;

import javafx.scene.shape.Circle;

/**
 * The visualization of a content item in the micro visual state
 *
 * @author Andrea Nutsi
 *
 */
public class MicroContentItem extends ContentItem {

    public MicroContentItem(Content dataItem) {
        super(dataItem);
    }

    @Override
    protected void render() {
        super.render();
        if (this.image != null) {
            Circle circle = new Circle(40);
            circle.setLayoutY(40);
            circle.setLayoutX(40);
            this.controller.setContentImageClip(circle);
        } else {
            Circle circle = new Circle(40);
            circle.setLayoutY(3);
            circle.setLayoutX(43);
            this.controller.setTitleClip(circle);
            this.controller.bindToTitle(this.title, 20);
        }
        if (this.authorImage != null) {
            Circle circle = new Circle(8);
            circle.setLayoutY(8);
            circle.setLayoutX(8);
            this.controller.setContentAuthorImageClip(circle);
        }
    }
}
