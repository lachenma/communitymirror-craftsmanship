
package org.sociotech.communitymirror.view.flow.visualitems.content;

import org.sociotech.communitymashup.data.Content;

import javafx.scene.shape.Circle;

/**
 * The visualization of a content item in the spotlight visual state
 *
 * @author Andrea Nutsi
 *
 */
public class SpotlightContentItem extends ContentItem {

    public SpotlightContentItem(Content dataItem) {
        super(dataItem);
    }

    @Override
    protected void render() {
        super.render();
        // if available set rounded author image
        if (this.authorImage != null) {
            Circle circle = new Circle(60);
            circle.setLayoutY(70);
            circle.setLayoutX(85);
            this.controller.setContentAuthorImageClip(circle);
        }
        this.controller.bindToDescription(this.description, 60);
    }
}
