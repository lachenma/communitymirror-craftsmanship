
package org.sociotech.communitymirror.view.flow.visualitems;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communityinteraction.events.InteractionEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragDraggedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.drag.DragStartedEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationEvent;
import org.sociotech.communityinteraction.events.gesture.rotation.RotationFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.zoom.ZoomZoomedEvent;
import org.sociotech.communityinteraction.events.touch.TouchPressedEvent;
import org.sociotech.communityinteraction.events.touch.TouchReleasedEvent;
import org.sociotech.communityinteraction.events.touch.TouchTappedEvent;
import org.sociotech.communityinteraction.helper.CIEventConsumerHelper;
import org.sociotech.communityinteraction.helper.CIListHelper;
import org.sociotech.communityinteraction.interfaces.CIEventConsumer;
import org.sociotech.communitymashup.data.Content;
import org.sociotech.communitymashup.data.InformationObject;
import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymashup.data.Organisation;
import org.sociotech.communitymashup.data.Person;
import org.sociotech.communitymashup.data.Tag;
import org.sociotech.communitymashup.data.impl.ContentImpl;
import org.sociotech.communitymashup.data.impl.OrganisationImpl;
import org.sociotech.communitymashup.data.impl.PersonImpl;
import org.sociotech.communitymashup.data.impl.TagImpl;
import org.sociotech.communitymirror.view.flow.FlowView;
import org.sociotech.communitymirror.visualgroups.layout.CircleVisualGroup;
import org.sociotech.communitymirror.visualitems.VisualGroup;
import org.sociotech.communitymirror.visualitems.components.spring.SpringConnector;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;
import org.sociotech.communitymirror.visualstates.Detail;
import org.sociotech.communitymirror.visualstates.Micro;
import org.sociotech.communitymirror.visualstates.Preview;
import org.sociotech.communitymirror.visualstates.VisualState;

import com.google.common.io.Resources;

import freemarker.cache.URLTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;
import javafx.beans.property.DoubleProperty;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.scene.text.Text;

/**
 * Abstract super class of elements used in the flow view.
 *
 * @author Peter Lachenmaier, Andrea Nutsi, Eva Loesch
 *
 * @param <T>
 *            Type of the data item that will be visualize by this class.
 */
public abstract class FlowVisualItem<T extends Item, V extends FXMLController> extends FXMLVisualItem<T, V> {

    /**
     * Preferred minimal number of related organisations for set of related
     * Items of this Item as defined in config.
     */
    protected int preferredMinimalNumberOfRelatedOrganisations = 0;

    /**
     * Preferred minimal number of related participants for set of related Items
     * of this Item as defined in config.
     */
    protected int preferredMinimalNumberOfRelatedPersons = 0;

    /**
     * Preferred minimal number of related events for set of related Items of
     * this Item as defined in config.
     */
    protected int preferredMinimalNumberOfRelatedEvents = 0;

    /**
     * Preferred minimal number of related blogposts for set of related Items of
     * this Item as defined in config.
     */
    protected int preferredMinimalNumberOfRelatedBlogPosts = 0;

    /**
     * Preferred minimal number of related comments for set of related Items of
     * this Item as defined in config.
     */
    protected int preferredMinimalNumberOfRelatedComments = 0;

    /**
     * Preferred minimal number of related microposts for set of related Items
     * of this Item as defined in config.
     */
    protected int preferredMinimalNumberOfRelatedMicroPosts = 0;

    /**
     * Preferred minimal number of related picture for set of related Items of
     * this Item as defined in config.
     */
    protected int preferredMinimalNumberOfRelatedPictures = 0;

    /**
     * Preferred minimal number of related tags for set of related Items of this
     * Item as defined in config.
     */
    protected int preferredMinimalNumberOfRelatedTags = 0;

    /**
     * Preferred minimal number of related participants for set of related Items
     * of this Item as defined in config.
     */
    protected int maximumNumberOfRelatedItems = 0;

    /**
     * Determines if the within a spring connection should be fixed.
     */
    private boolean fixed;

    /**
     * Indicates if drag event should be handled
     */
    private boolean performDrag = true;

    /**
     * The event listeners that notify the workspace and the view this visual
     * item belongs to about user interaction on this visual item.
     *
     * @author Eva Loesch
     */
    private List<WorkspaceNodeListener> workspaceListeners = new LinkedList<>();

    /**
     * The circle visual group this visual item belongs to within a workspace.
     *
     * @author Eva Loesch
     */
    private CircleVisualGroup circleGroup;

    /**
     * The visual item that is the parent node of this visual item.
     *
     * @author Eva Loesch
     */
    private FlowVisualItem<? extends Item, ? extends FXMLController> parentNode;

    /**
     * List of all visual items that are children of this visual item.
     *
     * @author Eva Loesch
     */
    private List<FlowVisualItem<? extends Item, ? extends FXMLController>> childNodes;

    /**
     * The spring connector of this visual item.
     */
    private SpringConnector offsetSpringConnector;

    /**
     * List of all related items of this item, that are connected to this item
     * in the view.
     *
     * @author Eva Lösch
     */
    private List<Item> connectedRelatedItems = new LinkedList<>();

    /**
     * Creates a flow visual item for the given data item without a parent node.
     *
     * @param T
     *            dataItem: the data item a flow visual item should be created
     *            for.
     */
    public FlowVisualItem(T dataItem) {
        super(dataItem);

        this.fixed = false;
        this.circleGroup = null;
        this.setParentNode(null);
        this.setChildNodes(new LinkedList<FlowVisualItem<? extends Item, ? extends FXMLController>>());

    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.visualitems.VisualComponent#onInit()
     */
    @Override
    protected void onInit() {
        super.onInit();

        // create spring connector
        // centerSpringConnector =
        // SpringConnector.createCenterSpringConnector(this);
        this.offsetSpringConnector = this.createSpringConnector();
    }

    /**
     * Override this method in concrete subclasses to create design specific
     * spring connectors.
     *
     * @return The spring connector for this component.
     */
    protected SpringConnector createSpringConnector() {
        return new SpringConnector(this, 85, 85);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.visualitems.VisualComponent#
     * getSpringConnectorXProperty()
     */
    @Override
    public DoubleProperty getSpringConnectorXProperty() {
        return this.offsetSpringConnector.getSpringConnectorXProperty();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.visualitems.VisualComponent#
     * getSpringConnectorYProperty()
     */
    @Override
    public DoubleProperty getSpringConnectorYProperty() {
        return this.offsetSpringConnector.getSpringConnectorYProperty();
    }

    /**
     * Creates a flow visual item for the given data item and a given parent
     * node.
     *
     * @param T
     *            dataItem: the data item a flow visual item should be created
     *            for.
     * @param FlowVisualItem<?
     *            extends Item, ? extends ItemController> parentNode: the parent
     *            visual item a child flow visual item should be created for.
     *
     * @author Eva Loesch
     */
    public FlowVisualItem(T dataItem, boolean fixed,
            FlowVisualItem<? extends Item, ? extends FXMLController> parentNode) {
        super(dataItem);

        this.fixed = fixed;
        this.circleGroup = null;
        this.setParentNode(parentNode);
        this.setChildNodes(new LinkedList<FlowVisualItem<? extends Item, ? extends FXMLController>>());
    }

    /**
     * Sets the fixed property of this flow visual item.
     *
     * @author Eva Loesch
     */
    public void setFixed(boolean fixed) {
        this.fixed = fixed;
    }

    /**
     * Gets the fixed property of this flow visual item.
     *
     * @author Eva Loesch
     */
    public boolean getFixed() {
        return this.fixed;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.springview.components.SpringConnectable#
     * isFixed()
     */
    @Override
    public boolean isFixed() {
        // if(visualState instanceof Detail) {
        // // detail is always fixed to get thrown
        // return true;
        // }
        return this.fixed;
    }

    // implementation of HandleDrag
    @Override
    public void onDragDragged(DragDraggedEvent dragEvent) {
        if (this.performDrag) {
            // no inertial drag after release
            if (dragEvent.isInertia() && !this.fixed && !(this.getVisualState() instanceof Preview
                    || this.getVisualState() instanceof Micro || this.getVisualState() instanceof Detail)) {
                return;
            }
            super.onDragDragged(dragEvent);
            dragEvent.consume();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleDrag#
     * onDragStarted(org.sociotech.communityinteraction.events.gesture.drag.
     * DragStartedEvent)
     */
    @Override
    public void onDragStarted(DragStartedEvent dragEvent) {
        this.fix();

        // check if drag should result in the item to be dragged
        // store result for DragDragged event
        this.performDrag = this.shouldPerformDrag(dragEvent);
        if (this.performDrag) {
            super.onDragStarted(dragEvent);
            dragEvent.consume();
        }
    }

    /**
     * Some VisualItems might have areas where a drag should not result in the
     * item do be dragged, but only be handled by the areas components (e.g. a
     * scrollable area) - check this using this function that has to be
     * overwritten by derived classes
     */
    protected boolean shouldPerformDrag(DragStartedEvent dragEvent) {
        // default: return true (no special areas)
        return true;
    }

    /**
     * Fixes this component to avoid spring forces
     */
    private void fix() {
        // fix to avoid spring forces
        this.fixed = true;

        // change color to indicate fix
        // circle.setFill(Color.RED);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.visualitems.VisualComponent#onTouchPressed(
     * org.sociotech.communityinteraction.events.touch.TouchPressedEvent)
     */
    @Override
    public void onTouchPressed(TouchPressedEvent event) {
        super.onTouchPressed(event);
        this.fix();

        this.updateZIndexOfRelatedWorkspace(event);
    }

    /**
     * Updates the z-index of the workspace the touched visual component belongs
     * to.
     *
     * @param event
     *            : TouchPressedEvent that has occurred on this VisualComponent
     */
    private void updateZIndexOfRelatedWorkspace(TouchPressedEvent event) {
        VisualGroup parentGroup = this.getParentVisualGroup();

        // if touched item is part of a workspace, update also z-index of the
        // corresponding WorkspaceVisualGroup
        if (parentGroup instanceof CircleVisualGroup) {
            // get workspace visual group
            VisualGroup workspaceVisualGroup = parentGroup.getParentVisualGroup();
            List<CIEventConsumer> consumers = new LinkedList<>();
            consumers.add(workspaceVisualGroup);
            event.setTargetConsumers(consumers);

            workspaceVisualGroup.onTouchPressed(event);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.visualitems.VisualComponent#onTouchReleased
     * (org.sociotech.communityinteraction.events.touch.TouchReleasedEvent)
     */
    @Override
    public void onTouchReleased(TouchReleasedEvent event) {
        super.onTouchReleased(event);
        this.releaseFix();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleDrag#
     * onDragFinished(org.sociotech.communityinteraction.events.gesture.drag.
     * DragFinishedEvent)
     */
    @Override
    public void onDragFinished(DragFinishedEvent dragEvent) {
        this.releaseFix();

        // perform user activity logging to log this drag finished event
        this.performUserActivityLogging(dragEvent);

        if (this.performDrag) {
            super.onDragFinished(dragEvent);
        }

        // consume event
        dragEvent.consume();
    }

    /**
     * Returns the visual state that follows the current state.
     *
     * @return The next visual state.
     */
    protected VisualState getNextVisualState() {

        VisualState currentVisualState = this.getVisualState();

        if (currentVisualState == null) {
            // Start with Preview
            return VisualState.PREVIEW;
        } else if (currentVisualState.isPreview()) {
            // Detail after Preview
            return VisualState.DETAIL;
        } else if (currentVisualState.isDetail()) {
            // Stay in Detail
            return VisualState.DETAIL;
        } else {
            // Default current
            return currentVisualState;
        }
    }

    /**
     * Returns the visual state that follows the current state.
     *
     * @return The next visual state.
     */
    protected VisualState getPreviousVisualState() {

        VisualState currentVisualState = this.getVisualState();

        if (currentVisualState == null) {
            // no previous
            return null;
        } else if (currentVisualState.isPreview()) {
            // close
            return null;
        } else if (currentVisualState.isDetail()) {
            // Preview before Detail
            return VisualState.PREVIEW;
        } else {
            // Default current
            return currentVisualState;
        }
    }

    /**
     * Releases a possible fix on this component
     */
    private void releaseFix() {
        // enable spring forces again after drag
        this.fixed = false;

        // reset fix color
        // circle.setFill(Color.WHITE);
    }

    /**
     * Notifies the workspace and the view this visual item belongs to about a
     * touch tapped event on this visual item.
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleTouch#onTouchTapped(org.sociotech.communityinteraction.events.touch.TouchTappedEvent)
     *
     * @author Andrea Nutsi, Eva Loesch
     */
    @Override
    public void onTouchTapped(TouchTappedEvent event) {
        // perform user activity logging to log this touch tapped event
        this.performUserActivityLogging(event);

        Text inDetailCloseButton = this.getInDetailCloseButton();
        boolean touchWithinInDetailCloseButton = false;
        if (inDetailCloseButton != null) {
            Bounds inDetailCloseButtonBoundsLocal = inDetailCloseButton.getBoundsInLocal();
            if (inDetailCloseButtonBoundsLocal != null) {
                Bounds inDetailCloseButtonBoundsInScene = inDetailCloseButton
                        .localToScene(inDetailCloseButtonBoundsLocal);
                if (inDetailCloseButtonBoundsInScene != null) {
                    // accept touches next to the button
                    inDetailCloseButtonBoundsInScene = new BoundingBox(
                            inDetailCloseButtonBoundsInScene.getMinX() - 40.0,
                            inDetailCloseButtonBoundsInScene.getMaxY() - 40.0,
                            inDetailCloseButtonBoundsInScene.getWidth() + 80.0,
                            inDetailCloseButtonBoundsInScene.getHeight() + 80.0);
                    touchWithinInDetailCloseButton = inDetailCloseButtonBoundsInScene.contains(event.getSceneX(),
                            event.getSceneY());
                }
            }
        }
        if (touchWithinInDetailCloseButton) {
            for (WorkspaceNodeListener listener : this.workspaceListeners) {
                listener.onWorkspaceNodeInDetailClosed(new WorkspaceNodeInteractionEvent<>(this, event));
            }
        }

        // notify flow view and/or workspace to do the action
        if (!touchWithinInDetailCloseButton) {
            this.notifyWorkspace(new WorkspaceNodeInteractionEvent<>(this, event));
        }

        event.consume();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.visualitems.VisualComponent#
     * onRotationFinished(org.sociotech.communityinteraction.events.gesture.
     * rotation.RotationFinishedEvent)
     */
    @Override
    public void onRotationFinished(RotationFinishedEvent event) {
        // perform user activity logging to log this touch tapped event
        this.performUserActivityLogging(event);

        event.consume();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communityinteraction.interactionhandling.HandleZoom#
     * onZoomZoomed(org.sociotech.communityinteraction.events.gesture.zoom.
     * ZoomZoomedEvent)
     */
    @Override
    public void onZoomZoomed(ZoomZoomedEvent zoomEvent) {

        double oldScaleX = this.getScaleX();
        double scaleX = this.getScaleX() * zoomEvent.getZoomFactor();
        double scaleY = this.getScaleY() * zoomEvent.getZoomFactor();

        // limit scale
        if (scaleX > 0.4 && scaleX < 1.8) {
            // scale in both directions
            this.setScaleX(scaleX);
            this.setScaleY(scaleY);
        }

        // check zoom levels
        if (scaleX < 0.6 && oldScaleX > 0.6 || scaleX < 0.9 && oldScaleX > 0.9 || scaleX > 1.2 && oldScaleX < 1.2
                || scaleX > 1.5 && oldScaleX < 1.5) {

            // perform user activity logging to log this touch tapped event
            this.performUserActivityLogging(zoomEvent);

            // switch to next visual state
            // notify flow view and/or workspace to do the action
            this.notifyWorkspace(new WorkspaceNodeInteractionEvent<>(this, zoomEvent));
        }

        // and consume event
        zoomEvent.consume();
    }

    /**
     * Adds a event listener that is notified about user interaction on this
     * visual item.
     *
     * @param WorkspaceNodeListener
     *            workspaceListeners: the workspace event listener that should
     *            be set as workspace event listener of this visual item.
     * @author Eva Loesch
     */
    public void addWorkspaceListener(WorkspaceNodeListener workspaceListener) {
        if (!this.workspaceListeners.contains(workspaceListener)) {
            this.workspaceListeners.add(workspaceListener);
        }
    }

    public void removeWorkspaceListeners() {
        this.workspaceListeners.clear();
    }

    /**
     * Method to notify the workspace this visual item belongs to about user
     * interaction on this visual item.
     *
     * @param WorkspaceNodeInteractionEvent<T,
     *            V> event : the event that has occurred on this visual item.
     * @author Eva Loesch
     */
    protected synchronized void notifyWorkspace(WorkspaceNodeInteractionEvent<T, V> event) {
        List<WorkspaceNodeListener> workspaceListenersCopy = new LinkedList<>();
        for (WorkspaceNodeListener listener : this.workspaceListeners) {
            workspaceListenersCopy.add(listener);
        }

        // call right method only on listeners that exist when this method is
        // called and not on listeners that are created by calling onTouchTapped
        for (WorkspaceNodeListener listener : workspaceListenersCopy) {
            CIEvent sourceEvent = event.getSourceEvent();

            if (sourceEvent instanceof TouchTappedEvent) {
                listener.onWorkspaceNodeTouchTapped(event);
            } else if (sourceEvent instanceof ZoomZoomedEvent) {

                ZoomZoomedEvent zoomedEvent = (ZoomZoomedEvent) sourceEvent;

                if (zoomedEvent.getZoomFactor() < 1) {
                    listener.onWorkspaceNodeZoomZoomedSmaller(event);
                } else {
                    listener.onWorkspaceNodeZoomZoomedBigger(event);
                }
            }
        }
    }

    /**
     * Adds a given related item to the list of all related items that are
     * connected with this item within a workspace.
     *
     * @param InformationObject
     *            relatedItem : the related item that should be added to the
     *            list of connected related items
     * @author Eva Loesch
     */
    public void addConnectedRelatedItem(Item relatedItem) {
        if (!this.connectedRelatedItems.contains(relatedItem)) {
            this.connectedRelatedItems.add(relatedItem);
        }
    }

    /**
     * Adds a given list of related items to the list of all related items that
     * are connected with this item within a workspace.
     *
     * @param EList<InformationObject>
     *            relatedItems : the list of related items that should be added
     *            to the list of connected related items
     * @author Eva Loesch
     */
    public void addConnectedRelatedItems(List<Item> relatedItems) {
        for (Item relatedItem : relatedItems) {
            if (!this.connectedRelatedItems.contains(relatedItem)) {
                this.connectedRelatedItems.add(relatedItem);
            }
        }
    }

    /**
     * Removes the given related item from the list of all related items that
     * are connected with this item within a workspace.
     *
     * @param InformationObject
     *            relatedItem : the related item that should be removed from the
     *            list of connected related items
     * @author Eva Loesch
     */
    public void removeFromConnectedRelatedItems(Item relatedItem) {
        if (!this.connectedRelatedItems.contains(relatedItem)) {
            this.connectedRelatedItems.remove(relatedItem);
        }
    }

    // public Set<Item> getRelatedItemsToAdd(Set<Item> alreadyConnectedItems,
    // Set<Item> preferredDataTypes, int maxNumberOfRelatedItems)
    // {
    // Set<Item> allRelatedItems = this.getAllRelatedItems();
    //
    // // calculate number of items to add
    // int numberOfAlreadyConnectedItems = alreadyConnectedItems.size();
    // int numberOfItemsToAdd = Math.min(allRelatedItems.size() -
    // numberOfAlreadyConnectedItems, maxNumberOfRelatedItems -
    // numberOfAlreadyConnectedItems);
    //
    // // create sorted list of all items corresponding to the list of preferred
    // data types
    // Set<HashSet<Item>> preferredItemSets = new HashSet<HashSet<Item>>();
    //
    // Iterator<Item> iterator = preferredDataTypes.iterator();
    // while(iterator.hasNext())
    // {
    // Item item = (Item) iterator.next();
    //
    // Set<Item> itemSet = new
    // }
    //
    // // create empty set
    // Set<Item> relatedItemsToAdd = new HashSet<Item>();
    //
    // // take items from sorted list and add to set of relatedItemsToAdd until
    // numberOfItemsToAdd is reached
    //
    // while(numberOfItemsToAdd > 0)
    // {
    // Item itemToAdd = null;
    // relatedItemsToAdd.add(itemToAdd);
    // }
    //
    // return relatedItemsToAdd;
    // }

    /**
     * Gets the visual item that is the parent node of this visual item.
     *
     * @author Eva Loesch
     */
    public FlowVisualItem<? extends Item, ? extends FXMLController> getParentNode() {
        return this.parentNode;
    }

    /**
     * Sets the visual item that is the parent node of this visual item.
     *
     * @param WorkspaceNodeVisualItem<?
     *            extends Item, ? extends ItemController> parentNode : the
     *            visual item that should be set as parent node of this visual
     *            item
     * @author Eva Loesch
     */
    public void setParentNode(FlowVisualItem<? extends Item, ? extends FXMLController> parentNode) {
        this.parentNode = parentNode;
    }

    /**
     * Gets the circle visual group this visual item belongs to within a
     * workspace.
     *
     * @author Eva Loesch
     */
    public CircleVisualGroup getCircleGroup() {
        return this.circleGroup;
    }

    /**
     * Sets the circle visual group this visual item belongs to within a
     * workspace.
     *
     * @param CircleNodeSpringLayouter
     *            circleGroup : the circle visual group that should be set as
     *            circle visual group for this visual item.
     * @author Eva Loesch
     */

    public void setCircleGroup(CircleVisualGroup circleGroup) {
        this.circleGroup = circleGroup;
    }

    // **** TODO: check if the following methods are needed
    // ************************************

    /**
     * Gets the list of all workspace node visual items that are children of
     * this workspace node visual item.
     *
     * @author Eva Loesch
     */
    public List<FlowVisualItem<? extends Item, ? extends FXMLController>> getChildNodes() {
        return this.childNodes;
    }

    /**
     * Sets the list of all workspace node visual items that are children of
     * this workspace node visual item.
     *
     * @param EList<WorkspaceNodeVisualItem<?
     *            extends Item, ? extends ItemController>> childNodes : the list
     *            of workspace node visual items that should be set as child
     *            nodes of this workspace node visual item
     * @author Eva Loesch
     */
    public void setChildNodes(List<FlowVisualItem<? extends Item, ? extends FXMLController>> childNodes) {
        this.childNodes = childNodes;
    }

    /**
     * Adds a new child node to the list of all visual items that are children
     * of this visual item.
     *
     * @param childNode
     *            : the visual item that should be added to the list of child
     *            nodes of this visual item
     * @author Eva Loesch
     */
    public void addChildNode(FlowVisualItem<? extends Item, ? extends FXMLController> childNode) {
        this.childNodes.add(childNode);
    }

    /**
     * Removes a new child node from the list of all visual items that are
     * children of this visual item.
     *
     * @param childNode
     *            : the visual item that should be removed from the list of
     *            child nodes of this visual item
     * @author Eva Loesch
     */
    public void removeChildNode(FlowVisualItem<? extends Item, ? extends FXMLController> childNode) {
        this.childNodes.remove(childNode);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.visualitems.VisualComponent#
     * getConsumersToTop(org.sociotech.communityinteraction.events.CIEvent)
     */
    @Override
    public List<CIEventConsumer> getConsumersToTop(CIEvent event) {

        // create new list for result
        List<CIEventConsumer> consumerList = CIListHelper.createConsumerList();

        // ONLY: mine
        // use behaviors
        consumerList.addAll(CIEventConsumerHelper.collectConsumersFromBehaviors(this.interactionBehaviors, event));

        return consumerList;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.visualitems.VisualComponent#getWeight()
     */
    @Override
    public double getWeight() {
        // use visual state for weight
        if (this.visualState instanceof Detail) {
            return 8.0;
        }
        if (this.visualState instanceof Preview) {
            return 2.0;
        }

        // else
        return super.getWeight();
    }

    /**
     * Get list of all related organizations of this FlowVisualItem.
     *
     * @return list of related organizations (DEFAULT: empty list)
     */
    protected List<Organisation> getRelatedOrganizations() {
        return new LinkedList<>();
    }

    /**
     * Get list of all related persons of this FlowVisualItem.
     *
     * @return list of related persons (DEFAULT: empty list)
     */
    protected List<Person> getRelatedPersons() {
        return new LinkedList<>();
    }

    /**
     * Get list of all related contents of this FlowVisualItem.
     *
     * @return list of related contents (DEFAULT: empty list)
     */
    protected List<Content> getRelatedContents() {
        return new LinkedList<>();
    }

    /**
     * Get list of all related tags of this FlowVisualItem.
     *
     * @return list of related tags (DEFAULT: empty list)
     */
    protected List<Tag> getRelatedTags() {
        return new LinkedList<>();
    }

    /**
     * Get list of all related events of this FlowVisualItem.
     *
     * @return list of related events (DEFAULT: empty list)
     */
    private List<Content> getRelatedEvents() {
        List<Content> events = new LinkedList<>();
        for (Content contentItem : this.getRelatedContents()) {
            if (contentItem.hasMetaTag("agendaitem")) {
                events.add(contentItem);
            }
        }
        return events;
    }

    /**
     * Get list of all related microPosts of this FlowVisualItem.
     *
     * @return list of related microPosts (DEFAULT: empty list)
     */
    private List<Content> getRelatedMicroPosts() {
        List<Content> microPosts = new LinkedList<>();
        for (Content contentItem : this.getRelatedContents()) {
            if (contentItem.hasMetaTag("twitter")) {
                microPosts.add(contentItem);
            }
        }
        return microPosts;
    }

    /**
     * Get list of all related blogPosts of this FlowVisualItem.
     *
     * @return list of related blogPosts (DEFAULT: empty list)
     */
    private List<Content> getRelatedBlogPosts() {
        List<Content> blogPosts = new LinkedList<>();
        for (Content contentItem : this.getRelatedContents()) {
            if (contentItem.hasMetaTag("news")) {
                blogPosts.add(contentItem);
            }
        }
        return blogPosts;
    }

    /**
     * Get list of all related pictures of this FlowVisualItem.
     *
     * @return list of related pictures (DEFAULT: empty list)
     */
    private List<Content> getRelatedPictures() {
        List<Content> pictures = new LinkedList<>();
        for (Content contentItem : this.getRelatedContents()) {
            if (contentItem.hasMetaTag("impression_image")) {
                pictures.add(contentItem);
            }
        }
        return pictures;
    }

    /**
     * @param preferredMinimalNumberOfRelatedOrganisations
     *            the preferredMinimalNumberOfRelatedOrganisations to set
     */
    public void setPreferredMinimalNumberOfRelatedOrganisations(int preferredMinimalNumberOfRelatedOrganisations) {
        this.preferredMinimalNumberOfRelatedOrganisations = preferredMinimalNumberOfRelatedOrganisations;
    }

    /**
     * @param preferredMinimalNumberOfRelatedPersons
     *            the preferredMinimalNumberOfRelatedPersons to set
     */
    public void setPreferredMinimalNumberOfRelatedPersons(int preferredMinimalNumberOfRelatedPersons) {
        this.preferredMinimalNumberOfRelatedPersons = preferredMinimalNumberOfRelatedPersons;
    }

    /**
     * @param preferredMinimalNumberOfRelatedEvents
     *            the preferredMinimalNumberOfRelatedEvents to set
     */
    public void setPreferredMinimalNumberOfRelatedEvents(int preferredMinimalNumberOfRelatedEvents) {
        this.preferredMinimalNumberOfRelatedEvents = preferredMinimalNumberOfRelatedEvents;
    }

    /**
     * @param preferredMinimalNumberOfRelatedBlogPost
     *            the preferredMinimalNumberOfRelatedBlogPost to set
     */
    public void setPreferredMinimalNumberOfRelatedBlogPosts(int preferredMinimalNumberOfRelatedBlogPosts) {
        this.preferredMinimalNumberOfRelatedBlogPosts = preferredMinimalNumberOfRelatedBlogPosts;
    }

    /**
     * @param preferredMinimalNumberOfRelatedComment
     *            the preferredMinimalNumberOfRelatedComment to set
     */
    public void setPreferredMinimalNumberOfRelatedComments(int preferredMinimalNumberOfRelatedComments) {
        this.preferredMinimalNumberOfRelatedComments = preferredMinimalNumberOfRelatedComments;
    }

    /**
     * @param preferredMinimalNumberOfRelatedMicroPost
     *            the preferredMinimalNumberOfRelatedMicroPost to set
     */
    public void setPreferredMinimalNumberOfRelatedMicroPosts(int preferredMinimalNumberOfRelatedMicroPosts) {
        this.preferredMinimalNumberOfRelatedMicroPosts = preferredMinimalNumberOfRelatedMicroPosts;
    }

    /**
     * @param preferredMinimalNumberOfRelatedPicture
     *            the preferredMinimalNumberOfRelatedPicture to set
     */
    public void setPreferredMinimalNumberOfRelatedPictures(int preferredMinimalNumberOfRelatedPictures) {
        this.preferredMinimalNumberOfRelatedPictures = preferredMinimalNumberOfRelatedPictures;
    }

    /**
     * @param preferredMinimalNumberOfRelatedTags
     *            the preferredMinimalNumberOfRelatedTags to set
     */
    public void setPreferredMinimalNumberOfRelatedTags(int preferredMinimalNumberOfRelatedTags) {
        this.preferredMinimalNumberOfRelatedTags = preferredMinimalNumberOfRelatedTags;
    }

    /**
     * @param maximumNumberOfRelatedItems
     *            the maximumNumberOfRelatedItems to set
     */
    public void setMaximumNumberOfRelatedItems(int maximumNumberOfRelatedItems) {
        this.maximumNumberOfRelatedItems = maximumNumberOfRelatedItems;
    }

    /**
     * Returns the list of all related items of this item or a empty list if no
     * related items exist.
     */
    public Set<Item> getAllRelatedItems() {
        Set<Item> relatedItems = new HashSet<>();

        // add related organizations
        relatedItems.addAll(this.getRelatedOrganizations());

        // add related persons
        relatedItems.addAll(this.getRelatedPersons());

        // add related tags
        // relatedItems.addAll(this.getRelatedTags()); // TODO reactivate

        // add related contents
        relatedItems.addAll(this.getRelatedContents());

        // add relations via connections ...
        if (this.getDataItem() instanceof InformationObject) {
            InformationObject item = (InformationObject) this.getDataItem();
            relatedItems.addAll(item.getInformationObjectsConnected());
        }

        // correct size if one of the addAlls added a NULL element
        if (relatedItems.size() == 1) {
            if (relatedItems.iterator().next() == null) {
                return new HashSet<>();
            }
        }

        return relatedItems;
    };

    /**
     * Creates a set of selected related items according to parameters in
     * configuration and considering the set of already connected related items.
     *
     * @param set
     *            of items that have already been connected and should not be
     *            included in list of selected related Items to connect.
     * @return set of selected related items
     */
    public Set<Item> getSelectedSetOfRelatedItemsToConnect(Set<Item> connectedRelatedItems) {
        // get all related items of this FlowVisualItem
        List<Item> allRelatedItems = new LinkedList<>();
        allRelatedItems.addAll(this.getAllRelatedItems());

        // get related organizations
        List<Organisation> relatedOrganisations = this.getRelatedOrganizations();
        int numberOfOrganisationsToConnect = this.preferredMinimalNumberOfRelatedOrganisations;

        // get related persons
        List<Person> relatedPersons = this.getRelatedPersons();
        int numberOfPersonsToConnect = this.preferredMinimalNumberOfRelatedPersons;

        // get related events
        List<Content> relatedEvents = this.getRelatedEvents();
        int numberOfEventsToConnect = this.preferredMinimalNumberOfRelatedEvents;

        // get related blogPosts
        List<Content> relatedBlogPosts = this.getRelatedBlogPosts();
        int numberOfBlogPostsToConnect = this.preferredMinimalNumberOfRelatedBlogPosts;

        // get related microPosts
        List<Content> relatedMicroPosts = this.getRelatedMicroPosts();
        int numberOfMicroPostsToConnect = this.preferredMinimalNumberOfRelatedMicroPosts;

        // get related pictures
        List<Content> relatedPictures = this.getRelatedPictures();
        int numberOfPicturesToConnect = this.preferredMinimalNumberOfRelatedPictures;

        // get related tags
        List<Tag> relatedTags = this.getRelatedTags();
        int numberOfTagsToConnect = this.preferredMinimalNumberOfRelatedTags;

        int maxNumberOfItemsToConnect = this.maximumNumberOfRelatedItems;

        if (connectedRelatedItems != null && connectedRelatedItems.size() > 0) {
            for (Item connectedItem : connectedRelatedItems) {
                if (connectedItem instanceof OrganisationImpl) {
                    relatedOrganisations.remove(connectedItem);
                    allRelatedItems.remove(connectedItem);

                    numberOfOrganisationsToConnect--;
                } else if (connectedItem instanceof PersonImpl) {
                    relatedPersons.remove(connectedItem);
                    allRelatedItems.remove(connectedItem);

                    numberOfPersonsToConnect--;
                } else if (connectedItem instanceof ContentImpl) {
                    if (relatedEvents.contains(connectedItem)) {
                        relatedEvents.remove(connectedItem);
                        allRelatedItems.remove(connectedItem);

                        numberOfEventsToConnect--;
                    } else if (relatedBlogPosts.contains(connectedItem)) {
                        relatedBlogPosts.remove(connectedItem);
                        allRelatedItems.remove(connectedItem);

                        numberOfBlogPostsToConnect--;
                    } else if (relatedMicroPosts.contains(connectedItem)) {
                        relatedMicroPosts.remove(connectedItem);
                        allRelatedItems.remove(connectedItem);

                        numberOfMicroPostsToConnect--;
                    } else if (relatedPictures.contains(connectedItem)) {
                        relatedPictures.remove(connectedItem);
                        allRelatedItems.remove(connectedItem);

                        numberOfPicturesToConnect--;
                    }
                } else if (connectedItem instanceof TagImpl) {
                    relatedTags.remove(connectedItem);
                    allRelatedItems.remove(connectedItem);

                    numberOfTagsToConnect--;
                } else {
                    allRelatedItems.remove(connectedItem);
                }
            }

            // update number of items to connect
            maxNumberOfItemsToConnect = maxNumberOfItemsToConnect - connectedRelatedItems.size();
        }

        // initialize set of selected related items
        List<Item> selectedRelatedItems = new LinkedList<>();

        // add preferred number of related *Organizations* to list if available
        int numberToConnect = Math.min(numberOfOrganisationsToConnect,
                (maxNumberOfItemsToConnect - selectedRelatedItems.size()));
        this.addNItemsFromShuffeledListToList(relatedOrganisations, selectedRelatedItems, numberToConnect);

        // add preferred number of related *Persons* to list if available
        numberToConnect = Math.min(numberOfPersonsToConnect, (maxNumberOfItemsToConnect - selectedRelatedItems.size()));
        this.addNItemsFromShuffeledListToList(relatedPersons, selectedRelatedItems, numberToConnect);

        // add preferred number of related *Events* to list if available
        numberToConnect = Math.min(numberOfEventsToConnect, (maxNumberOfItemsToConnect - selectedRelatedItems.size()));
        this.addNItemsFromShuffeledListToList(relatedEvents, selectedRelatedItems, numberToConnect);

        // add preferred number of related *MicroPosts* to list if available
        numberToConnect = Math.min(numberOfMicroPostsToConnect,
                (maxNumberOfItemsToConnect - selectedRelatedItems.size()));
        this.addNItemsFromShuffeledListToList(relatedMicroPosts, selectedRelatedItems, numberToConnect);

        // add preferred number of related *BlogPosts* to list if available
        numberToConnect = Math.min(numberOfBlogPostsToConnect,
                (maxNumberOfItemsToConnect - selectedRelatedItems.size()));
        this.addNItemsFromShuffeledListToList(relatedBlogPosts, selectedRelatedItems, numberToConnect);

        // add preferred number of related *Tags* to list if available
        numberToConnect = Math.min(numberOfTagsToConnect, (maxNumberOfItemsToConnect - selectedRelatedItems.size()));
        // this.addNItemsFromShuffeledListToList(relatedTags,
        // selectedRelatedItems, numberToConnect);
        // TODO reactivate

        // add preferred number of related *Pictures* to list if available
        numberToConnect = Math.min(numberOfPicturesToConnect,
                (maxNumberOfItemsToConnect - selectedRelatedItems.size()));
        this.addNItemsFromShuffeledListToList(relatedPictures, selectedRelatedItems, numberToConnect);

        // Fill up with any related items until maximum number of related items
        // is reached if available
        numberToConnect = maxNumberOfItemsToConnect - selectedRelatedItems.size();
        if (numberToConnect > 0) {
            // remove all items from list of selectedRelatedItems from list of
            // allRelatedItems
            allRelatedItems.removeAll(selectedRelatedItems);

            // add the preferred number of related items of type "Content" to
            // list if available
            this.addNItemsFromShuffeledListToList(allRelatedItems, selectedRelatedItems, numberToConnect);
        }

        // shuffle list of selected related items
        Collections.shuffle(selectedRelatedItems);

        // return list of selected related items
        return new HashSet<>(selectedRelatedItems);
    }

    /**
     * Shuffle FromList and then add first n items of FromList to ToList.
     *
     * @param fromList
     *            : list from which the items should be copied to toList
     * @param toList
     *            : list to which the items should be copied from fromList
     * @param number
     *            : number of items that should be copied from fromList to
     *            toList
     */
    protected void addNItemsFromShuffeledListToList(List<? extends Item> fromList, List<Item> toList, int number) {
        if (fromList != null && toList != null && fromList.size() > 0 && number > 0) {
            // generate ArrayList to ensure that shuffle can be performed
            ArrayList<Item> fromArrayList = new ArrayList<>();
            for (Item item : fromList) {
                fromArrayList.add(item);
            }

            Collections.shuffle(fromArrayList);

            int count = 0;
            for (Item item : fromArrayList) {
                if (count == number) {
                    break;
                }

                toList.add(item);
                count++;
            }
        }
    }

    /**
     * Method to be overwritten in subclasses for particles with
     * InDetailCloseButton
     *
     * @return the WebView of the particle
     */
    protected Text getInDetailCloseButton() {
        // default: return nothing
        return null;
    }

    /**
     * Uses the CommunityMirror User Activity Logger to log user interaction
     *
     * @param event
     *            : interaction event that should by logged
     */
    private void performUserActivityLogging(InteractionEvent event) {
        // initialize message and userAction
        String message = "";
        String userAction = "";

        // get from visual component
        String fromVisualState = this.getVisualState().toString();
        String toVisualState = "";

        if (event instanceof RotationEvent) {
            message = "Particle rotated!";
            userAction = "rotatedParticle";
        }

        else if (event instanceof DragFinishedEvent) {
            message = "Particle dragged!";
            userAction = "draggedParticle";
        } else if (event instanceof ZoomZoomedEvent) {
            ZoomZoomedEvent zoomEvent = (ZoomZoomedEvent) event;

            // get new visual state
            VisualState newVisualState = null;

            if (zoomEvent.getZoomFactor() < 1) {
                message = "Particle zoomed to previous visual state!";
                userAction = "particleToPreviousVisualState";

                newVisualState = this.getPreviousVisualState();
                if (newVisualState != null) {
                    toVisualState = this.getPreviousVisualState().toString();
                } else {
                    toVisualState = "null";
                }
            } else {
                message = "Particle zoomed to next visual state!";
                userAction = "particleToNextVisualState";

                newVisualState = this.getNextVisualState();
                toVisualState = newVisualState.toString();
            }
        } else if (event instanceof TouchTappedEvent) {
            message = "Particle tapped!";
            userAction = "particleToNextVisualState";

            // get to VisualState state
            VisualState newVisualState = this.getNextVisualState();
            toVisualState = newVisualState.toString();
        }

        FlowView.logUserInteractionEvent(message, userAction, event, this, fromVisualState, toVisualState);
    }

    /**
     * Process the detail template for the visual item using FreeMarker an
     * return the processed content to pass to the WebView in the detail view
     */
    public String processDetailTemplate(String tname, Map<String, Object> templateData) {
        String res = "<p>failed processing detail template</p>";
        try {
            URL ftlUrl = null;
            String ftlPath = "ftl";
            if (this.themeResources != null) {
                ftlUrl = this.themeResources.getResourceURL(ftlPath);
            } else {
                ftlUrl = Resources.getResource("flow/" + ftlPath);
                this.logger.warn(this.getClass().getName() + " using deprecated FTL path.");
            }
            Configuration cfg = new Configuration(new Version("2.3.23"));
            final URL templateDirUrl = ftlUrl;
            cfg.setTemplateLoader(new URLTemplateLoader() {
                @Override
                protected URL getURL(String templateName) {
                    if (templateName.endsWith("_de_DE.ftl") || templateName.endsWith("_en_US.ftl")) {
                        templateName = templateName.substring(0, templateName.length() - 10) + ".ftl";
                    }
                    try {
                        URL resUrl = new URL(templateDirUrl.toExternalForm() + "/" + templateName);
                        return resUrl;
                    } catch (Exception e) {}
                    return null;
                }
            });
            cfg.setDefaultEncoding("UTF-8");
            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            cfg.setLogTemplateExceptions(false);
            // get the standard template for this item type
            Template template = cfg.getTemplate(tname);
            templateData.put("baseurl", ftlUrl.toExternalForm());
            templateData.put("item", this);
            StringWriter out = new StringWriter();
            template.process(templateData, out);
            res = out.getBuffer().toString();
            out.flush();
        } catch (IOException e) {
            e.printStackTrace(System.err);
            res = e.getMessage();
        } catch (TemplateException e) {
            e.printStackTrace(System.err);
            res = e.getMessage();
        }
        return res;
    }
}
