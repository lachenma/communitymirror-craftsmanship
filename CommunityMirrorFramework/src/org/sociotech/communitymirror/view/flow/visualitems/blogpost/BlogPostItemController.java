
package org.sociotech.communitymirror.view.flow.visualitems.blogpost;

import java.net.URL;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 * The controller of a blog post item
 *
 * @author Andrea Nutsi
 *
 */
public class BlogPostItemController extends FXMLController implements Initializable {

    @FXML
    Text blogDate;

    @FXML
    Text blogAuthor;

    @FXML
    Text blogTitle;

    @FXML
    Text blogTitleLong;

    @FXML
    Text blogTitleShort;

    @FXML
    WebView detailWebView;

    @FXML
    ImageView blogImage;

    @FXML
    ImageView blogImageLarge;

    @FXML
    ImageView blogImageSquare;

    @FXML
    ImageView blogImageRound;

    @FXML
    ImageView blogAuthorImage;

    @FXML
    ImageView kreissegment_authorImage;

    @FXML
    Text blogContent;

    @FXML
    Text tagsCounter;

    @FXML
    Text blogTags;

    @FXML
    Group groupInsideScrollPane;

    @FXML
    ScrollPane scrollPane;

    @FXML
    ImageView categoryIcon;

    @FXML
    Circle categoryIconBackground;

    @FXML
    Text inDetailCloseButton;

    @FXML
    Circle graphConnection01;
    @FXML
    Circle graphConnection02;
    @FXML
    Circle graphConnection03;
    @FXML
    Circle graphConnection04;
    @FXML
    Circle graphConnection05;
    @FXML
    Circle graphConnection06;
    @FXML
    Circle graphConnection07;
    @FXML
    Circle graphConnection08;

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(BlogPostItemController.class.getName());

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        try {
            assert this.blogDate != null : "fx:id=\"blogDate\" was not injected: check your FXML file 'blog.fxml'.";
            assert this.blogAuthor != null : "fx:id=\"commentPath\" was not injected: check your FXML file 'blog.fxml'.";
            assert this.blogTitle != null : "fx:id=\"blogTitle\" was not injected: check your FXML file 'blog.fxml'.";
            assert this.blogTitleLong != null : "fx:id=\"blogTitleLong\" was not injected: check your FXML file 'blog.fxml'.";
            assert this.blogContent != null : "fx:id=\"blogContent\" was not injected: check your FXML file 'blog.fxml'.";
            assert this.blogImage != null : "fx:id=\"blogImage\" was not injected: check your FXML file 'blog.fxml'.";
            assert this.blogImageLarge != null : "fx:id=\"blogImageLarge\" was not injected: check your FXML file 'blog.fxml'.";
            assert this.blogAuthorImage != null : "fx:id=\"blogAuthorImage\" was not injected: check your FXML file 'blog.fxml'.";
            assert this.kreissegment_authorImage != null : "fx:id=\"kreissegment_authorImage\" was not injected: check your FXML file 'blog.fxml'.";
            assert this.tagsCounter != null : "fx:id=\"tagsCounter\" was not injected: check your FXML file 'blog.fxml'.";
            assert this.blogTags != null : "fx:id=\"blogTags\" was not injected: check your FXML file 'blog.fxml'.";
            assert this.groupInsideScrollPane != null : "fx:id=\"groupInsideScrollPane\" was not injected: check your FXML file 'blog.fxml'.";
            assert this.scrollPane != null : "fx:id=\"scrollPane\" was not injected: check your FXML file 'blog.fxml'.";
        } catch (AssertionError e) {
            this.logger.error(e.getMessage());
        }

    }

    protected void bindToBlogDateField(String blogDate) {
        this.bindToFXMLElement(this.blogDate, blogDate);
    }

    protected void bindToBlogAuthorField(String blogAuthor) {
        this.bindToFXMLElement(this.blogAuthor, blogAuthor);
    }

    protected void bindToBlogTitleField(String blogTitle) {
        this.bindToFXMLElement(this.blogTitle, blogTitle);
        this.bindToFXMLElement(this.blogTitleShort, this.truncateText(blogTitle, 70, true));
    }

    protected void bindToBlogTitleLongField(String blogTitle) {
        this.bindToFXMLElement(this.blogTitleLong, blogTitle);
        this.bindToFXMLElement(this.blogTitleShort, this.truncateText(blogTitle, 70, true));
    }

    protected void bindToTagsCounter(String numberTags) {
        this.bindToFXMLElement(this.tagsCounter, numberTags);
    }

    // protected void setBlogContent(String HTMLContent){
    // this.blogContent.getEngine().loadContent(HTMLContent);
    // }

    protected void bindToBlogContent(String content) {
        this.bindToFXMLElement(this.blogContent, content);
    }

    protected void bindToBlogContent(String content, int length) {
        this.bindToFXMLElement(this.blogContent, StringUtils.abbreviate(content, length));
    }

    protected void setBlogImage(Image blogImage) {
        if (this.blogImage != null) {
            this.blogImage.setImage(blogImage);
        }
    }

    protected void setBlogImageLarge(Image blogImage) {
        if (this.blogImageLarge != null) {
            this.blogImageLarge.setImage(blogImage);
        }
    }

    protected void setBlogImageSquare(Image blogImageSquare) {
        if (this.blogImageSquare != null) {
            this.blogImageSquare.setImage(blogImageSquare);
        }
    }

    protected void setBlogImageRound(Image blogImageRound) {
        if (this.blogImageRound != null) {
            this.blogImageRound.setImage(blogImageRound);
        }
    }

    protected void setBlogAuthorImage(Image blogAuthorImage) {
        if (this.kreissegment_authorImage != null) {
            this.kreissegment_authorImage.setVisible(true);
        }
        if (this.blogAuthorImage != null) {
            this.blogAuthorImage.setImage(blogAuthorImage);
        }
    }

    protected void setBlogImageClip(Node clip) {
        if (this.blogImage != null) {
            this.blogImage.setClip(clip);
        }
    }

    protected void setBlogAuthorImageClip(Node clip) {
        if (this.blogAuthorImage != null) {
            this.blogAuthorImage.setClip(clip);
        }

    }

    protected void setBlogTitleClip(Node clip) {
        if (this.blogTitle != null) {
            this.blogTitle.setClip(clip);
        }
    }

    protected void bindToBlogTags(String tags) {
        this.bindToFXMLElement(this.blogTags, tags);
    }

    protected void setCategoryIcon(Image categoryIcon) {
        if (this.categoryIcon != null) {
            if (categoryIcon != null) {
                this.categoryIcon.setImage(categoryIcon);
                this.categoryIcon.setOpacity(1.0);
                if (this.categoryIconBackground != null) {
                    this.categoryIconBackground.setOpacity(1.0);
                }
            } else {
                this.categoryIcon.setOpacity(0.0);
                if (this.categoryIconBackground != null) {
                    this.categoryIconBackground.setOpacity(0.0);
                }
            }
        }
    }

    public void adjustScrollPaneHeight() {
        double contentHeight = 9999999;
        if (this.groupInsideScrollPane != null) {
            contentHeight = this.groupInsideScrollPane.getLayoutBounds().getHeight();
        }
        if (contentHeight > 325.0) {
            this.scrollPane.setPrefViewportHeight(350);
        } else {
            this.scrollPane.setPrefViewportHeight(contentHeight + 25);
        }
    }

    public ScrollPane getScrollPane() {
        double contentHeight = 9999999;
        if (this.groupInsideScrollPane != null) {
            contentHeight = this.groupInsideScrollPane.getLayoutBounds().getHeight();
        }
        if (contentHeight > 350.0) {
            return this.scrollPane;
        }
        return null;
    }

    protected void setWebContent(String webContent) {
        if (this.detailWebView != null) {
            WebEngine engine = this.detailWebView.getEngine();
            engine.loadContent(webContent);
        }
    }

    public WebView getWebView() {
        return this.detailWebView;
    }

    public Text getInDetailCloseButton() {
        return this.inDetailCloseButton;
    }

    public void showGraphConnections(int count) {
        // if the FXML includes graphConnection point definitions (for example
        // detail mode might not ...)
        if (this.graphConnection01 != null) {
            this.graphConnection01.setVisible(count > 0);
            this.graphConnection02.setVisible(count > 1);
            this.graphConnection03.setVisible(count > 2);
            this.graphConnection04.setVisible(count > 3);
            this.graphConnection05.setVisible(count > 4);
            this.graphConnection06.setVisible(count > 5);
            this.graphConnection07.setVisible(count > 6);
            this.graphConnection08.setVisible(count > 7);
        }
    }

}
