package org.sociotech.communitymirror.view.flow.visualitems.tag;

import java.util.LinkedList;
import java.util.List;

import org.sociotech.communitymashup.data.Content;
import org.sociotech.communitymashup.data.InformationObject;
import org.sociotech.communitymashup.data.Organisation;
import org.sociotech.communitymashup.data.Person;
import org.sociotech.communitymashup.data.Tag;
import org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem;
import org.sociotech.communitymirror.visualitems.components.spring.SpringConnector;

/**
 * The visualization of a tag item
 * @author Andrea Nutsi
 *
 */
public abstract class TagItem  extends FlowVisualItem<Tag, TagItemController>{

	/**
	 * The data of this tag
	 */
	private Tag dataItem;
	
	/**
	 * The content of this tag
	 */
	protected String tagContent = "";
	
	protected String tagsNumber = "";
	
	public TagItem(Tag dataItem) {
		super(dataItem);
		this.dataItem = dataItem;
	}

	@Override
	protected void onInit() {
		tagContent = dataItem.getName();
		tagsNumber = dataItem.getTagged().size() + "";
		
		render();
		super.onInit();
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#createSpringConnector()
	 */
	@Override
	protected SpringConnector createSpringConnector() {
		return new SpringConnector(this, 12, 10);
	}
	
	/**
	 * Method to override in subclasses to generate the visual representation
	 */
	protected abstract void render();
	
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#getRelatedOrganizations()
	 */
	@Override
	protected List<Organisation> getRelatedOrganizations()
	{
		List<Organisation> relatedOrganisations = new LinkedList<Organisation>();
		for(InformationObject taggedInformationObject : dataItem.getTagged())
		{
			if(taggedInformationObject instanceof Organisation)
			{
				Organisation taggedOrganisation = (Organisation) taggedInformationObject;
				relatedOrganisations.add(taggedOrganisation);
			}
		}
		return relatedOrganisations;
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#getRelatedPersons()
	 */
	@Override
	protected List<Person> getRelatedPersons()
	{
		List<Person> relatedPersons = new LinkedList<Person>();
		for(InformationObject taggedInformationObject : dataItem.getTagged())
		{
			if(taggedInformationObject instanceof Person)
			{
				Person taggedPerson = (Person) taggedInformationObject;
				relatedPersons.add(taggedPerson);
			}
		}
		return relatedPersons;
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#getRelatedContents()
	 */
	@Override
	protected List<Content> getRelatedContents()
	{
		List<Content> relatedContents = new LinkedList<Content>();
		for(InformationObject taggedInformationObject : dataItem.getTagged())
		{
			if(taggedInformationObject instanceof Content)
			{
				Content taggedContent = (Content) taggedInformationObject;
				relatedContents.add(taggedContent);
			}
		}
		return relatedContents;
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#getRelatedTags()
	 */
	@Override
	protected List<Tag> getRelatedTags()
	{
		List<Tag> relatedTags = new LinkedList<Tag>();
		for(InformationObject taggedInformationObject : dataItem.getTagged())
		{
			if(taggedInformationObject instanceof Tag)
			{
				Tag taggedTag = (Tag) taggedInformationObject;
				relatedTags.add(taggedTag);
			}
		}
		return relatedTags;
	}
}
