package org.sociotech.communitymirror.view.flow.visualitems.picture;

import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;

import org.sociotech.communitymashup.data.Content;

import com.google.common.io.Resources;

/**
 * The visualization of a picture item in the detail visual state
 * @author Andrea Nutsi
 *
 */
public class DetailPictureItem extends PictureItem {

	public DetailPictureItem(Content dataItem) {
		super(dataItem);
	}


	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.flow.visualitems.picture.PictureItem#render()
	 */
	protected void render() {
		try {
			// Get FXML for tag
			FXMLLoader loader = new FXMLLoader();

			URL location;
			String fxmlPath = "fxml/inDetail/picture.fxml";
	        if(this.themeResources != null) {
	        	location = this.themeResources.getResourceURL(fxmlPath);
	        } else {
	        	location = Resources.getResource("flow/" + fxmlPath);
	        	logger.warn(this.getClass().getName() + " using deprecated FXML path.");
	        }
			loader.setLocation(location);
			AnchorPane picturePane = (AnchorPane) loader.load(location.openStream());
			PictureItemController controller = loader.getController();

			Circle circle = new Circle(40);
			circle.setLayoutY(40);
			circle.setLayoutX(40);
			controller.setPictureImageClip(circle);
			controller.setPictureContent(pictureContent);

			//controller.bindToPictureCreationDateField(pictureCreationDate);
			controller.setPictureContentLarge(pictureContent);

			if(pictureDescription!= "") controller.bindToPictureDescriptionField(pictureDescription);

			//prevent ConcurrentModificationException
			int size = picturePane.getChildren().size();
			for(int i = 0; i < size; i++){
				addNode(picturePane.getChildren().get(0));
			}
		} catch (IOException e) {
			logger.error("Problems loading fxml-file for PictureItem" + e.getStackTrace());
		}
	}
}
