package org.sociotech.communitymirror.view.flow.visualitems;

import java.util.EventObject;

import org.sociotech.communityinteraction.events.CIEvent;
import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

/**
 * An event object used for user interaction events that occure on workspace node visual items.
 * 
 * @author Eva Loesch 
 */
public class WorkspaceNodeInteractionEvent<T extends Item, V extends FXMLController> extends EventObject{

	/**
	 * The unique serial version ID of this event.
	 * @author Eva Loesch 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * The workspace node visual item this workspace node interaction event has occurred on.
	 * @author Eva Loesch 
	 */
	private FlowVisualItem <T, V> sourceNode;

	/**
	 * The CIEvent which has has triggered this WorkspaceNodeInteractionEvent.
	 * @author Eva Loesch 
	 */
	private CIEvent sourceEvent;
	
	
	public WorkspaceNodeInteractionEvent(FlowVisualItem <T, V> sourceNode, CIEvent sourceEvent) {
		super(sourceNode);
		this.sourceNode = sourceNode;
		this.setSourceEvent(sourceEvent);
	}

	/**
	 * Gets the workspace node visual item this workspace node interaction event has occurred on.
	 * @author Eva Loesch 
	 */
	public FlowVisualItem <T, V> getSourceNode() {
		return sourceNode;
	}

	/**
	 * Sets the workspace node visual item this workspace node interaction event has occurred on.
	 * 
	 * @param WorkspaceNodeVisualItem <T, V> sourceNode: the workspace node visual item that should be set as the source node of this event
	 * @author Eva Loesch 
	 */
	public void setSourceNode(FlowVisualItem <T, V> sourceNode) {
		this.sourceNode = sourceNode;
	}

	/**
	 * Gets the CIEvent that has triggered this workspace node interaction event.
	 */
	public CIEvent getSourceEvent() {
		return sourceEvent;
	}

	/**
	 * Sets the CIEvent that has triggered this workspace node interaction event. 
	 * @param sourceEvent : CIEvent that should be set as source event of this workspace node interaction event.
	 * 
	 */
	public void setSourceEvent(CIEvent sourceEvent) {
		this.sourceEvent = sourceEvent;
	}

}
