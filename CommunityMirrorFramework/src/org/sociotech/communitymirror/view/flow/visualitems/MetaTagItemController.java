
package org.sociotech.communitymirror.view.flow.visualitems;

import java.net.URL;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;

/**
 * The controller of the about item
 *
 * @author Michael Koch
 *
 */
public class MetaTagItemController extends FXMLController implements Initializable {

    @FXML
    Text metaTagName;

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(MetaTagItemController.class.getName());

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        try {
            assert this.metaTagName != null : "fx:id=\"metaTagName\" was not injected: check your FXML file 'metatag.fxml'.";
        } catch (AssertionError e) {
            this.logger.error(e.getMessage());
        }
    }

}
