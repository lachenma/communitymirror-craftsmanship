package org.sociotech.communitymirror.view.flow.visualitems.picture;

import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

import org.sociotech.communitymashup.data.Content;

import com.google.common.io.Resources;

/**
 * The visualization of a picture item in the preview visual state
 * @author Andrea Nutsi
 *
 */
public class PreviewPictureItem extends PictureItem {

	public PreviewPictureItem(Content dataItem) {
		super(dataItem);
	}



	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.flow.visualitems.picture.PictureItem#render()
	 */
	protected void render() {
		try {
			// Get FXML for tag
			FXMLLoader loader = new FXMLLoader();

			URL location;
			String fxmlPath = "fxml/inPreview/picture.fxml";
	        if(this.themeResources != null) {
	        	location = this.themeResources.getResourceURL(fxmlPath);
	        } else {
	        	location = Resources.getResource("flow/" + fxmlPath);
	        	logger.warn(this.getClass().getName() + " using deprecated FXML path.");
	        }
			loader.setLocation(location);
			AnchorPane picturePane = (AnchorPane) loader.load(location.openStream());
			PictureItemController controller = loader.getController();

			Circle circle = new Circle(40);
			circle.setLayoutY(40);
			circle.setLayoutX(40);
			controller.setPictureImageClip(circle);
			controller.setPictureContent(pictureContent);


			//controller.bindToPictureCreationDateField(pictureCreationDate);


			Rectangle rectangle = new Rectangle(240, 75);
			rectangle.setLayoutX(5);
			if(pictureContent.getHeight() < 400){
				//landscape format
				rectangle.setLayoutY(55);
			}else{
				//portrait format
				rectangle.setLayoutY(130);
			}

			controller.setPictureImageLargeClip(rectangle);
			controller.setPictureContentLarge(pictureContent);

			if(pictureDescription!= "") controller.bindToPictureDescriptionField(pictureDescription, 40);

			//prevent ConcurrentModificationException
			int size = picturePane.getChildren().size();
			for(int i = 0; i < size; i++){
				addNode(picturePane.getChildren().get(0));
			}
		} catch (IOException e) {
			logger.error("Problems loading fxml-file for PictureItem" + e.getStackTrace());
		}
	}
}
