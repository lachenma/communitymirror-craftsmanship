package org.sociotech.communitymirror.view.flow.visualitems;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.sociotech.communitymashup.data.Content;
import org.sociotech.communitymashup.data.Event;
import org.sociotech.communitymashup.data.InformationObject;
import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymashup.data.MetaInformation;
import org.sociotech.communitymashup.data.MetaTag;
import org.sociotech.communitymashup.data.Organisation;
import org.sociotech.communitymashup.data.Person;
import org.sociotech.communitymashup.data.Tag;
import org.sociotech.communitymirror.view.flow.components.EventDateComparator;

import com.google.common.io.Resources;

/**
 * The visualization of a time item showing the current time
 * @author Andrea Nutsi
 *
 */
public class TimeItem extends FlowVisualItem<MetaTag, TimeItemController>{

	/**
	 * The controller of this time item
	 */
	private TimeItemController controller;

	/**
	 * The log4j logger reference.
	 */
	private final Logger logger = LogManager.getLogger(TimeItem.class.getName());

	/**
	 * The related content items
	 */
	private List<Item> relatedItems = new LinkedList<Item>();

	/**
	 * The data item representing the metatag
	 */
	private MetaTag dataItem;

	public TimeItem(MetaTag dataItem) {
		super(dataItem);
		this.dataItem = dataItem;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onInit()
	 */
	@Override
	protected void onInit() {
		render();
		setRelatedItems();

		super.onInit();
	}

	protected void render() {
		try {
			// Get FXML for time item
			FXMLLoader loader = new FXMLLoader();
			URL location;
			String fxmlPath = "fxml/inPreview/time.fxml";
	        if(this.themeResources != null) {
	        	location = this.themeResources.getResourceURL(fxmlPath);
	        } else {
	        	location = Resources.getResource("flow/" + fxmlPath);
	        	logger.warn(this.getClass().getName() + " using deprecated FXML path.");
	        }
			AnchorPane timePane = (AnchorPane) loader.load(location.openStream());
			controller = loader.getController();

			bindToTime();

			//add Node to Scene
			//prevent ConcurrentModificationException
			int size = timePane.getChildren().size();
			for(int i = 0; i < size; i++){
				addNode(timePane.getChildren().get(0));
			}
	//		addNode(comment);

		} catch (IOException e) {
			logger.error("Problems loading fxml-file for TimeItem" + e.getStackTrace());
		}

	}

	/**
	 * Binds current time to fxml text element showing the time
	 */
	private void bindToTime() {
		Timeline timeline = new Timeline(
				new KeyFrame(Duration.seconds(0),
						new EventHandler<ActionEvent>() {
					@Override public void handle(ActionEvent actionEvent) {
						Calendar time = Calendar.getInstance();
						SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
						controller.getTime().textProperty().bind(new SimpleStringProperty(sdf.format(time.getTime())));
					}
				}
						),
						new KeyFrame(Duration.seconds(1))
				);
		timeline.setCycleCount(Animation.INDEFINITE);
		timeline.play();
	}

	public void setRelatedItems(){

		// first get the InformationItem objects
		EList<Item> objects = dataItem.getMetaTagged();
		// and now the Event objects for the startdate
		LinkedList<Item> events = new LinkedList<Item>();
		for (Item o : objects) {
			EList<MetaInformation> metaInformations = ((InformationObject)o).getMetaInformations();
			if(!metaInformations.isEmpty()){
				for(MetaInformation metaInformation : metaInformations){
					if(metaInformation instanceof Event){
						events.add((Event)metaInformation); // the first event found should be the startdate ...
					}
				}
			}
		}

		if(!events.isEmpty()){

			//get current time
			Calendar now = Calendar.getInstance();

			LinkedList<Event> eventsAfter = new LinkedList<Event>();

			//filter for items that are after current time
			for(Item event : events){
				if(event instanceof Event){
					Event eventItem = (Event) event;
					//consider only events that are after current time
					if(eventItem.getDate().after(now.getTime())){
						eventsAfter.add(eventItem);
					}
				}
			}

			//get events that are in the next 2 hours, if none, increase time frame
			LinkedList<Event> eventsInIntervall = new LinkedList<Event>();
			for(int i=1; i<100; i*=2){
				eventsInIntervall = getEventsWithinNextXDays(eventsAfter,i);
				if(eventsInIntervall.size() > 20){
					break;
				}
			}

			//sort by date
			EventDateComparator comparator = new EventDateComparator();
			eventsInIntervall.sort(comparator);

			for(Item event : eventsInIntervall){
				if(event instanceof Event){
					Event eventItem = (Event) event;
					//get the information object belonging to this event
					EList<InformationObject> informationObjects = eventItem.getInformationObjects();
					if(!informationObjects.isEmpty()) {
						for(InformationObject io : informationObjects){
							if (objects.contains(io)) {
								relatedItems.add(io);
							}
						}
					}
				}
				if(relatedItems.size() > 8){
					break;
				}
			}
		}
	}

	/**
	 * Filters events to be within the specified time frame from now on
	 * @param events events to be checked
	 * @param hours defining the size of the time interval
	 * @return
	 */
	@SuppressWarnings("unused")
	private LinkedList<Event> getEventsWithinNextXHours(
			LinkedList<Event> events, int hours) {

		LinkedList<Event> returnList = new LinkedList<Event>();

		//get current time
		Calendar now = Calendar.getInstance();
		now.set(Calendar.HOUR, now.get(Calendar.HOUR) + hours);

		for(Event event : events){
			if(event.getDate().before(now.getTime())){

				returnList.add(event);

			}
		}

		return returnList;
	}

	/**
	 * Filters events to be within the specified time frame from now on
	 * @param events events to be checked
	 * @param days defining the size of the time interval
	 * @return
	 */
	private LinkedList<Event> getEventsWithinNextXDays(
			LinkedList<Event> events, int days) {

		LinkedList<Event> returnList = new LinkedList<Event>();

		//get current time
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DAY_OF_MONTH, days);

		for(Event event : events){
			if(event.getDate().before(now.getTime())){

				returnList.add(event);

			}
		}

		return returnList;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#getRelatedOrganizations()
	 */
	@Override
	protected List<Organisation> getRelatedOrganizations()
	{
		List<Organisation> relatedOrganisations = new LinkedList<Organisation>();
		for(Item taggedItem : relatedItems)
		{
			if(taggedItem instanceof Organisation)
			{
				Organisation taggedOrganisation = (Organisation) taggedItem;
				relatedOrganisations.add(taggedOrganisation);
			}
		}
		return relatedOrganisations;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#getRelatedPersons()
	 */
	@Override
	protected List<Person> getRelatedPersons()
	{
		List<Person> relatedPersons = new LinkedList<Person>();
		for(Item taggedItem : relatedItems)
		{
			if(taggedItem instanceof Person)
			{
				Person taggedPerson = (Person) taggedItem;
				relatedPersons.add(taggedPerson);
			}
		}
		return relatedPersons;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#getRelatedContents()
	 */
	@Override
	protected List<Content> getRelatedContents()
	{
		List<Content> relatedContents = new LinkedList<Content>();
		for(Item taggedItem : relatedItems)
		{
			if(taggedItem instanceof Content)
			{
				Content taggedContent = (Content) taggedItem;
				relatedContents.add(taggedContent);
			}
		}
		return relatedContents;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#getRelatedTags()
	 */
	@Override
	protected List<Tag> getRelatedTags()
	{
		List<Tag> relatedTags = new LinkedList<Tag>();
		for(Item taggedItem : relatedItems)
		{
			if(taggedItem instanceof Tag)
			{
				Tag taggedTag = (Tag) taggedItem;
				relatedTags.add(taggedTag);
			}
		}
		return relatedTags;
	}
}
