package org.sociotech.communitymirror.view.flow.visualitems.picture;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

/**
 * The controller of a Picture item
 * @author Andrea Nutsi
 *
 */
public class PictureItemController extends FXMLController implements Initializable{

	@FXML
	Text pictureDescription;
	
	@FXML
	Text pictureCreationDate;
	
	@FXML
	ImageView pictureContent;
	
	@FXML
	ImageView pictureContentLarge;
	
	/**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(PictureItemController.class.getName());

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try{
			 assert pictureDescription != null : "fx:id=\"pictureDescription\" was not injected: check your FXML file 'picture.fxml'.";
			 assert pictureCreationDate != null : "fx:id=\"pictureCreationDate\" was not injected: check your FXML file 'picture.fxml'.";
			 assert pictureContent != null : "fx:id=\"pictureContent\" was not injected: check your FXML file 'picture.fxml'.";
			 assert pictureContentLarge != null : "fx:id=\"pictureContentLarge\" was not injected: check your FXML file 'picture.fxml'.";
		 }catch(AssertionError e){
			 logger.error(e.getMessage());
		 }

	}
	
	protected void bindToPictureDescriptionField(String pictureDescription){
		bindToFXMLElement(this.pictureDescription, pictureDescription);
	}
	
	protected void bindToPictureDescriptionField(String pictureDescription, int length){
		bindToFXMLElement(this.pictureDescription, StringUtils.abbreviate(pictureDescription, length));
	}

	protected void bindToPictureCreationDateField(String pictureCreationDate){
		bindToFXMLElement(this.pictureCreationDate, pictureCreationDate);
	}
	
	protected void setPictureContentLarge(Image picture){
		this.pictureContentLarge.setImage(picture);
	}
	
	protected void setPictureImageLargeClip(Node clip) {
		this.pictureContentLarge.setClip(clip);
	}
	
	protected void setPictureContent(Image picture){
		this.pictureContent.setImage(picture);
	}

	protected void setPictureImageClip(Node clip) {
		this.pictureContent.setClip(clip);
	}
	
}
