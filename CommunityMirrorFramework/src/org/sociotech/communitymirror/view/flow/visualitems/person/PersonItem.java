
package org.sociotech.communitymirror.view.flow.visualitems.person;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.sociotech.communityinteraction.events.gesture.drag.DragStartedEvent;
import org.sociotech.communitymashup.data.Content;
import org.sociotech.communitymashup.data.MetaTag;
import org.sociotech.communitymashup.data.Organisation;
import org.sociotech.communitymashup.data.Person;
import org.sociotech.communitymashup.data.Tag;
import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.utils.ImageHelper;
import org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem;

import com.google.common.io.Resources;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;

/**
 * The visualization of a person item
 *
 * @author Andrea Nutsi, Michael Koch
 *
 */
public abstract class PersonItem extends FlowVisualItem<Person, PersonItemController> {

    /**
     * The type of person item to visualize
     */
    protected String state = null;

    /**
     * The data item of this person
     */
    private Person dataItem;

    /**
     * The name of this person
     */
    protected String name = "";

    /**
     * The first name of this person
     */
    protected String firstName = "";

    /**
     * The last name of this person
     */
    protected String lastName = "";

    /**
     * The initials of this person's name
     */
    protected String initials = "";

    /**
     * The description of this person
     */
    protected String description = "";

    /**
     * The image of this person
     */
    protected Image image;
    protected String imageUrl;

    /**
     * Square version of the image of this person
     */
    protected Image squareImage;

    /**
     * Square version of the image of this person
     */
    protected Image roundImage;

    /**
     * The default micro image for a person
     */
    protected Image defaultMicroImage;

    /**
     * The name of the organization of this person
     */
    protected String organizationName = "";

    /**
     * Image representing a QR code
     */
    protected Image personQR;

    /**
     * indicates whether default profile image is used
     */
    protected boolean defaultImage = false;

    private PersonItemController controller;

    public PersonItem(Person dataItem) {
        super(dataItem);
        String concreteClass = this.getClass().getSimpleName();
        if (concreteClass.endsWith("PersonItem")) {
            this.state = concreteClass.substring(0, concreteClass.length() - "PersonItem".length());
        }
        this.dataItem = dataItem;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#onInit
     * ()
     */
    @Override
    protected void onInit() {
        if (this.dataItem != null) {
            this.getPersonInfo();
        }
        this.render();
        super.onInit();
    }

    /**
     * Method to override in subclasses to generate the visual representation
     */
    protected void render() {
        FXMLLoader loader = new FXMLLoader();
        AnchorPane personPane = null;
        try {
            URL fxmlUrl = null;
            // get FXML for person
            String fxmlPath = "fxml/in" + this.state + "/person.fxml";
            if (this.themeResources != null) {
                fxmlUrl = this.themeResources.getResourceURL(fxmlPath);
            } else {
                fxmlUrl = Resources.getResource("flow/" + fxmlPath);
                this.logger.warn(this.getClass().getName() + " using deprecated FXML path.");
            }
            loader.setLocation(fxmlUrl);
            personPane = (AnchorPane) loader.load(fxmlUrl.openStream());
        } catch (IOException e) {
            this.logger.error("Problems loading fxml-file for PersonItem:");
            this.logger.error("    " + e.getMessage());
            for (int i = 0; i < e.getStackTrace().length && i < 10; i++) {
                this.logger.error("    " + e.getStackTrace()[i]);
            }
        }
        this.controller = loader.getController();

        // get and set category information from meta tags
        String categoryString = CommunityMirror.getConfiguration().getString("view.flow.particles.category_icons");
        String category_icons[] = (categoryString == null) ? new String[0] : categoryString.split(",");
        categoryString = null;
        EList<MetaTag> metaTags = this.dataItem.getMetaTags();
        for (MetaTag metaTag : metaTags) {
            for (String category : category_icons) {
                if (category.equals(metaTag.getName())) {
                    categoryString = metaTag.getName();
                    break;
                }
            }
        }
        this.setCategory(categoryString);

        /*
         * get and process template for details view - use FreeMarker template
         * engine
         */
        if (this.state.equals("Detail")) {
            Map<String, Object> templateData = new HashMap<>();
            templateData.put("category", categoryString);
            templateData.put("item", this);
            // perhaps we do not need this here ...
            templateData.put("firstName", this.firstName);
            templateData.put("lastName", this.lastName);
            templateData.put("name", this.name);
            templateData.put("organizationName", this.organizationName);
            templateData.put("description", this.description);
            templateData.put("imageUrl", this.imageUrl);
            this.controller.setWebContent(this.processDetailTemplate("detailPerson.ftl", templateData));
        }

        this.controller.bindToPersonFirstName(this.firstName);
        this.controller.bindToPersonLastName(this.lastName);

        this.controller.bindToPersonNameField(this.name, 180);

        this.controller.bindToPersonOrganizationName(this.organizationName);

        this.controller.bindToDescription(this.description);

        int circleSize = 40;
        if (this.state.equals("Spotlight")) {
            circleSize = 35;
        }
        Circle circle = new Circle(circleSize);
        circle.setLayoutY(circleSize);
        circle.setLayoutX(circleSize);
        this.controller.setPersonImageClip(circle);
        if (this.defaultImage) {
            this.controller.setPersonImage(this.defaultMicroImage);
            this.controller.bindToPersonInitials(this.initials);
        } else {
            this.controller.setPersonImage(this.image);
        }

        this.controller.setPersonImageLarge(this.image);
        this.controller.setPersonImageSquare(this.squareImage);
        this.controller.setPersonImageRound(this.roundImage);

        // show connection points for graph
        this.controller.showGraphConnections(this.getAllRelatedItems().size());

        // prevent ConcurrentModificationException
        int size = personPane.getChildren().size();
        for (int i = 0; i < size; i++) {
            this.addNode(personPane.getChildren().get(0));
        }
    }

    private void setCategory(String newCategory) {
        Image categoryIcon = null;
        if (newCategory != null) {
            String iconPath = "images/category_icon_" + newCategory + ".png";
            URL location;
            if (this.themeResources != null) {
                location = this.themeResources.getResourceURL(iconPath);
            } else {
                location = Resources.getResource("flow/" + iconPath);
                this.logger.warn(this.getClass().getName() + " using deprecated Resource path.");
            }
            // If the icon file does not exist, ImageHelper will simply return
            // null. Consequently, passing null into the controller will hide
            // the category icon.
            categoryIcon = ImageHelper.createImage(location, true);
        }
        this.controller.setCategoryIcon(categoryIcon);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * shouldPerformDrag()
     */
    @Override
    protected boolean shouldPerformDrag(DragStartedEvent dragEvent) {
        WebView webView = this.controller.getWebView();
        if (webView != null) {
            Bounds boundsLocal = webView.getBoundsInLocal();
            if (boundsLocal != null) {
                Bounds boundsInScene = webView.localToScene(boundsLocal);
                if (boundsInScene != null) {
                    return !boundsInScene.contains(dragEvent.getSceneX(), dragEvent.getSceneY());
                }
            }
        }
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * getInDetailCloseButton()
     */
    @Override
    protected Text getInDetailCloseButton() {
        return this.controller.getInDetailCloseButton();
    }

    private void getPersonInfo() {
        this.name = this.dataItem.getName();
        this.firstName = this.dataItem.getFirstname();
        this.lastName = this.dataItem.getLastname();
        if (this.firstName != null && this.lastName != null) {
            this.initials = this.firstName.charAt(0) + ". " + this.lastName.charAt(0) + ".";
        } else {
            this.firstName = this.dataItem.getName();
        }
        this.description = this.dataItem.getStringValue();

        EList<Organisation> organizations = this.dataItem.getOrganisations();
        if (!organizations.isEmpty()) {
            for (Organisation organization : organizations) {
                this.organizationName += organization.getName() + " ";
                break; // one is enough
            }
        }

        // get Images of this person

        // create Metatag list
        LinkedList<String> metaTagList = new LinkedList<>();
        metaTagList.add("profile_image_big_main");
        metaTagList.add("profile_image_big");

        for (String metaTag : metaTagList) {
            org.sociotech.communitymashup.data.Image mashupImage = this.dataItem
                    .getAttachedImageWithMetaTagName(metaTag);
            if (mashupImage != null) {
                String fileUrl = mashupImage.getFileUrl();
                this.imageUrl = fileUrl;
                this.image = ImageHelper.createImage(fileUrl, true);
                if (this.image == null) {
                    continue;
                }
                break;
            }
        }
        if (this.image == null) {
            URL imageUrl, defaultUrl;
            String imagePath = "images/person_image_default.png";
            String defaultPath = "images/person_image_default_circle.png";
            if (this.themeResources != null) {
                imageUrl = this.themeResources.getResourceURL(imagePath);
                defaultUrl = this.themeResources.getResourceURL(defaultPath);
            } else {
                imageUrl = Resources.getResource("flow/" + imagePath);
                defaultUrl = Resources.getResource("flow/" + defaultPath);
                this.logger.warn(this.getClass().getName() + " using deprecated Resource path.");
            }
            this.image = ImageHelper.createImage(imageUrl, true);
            this.defaultMicroImage = ImageHelper.createImage(defaultUrl, true);
            this.defaultImage = true;
        }

        // Automatically remove transparency from image
        double imgWidth = 256;
        double imgHeight = imgWidth;
        if (this.image != null) {
            imgWidth = this.image.getWidth();
            imgHeight = this.image.getHeight();
        }
        Canvas canvas = new Canvas(imgWidth, imgHeight);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.GRAY); // adjust here if background
                                // should be blended to black or
                                // white instead
        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        gc.drawImage(this.image, 0, 0);
        this.image = canvas.snapshot(null, null);

        // create square image
        int rawWidth = (int) Math.round(this.image.getWidth());
        int rawHeight = (int) Math.round(this.image.getHeight());
        int targetSize = Math.round(Math.min(rawWidth, rawHeight));
        if (targetSize > 0) {
            WritableImage newImage = new WritableImage(targetSize, targetSize);
            int xOffset = (int) Math.round((this.image.getWidth() - targetSize) / 2);
            int yOffset = (int) Math.round((this.image.getHeight() - targetSize) / 2);
            newImage.getPixelWriter().setPixels(0, 0, targetSize, targetSize, this.image.getPixelReader(), xOffset,
                    yOffset);
            this.squareImage = newImage;

            // create round image
            ImageView tempView = new ImageView();
            tempView.setImage(this.squareImage);
            Circle clip = new Circle(this.squareImage.getWidth() / 2, this.squareImage.getWidth() / 2,
                    this.squareImage.getWidth() / 2);
            tempView.setClip(clip);
            SnapshotParameters parameters = new SnapshotParameters();
            parameters.setFill(Color.TRANSPARENT);
            this.roundImage = tempView.snapshot(parameters, null);
        }

        org.sociotech.communitymashup.data.Image mashupImageQR = this.dataItem
                .getAttachedImageWithMetaTagName("profile_qr");
        if (mashupImageQR != null) {
            this.personQR = ImageHelper.createImage(mashupImageQR.getFileUrl(), true);
        }

    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * getRelatedOrganizations()
     */
    @Override
    protected List<Organisation> getRelatedOrganizations() {
        Person person = this.getDataItem();
        return person.getOrganisations();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * getRelatedContents()
     */
    @Override
    protected List<Content> getRelatedContents() {
        Person person = this.getDataItem();
        return person.getContents();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#
     * getRelatedTags()
     */
    @Override
    protected List<Tag> getRelatedTags() {
        Person person = this.getDataItem();

        List<Tag> relatedTags = new LinkedList<>();
        for (Tag tag : person.getTags()) {
            // if(tag.getTagged().size() > 1)
            relatedTags.add(tag);
        }

        return relatedTags;
    }
}
