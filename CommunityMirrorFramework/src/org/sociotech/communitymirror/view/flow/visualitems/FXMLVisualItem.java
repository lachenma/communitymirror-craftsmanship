package org.sociotech.communitymirror.view.flow.visualitems;

import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.visualitems.VisualItem;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

/**
 * Abstract super class of all fxml based visual items
 * 
 * @author Peter Lachenmaier
 *
 * @param <T> The type of the data item
 * @param <V> The type of the item controller
 */
public abstract class FXMLVisualItem<T extends Item, V extends FXMLController> extends VisualItem<T> {

	/**
	 * The item controller set in the fxml file
	 */
	private V itemController;
	
	public FXMLVisualItem(T dataItem) {
		super(dataItem);
	}

	/**
	 * Returns the set item controller
	 * 
	 * @return The set item controller
	 */
	public V getItemController() {
		return itemController;
	}

	/**
	 * Sets the item controller.
	 * 
	 * @param itemController Item controller to use
	 */
	public void setItemController(V itemController) {
		this.itemController = itemController;
	}
	
}
