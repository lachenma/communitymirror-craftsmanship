package org.sociotech.communitymirror.view.flow.visualitems;

import java.util.EventListener;

import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.view.flow.components.VisualComponentsPile;
import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

public interface PileEventListener extends EventListener{
	
	<T extends Item, V extends FXMLController> void  onPileLeft(WorkspaceNodeInteractionEvent<T, V> event);
	
	<T extends Item, V extends FXMLController> void  onPileEmpty(VisualComponentsPile<T, V> pile);

}
