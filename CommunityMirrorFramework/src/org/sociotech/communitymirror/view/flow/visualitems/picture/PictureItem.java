package org.sociotech.communitymirror.view.flow.visualitems.picture;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.sociotech.communitymashup.data.Content;
import org.sociotech.communitymashup.data.Image;
import org.sociotech.communitymashup.data.Tag;
import org.sociotech.communitymirror.utils.ImageHelper;
import org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem;

/**
 * The visualization of a picture item
 * @author Andrea Nutsi
 *
 */
public abstract class PictureItem  extends FlowVisualItem<Content, PictureItemController>{

	/**
	 * The data of this image
	 */
	private Content dataItem;

	/**
	 * The description of this picture
	 */
	protected String pictureDescription = "";
	
	/**
	 * The creation date of this picture
	 */
	protected String pictureCreationDate = "";
	
	/**
	 * The image of this picture item
	 */
	protected javafx.scene.image.Image pictureContent;
	
	public PictureItem(Content dataItem) {
		super(dataItem);
		this.dataItem = dataItem;
	}

	@Override
	protected void onInit() {
		
		this.pictureDescription = this.dataItem.getStringValue();
		this.pictureCreationDate = this.dataItem.getCreatedPretty();
		EList<Image> images = this.dataItem.getImages();
		//Image mashupImage = this.dataItem.getAttachedImageWithMetaTagName("impression_image");
		
		if(!images.isEmpty()){
			Image mashupImage = images.get(0);
			if(mashupImage != null){
				this.pictureContent = ImageHelper.createImage(mashupImage.getFileUrl(), true);
			}
			else{
				logger.warn("Picture Image is Null");
			}
		}
		
		render();
		super.onInit();
	}
	
	/**
	 * Method to override in subclasses to generate the visual representation
	 */
	protected abstract void render();
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.flow.visualitems.FlowVisualItem#getRelatedTags()
	 */
	@Override
	protected List<Tag> getRelatedTags()
	{
		List<Tag> relatedTags = new LinkedList<Tag>();
		for(Tag tag : dataItem.getTags()){
			//if(tag.getTagged().size() > 1) 
			relatedTags.add(tag);
		}
		
		return relatedTags;
	}
}
