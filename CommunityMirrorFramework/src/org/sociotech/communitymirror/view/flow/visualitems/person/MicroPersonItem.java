
package org.sociotech.communitymirror.view.flow.visualitems.person;

import org.sociotech.communitymashup.data.Person;

/**
 * The visualization of a person item in the micro visual state
 *
 * @author Andrea Nutsi
 *
 */
public class MicroPersonItem extends PersonItem {
    public MicroPersonItem(Person dataItem) {
        super(dataItem);
    }
}
