package org.sociotech.communitymirror.view.htmlline.components;

import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.visualitems.renderer.VisualItemRenderer;
import org.sociotech.communitymirror.visualstates.VisualState;

public class HTMLLineElementRenderer extends VisualItemRenderer<Item, HTMLLineElement> {

	private double height;
	private String baseUrl = null;
	private String tpl = null;

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.renderer.VisualItemRenderer#renderItem(org.sociotech.communitymashup.data.Item, org.sociotech.communitymirror.visualstates.VisualState)
	 */
	@Override
	public HTMLLineElement renderItem(Item item, VisualState state) {
		return new HTMLLineElement(item, state, height, baseUrl, tpl);
	}

	/**
	 * Returns the setting for the CommunityMashup base url.
	 * 
	 * @return The CommunityMashup base url.
	 */
	public String getBaseUrl() {
		return baseUrl;
	}

	/**
	 * Sets the CommunityMashup base url to retrieve the html content from.
	 * 
	 * @param baseUrl CommunityMashup base url to load html content from.
	 */
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	/**
	 * Returns the template setting.
	 * 
	 * @return The template setting. Null if set to default.
	 */
	public String getTpl() {
		return tpl;
	}

	/**
	 * Sets the html template name used for rendering.
	 * 
	 * @param tpl The html template name. Can be null to use the default one.
	 */
	public void setTpl(String tpl) {
		this.tpl = tpl;
	}

	/**
	 * Returns the current height setting.
	 * 
	 * @return The current height setting.
	 */
	public double getHeight() {
		return height;
	}

	/**
	 * Sets the height for new rendered visual html items.
	 * 
	 * @param height Height for new rendered visual html items.
	 */
	public void setHeight(double height) {
		this.height = height;
	}

}
