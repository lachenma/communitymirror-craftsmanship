package org.sociotech.communitymirror.view.htmlline.visualitems;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.web.WebView;

import org.sociotech.communitymashup.data.Item;
import org.sociotech.communitymirror.visualitems.VisualItem;
import org.sociotech.communitymirror.visualstates.VisualState;

/**
 * Basic html representation of a data item.
 *
 * @author Peter Lachenmaier
 */
public class HTMLItem extends VisualItem<Item> {

	/**
	 * The url rendered in the html view.
	 */
	protected String baseUrl;
	protected double height;
	protected String template;
	protected double width;

	/**
	 * Creates a visual html representation of the given data item by
	 * puting the mashup generated html in a web view. It uses the default
	 * template from the mashup.
	 *
	 * @param dataItem Item to visualize.
	 * @param state The visual state
	 * @param height The intended height of the html component
	 * @param baseUrl Base url containing the full mashup endpoint path, e. g. http://localhost:8080/html/mashup/
	 */
	public HTMLItem(Item dataItem, VisualState state, double height, String baseUrl) {
		this(dataItem, state, height, baseUrl, null);
	}

	/**
	 * Creates a visual html representation of the given data item by
	 * puting the mashup generated html in a web view. It uses the default
	 * template from the mashup.
	 *
	 * @param dataItem Item to visualize.
	 * @param state The visual state
	 * @param height The intended height of the html component
	 * @param baseUrl Base url containing the full mashup endpoint path, e. g. http://localhost:8080/html/mashup/
	 * @param tpl Template name to be passed to the CommunityMashup.
	 */
	public HTMLItem(Item dataItem, VisualState state, double height, String baseUrl, String tpl) {
		super(dataItem);
		this.baseUrl = baseUrl;
		this.height = height;
		this.template = tpl;
		this.setVisualState(state);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onInit()
	 */
	@Override
	protected void onInit() {
		// 1:1
		width = height;

		WebView webview = new WebView();
		webview.setPrefHeight(height);
		webview.setPrefWidth(width);

		webview.setContextMenuEnabled(false);
		webview.setOpacity(0.8);

		webview.setOnMousePressed(new EventHandler<Event>() {

			@Override
			public void handle(Event arg0) {
				touched();
			}
		});

		webview.setOnTouchPressed(new EventHandler<Event>() {

			@Override
			public void handle(Event arg0) {
				touched();
			}
		});

		webview.setOnMouseReleased(new EventHandler<Event>() {

			@Override
			public void handle(Event arg0) {
				released();
			}
		});

		webview.setOnTouchReleased(new EventHandler<Event>() {

			@Override
			public void handle(Event arg0) {
				released();
			}
		});

		String htmlContentUrl = baseUrl + "getItemsWithIdent?ident=" + dataItem.getIdent();
		webview.getEngine().load(htmlContentUrl);

		// add the web view
		this.addNode(webview);

	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onUpdate(double, long)
	 */
	@Override
	protected void onUpdate(double framesPerSecond, long nanoTime) {

	}


	/**
	 * Will be called when the touch is released
	 */
	protected void released() {

	}

	/**
	 * Will be called when the component is touched
	 */
	protected void touched() {

	}

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }
	
	
}
