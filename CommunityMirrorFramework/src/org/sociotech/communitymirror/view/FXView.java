
package org.sociotech.communitymirror.view;

import java.net.URL;
import java.util.List;

import org.apache.commons.configuration2.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymashup.data.DataSet;
import org.sociotech.communitymirror.configuration.ThemeResourceLoader;
import org.sociotech.communitymirror.visualitems.components.BackgroundImageComponent;
import org.sociotech.communitymirror.visualitems.components.BackgroundImageComponent.BackgroundImageMode;

import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * The abstract super class of all java fx based views.
 *
 * @author Peter Lachenmaier, Martin Burkhard
 */
public abstract class FXView extends View {

    /**
     * Reference to the configuration.
     */
    protected final Configuration configuration;

    /**
     * Reference to the view's scene.
     */
    protected Scene scene;

    /**
     * Reference to the used data set.
     */
    protected DataSet dataSet;

    /**
     * Background color of the scene.
     */
    private String backgroundColor;

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(FXView.class.getName());

    /**
     * The bounds of this view
     */
    protected Bounds viewBounds;

    /**
     * The path of the background image. Null if not set.
     */
    private String backgroundImageUrl = null;

    /**
     * The opacity of the background image. Default opacity 100%
     */
    private double opacity = 1.0;

    /**
     * The component for the background image
     */
    private BackgroundImageComponent backgroundImageComponent;

    /**
     * The loader for theme resources
     */
    protected ThemeResourceLoader themeResources;

    /**
     * The display mode for the background image. Recommended default is
     * STRETCH, but can be adjusted in the configuration.
     */
    private BackgroundImageMode backgroundImageMode;

    /**
     * Creates the FXView with the
     * given width and height. Additionally sets the background color.
     *
     * @param width
     *            The width of the view
     * @param height
     *            The height of the view
     */
    public FXView(double width, double height, Configuration configuration, DataSet dataSet) {
        super();
        this.configuration = configuration;
        this.dataSet = dataSet;

        // create new theme resource loader
        this.themeResources = new ThemeResourceLoader(configuration);

        List<URL> customFonts = this.themeResources.getFolderContents("fonts");
        for (URL foundFont : customFonts) {
            if (foundFont.getPath().toLowerCase().endsWith(".ttf")) {
                Font result = Font.loadFont(foundFont.toExternalForm(), 18);
                if (result == null) {
                    this.logger.warn("Failed to load custom font: " + foundFont.getPath());
                }
            }
        }

        this.scene = new Scene(this, width, height);

        // view bounds are the bounds of the complete scene
        this.viewBounds = new BoundingBox(0.0, 0.0, width, height);

        this.backgroundColor = configuration.getString("theme.colors.background");

        // check if file name is set, cause filepath is combined value and
        // always set
        if (configuration.getString("theme.background.image.filename") != null) {
            this.backgroundImageUrl = this.themeResources
                    .getResourceURLString(configuration.getString("theme.background.image.resourcepath"));
        }

        // check if opacity is set
        if (configuration.getString("theme.background.image.opacity") != null) {
            this.opacity = configuration.getDouble("theme.background.image.opacity");
        }

        // check if background mode is set; if not, default to STRETCH
        this.backgroundImageMode = BackgroundImageMode.STRETCH;
        if (configuration.getString("theme.background.image.mode") != null) {
            String config_mode = configuration.getString("theme.background.image.mode");
            for (BackgroundImageMode possible_mode : BackgroundImageMode.values()) {
                if (possible_mode.name().equals(config_mode.toUpperCase())) {
                    this.backgroundImageMode = possible_mode;
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.communitymirror.view.View#onInit()
     */
    @Override
    protected void onInit() {

        this.scene.setFill(Color.web(this.backgroundColor));

        if (this.backgroundImageUrl != null) {
            try {
                this.backgroundImageComponent = new BackgroundImageComponent(this.backgroundImageUrl,
                        this.scene.getWidth(), this.scene.getHeight(), this.backgroundImageMode);
                this.backgroundImageComponent.init();
                this.backgroundImageComponent.setOpacity(this.opacity);
                this.addVisualComponent(this.backgroundImageComponent, true);
            } catch (Exception e) {
                // let background be empty
                this.logger.warn("Could not load background image from " + this.backgroundImageUrl, e);
            }

        }

    }

    /**
     * Returns the JavaFX scene.
     *
     * @return The JavaFX scene.
     */
    public synchronized Scene getFXScene() {
        return this.scene;
    }

    public synchronized double getWidth() {
        return this.scene.getWidth();
    }

    public synchronized double getHeight() {
        return this.scene.getHeight();
    }

    public synchronized DataSet getDataSet() {
        return this.dataSet;
    }

    /**
     * Returns the bounds of this view.
     *
     * @return The bounds of this view.
     */
    protected Bounds getViewBounds() {
        return this.viewBounds;
    }

}
