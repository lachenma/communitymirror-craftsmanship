package org.sociotech.communitymirror.view.picture;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.configuration2.Configuration;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.sociotech.communitymashup.data.DataSet;
import org.sociotech.communitymashup.data.observer.dataset.DataSetChangeObserver;
import org.sociotech.communitymashup.data.observer.dataset.DataSetChangedInterface;
import org.sociotech.communitymirror.utils.RandomFactory;
import org.sociotech.communitymirror.view.FXView;
import org.sociotech.communitymirror.view.picture.components.ItemLine;
import org.sociotech.communitymirror.view.picture.components.ItemLineElement;
import org.sociotech.communitymirror.view.picture.components.LineElementProducer;
import org.sociotech.communitymirror.view.picture.components.PictureLineElementFactory;
import org.sociotech.communitymirror.view.picture.components.PictureLineElementRenderer;
import org.sociotech.communitymirror.visualstates.VisualState;

// TODO refactor to have a line view from which this view an all the other line based inherit
/**
 * View to nicely show datasets with lots of images.
 *
 * @author Peter Lachenmaier
 */
public class PictureView extends FXView implements LineElementProducer, DataSetChangedInterface {

	protected static final double topOffset = 200.0;
	protected static final double bottomOffset = 40.0;

	private List<ItemLine> itemLines = new LinkedList<>();

	private PictureLineElementFactory factory;
	private PictureLineElementRenderer renderer;

	// space between lines and between items (negative to overlap)
	protected double spaceBetween = -15.0;

	protected boolean produceAsynchronous = true;

	/**
	 * Indicates if the dataSet has changed since the last update of the local references.
	 */
	protected boolean dataSetChanged = true;
	private EList<org.sociotech.communitymashup.data.Image> allImages;

	/**
	 * Reference to the first item line.
	 */
	protected ItemLine firstLine;

	/**
	 * Reference to the second item line.
	 */
	protected ItemLine secondLine;

	/**
	 * Reference to the third item line. Null if only two lines are created.
	 */
	protected ItemLine thirdLine;

	/**
	 * Reference to the data set change observer.
	 */
	private DataSetChangeObserver dataSetChangeObserver;

	/**
	 * Uses {@link PictureView#FXView(double, double)} to create the
	 * FXView with the given width and height. Additionally sets the background
	 * color.
	 *
	 * @param width
	 *            The width of the view
	 * @param height
	 *            The height of the view
	 * @param dataSet
	 * 			  The data set
	 */
	public PictureView(double width, double height, Configuration configuration, DataSet dataSet) {
		super(width, height, configuration, dataSet);

		// create observer to be notified on data set changes
		dataSetChangeObserver = new DataSetChangeObserver(dataSet, this);
		// TODO stop at end
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.FXView#onInit()
	 */
	@Override
	protected void onInit() {
		super.onInit();

		// create factory
		factory = new PictureLineElementFactory();

		renderer = new PictureLineElementRenderer();
		factory.setDefaultRenderer(renderer);

		produceAsynchronous = configuration.getBoolean("view.line.asynchronous", produceAsynchronous);

		// create the lines
		createItemLines();

		// init lines
		for(ItemLine line : itemLines) {
			line.init();
		}
	}

	private void createThreeLines(double spaceBetween, double allLineHeight,
			double width) {
		// calc heights
		double firstHeight  = 0.38 * 0.38 * allLineHeight;
		double secondHeight =        0.62 * allLineHeight;
		double thirdHeight  = 0.62 * 0.38 * allLineHeight;

		firstLine  = new ItemLine(firstHeight,  topOffset, width, spaceBetween, this, produceAsynchronous);
		secondLine = new ItemLine(secondHeight, topOffset + firstHeight + spaceBetween, width, spaceBetween, this, produceAsynchronous);
		thirdLine  = new ItemLine(thirdHeight,  topOffset + firstHeight + secondHeight + 2 * spaceBetween, width, spaceBetween, this, produceAsynchronous);

		firstLine.setSpeedFactor(1/ (0.38 * 0.38));
		secondLine.setSpeedFactor(1/ (0.62));
		thirdLine.setSpeedFactor(1/ (0.62 * 0.38));

		// fill it up
		 firstLine.fillLine();
		secondLine.fillLine();
		 thirdLine.fillLine();

		// keep reference
		itemLines.add(firstLine);
		itemLines.add(secondLine);
		itemLines.add(thirdLine);

		// add lines
		addNode(firstLine);
		// third line before second to have second on top
		addNode(thirdLine);
		addNode(secondLine);

		// add as visual component too, to get lifecycle management
		addVisualComponent(firstLine);
		addVisualComponent(thirdLine);
		addVisualComponent(secondLine);
	}

	/**
	 * Creates the item lines
	 */
	private void createItemLines() {
		// number of lines
		int numberOfLines = 3;

		// room for all lines
		double allLineHeight = getHeight() - topOffset - bottomOffset - (numberOfLines -1)*spaceBetween;

		// width of all lines
		double width = getWidth();

		if(numberOfLines == 3) {
			createThreeLines(spaceBetween, allLineHeight, width);
		}
		else {
			createTwoLines(spaceBetween, allLineHeight, width);
		}
	}

	private void createTwoLines(double spaceBetween, double allLineHeight,
			double width) {
		// calc heights
		double firstHeight  = 0.38 * allLineHeight;
		double secondHeight = 0.62 * allLineHeight;

		// create new item lines
		firstLine  = new ItemLine(firstHeight,  topOffset, width, spaceBetween, this, produceAsynchronous);
		secondLine = new ItemLine(secondHeight, topOffset + firstHeight + spaceBetween, width, spaceBetween, this, produceAsynchronous);

		firstLine.setSpeedFactor(1/ (0.38));
		secondLine.setSpeedFactor(1/ (0.62));

		// fill it up
		 firstLine.fillLine();
		secondLine.fillLine();

		// keep reference
		itemLines.add(firstLine);
		itemLines.add(secondLine);

		// add lines
		addNode(firstLine);
		addNode(secondLine);

		// add as visual component too, to get lifecycle management
		addVisualComponent(firstLine);
		addVisualComponent(secondLine);
	}

	/**
	 * Updates the local references if needed.
	 */
	protected void updateReferencesOnDemand() {
		if(dataSetChanged) {
			// be nice and only update after data set has changed
			updateReferences();
			dataSetChanged = false;
		}
	}

	/**
	 * Updates the local reference to all images.
	 */
	protected void updateReferences() {
		this.allImages = dataSet.getImages();
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.picture.components.LineElementProducer#produceNextLineElement(double)
	 */
	@Override
	public synchronized ItemLineElement produceNextLineElement(double height, ItemLine line) {
		updateReferencesOnDemand();
		if(allImages == null || allImages.isEmpty() || factory == null) {
			// nothing can be produced
			return null;
		}

		int randomIndex = RandomFactory.getRandomInt(allImages.size());

		// pick image at random index
		org.sociotech.communitymashup.data.Image nextImage = allImages.get(randomIndex);

		// set the height as hint for the renderer
		renderer.setHeight(height);
		// TODO better way to pass parameters
		return factory.produceVisualItem(nextImage, VisualState.PREVIEW, null);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.FXView#onUpdate(double)
	 */
	@Override
	protected void onUpdate(double framesPerSecond, long nanoTime) {
		super.onUpdate(framesPerSecond, nanoTime);
		//System.out.println("updating at fps: " + framesPerSecond);
		// update all lines
		for(ItemLine line : itemLines) {
			line.update(framesPerSecond, nanoTime);
		}
	}

	/**
	 * Will be called whenever the data set changes.
	 *
	 * @param notification Notification with change details.
	 */
	@Override
	public void dataSetChanged(Notification notification) {
		if(notification == null)
		{
			return;
		}

		// new information object added to the data set
		if(notification.getEventType() == Notification.ADD && notification.getNotifier() instanceof DataSet)
		{
			// possible new element added
			this.dataSetChanged = true;
		}

	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.view.FXView#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();

		// stop observing
		dataSetChangeObserver.disconnect();
	}

}
