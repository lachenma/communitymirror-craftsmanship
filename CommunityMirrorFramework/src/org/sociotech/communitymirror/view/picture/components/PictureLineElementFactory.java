package org.sociotech.communitymirror.view.picture.components;

import org.sociotech.communitymashup.data.Image;
import org.sociotech.communitymirror.visualitems.factory.GenericItemFactory;

/**
 * The factory for picture line elements
 * 
 * @author Peter Lachenmaier
 */
public class PictureLineElementFactory extends GenericItemFactory<Image, PictureLineElement>{
	
}
