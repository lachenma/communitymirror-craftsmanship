package org.sociotech.communitymirror.view.picture.components;


/**
 * Interface that must be implemented to be used as line element producer.
 * 
 * @author Peter Lachenmaier
 */
public interface LineElementProducer {
	/**
	 * Produces the next line element with the given height.
	 * 
	 * @param height Intended height for the line element.
	 * @param line The line to produce the item for
	 * 
	 * @return The produced element or null if not possible.
	 */
	public ItemLineElement produceNextLineElement(double height, ItemLine line);
}
