package org.sociotech.communitymirror.view.picture.components;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import javafx.application.Platform;
import javafx.scene.CacheHint;
import javafx.scene.Node;

import org.sociotech.communityinteraction.events.gesture.scroll.ScrollFinishedEvent;
import org.sociotech.communityinteraction.events.gesture.scroll.ScrollScrolledEvent;
import org.sociotech.communityinteraction.events.touch.TouchPressedEvent;
import org.sociotech.communityinteraction.events.touch.TouchReleasedEvent;
import org.sociotech.communityinteraction.events.touch.TouchTappedEvent;
import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.VisualGroup;

/**
 * A line of visual items.
 * 
 * @author Peter Lachenmaier
 *
 */
public class ItemLine extends VisualGroup {

	protected final double top;
	protected final double height;
	protected final double width;
	private double widthFactor = 1.5;
	private double fullWidth;

	private final double space;
	
	private static final double  speedFactorVal = 3.0;
	private double internalSpeedFactor = speedFactorVal;
	private double speedFactor = 1.0;
	
	/**
	 * Direction of the animation. Positive is right to left. 
	 */
	private double direction = 1.0;
	
	private int holdCounter = 0;
	
	/**
	 * Queue holding asynchronous produced elements 
	 */
	private Queue<ItemLineElement> producedItems = new ConcurrentLinkedQueue<ItemLineElement>();
	
	/**
	 * Whether to start production of new element asynchronous.
	 */
	private boolean produceAsynchronous = true;
	
	/**
	 * Reset hold after a while cause its possible to irritate hold counter with swipes 
	 */
	private double holdResetCount = 0;
	
	/**
	 * Keeps all active line elements
	 */
	private LinkedList<ItemLineElement> activeLineElements = new LinkedList<>();
	
	/**
	 * The producer for new line elements
	 */
	private LineElementProducer elementProducer;
	
	protected boolean scrolled;
	private double initialX;
	private double toX;
	private double shift;
	

	/**
	 * Creates a new item line.
	 * 
	 * @param height The height of the line.
	 * @param top The top offset.
	 * @param width The width.
	 * @param space The space between to line elements
	 * @param elementProducer The producer for new items.
	 */
	public ItemLine(double height, double top, double width, double space, LineElementProducer elementProducer) {
		super();
		this.top = top;
		this.height = height;
		this.width = width;
		this.space = space;
		this.elementProducer = elementProducer;
	}
	
	/**
	 * Creates a new item line.
	 * 
	 * @param height The height of the line.
	 * @param top The top offset.
	 * @param width The width.
	 * @param space The space between to line elements
	 * @param elementProducer The producer for new items.
	 * @param asynchronous Whether to produce new items asynchronous.
	 */
	public ItemLine(double height, double top, double width, double space, LineElementProducer elementProducer, boolean asynchronous) {
		this(height, top, width, space, elementProducer);
		this.produceAsynchronous = asynchronous;
	}
	/**
	 * Adds the given line element to this line.
	 * 
	 * @param lineElement
	 */
	public void addLineElement(ItemLineElement lineElement, boolean left) {
		if(lineElement == null) {
			return;
		}
		
		if(lineElement.getWidth() < 1.0) {
			// something went wrong in creation, so skip
			return;
		}
		
		double newX = 0;
		if(left) {
			ItemLineElement firstElement = activeLineElements.peekFirst();
			if(firstElement != null) {
				newX = firstElement.getX() - lineElement.getWidth() - 2*space;
			}
			
			// we have a new first element
			firstElement = lineElement;
			activeLineElements.addFirst(lineElement);
		}
		else {
			ItemLineElement lastElement = activeLineElements.peekLast();
			if(lastElement != null) {
				newX = lastElement.getX() + lastElement.getWidth() + 2*space;
			}
			
			// we have a new last element
			lastElement = lineElement;
			activeLineElements.addLast(lineElement);
		}
		
		// set coordinate of new line element
		lineElement.setX(newX);
		lineElement.setY(top);
		
		// set reference to line
		lineElement.setLine(this);
		
		// directly add nodes (unfortunately no double inheritance on generic types)
		if(lineElement instanceof Node) {
			this.getChildren().add((Node)lineElement);
		}
		
		// add as visual component to get lifecycle management
		if(lineElement instanceof VisualComponent) {
			this.addVisualComponent((VisualComponent)lineElement);
		}
	}
	
	/**
	 * Fills the line with new elements.
	 */
	public void fillLine() {
		fillLine(produceAsynchronous);
	}

	/**
	 * Fills the line with new elements
	 * 
	 * @param asynchronous Whether to fill asynchronous or not
	 */
	protected void fillLine(boolean asynchronous) {
		if(elementProducer == null || (isFilledRight() && isFilledLeft()))  {
			// nothing can be produced
			return;
		}
		
		if(!isFilledRight()) {
			fillSide(asynchronous, false);
		}
		
		if(!isFilledLeft()) {
			fillSide(asynchronous, true);
		}
	}

	/**
	 * Fills one element to a side of the line.
	 * 
	 * @param asynchronous Whether to fill asynchronous or not
	 * @param left Whether to fill the left side (true) or the right side.
	 */
	private void fillSide(boolean asynchronous, boolean left) {
		// fill right side
		if(!asynchronous) {
			// produce it directly
			ItemLineElement nextElement = elementProducer.produceNextLineElement(height, this);

			if(nextElement == null) {
				// no production, so stop
				return;
			}

			// add the produced element
			addLineElement(nextElement, left);
			
			// fill until the line is filled
			//fillLine(asynchronous);
		}
		else {
			if(!producedItems.isEmpty()) {
				// get element from queue and add it
				addLineElement(producedItems.poll(), left);
				
				// fill until the line is filled
				//fillLine(asynchronous);
			}
			else {
				// produce new one in thread
				//new AsyncProducer(this).start();
				
				// produce later in main thread
				Platform.runLater(new AsyncProducer(this));
			}
		}
	}

	/**
	 * Returns whether the line is filled or not on the right side.
	 * 
	 * @return Whether the line is filled or not
	 */
	private boolean isFilledRight() {
		if(activeLineElements.isEmpty()) {
			// no last element, so not filled
			return false;
		}
		
		// return if the last element crosses the right border
		return (activeLineElements.getLast().getX() + activeLineElements.getLast().getWidth()) > fullWidth;
	}
	
	/**
	 * Returns whether the line is filled or not on the left side.
	 * @return Whether the line is filled or not
	 */
	private boolean isFilledLeft() {
		if(activeLineElements.isEmpty()) {
			// no first element, so not filled
			return false;
		}
		
		// return if the first element is crossing the left border
		return (activeLineElements.getFirst().getX() < 0.0);
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualGroup#onUpdate(double)
	 */
	@Override
	protected void onUpdate(double framesPerSecond, long nanoTime) {
		double speedNormalizationFactor = speedNormalizationFactor(framesPerSecond);
		
		if(!scrolled) {
			// move only if no scrolling is currently performed
			double delta = direction * 0.1 * speedNormalizationFactor * speedFactor * internalSpeedFactor;
			updatePosition(delta);
		}
		
		updateHoldResetCount(speedNormalizationFactor);
		
		if(isInElementAnimation()) {
			this.setCache(false);
			//System.out.println("Cache Off");
		}
		else {
			this.setCacheHint(CacheHint.SPEED);
			this.setCache(true);
			//System.out.println("Cache On");
		}
			
		// call super to apply changes
		super.onUpdate(framesPerSecond, nanoTime);
		
	}

	/**
	 * Returns if one of the active line element is in an animation
	 * 
	 * @return True if one of the active lie elements is in an animation
	 */
	private boolean isInElementAnimation() {
		for(ItemLineElement element : activeLineElements) {
			if(element.isInAnimation()) return true;
		}
				
		return false;
	}

	/**
	 * Updates the position by moving the given delta x.
	 * 
	 * @param delta Delta in x direction.
	 */
	private void updatePosition(double delta) {
		double newX = this.getPositionX() - delta;
		
		if(newX > toX && direction < 0.0) {
			// moved over right border
			shift(-1.0 * shift);
			newX += shift;
		} else if(newX < toX && direction > 0.0) {
			// moved over left border
			shift(-1.0 * shift);
			newX += shift;
		} else if(newX - shift > toX && direction > 0.0) {
			// scrolled over right border while moving left
			shift(shift);
			newX -= shift;
		} else if(newX - shift < toX && direction < 0.0) {
			// scrolled over left border while moving right
			shift(shift);
			newX -= shift;
		} //else {
			// fill up lines only when no shifting to save performance in this frame
			fillLine();
		//}
		
		//System.out.println("NX: " + newX);
		//this.move(newX - this.getPositionX(), 0);
		this.setPositionX(newX);
	}

	/**
	 * Shifts the line by the given offset in x direction.
	 * 
	 * @param xOffset Offset to shift the line.
	 */
	protected void shift(double xOffset) {
	
		// shift all active elements
		// copy to allow remove
		List<ItemLineElement> tmpItemList = new LinkedList<>(activeLineElements);
		for(ItemLineElement lineElement : tmpItemList) {
			// offset is negative
			lineElement.setX(lineElement.getX() + xOffset);
			if(lineElement.getX() + lineElement.getWidth() < 0 || lineElement.getX() > fullWidth) {
				lineElement.destroy();
				activeLineElements.remove(lineElement);
				
				// remove from 
				if(lineElement instanceof VisualComponent) {
					removeVisualComponent((VisualComponent) lineElement);
					// remove from java fx tree
					removeNode((VisualComponent)lineElement);
				}
			}
		}
	}

	/**
	 * Updates the hold reset depending on the speed normalization
	 * 
	 * @param speedNormalizationFactor Factor for speed normalization
	 */
	private void updateHoldResetCount(double speedNormalizationFactor) {
		holdResetCount -= speedNormalizationFactor;
		if(holdResetCount < 0) {
			resetHold();
		}
	}

	/**
	 * Sets the speed factor of the animation
	 * 
	 * @param speedFactor Speed factor.
	 */
	public void setSpeedFactor(double speedFactor) {
		this.speedFactor = speedFactor;
	}
	
	/**
	 * Calculates a speed normalization factor depending on the rendering speed.
	 * @param framesPerSecond Curren rendering speed in frames per second.
	 * @return The normalization factor relative to 30 fps
	 */
	private double speedNormalizationFactor(double framesPerSecond) {
		return 30.0 / framesPerSecond;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualGroup#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		// destroy all line elements
		List<ItemLineElement> tmpItemList = new LinkedList<>(activeLineElements);
		for(ItemLineElement lineElement : tmpItemList) {
			// remove from java fx tree
			this.getChildren().remove(lineElement);
			lineElement.destroy();
			activeLineElements.remove(lineElement);
		}
	}

	/**
	 * Increases the hold counter by one. Line will hold while hold counter is biggger than 0.
	 */
	public synchronized void hold() {
		holdCounter++;
		internalSpeedFactor = 0;
		holdResetCount = 20 * 30;
	}

   /**
	* Decreases the hold counter by one. Line will hold while hold counter is biggger than 0.
	*/
	public synchronized void releaseHold() {
		holdCounter--;
		if(holdCounter <= 0) {
			holdCounter = 0;
			resetHold();
		}
	}

	/**
	 * Resets the hold
	 */
	protected void resetHold() {
		// reset speed
		internalSpeedFactor = speedFactorVal;
		holdResetCount = 0;
		scrolled = false;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualGroup#onInit()
	 */
	@Override
	protected void onInit() {
		super.onInit();
		
		initializeInteraction();
        
		// calc full width
		fullWidth = width * widthFactor;
		
		// calculate overlapping
		double overlap = (fullWidth - width) / 2.0;
		initialX = -1.0 * overlap;
		this.setPositionX(initialX);
		
		// calculate shift
		shift = direction * 0.66 * overlap;
		toX = initialX - shift;
		
        // fill up line with items
		// always synchronous
		fillLine(false);
		
		// no caching on the line
        this.setCache(false);
	}
	
	/**
	 * Initializes the interaction of this item line;
	 */
	protected void initializeInteraction() {
		
		// simply make it scrollable -> this lead to call of onScroll...
		this.makeScrollable();
		
		// make it touchable to hold lines
		this.makeTouchable();
		
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onTouchPressed(org.sociotech.communityinteraction.events.touch.TouchPressedEvent)
	 */
	@Override
	public void onTouchPressed(TouchPressedEvent event) {
		if(!scrolled) {
			hold();
		}
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onTouchReleased(org.sociotech.communityinteraction.events.touch.TouchReleasedEvent)
	 */
	@Override
	public void onTouchReleased(TouchReleasedEvent event) {
		releaseHold();
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onTouchTapped(org.sociotech.communityinteraction.events.touch.TouchTappedEvent)
	 */
	@Override
	public void onTouchTapped(TouchTappedEvent event) {
		// let user release hold by taps
		// can only happen if touch release events get lost
		releaseHold();
		scrolled = false;
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onScrollScrolled(org.sociotech.communityinteraction.events.gesture.scroll.ScrollScrolledEvent)
	 */
	@Override
	public void onScrollScrolled(ScrollScrolledEvent scrollEvent) {
		updatePosition(-1.0 * scrollEvent.getDeltaX());
		scrolled = true;
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualComponent#onScrollFinished(org.sociotech.communityinteraction.events.gesture.scroll.ScrollFinishedEvent)
	 */
	@Override
	public void onScrollFinished(ScrollFinishedEvent scrollEvent) {
		scrolled = false;
	}
	
	/**
	 * Returns the height of the line.
	 * 
	 * @return The height of the line.
	 */
	public double getHeight() {
		return height;
	}

	/**
	 * Returns the width of the line.
	 * 
	 * @return The width of the line.
	 */
	public double getWidth() {
		return width;
	}
		
	/**
	 * Producer thread for line elements.
	 * 
	 * @author Peter Lachenmaier
	 */
	protected class AsyncProducer extends Thread {

		private ItemLine line;
		
		public AsyncProducer(ItemLine line) {
			this.line = line;
		}
		/* (non-Javadoc)
		 * @see java.lang.Thread#run()
		 */
		@Override
		public void run() {
			if(elementProducer == null) {
				// nothing can be produced
				return;
			}
			// produce it
			ItemLineElement newElement = elementProducer.produceNextLineElement(height, line);
			if(newElement != null && newElement.isValid()) {
				// add it to queue
				producedItems.add(newElement);
			}
		}
	}
}
