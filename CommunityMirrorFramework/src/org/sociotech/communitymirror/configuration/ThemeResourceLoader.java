
package org.sociotech.communitymirror.configuration;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration2.Configuration;

/**
 * Resource loader for theme resources.
 *
 * @author Peter Lachenmaier
 */
public class ThemeResourceLoader extends ResourceLoader {

    /**
     * Creates the resource loader based on the given configuration.
     *
     * @param configuration
     *            CommunityMirror configuration
     */
    public ThemeResourceLoader(Configuration configuration) {
        super(ThemeResourceLoader.getResourceFolderList(configuration));
    }

    private static List<String> getResourceFolderList(Configuration configuration) {
        String resourceHierarchy = configuration.getString("theme.resourcehierarchy");
        List<String> result = new ArrayList<>();
        if (!resourceHierarchy.isEmpty()) {
            String themesFolder = configuration.getString("theme.folder");
            String[] folderCandidates = resourceHierarchy.split(",");
            for (String folderCandidate : folderCandidates) {
                result.add(themesFolder + folderCandidate.trim());
            }
        } else {
            result.add(configuration.getString("theme.resourcefolder"));
            result.add(configuration.getString("theme.defaultresourcefolder"));
        }
        return result;
    }
}
