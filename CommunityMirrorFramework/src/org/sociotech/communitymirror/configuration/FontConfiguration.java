package org.sociotech.communitymirror.configuration;

import org.apache.commons.configuration2.Configuration;

/**
 * Helper class for easier handling of font configurations.
 * 
 * @author Peter Lachenmaier
 */
public class FontConfiguration {
	
	/**
	 * The font family.
	 */
	private String fontFamily;
	
	/**
	 * Construct the default font configuration.
	 * 
	 * @param configuration CMF configuration
	 */
	public FontConfiguration(Configuration configuration) {
		this(configuration, "default");
	}
	
	/**
	 * Constructs the font configuration for the given font class.
	 * 
	 * @param configuration CMF Configuration
	 * @param fontClass Font class (e.g. serif or default)
	 */
	public FontConfiguration(Configuration configuration, String fontClass) {
		setFontFamily(configuration.getString("theme.font." + fontClass + ".family"));
	}

	/**
	 * Returns the configured font family.
	 * 
	 * @return The configured font family.
	 */
	public String getFontFamily() {
		return fontFamily;
	}

	/**
	 * Sets the font family.
	 * 
	 * @param fontFamily Font family.
	 */
	public void setFontFamily(String fontFamily) {
		this.fontFamily = fontFamily;
	}
}
