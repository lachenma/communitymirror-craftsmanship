
package org.sociotech.communitymirror.components;

import java.net.URL;
import java.util.List;

import org.apache.commons.configuration2.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communityinteraction.events.touch.TouchPressedEvent;
import org.sociotech.communitymirror.view.View;
import org.sociotech.communitymirror.view.flow.FlowView;
import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.VisualGroup;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

/**
 * VisualComponent showing an image - e.g. for displaying logos in the front
 * layer
 *
 * @author Michael Koch
 *
 */

public class MediaViewComponent extends VisualComponent {

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(MediaViewComponent.class);

    public MediaViewComponent() {
        super();
        // loading the video will be done in configure()
    }

    String action = null;

    /**
     * Configure visual component using the given configuration and
     * configuration prefix. This method is called after the component is
     * created - and before it is inserted in the scene.
     *
     * @author Michael Koch
     */
    @Override
    public void configure(Configuration configuration, String confPrefix) {

        String filename = configuration.getString(confPrefix + ".filename");
        if (filename == null) {
            this.logger.warn("No filename found for MediaViewComponent - " + confPrefix + ".filename");
            return;
        }
        URL url = this.getClass().getClassLoader().getResource(filename);
        if (url == null && filename.startsWith("http")) {
            try {
                url = new URL(filename);
            } catch (Exception e) {
                // TODO
            }
        }
        if (url == null) {
            this.logger.warn("Could not open file/url " + filename + " in MediaView");
            return;
        }

        System.err.println("trying to display video from " + url.toExternalForm());
        Media media = new Media(url.toExternalForm());
        MediaPlayer mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setAutoPlay(true);

        MediaView mediaView = new MediaView(mediaPlayer);
        mediaView.setFitHeight(500);
        mediaView.setFitWidth(500);

        this.getChildren().add(mediaView);

        mediaPlayer.play();

    }

    @Override
    public void onTouchPressed(TouchPressedEvent event) {
        super.onTouchPressed(event);
        System.err.println("onTouchPressed");
        if (!this.isVisible()) {
            return;
        }

        // if there is an action defined, then perform it ...
        if (this.action != null && this.action.length() > 0) {

            if (this.action.startsWith("closeGroupAndAddItem(")) {

                VisualGroup visualGroup = this.getParentVisualGroup();
                visualGroup.setVisible(false);
                List<VisualComponent> visualComponents = visualGroup.getVisualComponents();
                for (VisualComponent visualComponent : visualComponents) {
                    visualComponent.setVisible(false);
                    // visualGroup.removeVisualComponent(visualComponent);
                }

                // add a VisualItem for the Item referenced in brackets
                View view = this.getParentView();
                if (view != null && view instanceof FlowView) {
                    int pos = this.action.indexOf("(");
                    if (pos > 0) {
                        int pos2 = this.action.indexOf(")");
                        String ident = this.action.substring(pos + 1, pos2);
                        ((FlowView) view).addVisualItem(ident, this.getPositionX(), this.getPositionY());
                    }
                }
            }
            if (this.action.equals("close")) {

                this.getChildren().clear();

            }
            if (this.action.equals("closeGroup")) {

                VisualGroup visualGroup = this.getParentVisualGroup();
                visualGroup.setVisible(false);
                /*
                 * List<VisualComponent> visualComponents =
                 * visualGroup.getVisualComponents();
                 * for (VisualComponent visualComponent : visualComponents) {
                 * visualGroup.removeVisualComponent(visualComponent);
                 * }
                 */
            }

        }
    }

}
