
package org.sociotech.communitymirror.components.bus.schedulereaders;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sociotech.communitymirror.components.bus.BusDeparture;
import org.sociotech.communitymirror.view.flow.components.VisualComponentsPile;

/**
 * This class implements a BusScheduleReader capable of providing bus schedule
 * data for the MVV bus network in Munich. Adapted from class TransportManager
 * in project TUMCampusApp (see
 * http://www.programcreek.com/java-api-examples/index.php?source_dir=TumCampusApp-master/)
 *
 * @author Julian Fietkau, Michael Koch
 *
 */

public class MVVBusScheduleReader implements BusScheduleReader {

    /**
     * The log4j logger reference.
     */
    private final Logger logger = LogManager.getLogger(VisualComponentsPile.class.getName());

    private String departureBusStop;
    private String departureBusStopID;

    private int secondsBetweenUpdates = 5 * 60;
    private long lastUpdated = 0;

    private List<BusDeparture> schedule;

    public MVVBusScheduleReader(String departureBusStop) {
        this.departureBusStop = departureBusStop;
        this.departureBusStopID = departureBusStop;

        this.schedule = new ArrayList<>();
        this.update();
    }

    @Override
    public void update() {
        if (this.lastUpdated < System.currentTimeMillis() - this.secondsBetweenUpdates * 1000) {
            this.remoteUpdate();
            this.lastUpdated = System.currentTimeMillis();
        }
    }

    public void remoteUpdate() {
        // No HTTPS support :(
        String MVV_API_BASE = "http://efa.mvv-muenchen.de/mobile/";

        String OUTPUT_FORMAT = "outputFormat=JSON";
        String STATELESS = "stateless=1";
        String COORD_OUTPUT_FORMAT = "coordOutputFormat=WGS84";
        String DEPARTURE_QUERY_TYPE = "type_dm=stop";
        String DEPARTURE_QUERY_COMMON = "itOptionsActive=1&ptOptionsActive=1&mergeDep=1&useAllStops=1&mode=direct";

        String DEPARTURE_QUERY = "XSLT_DM_REQUEST";
        String DEPARTURE_QUERY_CONST;
        String[] DEPARTURE_QUERY_CONST_PARAMS = { OUTPUT_FORMAT, STATELESS, COORD_OUTPUT_FORMAT, DEPARTURE_QUERY_TYPE,
                DEPARTURE_QUERY_COMMON };

        StringBuilder departureQueryBuilder = new StringBuilder(MVV_API_BASE);
        departureQueryBuilder.append(DEPARTURE_QUERY).append('?');
        for (String param : DEPARTURE_QUERY_CONST_PARAMS) {
            departureQueryBuilder.append(param).append('&');
        }
        DEPARTURE_QUERY_CONST = departureQueryBuilder.toString();

        // Set the following parameters before appending
        String LANGUAGE = "language=";
        String DEPARTURE_QUERY_STATION = "name_dm=";

        try {
            String language = LANGUAGE + Locale.getDefault().getLanguage();
            // ISO-8859-1 is needed for mvv
            String departureQuery = DEPARTURE_QUERY_STATION + URLEncoder.encode(this.departureBusStopID, "ISO-8859-1");

            String query = DEPARTURE_QUERY_CONST + language + '&' + departureQuery;

            String data = "";

            // for quick testing, assign some cached data here so we don't have
            // to query the MVV server each launch
            // data = ...

            if (data.length() == 0) {
                // Download departures
                URL url = new URL(query);
                InputStreamReader isr = new InputStreamReader(url.openStream(), "UTF-8");
                BufferedReader br = new BufferedReader(isr);

                String line;
                while ((line = br.readLine()) != null) {
                    data = data + line;
                }
            }

            // JSONObject is allergic to duplicate keys, but the MVV site
            // returns duplicate 'speedFactor' keys for some objects. We just
            // throw those away before parsing.
            data = data.replaceAll("\"speedFactor\":\"100\", ", "");

            JSONArray departures = new JSONObject(data).optJSONArray("departureList");
            if (departures == null) {
                return;
            }

            this.schedule.clear();
            for (int i = 0; i < departures.length(); i++) {
                JSONObject departure = departures.getJSONObject(i);
                JSONObject servingLine = departure.getJSONObject("servingLine");
                JSONObject dateTime = departure.getJSONObject("dateTime");
                String lineDestination = servingLine.getString("direction");
                OffsetDateTime departureOffsetDate = OffsetDateTime.of(dateTime.getInt("year"),
                        dateTime.getInt("month"), dateTime.getInt("day"), dateTime.getInt("hour"),
                        dateTime.getInt("minute"), 0, // seconds
                        0, // nanoseconds
                        (LocalDateTime.now().atZone(ZoneId.systemDefault())).getOffset());
                this.schedule.add(new BusDeparture(servingLine.getString("symbol"), lineDestination,
                        this.departureBusStop, departureOffsetDate));
            }

            Collections.sort(this.schedule, new Comparator<BusDeparture>() {
                @Override
                public int compare(BusDeparture lhs, BusDeparture rhs) {
                    return lhs.getPlannedDeparture().compareTo(rhs.getPlannedDeparture());
                }
            });

        } catch (UnsupportedEncodingException e) {
            throw new Error(e); // Programming error. Fail hard.
        } catch (IOException | JSONException e) {
            // We got no valid JSON, mvg-live is probably bugged
            e.printStackTrace();
            this.logger.info(
                    "Got invalid result from MVV web query. MVV website might be down. If problem persists, check if the format changed.");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.urbanlifeplus.communitymirror.components.bus.
     * BusScheduleReader#getUpcomingDepartures(java.lang.String,
     * java.time.Duration)
     */
    @Override
    public List<BusDeparture> getUpcomingDepartures(String fromBusStop, Duration timeframeFromNow) {
        List<BusDeparture> result = new ArrayList<>();

        if (fromBusStop.equals(this.departureBusStop)) {
            for (BusDeparture candidate : this.schedule) {
                if (candidate.getPlannedDeparture().compareTo(OffsetDateTime.now()) > 0 && candidate
                        .getPlannedDeparture().compareTo(OffsetDateTime.now().plus(timeframeFromNow)) <= 0) {
                    result.add(candidate);
                }
            }
        }

        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.urbanlifeplus.communitymirror.components.bus.
     * BusScheduleReader#getUpcomingDepartures(java.lang.String, int)
     */
    @Override
    public List<BusDeparture> getUpcomingDepartures(String fromBusStop, int maxNumber) {
        List<BusDeparture> result = new ArrayList<>();

        if (fromBusStop.equals(this.departureBusStop)) {
            for (BusDeparture candidate : this.schedule) {
                if (candidate.getPlannedDeparture().compareTo(OffsetDateTime.now()) > 0) {
                    result.add(candidate);
                    if (result.size() == maxNumber) {
                        break;
                    }
                }
            }
        }

        return result;
    }
}
