
package org.sociotech.communitymirror.components.bus.schedulereaders;

import java.time.Duration;
import java.util.List;

import org.sociotech.communitymirror.components.bus.BusDeparture;

/**
 * This interface describes a type of class that can pull bus schedule data from
 * somewhere and prepare it for display by a BusComponent.
 *
 * @author Julian Fietkau
 *
 */
public interface BusScheduleReader {

    /**
     * Based on the available bus schedule data, get a list of bus departures
     * from a specific station that will occur within a certain timeframe (e.g.
     * the next 10 minutes, the next hour, ...).
     *
     * Please note that this method may be called very often (whenever a
     * BusComponent attempts to refresh its contents, for example). If the
     * implementation involves heavy calculations or external resources,
     * consider sensible internal caching of the results.
     *
     * @param fromBusStop
     *            the name of the departure bus stop
     * @param timeframeFromNow
     *            the timeframe, starting right now
     * @return a list of departures that fit the criteria
     */
    List<BusDeparture> getUpcomingDepartures(String fromBusStop, Duration timeframeFromNow);

    /**
     * Based on the available bus schedule data, get a certain number of
     * upcoming bus departures from a specific station. Please note that the
     * resulting list may be shorter than maxNumber if a sufficient number of
     * departures can not be found in the schedule data.
     *
     * Please note that this method may be called very often (whenever a
     * BusComponent attempts to refresh its contents, for example). If the
     * implementation involves heavy calculations or external resources,
     * consider sensible internal caching of the results.
     *
     * @param fromBusStop
     *            the name of the departure bus stop
     * @param timeframeFromNow
     *            the timeframe, starting right now
     * @return a list of departures that fit the criteria
     */
    List<BusDeparture> getUpcomingDepartures(String fromBusStop, int maxNumber);

    /**
     * This method is called by the bus component every time it refreshes its
     * contents (interval is configurable, default 10 seconds). The
     * BusScheduleReader may use this method for any regular tasks, such as
     * updating its schedule data from a remote server. It is strongly
     * recommended that the BusScheduleReader keeps track of its own rate limit
     * goals.
     */
    public void update();

}
