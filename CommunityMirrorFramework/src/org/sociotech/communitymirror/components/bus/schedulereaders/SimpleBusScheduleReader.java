
package org.sociotech.communitymirror.components.bus.schedulereaders;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.sociotech.communitymirror.components.bus.BusDeparture;

/**
 * This class implements a BusScheduleReader capable of providing bus schedule
 * data for testing purposes. It fills its schedule with busses of a few random
 * lines departing from a single station every ten minutes for the next seven
 * days. It is not timezone-aware.
 *
 * @author Julian Fietkau
 *
 */

public class SimpleBusScheduleReader implements BusScheduleReader {

    private String departureBusStop;
    private List<String> departureBusLines;
    private Map<String, List<String>> departureLineDestinations;

    private List<BusDeparture> schedule;

    private Random random;

    public SimpleBusScheduleReader(String departureBusStop) {
        this.departureBusStop = departureBusStop;

        this.departureBusLines = new ArrayList<>();
        this.departureBusLines.add("015");
        this.departureBusLines.add("017");
        this.departureBusLines.add("026");

        this.departureLineDestinations = new HashMap<>();
        this.departureLineDestinations.put("015", new ArrayList<String>());
        this.departureLineDestinations.put("017", new ArrayList<String>());
        this.departureLineDestinations.put("026", new ArrayList<String>());
        this.departureLineDestinations.get("015").add("Donk Wendeplatz");
        this.departureLineDestinations.get("017").add("Am Brückensteg");
        this.departureLineDestinations.get("017").add("Voltastraße");
        this.departureLineDestinations.get("017").add("Busbahnhof");
        this.departureLineDestinations.get("026").add("Karrenweg");
        this.departureLineDestinations.get("026").add("Merreter");
        this.departureLineDestinations.get("026").add("Odenkirchen Post");

        this.schedule = new ArrayList<>();
        this.random = new Random();

        // Fill the schedule with one bus every 10 minutes for the
        // upcoming week
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 6 * 24; j++) {
                LocalTime departureTime = LocalTime.of((int) Math.floor(j / 6), (j * 10) % 60);
                LocalDate departureDate = LocalDate.now().plusDays(i);

                // An Instant created directly from the LocalTime and LocalDate
                // throws an UnsupportedTemporalTypeException if we try to use
                // it to make our OffsetDateTime, so we take the roundabout way
                // through a ZonedDayTime. After reading up on the differences
                // between OffsetDateTime and ZonedDateTime, I still think
                // OffsetDateTime is the one we want in the end.
                ZonedDateTime zonedDateTime = ZonedDateTime.of(departureDate, departureTime, ZoneId.systemDefault());
                Instant departureInstant = Instant.from(zonedDateTime);

                OffsetDateTime departureOffsetDate = OffsetDateTime.of(LocalDateTime.of(departureDate, departureTime),
                        ZoneId.systemDefault().getRules().getOffset(departureInstant));

                String departureBusLine = this.getRandomItem(this.departureBusLines);
                String departureLineDestination = this
                        .getRandomItem(this.departureLineDestinations.get(departureBusLine));
                BusDeparture departure = new BusDeparture(departureBusLine, departureLineDestination,
                        this.departureBusStop, departureOffsetDate, Duration.ZERO);

                this.schedule.add(departure);
            }
        }
    }

    /**
     * Utility function to return a random item from a list
     *
     * @param theList
     */
    private <E> E getRandomItem(List<E> theList) {
        if (theList == null || theList.size() == 0) {
            throw new IllegalArgumentException("list must contain at least one item");
        }
        int index = this.random.nextInt(theList.size());
        return theList.get(index);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.urbanlifeplus.communitymirror.components.bus.
     * BusScheduleReader#getUpcomingDepartures(java.lang.String,
     * java.time.Duration)
     */
    @Override
    public List<BusDeparture> getUpcomingDepartures(String fromBusStop, Duration timeframeFromNow) {
        List<BusDeparture> result = new ArrayList<>();

        if (fromBusStop.equals(this.departureBusStop)) {
            for (BusDeparture candidate : this.schedule) {
                if (candidate.getPlannedDeparture().compareTo(OffsetDateTime.now()) > 0 && candidate
                        .getPlannedDeparture().compareTo(OffsetDateTime.now().plus(timeframeFromNow)) <= 0) {
                    result.add(candidate);
                }
            }
        }

        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.sociotech.urbanlifeplus.communitymirror.components.bus.
     * BusScheduleReader#getUpcomingDepartures(java.lang.String, int)
     */
    @Override
    public List<BusDeparture> getUpcomingDepartures(String fromBusStop, int maxNumber) {
        List<BusDeparture> result = new ArrayList<>();

        if (fromBusStop.equals(this.departureBusStop)) {
            for (BusDeparture candidate : this.schedule) {
                if (candidate.getPlannedDeparture().compareTo(OffsetDateTime.now()) > 0) {
                    result.add(candidate);
                    if (result.size() == maxNumber) {
                        break;
                    }
                }
            }
        }

        return result;
    }

    @Override
    public void update() {
        // This SimpleBusScheduleReader does not need any run-time updates.
    }
}
