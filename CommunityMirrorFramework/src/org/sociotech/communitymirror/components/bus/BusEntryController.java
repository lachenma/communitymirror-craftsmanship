
package org.sociotech.communitymirror.components.bus;

import java.net.URL;
import java.util.ResourceBundle;

import org.sociotech.communitymirror.visualitems.fxml.FXMLController;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;

/**
 * Controller class for bus departure entry
 *
 * @author Julian Fietkau
 *
 */
public class BusEntryController extends FXMLController implements Initializable {

    @FXML
    Text lineNumber;

    @FXML
    Text lineDestination;

    @FXML
    Text departureTime;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void setLineNumber(String text) {
        if (this.lineNumber != null) {
            this.lineNumber.setText(text);
        }
    }

    public void setLineDestination(String text) {
        if (this.lineDestination != null) {
            this.lineDestination.setText(text);
        }
    }

    public void setDepartureTime(String text) {
        if (this.departureTime != null) {
            this.departureTime.setText(text);
        }
    }
}
