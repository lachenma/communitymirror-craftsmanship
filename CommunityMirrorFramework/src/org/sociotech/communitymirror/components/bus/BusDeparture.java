
package org.sociotech.communitymirror.components.bus;

import java.time.Duration;
import java.time.OffsetDateTime;

/**
 * Class describing a specific bus departure. This is a simple, immutable data
 * container.
 *
 * @author Julian Fietkau
 *
 */

public class BusDeparture {

    private final String busLine;
    private final String lineDestination;
    private final String fromStation;
    private final OffsetDateTime plannedDeparture;
    private final Duration lateBy;

    public BusDeparture(String busLine, String lineDestination, String fromStation, OffsetDateTime plannedDeparture,
            Duration lateBy) {
        this.busLine = busLine;
        this.lineDestination = lineDestination;
        this.fromStation = fromStation;
        this.plannedDeparture = plannedDeparture;
        this.lateBy = lateBy;
    }

    public BusDeparture(String busLine, String lineDestination, String fromStation, OffsetDateTime plannedDeparture) {
        this(busLine, lineDestination, fromStation, plannedDeparture, Duration.ZERO);
    }

    public String getBusLine() {
        return this.busLine;
    }

    public String getLineDestination() {
        return this.lineDestination;
    }

    public String getFromStation() {
        return this.fromStation;
    }

    public OffsetDateTime getPlannedDeparture() {
        return this.plannedDeparture;
    }

    public Duration getLateBy() {
        return this.lateBy;
    }

    @Override
    public String toString() {
        return this.busLine + ": " + this.fromStation + " -> " + this.lineDestination + " @ " + this.plannedDeparture
                + " + " + this.lateBy;

    }
}
