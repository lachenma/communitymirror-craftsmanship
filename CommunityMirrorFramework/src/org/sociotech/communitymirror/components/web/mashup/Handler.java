
package org.sociotech.communitymirror.components.web.mashup;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

/**
 * URLHandler for custom URLConnection for CommunityMirror - to be used in
 * JavaFX WebView/WebEngine.
 *
 * @author kochm
 *
 */

public class Handler extends URLStreamHandler {

    @Override
    protected URLConnection openConnection(URL url) throws IOException {
        return new CMURLConnection(url);
    }

}
