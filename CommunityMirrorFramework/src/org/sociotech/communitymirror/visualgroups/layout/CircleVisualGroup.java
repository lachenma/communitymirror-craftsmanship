package org.sociotech.communitymirror.visualgroups.layout;

import java.util.LinkedList;
import java.util.List;

import javafx.scene.paint.Color;

import org.sociotech.communitymirror.visualitems.VisualComponent;
import org.sociotech.communitymirror.visualitems.VisualGroup;
import org.sociotech.communitymirror.visualitems.components.spring.VisualSpringConnection;

import com.facebook.rebound.BaseSpringSystem;

/**
 * A visual group that aligns a number of visual components in a circular layout and connects them
 * with springs.
 * 
 * @author Peter Lachenmaier, Eva Loesch
 */
public class CircleVisualGroup extends VisualGroup {
	
	/**
	 * The list of layouted components 
	 */
	private List<VisualComponent> layoutedComponents;

	/**
	 * The center in the circle layout
	 */
	private VisualComponent center;
	
	/**
	 * The radius of the circle layout
	 */
	private double radius;
	
	/**
	 * Local reference to the spring system. 
	 */
	private BaseSpringSystem springSystem;
	
	/**
	 * VisualGroup that contains all created springs of this CircleVisualGroup.
	 */
	private VisualGroup springGroup = new VisualGroup();
	
	/**
	 * The list of all created springs
	 */
	private List<VisualSpringConnection> springs = new LinkedList<>();
	
	/**
	 * The list of all angles for layout of springs.
	 */
	private List<Double> angles;
	
	/**
	 * Determines if the position of the first component in list of layoutedComponents should keep it's current position.
	 */
	private boolean keepPositionOfFirstCircleComponent;
	
	/**
	 * @return the angles
	 */
	public List<Double> getAngles() {
		return angles;
	}

	/**
	 * Tells if the spring connections have to be created or not (because they are given in constructor)
	 */
	private boolean createSpringConnections = true;
	
	/**
	 * Creates a visual group with circular layout for the given list of components and springs.
	 * 
	 * @author Eva Loesch
	 * 
	 * @param components Components to align around center
	 * @param center Component in the center
	 * @param springs list of spring connections in the circle visual group
	 * @param radius Radius of the layout circle
	 * @param springSystem Refernce to the spring system to be used for spring connections
	 */
	public CircleVisualGroup(List<VisualComponent> components, VisualComponent center, List<VisualSpringConnection> springs, double radius, BaseSpringSystem springSystem) {
		this(false, components, null, center, radius, springSystem);
		this.springs = springs;
		this.createSpringConnections = false;
		
		// add all springs
		for(VisualSpringConnection springConnection : springs)
		{
			// add spring connection also as visual component to retrieve updates
			springGroup.addVisualComponent(springConnection);
		}
	}
	
	/**
	 * Creates a visual group with circular layout for the given list of components.
	 * 
	 * @param components Components to align around center
	 * @param angles list of angles for layout of springs
	 * @param center Component in the center
	 * @param radius Radius of the layout circle
	 * @param springSystem Refernce to the spring system to be used for spring connections
	 */
	public CircleVisualGroup(boolean keepPositionOfFirstCircleComponent, List<VisualComponent> components, List<Double> angles, VisualComponent center, double radius, BaseSpringSystem springSystem) {
		this.keepPositionOfFirstCircleComponent = keepPositionOfFirstCircleComponent;
		this.layoutedComponents = components;
		this.angles = angles;
		this.center = center;
		this.radius = radius;
		this.springSystem = springSystem;
		
		// add all components
		for(VisualComponent component : components) {
			addVisualComponent(component);
		}
		addVisualComponent(center);
	}
	
	/**
	 * Returns the central component of this circular visual group.
	 */
	public VisualComponent getCentralComponent()
	{
		return this.center;
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualGroup#onInit()
	 */
	@Override
	protected void onInit() {
		super.onInit();
		
		// init springGroup
		springGroup.init();
		
		// add the springGroup first in order to put the lowest z-index within this CircleVisualGroup to the springGroup
		this.addVisualComponent(springGroup, true);
		
		// position all components
		layoutCircular(this.keepPositionOfFirstCircleComponent);
		
		// and connect them to the center
		for(VisualComponent component : layoutedComponents) {
			connectComponentToCenter(component);
		}

		// now add all layouted components in front of springs
		for(VisualComponent component : layoutedComponents) {
			addNode(component);
		}
		
		// and finally the center component
		addNode(center);
	}

	/**
	 * Connects the given component to the center component.
	 * 
	 * @param component Component to connect
	 */
	protected void connectComponentToCenter(VisualComponent component) {
		if(!createSpringConnections) {
			// spring creation is switched off
			return;
		}
		
		VisualSpringConnection springConnection = VisualSpringConnection.connect(center, component, springSystem, true);
		
		// keep reference
		springs.add(springConnection);
		
		// and as visual component and node to springGroup
		springGroup.addVisualComponent(springConnection, true);
		
		// Debug to show the position alignment springs
		springConnection.getHiddenLayoutSpring().getLine().setStroke(Color.RED);
		addNode(springConnection.getHiddenLayoutSpring().getLine());
	}
	
	/**
	 * Removes a given spring connection from this circle visual group.
	 */
	private void removeSpringConnection(VisualSpringConnection spring) {
		springGroup.removeVisualComponent(spring, true);
		this.springs.remove(spring);
		spring.disconnect();
	}
	
	/**
	 * Removes a given visual component from the list of layouted visual components of this visual group.
	 */
	public void removeLayoutedComponent(VisualComponent component) {
		
		// remove layouted component from list of layouted components of this circle visual group
		this.layoutedComponents.remove(component);
		
		this.removeVisualComponent(component, true);
		
		// remove visual spring connection to deleted layouted component
		List<VisualSpringConnection> springIterationList = new LinkedList<VisualSpringConnection>(springs);
		for(VisualSpringConnection connection : springIterationList)
		{
			if(connection.getStartC().equals(component) || connection.getEndC().equals(component))
			{
				this.removeSpringConnection(connection);
				break;
			}
		}
	}
	
	/**
	 * Adds the given component additionally to the layout.
	 * 
	 * @param component Additional component to layout
	 */
	public void addLayoutedComponent(VisualComponent component) {
		
		// add the given layouted component to the list of layouted components of this circle visual group
		layoutedComponents.add(component);
		
		// repostion all elements
		layoutCircular(false);
		
		// connect the new component
		connectComponentToCenter(component);
		
		// add as node and visual component
		addVisualComponent(component, true);
	}
	
	/**
	 * Substitutes a given old component by a given new component within the layout.
	 * That means the old one is removed and the new one is added at the position of the old one to the list of components.
	 * 
	 * @param oldComponent : The component that has to be removed
	 * @param newComponent : The component that has to be added instead
	 */
	public void substituteLayoutedComponent(VisualComponent oldComponent, VisualComponent newComponent) {
		if(newComponent == null) {
			// simply delete old
			this.removeLayoutedComponent(oldComponent);
		}
		
		if(oldComponent != null && layoutedComponents.contains(oldComponent)) {
			// get position of old component within the list of components
			int index = layoutedComponents.indexOf(oldComponent);
			
			// add newComponent to the list of components at position of oldComponent
			layoutedComponents.add(index, newComponent);

			// remove oldComponent and its connections
			this.removeLayoutedComponent(oldComponent);
			
			// set newComponent to screen position of oldComponent
			newComponent.getSpringConnectorXProperty().set(oldComponent.getSpringConnectorXProperty().get());
			newComponent.getSpringConnectorYProperty().set(oldComponent.getSpringConnectorYProperty().get());

			// connect the new component
			connectComponentToCenter(newComponent);

			// add as node and visual component
			addVisualComponent(newComponent, true);

		}
		else {
			// simply add new
			this.addLayoutedComponent(newComponent);
		}
	}

	/**
	 * Calculates the position and layouts all components.
	 */
	protected void layoutCircular(boolean keepPositionOfFirstCircleComponent) {
		
		// align around spring connector position
		double centerX = center.getSpringConnectorXProperty().get();
		double centerY = center.getSpringConnectorYProperty().get();
					
		if(this.angles != null && this.angles.size() == this.layoutedComponents.size())
		{		
//			System.out.println("from hashmap");
//			System.out.println("--------------------------------------------------");
			
			int count = 0;
			for(VisualComponent component : layoutedComponents) {
				
				double curAngle = this.angles.get(count++);
				
				double xDiff = radius * Math.cos(curAngle);
				double yDiff = radius * Math.sin(curAngle);
					
				// set spring connector position
				component.getSpringConnectorXProperty().set(centerX + xDiff);
				component.getSpringConnectorYProperty().set(centerY + yDiff);
			}			
		}
		else
		{
			// init angles
			this.angles = new LinkedList<Double>();
			
			double startAngle = 0;
			
			// calculate the angle to start the circle layout with
			if(keepPositionOfFirstCircleComponent && layoutedComponents.size() > 0)
			{
				startAngle = calculateAngleWithinCircleGroup(layoutedComponents.get(0));
			}
			
			double divAngle = 2 * Math.PI / layoutedComponents.size();
			double curAngle = startAngle;
			
			int count = 0;
			for(VisualComponent component : layoutedComponents) {
				
				this.angles.add(count++, curAngle);
				
				double xDiff = radius * Math.cos(curAngle);
				double yDiff = radius * Math.sin(curAngle);
						
				component.getSpringConnectorXProperty().set(centerX + xDiff);
				component.getSpringConnectorYProperty().set(centerY + yDiff);
				
				curAngle += divAngle; 
				
				if(Math.toDegrees(curAngle) > 360)
				{
					curAngle = Math.toRadians((Math.toDegrees(curAngle) - 360));
				}
			}
		}
	}
	
	/**
	 * Calculates the angle between the central component and a given circle component within this circle visual group.
	 * @param circle Component the given circle component of the circle visual group 
	 * @return the calculated angle in radians
	 */
	private double calculateAngleWithinCircleGroup(VisualComponent circleComponent)
	{
		if(circleComponent == null)
		{
			return 0;
		}
		
		double xDiff = center.getSpringConnectorXProperty().get() - circleComponent.getSpringConnectorXProperty().get();
		
		// *** correct value if xDiff greater than radius (can happen caused by rounding issues) ***********************
		// (needed to ensure that acos is called only on values between -1 and 1)
		if(xDiff > 0 && xDiff > radius)
		{
			xDiff = radius;
		}
		else if(xDiff < 0 && xDiff < (radius * (-1)))
		{
			xDiff = radius * (-1);
		}
		// *************************************************************************************************************
		
		double calculatedAngle = Math.acos(xDiff/radius);
		
		// *** cover the whole 360 degrees instead of only the range of acosinus (0 to 180 degrees)  ********************
		{
			calculatedAngle = Math.toRadians(360 - Math.toDegrees(calculatedAngle));
		}
		
		calculatedAngle = Math.toRadians(Math.toDegrees(calculatedAngle) + 180);
		
		//take care that the anle is not bigger than 360 degrees
		if(Math.toDegrees(calculatedAngle) > 360)
		{
			calculatedAngle = Math.toRadians((Math.toDegrees(calculatedAngle) - 360));
		}
		// **************************************************************************************************************
	
		
//		// *** alternative way - does not work correctly *******************************************************************
//		double yDiff = center.getSpringConnectorYProperty().get() - circleComponent.getSpringConnectorYProperty().get();
//		if(yDiff > 0)
//		{
//			calculatedAngle = Math.toRadians(Math.toDegrees(calculatedAngle) + 180);
//		}
//		else
//		{
//			if(xDiff > 0)
//			{
//				calculatedAngle = Math.toRadians(Math.toDegrees(calculatedAngle) + 90);
//			}
//			else
//			{
//				calculatedAngle = Math.toRadians(Math.toDegrees(calculatedAngle) - 90);
//			}
//		}
//		// ****************************************************************************************
		
//		System.out.println("xDiff: " + xDiff);
//		System.out.println("yDiff: " + yDiff);
//		System.out.println("radius: " + radius);
//		System.out.println("xDiff/radius: " + xDiff/radius);
//		System.out.println("acos: " + Math.toDegrees(Math.acos(xDiff/radius)));
//		System.out.println("asin: " + Math.toDegrees(Math.asin(yDiff/radius)));
//		System.out.println("calculated angle: " + calculatedAngle);
//		System.out.println("---------------------------------------------------------");
		
		return calculatedAngle;
	}
	
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.visualitems.VisualGroup#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		// disconnect all springs
		for(VisualSpringConnection connection : springs) {
			connection.disconnect();
		}
	}

	/**
	 * Returns the spring connections used in this group to keep the connection between the components.
	 * 
	 * @return All spring connections used in this group.
	 */
	public List<VisualSpringConnection> getSprings() {
		return springs;
	}

	/**
	 * Returns the component used as center of the layout.
	 * 
	 * @return The center component.
	 */
	protected VisualComponent getCenterComponent() {
		return center;
	}
	/**
	 * Sets the radius of the layout. This changes the length of all springs maintaining the layout.
	 * 
	 * @param newRadius The new radius of the layout
	 */
	public void setRadius(double newRadius) {
		radius = newRadius;
		// change all springs
		for(VisualSpringConnection spring : springs) {
			spring.setRealLength(newRadius);
		}
	}

	/**
	 * Returns the current radius of the layout.
	 * 
	 * @return The current layout radius.
	 */
	public double getRadius() {
		return radius;
	}

	/**
	 * Gets the VisualGroup that contains all created springs of this CircleVisualGroup.
	 * @return the springGroup
	 */
	public VisualGroup getSpringGroup() {
		return springGroup;
	}

	/**
	 * Sets the VisualGroup that contains all created springs of this CircleVisualGroup.
	 * @param springGroup the springGroup to set
	 */
	public void setSpringGroup(VisualGroup springGroup) {
		this.springGroup = springGroup;
	}
}
