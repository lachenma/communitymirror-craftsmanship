package org.sociotech.communitymirror.processing;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentation;
import SimpleOpenNI.SimpleOpenNI;
import javafx.concurrent.Task;
import processing.core.PImage;
import processing.core.PMatrix3D;
import processing.core.PVector;

public class KinectV1UserUpdator extends UserUpdator {
	private SimpleOpenNI context;
	private int[] userMap;
	@SuppressWarnings("unused")
	private PImage userImage;

	public KinectV1UserUpdator(VisualUserRepresentation visualUserRepresentation, SimpleOpenNI context, int[] userMap) {
		super(visualUserRepresentation);
		this.context = context;
		this.userMap = userMap;
		
		this.updateUser();
	}
	
	private void updateUser(){
		ExecutorService exec = Executors.newSingleThreadExecutor();
		exec.submit(new Task<Void>() {
			protected Void call() {
				updateTrackedUser();
				return null;
			}
		});
	}

	protected void updateTrackedUser() {
		context.startTrackingSkeleton(userId);
		
		boolean isTracked = context.isTrackingSkeleton(userId);

		float[] position1 = null;
		float[] position2 = null;
		float[] orientation = null;

		if (isTracked) {
			// get position
			PVector positionVector = new PVector();
			// get position of the torso of the user
			context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_TORSO,
					positionVector);
			position1 = new float[] { positionVector.x, positionVector.y,
					positionVector.z };

			// get orientation
			PMatrix3D orientationMatrix = new PMatrix3D();
			context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_HEAD,
					positionVector);
			position2 = new float[] { positionVector.x, positionVector.y,
					positionVector.z };
			context.getJointOrientationSkeleton(userId, SimpleOpenNI.SKEL_HEAD,
					orientationMatrix);
			orientation = new float[] { orientationMatrix.determinant() };
			// TODO: check what to do with joint orientation
		}
		
		// update user
		CommunityMirror.getUserController().updateUser(userId, true, position1, position2, orientation, positionInUserImage, position2InUserImage);
	}

	/**
	 * Creates a new PImage for a given user. The image is created as color
	 * image if createRGB is true and else as silhouette image.
	 * 
	 * @param userId
	 *            the userId of the user for which to create the image
	 * @param useColorImage
	 *            boolean that tells if an colored image should be created
	 * 
	 * @return the new created PImage for the user
	 */
	protected PImage createUserPImage() {
		// decide which image should be created
		boolean useColorImage = visualUserRepresentation.getCurrentState().getImageCreator().isRGBImage();
		
		// update the SimpleOpenNI object
		context.update();

		// save user information in user map
		context.userMap(userMap);

		// get image to edit
		PImage imageToReturn = useColorImage ? context.rgbImage().get() : context
				.userImage().get();
		imageToReturn.loadPixels();

		// remove surrounding
		int width = imageToReturn.width;
		int height = imageToReturn.height;
		int colorXmin = width;
		int colorYmin = height;
		int colorXmax = 0;
		int colorYmax = 0;

		// go through all rows
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int i = y * width + x;
				// go through all pixels within row
				if (userMap[i] == userId+1) {
					if (colorXmin > x) {
						colorXmin = x;
					}
					if (colorXmax < x) {
						colorXmax = x;
					}
					if (colorYmin > y) {
						colorYmin = y;
					}
					if (colorYmax < y) {
						colorYmax = y;
					}
				} else {
					// make pixels transparent
					imageToReturn.pixels[i] = 0x00FFFFFF;
				}
			}
		}
		
		//TODO: calculate joint position in user image
		positionInUserImage[0] = 0;
		positionInUserImage[1] = 0;
		position2InUserImage[0] = 0;
		position2InUserImage[1] = 0;
		
		// take only area with user representation out of the whole image
		int w = colorXmax - colorXmin;
		int h = colorYmax - colorYmin;
		imageToReturn = imageToReturn.get(colorXmin, colorYmin, w, h);

		// TODO ensure that image is not scaled if it gets wider or higher e.g.
		// because the user has moved his arms or legs

		imageToReturn.updatePixels();
		imageToReturn.loadPixels();

		return imageToReturn;
	}
}
