package org.sociotech.communitymirror.processing;

import java.awt.image.BufferedImage;

import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentation;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import processing.core.PImage;

public abstract class UserUpdator{
	protected VisualUserRepresentation visualUserRepresentation;
	protected int userId;
	protected float[] positionInUserImage;
	protected float[] position2InUserImage;
	
	public UserUpdator(VisualUserRepresentation visualUserRepresentation) {
		super();
		this.visualUserRepresentation = visualUserRepresentation;
		this.userId = visualUserRepresentation.getUser().getUserId() - 1;
		this.positionInUserImage = new float[]{0, 0};
		this.position2InUserImage = new float[]{0, 0};
	}
	
	protected void updateNotTrackedUser() {
		CommunityMirror.getUserController().updateUser(userId, false, null, null, null, null, null);
	}
	
	/**
	 * Creates the next representation image for a visual user representation
	 * taking into account the current state of the representation.
	 * 
	 * @param visualUserRepresentation
	 * @param pixels
	 * @param w
	 * @param h
	 * 
	 * @return the new created representation image for the visual user
	 *         representation
	 */
	protected void createNextVisualUserRepresentationImage(VisualUserRepresentation visualUserRepresentation,
			int[] pixels, int w, int h) {

		// convert pixels into javafx image
		Image javaFXImage = createJavaFXImage(pixels, w, h);

		// create representation image according to the user representation's
		// current state

		Image nextRepresentationImage = visualUserRepresentation.getCurrentState().getImageCreator()
				.createRepresentatonImage(javaFXImage);

		// save next representation image
		visualUserRepresentation.setNextRepresentationImage(nextRepresentationImage);
	}

	/**
	 * Creates a javafx image out of a pixels array
	 * 
	 * @param pixels
	 * @param w
	 * @param h
	 * 
	 * @return the new created javafx Image
	 */
	private Image createJavaFXImage(int[] pixels, int w, int h) {
		if (pixels != null) {
			BufferedImage bufferedImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

			bufferedImage.setRGB(0, 0, w, h, pixels, 0, w);
			return SwingFXUtils.toFXImage(bufferedImage, null);
		}
		return null;
	}

	protected PImage createUserPImage(PImage bodyTrackImg) {
		// TODO Auto-generated method stub
		return null;
	}

}
