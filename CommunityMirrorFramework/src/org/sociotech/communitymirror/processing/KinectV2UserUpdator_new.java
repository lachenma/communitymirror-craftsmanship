package org.sociotech.communitymirror.processing;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.sociotech.communityinteraction.activitylogger.UserActivityLogger;
import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentation;

import KinectPV2.FaceData;
import KinectPV2.KJoint;
import KinectPV2.KQuaternion;
import KinectPV2.KSkeleton;
import KinectPV2.KinectPV2;
import javafx.concurrent.Task;
import processing.core.PImage;

@SuppressWarnings("unused")
public class KinectV2UserUpdator_new extends UserUpdator {
	private PImage userImage;
	private int userIndexColor;
	private int oldDepthWidth;
	private int oldDepthHeight;
	private int newDepthWidth;
	private int newDepthHeight;
	private float jointPositionX_InUserImage;
	private float jointPositionY_InUserImage;
	private float jointPositionX2_InUserImage;
	private float jointPositionY2_InUserImage;
	private int minX;
	private int minY;
	private int maxX;
	private int maxY;

	public KinectV2UserUpdator_new(VisualUserRepresentation visualUserRepresentation, PImage bodyTrackImg,
			PImage colorImage, ArrayList<KSkeleton> skeletonArrayDepth, ArrayList<KSkeleton> skeletonArrayColor,
			ArrayList<KSkeleton> skeletons3d) {
		super(visualUserRepresentation);
		
		this.updateUser(bodyTrackImg, colorImage, skeletonArrayColor, skeletonArrayDepth, skeletons3d);
	}
	
	private void updateUser(PImage bodyTrackImg, PImage colorImage, ArrayList<KSkeleton> skeletonArrayColor, ArrayList<KSkeleton> skeletonArrayDepth, ArrayList<KSkeleton> skeletons3d) {

		if (bodyTrackImg != null) {
			ExecutorService exec = Executors.newSingleThreadExecutor();
			
			// (1) create user PImage
			exec.submit(new Task<Void>() {
				protected Void call() {
					//long timeBeforeUpdate = System.nanoTime();
					userImage  = createUserPImage(bodyTrackImg, colorImage, skeletonArrayColor, skeletonArrayDepth);
//					long timeAfterUpdate = System.nanoTime();
//					System.out.println("creationTime: " + (timeAfterUpdate - timeBeforeUpdate));
					return null;
				}
			});
			
			// (2) create JavaFX image
			exec.submit(new Task<Void>() {
				protected Void call() {
					createNextVisualUserRepresentationImage(visualUserRepresentation, userImage.pixels, userImage.width,
							userImage.height);
					return null;
				}
			});

			// (3) update visibility, position, positionInUserImage and orientation of tracked user
			exec.submit(new Task<Void>() {
				protected Void call() {
					updateTrackedUser(skeletons3d);
					return null;
				}
			});
		} else {
			// update not tracked user
			updateNotTrackedUser();
		}
	}
	
	/**
	 * Creates a new PImage for a given user. The image is created as color
	 * image if createRGB is true and else as silhouette image.
	 * 
	 * @return the new created PImage for the user
	 */
	protected PImage createUserPImage(PImage bodyTrackImg, PImage colorImage, ArrayList<KSkeleton> skeletonArrayColor, ArrayList<KSkeleton> skeletonArrayDepth) {
		// (1) resize body track image to map color image
		PImage scaledBodyTrackImg = createScaledBodyTrackImage(bodyTrackImg, colorImage);
		
		// (2) get joint positions in depth image
		PImage userImage = cutUserImage(colorImage, skeletonArrayColor, skeletonArrayDepth);

		// (3) make other pixels transparent and find min/max - values
		makeOtherPixelsTransparent(scaledBodyTrackImg, userImage);

		// (4) cut user area from color image
		cutUserArea(userImage);
		
		return userImage;
	}

	protected void updateTrackedUser(ArrayList<KSkeleton> skeletons3d) {
		// get position and orientation
		float[] position1 = null;
		float[] position2 = null;
		float[] orientation = null;
		if (skeletons3d.size() > userId) {
			KSkeleton skeleton = skeletons3d.get(userId);
			KJoint[] joints = skeleton.getJoints();
			
			// get position of spine mid
			KJoint spineMid = joints[KinectPV2.JointType_SpineMid];
			// convert position values from m to mm
			position1 = new float[] { spineMid.getX() * 1000, spineMid.getY() * 1000, spineMid.getZ() * 1000 };
			
			// get position of spine base
			KJoint spineBase = joints[KinectPV2.JointType_SpineBase];
			// convert position values from m to mm
			position2 = new float[] { spineBase.getX() * 1000, spineBase.getY() * 1000, spineBase.getZ() * 1000 };
			
			// TODO: check if this kind of orientation is really usefull
			orientation = new float[] { 0, 0, 0, 0 };
			
			// get further positions for logging
			KJoint neck = joints[KinectPV2.JointType_Neck];
			KJoint head = joints[KinectPV2.JointType_Head];
			KJoint spineShoulder = joints[KinectPV2.JointType_SpineShoulder];
			KJoint shoulderLeft = joints[KinectPV2.JointType_ShoulderLeft];
			KJoint shoulderRight = joints[KinectPV2.JointType_ShoulderRight];
			KJoint hipLeft = joints[KinectPV2.JointType_HipLeft];
			KJoint hipRight = joints[KinectPV2.JointType_HipRight];
			
			// log kinect data
			UserActivityLogger userLogger = CommunityMirror.getUserActivityLogger();
			userLogger.logActivity(
					userLogger.detail("TimeStamp", new Timestamp(Calendar.getInstance().getTime().getTime()).toString()),
					userLogger.detail("LoggingType", "KinectData"),
					userLogger.detail("ScreenPosition", CommunityMirror.getConfiguration().getInt("screenPosition", 0)),
					userLogger.detail("UserID", userId),
					
					userLogger.detail("HeadPositionX", head.getX() * 1000),
					userLogger.detail("HeadPositionY", head.getY() * 1000),
					userLogger.detail("HeadPositionZ", head.getZ() * 1000),
					
					userLogger.detail("NeckPositionX", neck.getX() * 1000),
					userLogger.detail("NeckPositionY", neck.getY() * 1000),
					userLogger.detail("NeckPositionZ", neck.getZ() * 1000),
					
					userLogger.detail("ShoulderLeftPositionX", shoulderLeft.getX() * 1000),
					userLogger.detail("ShoulderLeftPositionY", shoulderLeft.getY() * 1000),
					userLogger.detail("ShoulderLeftPositionZ", shoulderLeft.getZ() * 1000),
					
					userLogger.detail("ShoulderRightPositionX", shoulderRight.getX() * 1000),
					userLogger.detail("ShoulderRightPositionY", shoulderRight.getY() * 1000),
					userLogger.detail("ShoulderRightPositionZ", shoulderRight.getZ() * 1000),
					
					userLogger.detail("SpineShoulderPositionX", spineShoulder.getX() * 1000),
					userLogger.detail("SpineShoulderPositionY", spineShoulder.getY() * 1000),
					userLogger.detail("SpineShoulderPositionZ", spineShoulder.getZ() * 1000),
					
					userLogger.detail("SpineMidPositionX", spineMid.getX() * 1000),
					userLogger.detail("SpineMidPositionY", spineMid.getY() * 1000),
					userLogger.detail("SpineMidPositionZ", spineMid.getZ() * 1000),
					
					userLogger.detail("SpineBasePositionX", spineBase.getX() * 1000),
					userLogger.detail("SpineBasePositionY", spineBase.getY() * 1000),
					userLogger.detail("SpineBasePositionZ", spineBase.getZ() * 1000),
					
					userLogger.detail("HipLeftPositionX", hipLeft.getX() * 1000),
					userLogger.detail("HipLeftPositionY", hipLeft.getY() * 1000),
					userLogger.detail("HipLeftPositionZ", hipLeft.getZ() * 1000),
					
					userLogger.detail("HipRightPositionX", hipRight.getX() * 1000),
					userLogger.detail("HipRightPositionY", hipRight.getY() * 1000),
					userLogger.detail("HipRightPositionZ", hipRight.getZ() * 1000));
		}
		// update user
		CommunityMirror.getUserController().updateUser(userId, true, position1, position2, orientation, positionInUserImage, position2InUserImage);
	}
	
	private PImage createScaledBodyTrackImage(PImage bodyTrackImg, PImage colorImage) {
		// (1) resize body track image to map color image
		double verticalAngleOfViewColor = 53.8;
		double verticalAngleOfViewDepth = 60;
		double adaptedColorImageHeight = colorImage.height * verticalAngleOfViewDepth/verticalAngleOfViewColor;
		double resizeFactor = (double) adaptedColorImageHeight / (double) bodyTrackImg.height;
		oldDepthWidth = bodyTrackImg.width; //512
		oldDepthHeight = bodyTrackImg.height; //424
		newDepthWidth = (int) (bodyTrackImg.width * resizeFactor);
		newDepthHeight = (int) (bodyTrackImg.height * resizeFactor);
		
		//long timeBeforeScaling = System.currentTimeMillis();
		
//		scaledBodyTrackImg = new PImage(newDepthWidth, newDepthHeight);
//		scaledBodyTrackImg.copy(bodyTrackImg, 0, 0, bodyTrackImg.width, bodyTrackImg.height, 0, 0, newDepthWidth,
//				newDepthHeight);
		
		PImage scaledBodyTrackImg = scaleImage(bodyTrackImg, newDepthWidth, newDepthHeight);
		scaledBodyTrackImg.updatePixels();
		
//		long timeAfterScaling = System.currentTimeMillis();
//		System.out.println("scalingTime: " + (timeAfterScaling - timeBeforeScaling));
		
		scaledBodyTrackImg.loadPixels();
		
		return scaledBodyTrackImg;
	}
	
	PImage scaleImage(PImage original, int newWidth, int newHeight)
	{       
	  PImage output = new PImage(newWidth, newHeight);
	  original.loadPixels();
	  output.loadPixels();
	 
	  // YD compensates for the x loop by subtracting the width back out
	  int YD = (original.height / newHeight) * original.width - original.width;
	  int YR = original.height % newHeight;
	  int XD = original.width / newWidth;
	  int XR = original.width % newWidth;       
	  int outOffset= 0;
	  int inOffset=  0;

	  for (int y= newHeight, YE= 0; y > 0; y--) {           
	    for (int x= newWidth, XE= 0; x > 0; x--) {
	      output.pixels[outOffset++]= original.pixels[inOffset];
	      inOffset+=XD;
	      XE+=XR;
	      if (XE >= newWidth) {
	        XE-= newWidth;
	        inOffset++;
	      }
	    }           
	    inOffset+= YD;
	    YE+= YR;
	    if (YE >= newHeight) {
	      YE -= newHeight;    
	      inOffset+=original.width;
	    }
	  }              
	  return output;
	}
	
	private PImage cutUserImage(PImage colorImage, ArrayList<KSkeleton> skeletonArrayColor, ArrayList<KSkeleton> skeletonArrayDepth){
		// (2a) get joint positions in depth image
		userIndexColor = 0;
		float jointDepthX = 0;
		float jointDepthY = 0;
		if (skeletonArrayDepth.size() > userId) {
			KSkeleton skeletonDepth = (KSkeleton) skeletonArrayDepth.get(userId);
			if (skeletonDepth.isTracked()) {
				// set user index color
				userIndexColor  = skeletonDepth.getIndexColor();
				
				// get joints of user
				KJoint[] joints = skeletonDepth.getJoints();
	
				// get X-position
				jointDepthX = joints[KinectPV2.JointType_SpineMid].getX();
				// find joint position in scaled depth image
				jointDepthX = jointDepthX / oldDepthWidth * newDepthWidth;
				
				// get y-position
				jointDepthY = joints[KinectPV2.JointType_SpineMid].getY();
				// find joint position in scaled depth image
				jointDepthY = jointDepthY / oldDepthHeight * newDepthHeight;
			}
		}
		
		// (2b) get joint positions in color image
		float jointColorX = 0;
		float jointColorY = 0;
		float jointColorX2 = 0;
		float jointColorY2 = 0;
		float shiftDepthX = 0;
		float shiftDepthY = 0;
		if (skeletonArrayColor.size() > userId) {
			KSkeleton skeletonColor = (KSkeleton) skeletonArrayColor.get(userId);
			if (skeletonColor.isTracked()) {
				KJoint[] joints = skeletonColor.getJoints();
				jointColorX = joints[KinectPV2.JointType_SpineMid].getX();
				jointColorY = joints[KinectPV2.JointType_SpineMid].getY();
				// System.out.println("jointColorY: " + jointColorY);

				jointColorX2 = joints[KinectPV2.JointType_SpineBase].getX();
				jointColorY2 = joints[KinectPV2.JointType_SpineBase].getY();
				// System.out.println("jointColorY2: " + jointColorY2);
			}
		}

		// (2c) calculate shift from color to depth
		shiftDepthX = (jointColorX - jointDepthX);
		shiftDepthY = (jointColorY - jointDepthY);
		
		// (2d) cut user image to size of body track image
		PImage userImage = colorImage;
		userImage = userImage.get((int) shiftDepthX, (int) shiftDepthY, newDepthWidth, userImage.height);
		userImage.loadPixels();

		// (2e) calculate joint position (midSpine) in user image
		jointPositionX_InUserImage = jointColorX - shiftDepthX;
		jointPositionY_InUserImage = jointColorY - shiftDepthY;

		// (2f) calculate joint position (midSpine) in user image
		jointPositionX2_InUserImage = jointColorX2 - shiftDepthX;
		jointPositionY2_InUserImage = jointColorY2 - shiftDepthY;
		
		return userImage;
	}
	
	private void makeOtherPixelsTransparent(PImage scaledBodyTrackImg, PImage userImage){
		// (3) make other pixels transparent and find min/max - values
		minX = userImage.width;
		minY = userImage.height;
		maxX = 0;
		maxY = 0;
		// go line by line through all pixels of body track image
		for (int x = 0; x < newDepthWidth; x++) {
			for (int y = 0; y < newDepthHeight; y++) {
				int index = y * newDepthWidth + x;
				int pixelColor = scaledBodyTrackImg.pixels[index];
				int i = y * userImage.width + x;
				// if pixel belongs to user
				if (pixelColor != 0) {
					// update minX, minY and maxX, maxY
					minX = x < minX ? x : minX;
					minY = y < minY ? y : minY;
					maxX = x > maxX ? x : maxX;
					maxY = y > maxY ? y : maxY;
					// copy pixels from scaledBodyTRackImg to userImage
					boolean useColorImage = visualUserRepresentation.getCurrentState().getImageCreator().isRGBImage();
					if(!useColorImage){
						userImage.pixels[i] = userIndexColor;
					}
				} else {
					// make other pixels in color image transparent
					if(i<userImage.pixels.length){
						userImage.pixels[i] = 0x00FFFFFF;
					}
				}
			}
		}
	}
	
	private void cutUserArea(PImage userImage) {
		// (4) cut user area from color image
		userImage.updatePixels();
		int w = Math.max((Math.min(userImage.width, maxX) - minX), 0);
		int h = Math.max((Math.min(userImage.height, maxY) - minY), 0);
		userImage = userImage.get(minX, minY, w, h);

		// calculate and store joint position in cut user area image
		this.positionInUserImage[0] = jointPositionX_InUserImage - minX;
		this.positionInUserImage[1] = jointPositionY_InUserImage - minY;

		// calculate and store joint position in cut user area image
		this.position2InUserImage[0] = jointPositionX2_InUserImage - minX;
		this.position2InUserImage[1] = jointPositionY2_InUserImage - minY;
		
		// load pixels
		userImage.loadPixels();
	}
}
