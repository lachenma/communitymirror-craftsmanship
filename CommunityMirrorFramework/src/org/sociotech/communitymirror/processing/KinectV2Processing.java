package org.sociotech.communitymirror.processing;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.sociotech.communitymirror.CommunityMirror;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentation;

import KinectPV2.KinectPV2;
import processing.core.PApplet;
import processing.core.PImage;

/**
 * Class for receiving and using data from windows kinect version 1 using
 * processing libraries.
 *
 * @author loesch
 *
 */
public class KinectV2Processing extends KinectProcessing {

	KinectPV2 kinect;
	long[] loggingTimes = new long[6]; 

	public KinectV2Processing(List<VisualUserRepresentation> visualuserRepresentations) {
		super(visualuserRepresentations);
		long time = System.currentTimeMillis();
		for(int i=0; i<visualuserRepresentations.size(); i++){
			loggingTimes[i] = time;
		}
		setup();
	}

	public void setup() {
		// kinect = new KinectPV2(this);
		kinect = new KinectPV2(new PApplet());

		kinect.enableColorImg(true);
		kinect.enableBodyTrackImg(true);

		kinect.enableSkeleton3DMap(true);
		kinect.enableSkeletonColorMap(true);
		kinect.enableSkeletonDepthMap(true);

		kinect.init();

		startUpdateService();
	}

	private void startUpdateService(){
		ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
        exec.scheduleAtFixedRate(new Runnable(){
            @Override
            public void run(){;
                updateUsers();
            }
        }, 0, 100, TimeUnit.MILLISECONDS);
	}

	private void updateUsers() {
		// look for tracked users
		@SuppressWarnings("unchecked")
		ArrayList<PImage> bodyTrackList = kinect.getBodyTrackUser();
		// update all tracked users
		for (int userId = 0; userId < bodyTrackList.size(); userId++) {
			// get body track image of user
			updatTrackedUser(userId, bodyTrackList.get(userId));
		}
		// update all not tracked users
		for (int userId = bodyTrackList.size(); userId < visualuserRepresentations.size(); userId++) {
			//UpdateUserTask task = new UpdateUserTask(userId);
			//task.run();
			CommunityMirror.getUserController().updateUser(userId, false, null, null, null, null, null);
		}
	}

	private void updatTrackedUser(int userId, PImage bodyTrackImg) {
		// create task to create user image
		new KinectV2UserUpdator(visualuserRepresentations.get(userId), bodyTrackImg, kinect.getColorImage(), kinect, loggingTimes);
	}
}
