package org.sociotech.communitymirror.processing;

import java.util.List;

import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentation;

/**
 * Abstract super class of all classes that receive and use data coming from
 * windows kinect device using processing library.
 *
 * @author loesch
 *
 */
public abstract class KinectProcessing{

	/**
	 *
	 */
	@SuppressWarnings("unused")
	private static final long serialVersionUID = -104938754139808516L;
	protected List<VisualUserRepresentation> visualuserRepresentations;

	public KinectProcessing(List<VisualUserRepresentation> visualuserRepresentations) {
		this.visualuserRepresentations = visualuserRepresentations;
	}
}
