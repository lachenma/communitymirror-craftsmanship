
package org.sociotech.communitymirror;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Rectangle2D;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Duration;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.ConfigurationBuilder;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.combined.CombinedConfigurationBuilder;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.osgi.service.log.LogService;
import org.sociotech.communityinteraction.activitylogger.UserActivityLogger;
import org.sociotech.communityinteraction.activitylogger.UserActivityLoggerFactory;
import org.sociotech.communityinteraction.behaviorfactories.DirectBehaviorFactory;
import org.sociotech.communityinteraction.behaviorfactories.InteractionBehaviorFactory;
import org.sociotech.communityinteraction.behaviorfactories.MouseToXBehaviorFactory;
import org.sociotech.communityinteraction.eventcreators.CIEventCreator;
import org.sociotech.communityinteraction.eventcreators.CommunicationEventCreator;
import org.sociotech.communityinteraction.eventcreators.FXEventCreator;
import org.sociotech.communityinteraction.eventcreators.GWEventCreator;
import org.sociotech.communityinteraction.eventcreators.GWReuseEventCreator;
import org.sociotech.communityinteraction.eventcreators.IntersectionEventCreator;
import org.sociotech.communityinteraction.eventcreators.KinectFrameEventCreator;
import org.sociotech.communityinteraction.eventcreators.TUIOFXEventCreator;
import org.sociotech.communityinteraction.visualizer.CIEventVisualizer;
import org.sociotech.communitymashup.data.DataSet;
import org.sociotech.communitymashup.framework.java.MashupConnector;
import org.sociotech.communitymirror.communication.ClientToSend;
import org.sociotech.communitymirror.communication.ServerToReceive;
import org.sociotech.communitymirror.context.ContextManager;
import org.sociotech.communitymirror.controllers.Controller;
import org.sociotech.communitymirror.controllers.user.UserController;
import org.sociotech.communitymirror.controllers.userRepresentation.VisualUserRepresentationController;
import org.sociotech.communitymirror.dataconnection.MashupLoggerBridge;
import org.sociotech.communitymirror.processing.KinectV1Processing;
import org.sociotech.communitymirror.processing.KinectV2Processing;
import org.sociotech.communitymirror.view.FXView;
import org.sociotech.communitymirror.view.ViewFactory;
import org.sociotech.communitymirror.view.flow.FlowView;
import org.sociotech.communitymirror.visualitems.user.User;
import org.sociotech.communitymirror.visualitems.user.UserFactory;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentation;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentationFactory;
import org.sociotech.communitymirror.visualstates.ComponentState;
import org.tuiofx.TuioFX;

/**
 * The application class of the CommunityMirror. This one can be started to open
 * up the CommunityMirror. It handles the connection to the CommunityMashup and
 * loads the overal mirror configuration.
 *
 * @author Peter Lachenmaier, Martin Burkhard, Michael Koch
 *
 */
public class CommunityMirror extends Application {

    /**
     * The title of the main application window
     */
    private static final String MAIN_WINDOW_TITLE = "CommunityMirror";

    /**
     * Key for the system property to specify a user configuration
     */
    private static final String USER_CONFIGURATION_SYSTEM_PROPERTY_KEY = "communitymirror.configuration.user.file";

    /**
     * Default user configuration name that will be set as system property
     * {@link org.sociotech.communitymirror.CommunityMirror#USER_CONFIGURATION_SYSTEM_PROPERTY_KEY}
     * if not specified by the user.
     */
    private static final String USER_CONFIGURATION_DEFAULT = "mirror.properties";

    /**
     * The CommunityMirror configuration.
     */
    public static Configuration configuration;

    /**
     * The used CommunityMashup data
     */
    private DataSet dataSet;

    /**
     * The log4j logger reference.
     */
    static private final Logger logger = LogManager.getLogger(CommunityMirror.class);

    /**
     * The logger for user activity
     */
    private static UserActivityLogger userActivityLogger;

    /**
     * The configuration path for specifying the user activity logger to be
     * used.
     */
    private final String userActivityLoggerConfigurationPath = "logger.userActivityLogger.class";

    /**
     * The context manager
     */
    protected static ContextManager contextManager;

    // Main Loop
    private Timeline eventLoop;

    // FPS counter
    private double framesPerSecond = 0.0;
    private long oldTime = 0;
    private long nanoTime;
    private long timeDiff;
    private final static int SMOOTH_FPS_VALUES = 10;
    private double fpsValues[] = new double[CommunityMirror.SMOOTH_FPS_VALUES];
    private int currentFPSindex = 0;
    private final static double INTENDED_FPS = 60.0;
    private boolean skipFrame = false;
    private double waitTimeNano = 1000000000.0 / CommunityMirror.INTENDED_FPS / 10.0;

    /**
     * The javafx stage.
     */
    private Window stage;

    /**
     * List of all active views
     */
    private List<FXView> viewList;

    /**
     * The main view
     */
    public FXView mainView;

    /**
     * Toggle for path line. Switch with key "L"
     */
    private boolean showPathLine = false;

    /**
     * The single interaction behavior factory used for the creation of all
     * interaction behaviors
     */
    private static InteractionBehaviorFactory interactionBehaviorFactory;

    /**
     * The creator of interaction events.
     */
    private static CIEventCreator interactionEventCreator;

    /**
     * The creator of message events.
     */
    private static CommunicationEventCreator communicationEventCreator;

    /**
     * The creator of intersection events.
     *
     * @author evaloesch
     */
    private static IntersectionEventCreator intersectionEventCreator;

    /**
     * The creator of kinect frame events.
     *
     * @author evaloesch
     */
    private static KinectFrameEventCreator kinectFrameEventCreator;

    /**
     * The list of all user representations;
     *
     * @author evaloesch
     */
    private static List<VisualUserRepresentation> visualUserRepresentations;

    private static VisualUserRepresentationController<VisualUserRepresentation> visualUserRepresentationController;

    private static UserController userController;

    /**
     * List of all controllers that have to updated.
     *
     * @author evaloesch
     */
    private List<Controller> controllers;

    /**
     * Reference to the interaction event visualizer if set.
     */
    private CIEventVisualizer interactionEventVisualizer;

    /**
     * server to receive messages from the left side via sockets.
     */
    @SuppressWarnings("unused")
    private ServerToReceive serverToReceiveFromLeft;

    /**
     * server to receive messages from the left side via sockets.
     */
    @SuppressWarnings("unused")
    private ServerToReceive serverToReceiveFromRight;

    /**
     * client to send messages via sockets to the left side.
     */
    private static ClientToSend clientToSendToLeft;

    /**
     * client to send messages via sockets to the right side.
     */
    private static ClientToSend clientToSendToRight;

    /**
     * The bounds of the main view of the community mirror.
     */
    private static Rectangle2D viewBounds;

    public static void main(String[] args) {

        // do static initialization (that has to be done before JavaFX
        // application is started
        CommunityMirror.doStaticInitialization(args);

        // start JavaFX application
        Application.launch(args);
    }

    /**
     * do static initialization - to be called from main() methods in derived
     * start classes
     */
    protected static void doStaticInitialization(String[] args) {
        // interpret possible command line parameters
        // important to do this before loading the configuration
        // cause first parameter is the user configuration
        // file name
        if (args != null && args.length > 0) {
            // set first parame as system property for user configuration
            System.setProperty(CommunityMirror.USER_CONFIGURATION_SYSTEM_PROPERTY_KEY, args[0]);
        }

        // load configuration
        CommunityMirror.loadConfiguration();

        // enable TUIOFX - has to be done before JavaFX application is started
        String eventCreatorConfig = CommunityMirror.configuration.getString("interaction.eventcreator");
        if (eventCreatorConfig.equalsIgnoreCase("tuiofx")) {
            TuioFX.enableJavaFXTouchProperties();
        }
    }

    /**
     * Loads the mirror configuration from the default location and sets the
     * local reference.
     */
    private static void loadConfiguration() {
        // set configuration to mirror.properies if not previously set by user
        if (System.getProperties().getProperty(CommunityMirror.USER_CONFIGURATION_SYSTEM_PROPERTY_KEY) == null) {
            CommunityMirror.logger
                    .debug("Setting system property " + CommunityMirror.USER_CONFIGURATION_SYSTEM_PROPERTY_KEY
                            + " to default " + CommunityMirror.USER_CONFIGURATION_DEFAULT);
            System.setProperty(CommunityMirror.USER_CONFIGURATION_SYSTEM_PROPERTY_KEY,
                    CommunityMirror.USER_CONFIGURATION_DEFAULT);
        }

        // set the path to the initial configuration file
        String path = "configuration/main_configuration.xml";

        // create a configuration builder
        org.apache.commons.configuration2.builder.fluent.Parameters params = new org.apache.commons.configuration2.builder.fluent.Parameters();
        CombinedConfigurationBuilder builder = new CombinedConfigurationBuilder().configure(params.fileBased()
                .setURL(org.sociotech.communitymirror.CommunityMirror.class.getClassLoader().getResource(path)));

        // load configuration
        try {
            CommunityMirror.logger.info("loading configuration from "
                    + org.sociotech.communitymirror.CommunityMirror.class.getClassLoader().getResource(path));
            CommunityMirror.configuration = builder.getConfiguration();

            // Debug: print loaded configuration files
            Set<String> builders = builder.builderNames();
            CommunityMirror.logger.debug("builders getConfiguration(): " + builders);
            for (String s : builders) {
                ConfigurationBuilder<?> cb = builder.getNamedBuilder(s);
                if (cb instanceof FileBasedConfigurationBuilder) {
                    CommunityMirror.logger.debug(
                            "- " + s + ": " + ((FileBasedConfigurationBuilder<?>) cb).getFileHandler().getPath());
                }
            }
        } catch (ConfigurationException e) {
            CommunityMirror.logger.error("Failed loading configuration", e);
        }
    }

    @Override
    public void start(Stage stage) throws Exception {

        this.stage = stage;

        // initialize context manager
        CommunityMirror.contextManager = new ContextManager(this);

        // initialize connection to CommunityMashup
        if (CommunityMirror.configuration.getBoolean("mashup.useMashupData", true)) {
            // connect to CommunityMashup
            this.connectToCommunityMashup();
        }

        // manage screen setup
        this.manageScreenSetup();

        // Initialize UserActivityLogger
        UserActivityLoggerFactory userActivityFactory = new UserActivityLoggerFactory();
        CommunityMirror.userActivityLogger = userActivityFactory.createUserActivityLogger(
                CommunityMirror.configuration.getString(this.userActivityLoggerConfigurationPath));

        if (CommunityMirror.userActivityLogger == null) {
            CommunityMirror.logger.error("Could not create user activity logger for class "
                    + CommunityMirror.configuration.getString(this.userActivityLoggerConfigurationPath) + ". Use "
                    + this.userActivityLoggerConfigurationPath + " to specify it.");
            return;
        }

        // Init View
        // ViewPort viewPort = new ViewPort(stage);
        ViewFactory viewFactory = new ViewFactory(CommunityMirror.configuration, this.dataSet, stage);
        this.mainView = viewFactory.createView(CommunityMirror.configuration.getString("view.class"));

        if (this.mainView == null) {
            CommunityMirror.logger.error("Could not create view for class "
                    + CommunityMirror.configuration.getString("view.class") + ". Use view.class to specify it.");
            return;
        }

        // Show Stage
        stage.setScene(this.mainView.getFXScene());
        stage.setTitle(CommunityMirror.MAIN_WINDOW_TITLE);
        if (CommunityMirror.configuration.getBoolean("mirror.fullscreen")) {
            stage.setFullScreen(true);
        }

        stage.show();
        // set view bounds
        CommunityMirror.viewBounds = new Rectangle2D(0, 0, stage.getWidth(), stage.getHeight());

        // initialize interaction component
        this.initializeInteraction();

        // initialize communication component
        this.initializeCommunication();

        // initialize all controllers
        this.initializeControllers();

        this.mainView.init();

        // initialize keyboard events like pause toogle
        this.initializeKeyEvents();

        this.viewList = new ArrayList<>();
        this.viewList.add(this.mainView);

        // Start Event Loop
        this.initEventLoop();
        this.startEventLoop();

        CommunityMirror.logger.info("CommunityMirror started.");
    }

    /**
     * Initializes necessary controllers depending on the configuration.
     *
     * @author evaloesch
     */
    private void initializeControllers() {
        // create an empty list for the controllers
        this.controllers = new LinkedList<>();

        // check if kinect sensing is enabled
        if (CommunityMirror.configuration.getBoolean("sensing.kinectProcessingEnabled", false)) {
            // create all users
            List<User> allUsers = UserFactory.createAllUsers(6);
            // create user controller
            CommunityMirror.userController = new UserController(allUsers, this.mainView);
            // add user controller
            this.controllers.add(CommunityMirror.userController);

            // create all visual user representations
            CommunityMirror.visualUserRepresentations = VisualUserRepresentationFactory
                    .createAllVisualUserRepresentationsForUsers(allUsers);
            // initialize kinect processing
            this.initializeKinectProcessing();

            // create visual user representation controller
            CommunityMirror.visualUserRepresentationController = new VisualUserRepresentationController<>(
                    CommunityMirror.visualUserRepresentations);
            // add visual user representation controller
            this.controllers.add(CommunityMirror.visualUserRepresentationController);
        }
    }

    /**
     * Initializes the PApplet that uses a processing library to receive and use
     * the data coming from windows kinect device. Different implementations of
     * the PApplet are used for different versions (v1 or v2) of kinect device.
     */
    private void initializeKinectProcessing() {
        switch (CommunityMirror.configuration.getInt("sensing.kinectVersion", 1)) {
        case 1:
            // init processing for kinect v1
            new KinectV1Processing(CommunityMirror.visualUserRepresentations);
            break;
        case 2:
            // init processing for kinect v2
            new KinectV2Processing(CommunityMirror.visualUserRepresentations);
            break;
        }
    }

    /**
     * Initializes the communication component depending on the configuration.
     */
    private void initializeCommunication() {
        // check if communication is enabled
        if (CommunityMirror.configuration.getBoolean("interaction.communication", false)) {
            // create communication events creator
            CommunityMirror.communicationEventCreator = new CommunicationEventCreator();
            // register mainView for communication events at creator
            CommunityMirror.communicationEventCreator.registerEventConsumer(this.mainView);

            // get port to receive messages from left side
            int portToReceiveFromLeft = CommunityMirror.configuration.getInt("communication.portToReceiveFromLeft", -1);
            // create server socket to receive
            this.serverToReceiveFromLeft = (portToReceiveFromLeft < 0) ? null
                    : new ServerToReceive(portToReceiveFromLeft);

            // get port to receive messages from right side
            int portToReceiveFromRight = CommunityMirror.configuration.getInt("communication.portToReceiveFromRight",
                    -1);
            // create server socket to receive
            this.serverToReceiveFromRight = (portToReceiveFromRight < 0) ? null
                    : new ServerToReceive(portToReceiveFromRight);

            // get port and host to send messages from the left side,
            // "localhost" as fallback
            int portToSendToLeft = CommunityMirror.configuration.getInt("communication.portToSendToLeft", -1);
            String hostToSendToLeft = CommunityMirror.configuration.getString("communication.hostToSendToLeft",
                    "localhost");
            // create socket
            CommunityMirror.clientToSendToLeft = (portToSendToLeft < 0) ? null
                    : new ClientToSend(hostToSendToLeft, portToSendToLeft);

            // get port and host to send messages from the right side,
            // "localhost" as fallback
            int portToSendToRight = CommunityMirror.configuration.getInt("communication.portToSendToRight", -1);
            String hostToSendToRight = CommunityMirror.configuration.getString("communication.hostToSendToRight",
                    "localhost");
            // create socket
            CommunityMirror.clientToSendToRight = (portToSendToRight < 0) ? null
                    : new ClientToSend(hostToSendToRight, portToSendToRight);
        }
    }

    /**
     * Initializes the interaction component depending on the configuration.
     */
    private void initializeInteraction() {

        // get the configured behavior factory
        String behaviorFactoryConfig = CommunityMirror.configuration.getString("interaction.behaviorfactory");

        if (behaviorFactoryConfig.equalsIgnoreCase("Direct")) {
            CommunityMirror.interactionBehaviorFactory = new DirectBehaviorFactory();
        } else if (behaviorFactoryConfig.equalsIgnoreCase("MouseToX")) {
            CommunityMirror.interactionBehaviorFactory = new MouseToXBehaviorFactory();
        } else {
            // default is direct behavior
            CommunityMirror.logger.warn("Could not interpret configured interaction behavior factory: "
                    + behaviorFactoryConfig + ". Setting it to the direct behavior factory.");
            CommunityMirror.interactionBehaviorFactory = new DirectBehaviorFactory();
        }

        // get the configured interaction event creator
        String eventCreatorConfig = CommunityMirror.configuration.getString("interaction.eventcreator");
        if (eventCreatorConfig.equalsIgnoreCase("FX")) {
            CommunityMirror.interactionEventCreator = new FXEventCreator(this.mainView.getFXScene());
        } else if (eventCreatorConfig.equalsIgnoreCase("TUIOFX")) {
            CommunityMirror.interactionEventCreator = new TUIOFXEventCreator(this.mainView.getFXScene());
            TuioFX tuioFX = new TuioFX((Stage) this.stage, org.tuiofx.Configuration.debug());
            // tuioFX.enableMTWidgets(true);
            tuioFX.start();
        } else if (eventCreatorConfig.equalsIgnoreCase("GW")) {
            try {
                // get gml and dll path from configuration
                String gmlPath = CommunityMirror.configuration.getString("interaction.gestureworks.gmlpath");
                String gwDllPath = CommunityMirror.configuration.getString("interaction.gestureworks.dllpath");
                CommunityMirror.interactionEventCreator = new GWEventCreator(this.mainView.getFXScene(),
                        CommunityMirror.MAIN_WINDOW_TITLE, gmlPath, gwDllPath);
            } catch (Throwable e) {
                CommunityMirror.logger.error("Could not use GW eventcreator - " + e.toString());
                CommunityMirror.logger.error("Using FX eventcreator instead.");
                // use FX as fallback ...
                CommunityMirror.interactionEventCreator = new FXEventCreator(this.mainView.getFXScene());
            }
        } else if (eventCreatorConfig.equalsIgnoreCase("GWReuse")) {
            // get gml and dll path from configuration
            String gmlPath = CommunityMirror.configuration.getString("interaction.gestureworks.gmlpath");
            String gwDllPath = CommunityMirror.configuration.getString("interaction.gestureworks.dllpath");
            CommunityMirror.interactionEventCreator = new GWReuseEventCreator(this.mainView.getFXScene(),
                    CommunityMirror.MAIN_WINDOW_TITLE, gmlPath, gwDllPath);
        } else {
            // default is the fx event creator
            CommunityMirror.logger.warn("Could not interpret configured interaction event creator: "
                    + eventCreatorConfig + ". Setting it to the fx event creator.");
            CommunityMirror.interactionEventCreator = new FXEventCreator(this.mainView.getFXScene());
        }

        // check if intersection events are enabled
        boolean intersectionEventsConfig = CommunityMirror.configuration.getBoolean("interaction.intersectionEvents",
                false);
        if (intersectionEventsConfig) {
            // create intersection events creator
            CommunityMirror.intersectionEventCreator = new IntersectionEventCreator(this.mainView);
        }

        // check if j4ksdk kinect sensing is enabled
        if (CommunityMirror.configuration.getBoolean("sensing.J4KSDKkinectEnabled", false)) {
            // create kinect frame event creator
            CommunityMirror.kinectFrameEventCreator = new KinectFrameEventCreator();
        }

        // look if interaction visualization is switched on
        boolean visualizer = CommunityMirror.configuration.getBoolean("interaction.visualizer", false);

        if (visualizer) {
            // create interaction visualizer
            this.interactionEventVisualizer = new CIEventVisualizer(CommunityMirror.interactionBehaviorFactory,
                    this.mainView);
            // register visualizer for interaction events at creator
            CommunityMirror.interactionEventCreator.registerEventConsumer(this.interactionEventVisualizer);
        }
    }

    /**
     * Initializes all global keyboard events. Currently - P: Pause or Resume -
     * L: Toggle path line of every component
     */
    protected void initializeKeyEvents() {

        // TODO integrate keyboard events in community interaction framework

        this.mainView.getFXScene().addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent keyevent) {
                // toggle path with l
                if (keyevent.getCode() == KeyCode.L) {
                    CommunityMirror.this.togglePositionPath();
                }
                if (keyevent.getCode() == KeyCode.SPACE) {
                    if (CommunityMirror.this.mainView instanceof FlowView) {
                        String panelMetaTag = CommunityMirror.configuration.getString("view.flow.panel.metatag");
                        if (panelMetaTag != null && panelMetaTag.length() > 0) {
                            ((FlowView) CommunityMirror.this.mainView).showItemPanel(panelMetaTag);
                        }
                    }
                }
            }
        });

        // add filter to consume pause key and consume all events in pause state
        this.mainView.getFXScene().addEventFilter(EventType.ROOT, new EventHandler<Event>() {

            @Override
            public void handle(Event event) {
                if (event instanceof KeyEvent) {
                    KeyEvent keyEvent = (KeyEvent) event;
                    if (keyEvent.getEventType() == KeyEvent.KEY_RELEASED && keyEvent.getCode() == KeyCode.P) {
                        // pause or resume
                        CommunityMirror.this.togglePause();
                    }
                } else if (CommunityMirror.this.isPaused()) {
                    // stop all interaction in pause state
                    event.consume();
                }
            }
        });
    }

    protected void togglePause() {
        if (isPaused()) {
            this.mainView.resume();
        } else {
            this.mainView.pause();
        }
    }

    boolean isPaused() {
        return this.mainView.getComponentState() == ComponentState.PAUSE;
    }

    /**
     * Activates or deactivates the path creation in the main view.
     */
    private void togglePositionPath() {
        // invert
        this.showPathLine = !this.showPathLine;

        if (!this.showPathLine) {
            this.mainView.createPositionPath(false);
            CommunityMirror.logger.debug("Deactivated path line");
        } else {
            this.mainView.createPositionPath(true);
            CommunityMirror.logger.debug("Activated path line");
        }

    }

    /**
     * Interprets the screen configuration and sets up the stage on the
     * configured screen.
     */
    private void manageScreenSetup() {
        // get screen index from configuration, 0 as fallback
        int screenIndex = CommunityMirror.configuration.getInt("mirror.screen", 0);

        Screen usedScreen;

        // try to get screen specified in configuration
        try {
            usedScreen = Screen.getScreens().get(screenIndex);
        } catch (Exception e) {
            // set screen to primary screen
            usedScreen = Screen.getPrimary();
        }

        Rectangle2D screenBounds = usedScreen.getBounds();

        // Set Stage boundaries to visible bounds of the main screen
        this.stage.setX(screenBounds.getMinX());
        this.stage.setY(screenBounds.getMinY());
        this.stage.setWidth(screenBounds.getWidth());
        this.stage.setHeight(screenBounds.getHeight());
    }

    /**
     * Connects to the configured CommunityMashup endpoint and loads the the
     * data set.
     */
    private void connectToCommunityMashup() {
        // get the mashup url from the configuration
        String mashupUrl = CommunityMirror.configuration.getString("mashup.xmlinterface");

        // look up in configuration wheter to write back data changes or not
        boolean writeBackToMashup = CommunityMirror.configuration.getBoolean("mashup.writeback");

        // get update interval from configuration
        int updateInterval = CommunityMirror.configuration.getInt("mashup.updateinterval");

        // if a file-URL is used, set back writeBackToMashup and updateInterval
        if (mashupUrl.startsWith("file:")) {
            writeBackToMashup = false;
            updateInterval = -1;
        }

        // create new mashup connector
        CommunityMirror.logger.debug("Loading dataset from " + mashupUrl);
        MashupConnector connector = new MashupConnector(mashupUrl, updateInterval, writeBackToMashup);
        connector.setLogService(new MashupLoggerBridge());
        connector.setAsynchronous(true);

        try {
            this.dataSet = connector.getDataSet();
            if (this.dataSet != null) {
                CommunityMirror.logger.debug("Finished loading dataset - size=" + this.dataSet.getItems().size());
            } else {
                CommunityMirror.logger.debug("Did not load a dataset (null value)");
            }
        } catch (Exception e) {
            CommunityMirror.logger.error(
                    "Error while creating data set from url " + mashupUrl + ". Aborting startup of CommunityMirror.",
                    e);
            // TODO throw exception or good return value
            return;
        }

        if (this.dataSet == null) {
            CommunityMirror.logger.error("DataSet is null. Aborting startup of CommunityMirror.");
            // TODO throw exception or good return value
            return;
        }

        this.dataSet.setCacheFolder(null);

        // switch on caching
        this.dataSet.setCacheFileAttachements(true);
        this.dataSet.setLogLevel(LogService.LOG_ERROR);

        boolean preLoadAttachments = CommunityMirror.configuration.getBoolean("mashup.attachmentspreload");
        if (preLoadAttachments) {
            // setting to true starts the preloading
            CommunityMirror.logger.debug("Pre loading attached files");
            connector.setPreLoadAttachedFiles(preLoadAttachments);
            CommunityMirror.logger.debug("Finished pre loading of attached files.");
        } else {
            // set without message
            connector.setPreLoadAttachedFiles(preLoadAttachments);
        }

    }

    /**
     * Inits the main event loop and the fps counter.
     */
    private void initEventLoop() {
        final Duration fpsDuration = Duration.millis(1000.0 / CommunityMirror.INTENDED_FPS);

        final KeyFrame eventLoop = new KeyFrame(fpsDuration, new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                if (CommunityMirror.this.isPaused()) {
                    // skip updates when paused
                    return;
                }

                // Calculate FPS
                CommunityMirror.this.calculateFps();

                if (CommunityMirror.this.skipFrame) {
                    // skip frame
                    return;
                }

                // update all controllers
                for (Controller controller : CommunityMirror.this.controllers) {
                    try {
                        controller.update();
                    } catch (Exception e) {
                        CommunityMirror.logger.warn("Exception in update loop of controller.", e);
                    }
                }

                // Update all views
                for (FXView view : CommunityMirror.this.viewList) {
                    try {
                        view.update(CommunityMirror.this.framesPerSecond, CommunityMirror.this.nanoTime);
                    } catch (Exception e) {
                        CommunityMirror.logger.warn("Exception in update loop of view.", e);
                    }
                }
            }

        });

        // Create Loop
        Timeline tl = new Timeline(eventLoop);
        tl.setCycleCount(Animation.INDEFINITE);
        this.eventLoop = tl;
    }

    /**
     * Starts the event loop
     */
    private void startEventLoop() {
        this.eventLoop.play();
    }

    /**
     * Calculates the current frame rate
     */
    private void calculateFps() {
        this.skipFrame = false;

        this.nanoTime = System.nanoTime();
        this.timeDiff = this.nanoTime - this.oldTime;

        if (this.timeDiff < this.waitTimeNano) {
            // skip frame if time to next frame is to small
            this.skipFrame = true;
            return;
        }

        this.framesPerSecond = 1000000000.0 / this.timeDiff;
        this.oldTime = this.nanoTime;

        if (this.framesPerSecond < 1.0) {
            this.framesPerSecond = 1.0;
        }

        // keep value in array
        this.fpsValues[this.currentFPSindex] = this.framesPerSecond;
        this.currentFPSindex++;
        if (this.currentFPSindex >= CommunityMirror.SMOOTH_FPS_VALUES) {
            // reset index to loop arround
            this.currentFPSindex = 0;
        }

        // smooth fps count
        this.framesPerSecond = this.fpsValues[0];
        for (int i = 1; i < this.fpsValues.length; i++) {
            this.framesPerSecond += this.fpsValues[i];
        }
        this.framesPerSecond /= CommunityMirror.SMOOTH_FPS_VALUES;
    }

    /**
     * Returns the globally used behavior factory.
     *
     * @return The globally used behavior factory.
     */
    public static InteractionBehaviorFactory getInteractionBehaviorFactory() {
        return CommunityMirror.interactionBehaviorFactory;
    }

    /**
     * Returns the globally used user activity logger.
     *
     * @return The globally used user activity logger.
     */
    public static UserActivityLogger getUserActivityLogger() {
        return CommunityMirror.userActivityLogger;
    }

    /**
     * Returns the globally used interaction event creator.
     *
     * @return The globally used interaction event creator.
     */
    public static CIEventCreator getInteractionEventCreator() {
        return CommunityMirror.interactionEventCreator;
    }

    /**
     * Returns the globally used communication event creator.
     *
     * @return The globally used communication event creator.
     *
     * @author Eva Lösch
     */
    public static CommunicationEventCreator getCommunicationEventCreator() {
        return CommunityMirror.communicationEventCreator;
    }

    /**
     * Returns the globally used intersection event creator if it exists.
     *
     * @return the intersectionEventCreator
     *
     * @author Eva Lösch
     */
    public static IntersectionEventCreator getIntersectionEventCreator() {
        return CommunityMirror.intersectionEventCreator;
    }

    /**
     * Returns the globally used kinect frame event creator if it exists.
     *
     * @return the kinectFrameEventCreator
     *
     * @author Eva Lösch
     */
    public static KinectFrameEventCreator getKinectFrameEventCreator() {
        return CommunityMirror.kinectFrameEventCreator;
    }

    /**
     * Returns the globally used client to send messages to the left side via
     * sockets.
     *
     * @return the client to send
     *
     * @author Eva Lösch
     */
    public static ClientToSend getClientToSendToLeft() {
        return CommunityMirror.clientToSendToLeft;
    }

    /**
     * Returns the globally used client to send messages to the right side via
     * sockets.
     *
     * @return the client to send
     *
     * @author Eva Lösch
     */
    public static ClientToSend getClientToSendToRight() {
        return CommunityMirror.clientToSendToRight;
    }

    /**
     * @return the visualUserRepresentations
     */
    public static List<VisualUserRepresentation> getVisualUserRepresentations() {
        return CommunityMirror.visualUserRepresentations;
    }

    public static VisualUserRepresentationController<VisualUserRepresentation> getVisualUserRepresentationController() {
        return CommunityMirror.visualUserRepresentationController;
    }

    public static UserController getUserController() {
        return CommunityMirror.userController;
    }

    /**
     * Returns the CommunityMirror configuration.
     */
    public static Configuration getConfiguration() {
        return CommunityMirror.configuration;
    }

    /**
     * Returns the CommunityMirror dataSet.
     */
    public DataSet getDataSet() {
        return this.dataSet;
    }

    /**
     * Returns the bounds of the main view.
     */
    public static Rectangle2D getViewBounds() {
        return CommunityMirror.viewBounds;
    }

    /**
     * Returns the main view of this Community mirror instance.
     */
    protected FXView getMainView() {
        return this.mainView;
    }

    /**
     * Returns the context manager
     */
    public static ContextManager getContextManager() {
        return CommunityMirror.contextManager;
    }

}
