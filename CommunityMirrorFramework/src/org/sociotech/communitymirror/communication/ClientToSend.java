package org.sociotech.communitymirror.communication;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;

/**
 * Class to connect to a server socket and send text messages to the server.
 * 
 * @author Lösch
 *
 */
public class ClientToSend {
	Socket server = null;
	PrintWriter outStreamPrintWriter;

	/**
	 * Creates a new ClientToSend and tries to connect to a server socket with given hostname and portnumber.
	 * 
	 * @param host	name of the host to connect with and send to
	 * @param port  number of the port to connect with and send to
	 */
	public ClientToSend(String host, int port) {
		this.connect(host, port);
	}

	/**
	 * Tries to connect to server socket with given hostname and portnumber
	 * within an own thread. As long as the client cannot connect to the server and a
	 * ConnectException occurs (e.g. because the server does not yet exist),
	 * the client creates a new thread and tries again.
	 * 
	 * @param host
	 *            name of the host to connect to
	 * @param port
	 *            number of the port to connect to
	 */
	private void connect(String host, int port) {
		Runnable clientTask = new Runnable() {
			@Override
			public void run() {
				try {
					server = new Socket(host, port);
					outStreamPrintWriter = new PrintWriter(server.getOutputStream(), true);
				} catch (ConnectException e) {
					System.err.println("Unable to connect to server");
					connect(host, port);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};

		Thread serverThread = new Thread(clientTask);
		serverThread.start();
	}

	/**
	 * Sends a given text message to the server socket that is connected to this
	 * client socket.
	 * 
	 * @param textToSend
	 *            the text message to send
	 */
	public void send(String textToSend) {
		if (outStreamPrintWriter != null)
			outStreamPrintWriter.println(textToSend);
	}
}
