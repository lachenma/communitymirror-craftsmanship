
package org.sociotech.communitymirror.utils;

import java.awt.image.BufferedImage;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;

import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

/**
 * Helper class to handle common image tasks.
 *
 * @author Peter Lachenmaier
 */
public class ImageHelper {

    /**
     * Creates a image from the given file url. If smooth is set to true a one
     * pixel
     * transparent border is created around the image to imitate subpixel
     * rendering.
     * Use this for animated images to smoothen the transitions.
     *
     * @param fileUrl
     *            Url of the image file
     * @param smooth
     *            Whether to smooth the image or not.
     * @return The created image or null in error case.
     */
    public static Image createImage(String fileUrl, boolean smooth) {
        try {
            return ImageHelper.createImage(new URL(fileUrl), smooth);
        } catch (MalformedURLException e) {
            return null;
        }
    }

    /**
     * Creates a image from the given file url. If smooth is set to true a one
     * pixel
     * transparent border is created around the image to imitate subpixel
     * rendering.
     * Use this for animated images to smoothen the transitions.
     *
     * @param fileUrl
     *            Url of the image file
     * @param smooth
     *            Whether to smooth the image or not.
     * @return The created image or null in error case.
     */
    public static Image createImage(URL fileUrl, boolean smooth) {
        if (fileUrl == null) {
            // need a valid url
            return null;
        }

        Image image = null;

        if (smooth) {
            try {
                BufferedImage i1 = ImageIO.read(fileUrl);

                BufferedImage i2 = new BufferedImage(i1.getWidth() + 2, i1.getHeight() + 2,
                        BufferedImage.TYPE_INT_ARGB);
                // paint a one pixel transparent border around the imgae
                i2.getGraphics().drawImage(i1, 1, 1, new java.awt.Color(0, 0, 0, 0), null);

                // transform the image to a javafx image
                image = SwingFXUtils.toFXImage(i2, null);
            } catch (Exception e1) {
                // leads to fall back with non smooth
            }
        }

        // not smooth or has not worked
        if (image == null) {
            try {
                image = new Image(fileUrl.toString());
            } catch (Exception e) {
                // do nothing, simply return null
            }
        }

        // broken files may be returned as 0x0 images. prevent this:
        if (image != null && image.getWidth() == 0) {
            image = null;
        }

        return image;
    }

    /**
     * Creates an Image snapshot from a Node with transparent background color.
     *
     * @param node
     *            The JavaFX Node
     * @return Image snapshot
     */
    public static Image renderImage(Node node) {
        return ImageHelper.renderImage(node, Color.TRANSPARENT);
    }

    /**
     * Creates an Image snapshot from a Node with custom background color
     *
     * @param node
     *            The JavaFX Node
     * @param backgroundColor
     *            Custom background color.
     * @return Image snapshot
     */
    public static Image renderImage(Node node, Color backgroundColor) {

        // Get layout bounds
        final Bounds layoutBounds = node.getLayoutBounds();

        // Convert dimensions
        int width = (int) layoutBounds.getWidth();
        int height = (int) layoutBounds.getHeight();

        // Create WritableImage
        WritableImage image = new WritableImage(width, height);

        // Create SnapshotParameters with transparent background
        SnapshotParameters parameters = new SnapshotParameters();
        parameters.setFill(backgroundColor);

        // Create image snapshot from Node
        node.snapshot(parameters, image);

        // Return image
        return image;
    }
}
