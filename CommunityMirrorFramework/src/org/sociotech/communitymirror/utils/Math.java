/*
 * Copyright (c) 2014. Martin Burkhard, CSCM Cooperation Systems Center Munich.
 * Institute for Software Technology at Universitaet der Bundeswehr Muenchen.
 *
 * This part of the program is made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.sociotech.communitymirror.utils;


public class Math {

    public static int clamp (int value, int min, int max) {
        return java.lang.Math.min(java.lang.Math.max(value, min), max);
    }

    public static float clamp (float value, float min, float max) {
        return java.lang.Math.min(java.lang.Math.max(value, min), max);
    }

    public static double clamp (double value, double min, double max) {
        return java.lang.Math.min(java.lang.Math.max(value, min), max);
    }
}
