package org.sociotech.communitymirror.controllers.userRepresentation;

import java.util.List;

import javafx.scene.image.Image;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sociotech.communitymirror.controllers.VisualComponentController;
import org.sociotech.communitymirror.visualitems.user.User;
import org.sociotech.communitymirror.visualitems.user.userRepresentation.VisualUserRepresentation;
import org.sociotech.communitymirror.visualitems.user.userState.InteractionPhase;

public class VisualUserRepresentationController<A> extends
		VisualComponentController<VisualUserRepresentation> {

	private final Logger logger = LogManager
			.getLogger(VisualUserRepresentationController.class.getName());

	public VisualUserRepresentationController(
			List<VisualUserRepresentation> visualComponents) {
		super(visualComponents);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communitymirror.controllers.VisualComponentController#update
	 * ()
	 */
	@Override
	public void update() {
		logger.debug("update visual user representation");
		// go through the list of all visual user representations controlled by
		// this controller
		for (VisualUserRepresentation visualUserRepresentation : this
				.getControlledVisualComponents()) {
			// update visual user representation
			this.updateVisualUserRepresentation(visualUserRepresentation);
			// THINK ABOUT: starting a new thread/runnable/task for updating all
			// user representations in parallel
		}
	}

	/**
	 * Updates a given visual user representation according to the parameters of
	 * the user that is represented by the visual user representation.
	 * 
	 * @param visualUserRepresentation
	 *            The visual user representation to update.
	 */
	public void updateVisualUserRepresentation(
			VisualUserRepresentation visualUserRepresentation) {
		// get the user represented by the given visual user representation
		User user = visualUserRepresentation.getUser();

		// if user is tracked do updates
		if (user.isTracked()) {
			// update representation image
			Image image = this.updateRepresentationImage(visualUserRepresentation);
			// update the position
			this.updatePositionAndScale(visualUserRepresentation, user, image);

			// update the state
			this.updateVisualUserRepresentationState(visualUserRepresentation,
					user);
		}

		// at last: update visibility
		visualUserRepresentation.setVisible(user.isTracked());
		visualUserRepresentation.setManaged(user.isTracked());
		// logger.debug("set rep visible: " + +user.getUserId() + ": "
		// + user.isTracked());
	}

	/**
	 * Updates the position and scale of a given visual user representation.
	 * 
	 * @param visualUserRepresentation
	 *            The visual user representation to update.
	 * @param user
	 *            The user of the visual user representation to update.
	 */
	private void updatePositionAndScale(
			VisualUserRepresentation visualUserRepresentation, User user, Image image) {
		// update position
		double imageWidth = 0;
		double imageHeight = 0;
		if(image!=null){
			imageWidth = image.getWidth();
			imageHeight = image.getHeight();
		}
		double posX = visualUserRepresentation.calculatePositionX(user, imageWidth);
		double posY = visualUserRepresentation.calculatePositionY(user, imageHeight);
		visualUserRepresentation.setPositionX(posX);
		visualUserRepresentation.setPositionY(posY);
		logger.debug("set position: " + posX + ", " + posY);
		// update scale
		double scale = visualUserRepresentation.calculateScale(user);
		visualUserRepresentation.setScaleX(scale);
		visualUserRepresentation.setScaleY(scale);
	}

	/**
	 * Updates the state of a given visual user representation according to the
	 * state of the user that is represented by that visual user representation.
	 * 
	 * @param visualUserRepresentation
	 *            The visual user representation to update.
	 * @param user
	 *            The user of the visual user representation to update.
	 */
	private void updateVisualUserRepresentationState(
			VisualUserRepresentation visualUserRepresentation, User user) {
		InteractionPhase interactionPhase = user.getState()
				.getInteractionPhase();
		// if current state is different
		if (visualUserRepresentation.getCurrentState().getInteractionPhase() != interactionPhase) {
			// switch to the new interaction phase of the user
			visualUserRepresentation.setCurrentState(interactionPhase);
		}
	}

	/**
	 * Updates the representation image of a user representation.
	 * 
	 * @param visualUserRepresentation
	 * @param pixels
	 * @param w
	 * @param h
	 */
	private Image updateRepresentationImage(
			VisualUserRepresentation visualUserRepresentation) {

		// get next representation image
		Image nextRepresentationImage = visualUserRepresentation
				.getNextRepresentationImage();
		// update current representation image with next representation image
		if (nextRepresentationImage != null) {
			visualUserRepresentation
					.setRepresentationImage(nextRepresentationImage);
		}
		return nextRepresentationImage;
	}
}
