package org.sociotech.communitymirror.controllers;

import java.util.List;

import org.sociotech.communitymirror.visualitems.VisualComponent;

/**
 * Abstract super class of all controllers controlling a list of subclasses of visual
 * components.
 * 
 * @param <T> the type of the visual components that are controlled
 * 
 * @author evalosch
 *
 */
public abstract class VisualComponentController<T extends VisualComponent>
		extends Controller {

	/**
	 * List of visual components that are controlled by this controller.
	 */
	private List<T> controlledVisualComponents;

	/**
	 * Creates a new visual component controller that controls a list of visual
	 * components.
	 * 
	 * @param visualComponents
	 *            The list of visual components to control.
	 */
	public VisualComponentController(List<T> visualComponents) {
		this.controlledVisualComponents = visualComponents;
	}

	/**
	 * @return the visualComponents controlled by this controller
	 */
	public List<T> getControlledVisualComponents() {
		return controlledVisualComponents;
	}

	/**
	 * @param controlledVisualComponents
	 *            The controlledVisualComponents to set.
	 */
	protected void setControlledVisualComponents(List<T> controlledVisualComponents) {
		this.controlledVisualComponents = controlledVisualComponents;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.sociotech.communitymirror.controllers.Controller#update()
	 */
	@Override
	public abstract void update();
}
