package org.sociotech.communitymirror.controllers;

/**
 * Abstract super class of all controllers.
 * 
 * @author evalosch
 *
 */
public abstract class Controller {

	/**
	 * Performs an update of all components that are controlled by this controller.
	 */
	public abstract void update();

}
