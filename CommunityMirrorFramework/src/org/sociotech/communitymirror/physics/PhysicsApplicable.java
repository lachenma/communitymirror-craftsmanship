package org.sociotech.communitymirror.physics;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.LongProperty;
import javafx.geometry.Bounds;

/**
 * Interface that must be implemented to enable the application of physics.
 * 
 * @author Peter Lachenmaier
 */
public interface PhysicsApplicable {

	/**
	 * Moves this component by the given values in x and y direction.
	 * 
	 * @param diffX Movement in x direction
	 * @param diffY Movement in y direction
	 */
	public void move(double diffX, double diffY);
	
	/**
	 * Resets the move made by other previously. 
	 */
	public void resetMove();
	
	/**
	 * Resets the rotation made by other previously. 
	 */
	public void resetRotation();
	
	/**
	 * Rotates this component by the given angle.
	 * 
	 * @param angle Rotation angle in degrees.
	 */
	public void rotate(double angle);
	
	/**
	 * Returns the property that is used for x positioning of this component.
	 * 
	 * @return The property that is used for x positioning of this component.
	 */
	public DoubleProperty getPositionXProperty();
	
	/**
	 * Returns the property that is used for y positioning of this component.
	 * 
	 * @return The property that is used for y positioning of this component.
	 */
	public DoubleProperty getPositionYProperty();

	/**
	 * Returns the position bounds of this component.
	 * 
	 * @return The position bounds of this component.
	 */
	public Bounds getPositionBounds();

	/**
	 * Returns the property for the opacity of this component.
	 * 
	 * @return The property for the opacity of this component.
	 */
	public DoubleProperty getOpacityProperty();
	
	/**
	 * Returns the property that contains the time in millis since the last interaction on this component.
	 * 
	 * @return The property that contains the time in millis since the last interaction on this component.
	 */
	public LongProperty getTimeSinceLastInteractionProperty();
	
	 /**
     * Destroys this component.
     */
    public void destroy();
}
