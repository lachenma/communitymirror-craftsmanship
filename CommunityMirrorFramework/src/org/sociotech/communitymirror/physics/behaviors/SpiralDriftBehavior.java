package org.sociotech.communitymirror.physics.behaviors;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

/**
 * A drift behavior that rotates the drift vector.
 * 
 * @author Peter Lachenmaier
 */
public class SpiralDriftBehavior extends DriftBehavior {
	
	/**
	 * The property for the rotation angle step.
	 */
	private final DoubleProperty rotationAngle;

	/**
	 * The property for the rotation angle step.
	 */
	private final DoubleProperty rotationAngleStep;

	/**
	 * Creates a drift behavior with rotating drift vector.
	 * 
	 * @param rotationAngleStep The rotation step
	 * @param rotationAngle The initial rotation angle
	 * @param velocity The drift velocity
	 * @param acceleration The drift acceleration
	 * @param targetVelocity The target velocity
	 */
	public SpiralDriftBehavior(double rotationAngleStep, double rotationAngle, double velocity, double acceleration, double targetVelocity) {
		super(1.0, 0.0, velocity, acceleration, targetVelocity);
		this.rotationAngleStep = new SimpleDoubleProperty(rotationAngleStep);
		this.rotationAngle     = new SimpleDoubleProperty(rotationAngle);
		// apply the rotation angle to get the initial drift vector
		applyRotationToDriftVector();
	}
	
	/**
	 * Creates a drift behavior with rotating drift vector. No acceleration.
	 * 
	 * @param rotationAngleStep The rotation step
	 * @param rotationAngle The initial rotation angle
	 * @param velocity The drift velocity
	 */
	public SpiralDriftBehavior(double rotationAngleStep, double rotationAngle, double velocity) {
		this(rotationAngleStep, rotationAngle, velocity, 0.0, velocity);
	}
	
	/**
	 * Creates a drift behavior with rotating drift vector. No acceleration. Velocity is 1.0.
	 * 
	 * @param rotationAngleStep The rotation step
	 */
	public SpiralDriftBehavior(double rotationAngleStep) {
		this(rotationAngleStep, 0.0, 1.0);
	}
	
	/**
	 * Returns the rotation angle step property.
	 * 
	 * @return The rotation angle step property.
	 */
	public DoubleProperty rotationAngleStepProperty() {
		return rotationAngleStep;
	}
	
	/**
	 * Sets the value for the rotation angle step.
	 * 
	 * @param rotationAngle Rotation angle
	 */
	public void setRotationAngleStep(double rotationAngle) {
		this.rotationAngleStep.set(rotationAngle);
	}
	
	/**
	 * Returns the rotation angle property.
	 * 
	 * @return The rotation angle property.
	 */
	public DoubleProperty rotationAngleProperty() {
		return rotationAngle;
	}
	
	/**
	 * Sets the value for the rotation angle step.
	 * 
	 * @param rotationAngle Rotation angle
	 */
	public void setRotationAngle(double rotationAngle) {
		this.rotationAngle.set(rotationAngle);
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.physics.behaviors.AcceleratablePhysicsBehavior#onUpdate(double, long)
	 */
	@Override
	protected void onUpdate(double framesPerSecond, long nanoTime) {
		super.onUpdate(framesPerSecond, nanoTime);
		
		//calculate new rotation angle depending on speed
		double newRotationAngle = rotationAngle.get() + rotationAngleStep.get() * getSpeedCorrection();
		
		// keep angle between 0 and 360
		if(newRotationAngle >= 360.0) {
			newRotationAngle -= 360.0;
		} else if(newRotationAngle <= 0.0) {
			newRotationAngle += 360.0;
		}
		
		// update property
		setRotationAngle(newRotationAngle);
		
		// apply rotation angle to drift vector
		applyRotationToDriftVector();
		
		//System.out.println("angle: " + rotationAngle.get() + " x: " + xDirectionProperty().get() + " y: " + yDirectionProperty().get());
	}

	/**
	 * Applies the rotation from the current rotation angle to the drift vector. 
	 */
	private void applyRotationToDriftVector() {
		this.xDirectionProperty().set(Math.cos(Math.toRadians(rotationAngle.get())));
		this.yDirectionProperty().set(Math.sin(Math.toRadians(rotationAngle.get())));
	}
}
