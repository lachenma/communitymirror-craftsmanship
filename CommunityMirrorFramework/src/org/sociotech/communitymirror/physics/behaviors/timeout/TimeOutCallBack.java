package org.sociotech.communitymirror.physics.behaviors.timeout;

import org.sociotech.communitymirror.physics.PhysicsApplicable;


/**
 * Interface that must be implemented to be called back by the {@link TimeOutAndCallbackBehavior}
 * when a component timed out with no interaction.
 * 
 * @author Peter Lachenmaier
 */
public interface TimeOutCallBack {
	/**
	 * Will be called if the configured component timed out with no interaction.
	 * 
	 * @param timeOutObject Object that timed out
	 */
	public void timedOut(PhysicsApplicable timeOutObject);
}
