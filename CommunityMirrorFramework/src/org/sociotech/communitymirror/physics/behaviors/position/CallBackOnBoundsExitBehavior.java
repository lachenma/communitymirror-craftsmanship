package org.sociotech.communitymirror.physics.behaviors.position;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;

import org.sociotech.communitymirror.physics.PhysicsApplicable;
import org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior;

/**
 * Behavior that observes if a component leaves given bounds. This behavior keeps the state if a component
 * left the bounds, so do not use this behavior for more than one component.
 * 
 * @author Peter Lachenmaier
 */
public class CallBackOnBoundsExitBehavior extends PhysicsBehavior {
	
	/**
	 * Bounds to observe.
	 */
	private Bounds bounds;
	
	/**
	 * Property holding the state if the component left the bounds or is still inside. 
	 */
	private BooleanProperty leftBoundsProperty = new SimpleBooleanProperty(false);

	/**
	 * Indicates if component ever was in  
	 */
	private boolean wasInBounds = false;
	
	/**
	 * Object to callback after the component leaves the bounds.
	 */
	private LeftBoundsCallback callback;
	
	/**
	 * Creates a callback behavior that calls the given callback after the object leaves the
	 * given bounds. Calls again if the object comes in and leaves again.
	 * 
	 * @param bounds Bounds which will be observed.
	 */
	public CallBackOnBoundsExitBehavior(Bounds bounds, LeftBoundsCallback callback) {
		this.bounds = bounds;
		this.callback = callback;
	}
	
	/**
	 * Creates the callback behavior without a callback. The state if the component is
	 * inside or outside the bounds can still be observed by the property 
	 * {@link CallBackOnBoundsExitBehavior#getLeftBoundsProperty()}
	 * 
	 * @param bounds Bounds which will be observed.
	 */
	public CallBackOnBoundsExitBehavior(Bounds bounds) {
		this(bounds, null);
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior#onApplyTo(org.sociotech.communitymirror.physics.PhysicsApplicable)
	 */
	@Override
	protected void onApplyTo(PhysicsApplicable component) {
		Bounds compBounds = component.getPositionBounds();
		
		if( compBounds.getMinX() > bounds.getMinX()  && 
				compBounds.getMaxX() < bounds.getMaxX() &&
				compBounds.getMaxY() > bounds.getMinY() &&
				compBounds.getMinY() < bounds.getMaxY()) {
			// in bounds
			wasInBounds = true;
			
			if(leftBoundsProperty.getValue()) {
				// already left bounds
				// came in,
				// so set property
				leftBoundsProperty.set(false);
			}	
		}
		else {
			// in bounds in previous apply call
			// check if it is completely outside now
			if( compBounds.getMinX() > bounds.getMaxX()  || 
				compBounds.getMaxX() < bounds.getMinX() ||
				compBounds.getMinY() > bounds.getMaxY() ||
				compBounds.getMaxY() < bounds.getMinY()) {
				if(!leftBoundsProperty.getValue()) {
					// exited bounds
					// so set property
					leftBoundsProperty.set(true);
				}
				
				// and callback if a callback was set
				if(callback != null && wasInBounds) {
					callback.leftBounds(bounds, component);
				}
			}	
		}
			
		
	}
	
	/**
	 * Returns the bounds that will be observed. The object is the same that will be
	 * provided as callback parameter in {@link LeftBoundsCallback#leftBounds(Bounds, PhysicsApplicable)}
	 * 
	 * @return The bounds that will be observed by this behavior.
	 */
	protected Bounds getBounds() {
		return bounds;
	}

	/**
	 * Creates a callback behavior with an additional given offset. 
	 * 
	 * @param bounds Bounds to observe
	 * @param callback Object to call after component leaves the bounds 
	 * @param offset Offset to all sides. Positive means inside, negative means outside.
	 * 
	 * @return The newly created stay in bounds behavior 
	 */
	public static CallBackOnBoundsExitBehavior createCallBackOnBoundsExitBehaviorWithOffset(Bounds bounds, LeftBoundsCallback callback, double offset) {
		return new CallBackOnBoundsExitBehavior(new BoundingBox(bounds.getMinX() + offset,
														bounds.getMinY() + offset,
														bounds.getWidth()  - 2 * offset,
														bounds.getHeight() - 2 * offset),
														callback);
	}
	
	/**
	 * Creates a stay in bounds behavior with an offset to all sides.
	 * 
	 * @param bounds Bounds
	 * @param callback
	 * @param offset Offset to all sides. Positive means inside, negative means outside.
	 * 
	 * @return The newly created stay in bounds behavior 
	 */
	public static CallBackOnBoundsExitBehavior createCallBackOnBoundsExitBehaviorWithOffset(Bounds bounds, double offset) {
		return new CallBackOnBoundsExitBehavior(new BoundingBox(bounds.getMinX() + offset,
														bounds.getMinY() + offset,
														bounds.getWidth()  - 2 * offset,
														bounds.getHeight() - 2 * offset));
	}
	/**
	 * Returns the property indicating if the object is in the bounds or left them.
	 * 
	 * @return The boolean property
	 */
	public BooleanProperty getLeftBoundsProperty() {
		return leftBoundsProperty;
	}
}
