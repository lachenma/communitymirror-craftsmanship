package org.sociotech.communitymirror.physics.behaviors.position;

import org.sociotech.communitymirror.physics.PhysicsApplicable;
import org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;

/**
 * Behavior that observes if a component traverses given bounds. This behavior
 * keeps the state if a component left the bounds, so do not use this behavior
 * for more than one component.
 * 
 * A component traverses given bounds, if any part of the component (and not necessarily the
 * whole component) is outside of that bounds.
 * 
 * A component exits given bounds, if the whole component is outside of that bounds (use CallBackOnBounsExitBehavior for that event).
 * 
 * @author Eva Lösch
 */
public class CallBackOnBoundsTraversedBehavior extends PhysicsBehavior {

	/**
	 * Bounds to observe.
	 */
	private Bounds bounds;

	/**
	 * Property holding the state if the component left the bounds or is still
	 * inside.
	 */
	private BooleanProperty traversesBoundsProperty = new SimpleBooleanProperty(false);

	/**
	 * Indicates if component ever was in
	 */
	private boolean wasInBounds = false;

	/**
	 * Object to callback after the component traversed the bounds.
	 */
	private TraversedBoundsCallback callback;

	private boolean callbackOnTraverseStarted;

	/**
	 * Creates a callback behavior that calls the given callback after the
	 * object has traversed the given bounds. Calls again if the object comes in
	 * and traverses again.
	 * 
	 * @param bounds
	 *            Bounds which will be observed.
	 * @param callback
	 *            ...
	 * @param callbackOnTraverseStarted
	 *            determines if the callback should be done only once when the component starts to traverse the bounds.                      
	 */
	public CallBackOnBoundsTraversedBehavior(Bounds bounds, TraversedBoundsCallback callback, boolean callbackOnTraverseStarted) {
		this.bounds = bounds;
		this.callback = callback;
		this.callbackOnTraverseStarted = callbackOnTraverseStarted;
	}

	/**
	 * Creates the callback behavior without a callback. The state if the
	 * component is inside or outside the bounds can still be observed by the
	 * property
	 * {@link CallBackOnBoundsTraversedBehavior#getTraversedBoundsProperty()}
	 * 
	 * @param bounds
	 *            Bounds which will be observed.
	 */
	public CallBackOnBoundsTraversedBehavior(Bounds bounds) {
		this(bounds, null, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior#onApplyTo
	 * (org.sociotech.communitymirror.physics.PhysicsApplicable)
	 */
	@Override
	protected void onApplyTo(PhysicsApplicable component) {
		Bounds compBounds = component.getPositionBounds();

		// check if component is completely within bounds
		if (compBounds.getMinX() > bounds.getMinX() && compBounds.getMaxX() < bounds.getMaxX()
				&& compBounds.getMaxY() > bounds.getMinY() && compBounds.getMinY() < bounds.getMaxY()) {
			// in bounds
			wasInBounds = true;

			if (traversesBoundsProperty.getValue()) {
				// already traversed bounds
				// came in,
				// so set property
				traversesBoundsProperty.set(false);
			}
		} else {
			// in bounds in previous apply call
			// check if it has traversed bounds at any side
			
			// check left side, right side, top, buttom
			if (compBounds.getMinX() < bounds.getMinX() || compBounds.getMaxX() > bounds.getMaxX() || compBounds.getMinY() < bounds.getMinY() || compBounds.getMaxY() > bounds.getMaxY()) {
				if (!traversesBoundsProperty.getValue()) {
					// exited bounds
					// so set property
					traversesBoundsProperty.set(true);
				}

				// and callback if a callback was set
				if (callback != null && wasInBounds) {
					callback.traversedBounds(bounds, component);
					if(callbackOnTraverseStarted){
						// create a callback only when the component starts traversing
						wasInBounds = false;
					}
				}
			}
		}

	}

	/**
	 * Returns the bounds that will be observed. The object is the same that
	 * will be provided as callback parameter in
	 * {@link TraversedBoundsCallback#traversedBounds(Bounds, PhysicsApplicable)}
	 * 
	 * @return The bounds that will be observed by this behavior.
	 */
	protected Bounds getBounds() {
		return bounds;
	}

	/**
	 * Creates a callback behavior with an additional given offset.
	 * 
	 * @param bounds
	 *            Bounds to observe
	 * @param callback
	 *            Object to call after component traverses the bounds
	 * @param offset
	 *            Offset to all sides. Positive means inside, negative means
	 *            outside.
	 * 
	 * @return The newly created callback behavior
	 */
	public static CallBackOnBoundsTraversedBehavior createCallBackOnBoundsTraversedBehaviorWithOffset(Bounds bounds,
			TraversedBoundsCallback callback, boolean callbackOnTraverseStarted, double offset) {
		return new CallBackOnBoundsTraversedBehavior(new BoundingBox(bounds.getMinX() + offset,
				bounds.getMinY() + offset, bounds.getWidth() - 2 * offset, bounds.getHeight() - 2 * offset), callback, callbackOnTraverseStarted);
	}

	/**
	 * Creates a call back behavior with an offset to all sides.
	 * 
	 * @param bounds
	 *            Bounds
	 * @param callback
	 * @param offset
	 *            Offset to all sides. Positive means inside, negative means
	 *            outside.
	 * 
	 * @return The newly created call back behavior
	 */
	public static CallBackOnBoundsTraversedBehavior createCallBackOnBoundsTraversedBehaviorWithOffset(Bounds bounds,
			double offset) {
		return new CallBackOnBoundsTraversedBehavior(new BoundingBox(bounds.getMinX() + offset,
				bounds.getMinY() + offset, bounds.getWidth() - 2 * offset, bounds.getHeight() - 2 * offset));
	}

	/**
	 * Returns the property indicating if the object is in the bounds or
	 * traversed them.
	 * 
	 * @return The boolean property
	 */
	public BooleanProperty getTraversedBoundsProperty() {
		return traversesBoundsProperty;
	}
}
