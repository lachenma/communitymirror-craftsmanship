package org.sociotech.communitymirror.physics.behaviors;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import org.sociotech.communitymirror.physics.PhysicsApplicable;

/**
 * Abstract super class of all physics behaviors.
 * 
 * @author Peter Lachenmaier
 */
public abstract class PhysicsBehavior {

	/**
	 * Enabled state of this behavior.
	 */
	private final BooleanProperty enabled;

	/**
	 * The nano time of the last update. 
	 */
	private long lastUpdateNanoTime = -1;

	/**
	 * Contains the continuously updated speed correction factor
	 */
	private double speedCorrection = 1.0;
	
	/**
	 * Creates the physics behavior.
	 */
	public PhysicsBehavior() {
		enabled = new SimpleBooleanProperty(true);
	}
	
	/**
	 * Return whether this behavior is enabled or disabled.
	 * 
	 * @return Whether this behavior is enabled or disabled.
	 */
	public boolean isEnabled() {
		return enabled.get();
	}

	/**
	 * Sets the given enabled state.
	 * 
	 * @param enabled New enabled state.
	 */
	protected void setEnabled(boolean enabled) {
		this.enabled.set(enabled);
	}
	
	/**
	 * Enables this behavior.
	 */
	public void enable() {
		this.setEnabled(true);
	}
	
	/**
	 * Disables this behavior.
	 */
	public void disable() {
		this.setEnabled(false);
	}
	
	/**
	 * Returns the continuously updated speed correction factor.
	 *  
	 * @return The continuously updated speed correction factor
	 */
	protected double getSpeedCorrection() {
		return speedCorrection;
	}

	/**
     * Updates this physics behavior.
     * 
     * @param framesPerSecond Current frames per second.
     * @param nanoTime Nano time of the update step
     */
    public void update(double framesPerSecond, long nanoTime) {

    	if(!isEnabled() || nanoTime <= lastUpdateNanoTime) {
    		// only one update in every step
    		// this allows the behavior to be added to several components
    		return;
    	}
    	
    	this.onUpdate(framesPerSecond, nanoTime);
    	// update updated time
    	lastUpdateNanoTime = nanoTime;
    }

	/**
	 * Overwrite this method in concrete subclasses to perform your update code.
	 *   
	 * @param framesPerSecond Current frames per second.
     * @param nanoTime Nano time of the update step 
	 */
	protected void onUpdate(double framesPerSecond, long nanoTime) {
		// calculate speed correction factor
		speedCorrection = 60.0 / framesPerSecond;
	}

	/**
	 * Applies this behavior to the given component.
	 * 
	 * @param component Component to apply the behavior to.
	 */
	public void applyTo(PhysicsApplicable component) {
		if(!isEnabled()) {
			return;
		}
		
		// perform application
		onApplyTo(component);
	}

	/**
	 * Will be called when this behavior should be applied to the given component. Overwrite this method
	 * in concrete behavior implementations.
	 * 
	 * @param component Component to apply this behavior to.
	 */
	protected abstract void onApplyTo(PhysicsApplicable component);
}
