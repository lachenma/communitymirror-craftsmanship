package org.sociotech.communitymirror.physics.behaviors;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

import org.sociotech.communitymirror.physics.PhysicsApplicable;

/**
 * Behavior that let a component with a defined speed.
 * 
 * @author Peter Lachenmaier
 */
public class RotationBehavior extends AcceleratablePhysicsBehavior {

	/**
	 * The property for the rotation angle step.
	 */
	private final DoubleProperty rotationAngleStep;

	/**
	 * Creates a rotation behavior with the given acceleration and velocity. Acceleration will be performed
	 * until the target velocity will be reached. Velocity 1.0 and rotation angle 1.0 means 1 degree per second.
	 * 
	 * @param rotationAngle Rotation angle in one step
	 * @param velocity Current velocity
	 * @param acceleration Acceleration to apply in every step
	 * @param targetVelocity Velocity to reach due to acceleration
	 */
	public RotationBehavior(double rotationAngle, double velocity, double acceleration, double targetVelocity) {
		super(velocity, acceleration, targetVelocity);
		this.rotationAngleStep  = new SimpleDoubleProperty(rotationAngle);
	}
	
	/**
	 * Creates a drift behavior with the given velocity in the given direction. Acceleration will be set to zero and
	 * the target velocity will be set to the initial velocity value. 
	 * 
	 * @param rotationAngle Rotation angle in one step
	 * @param velocity Current velocity
	 */
	public RotationBehavior(double rotationAngle, double velocity) {
		// zero acceleration and target velocity is initial velocity
		this(rotationAngle, velocity, 0.0, velocity);
	}
	
	/**
	 * Creates a drift behavior with the given velocity in the given direction. Acceleration will be set to zero and
	 * the target velocity will be set to the initial velocity value. Default velocity is 1.0 
	 * 
	 * @param rotationAngle Rotation angle in one step
	*/
	public RotationBehavior(double rotationAngle) {
		// 1.0 velocity
		this(rotationAngle, 1.0);
	}
		
	/**
	 * Returns the rotation angle step property.
	 * 
	 * @return The rotation angle step property.
	 */
	public DoubleProperty rotationAngleStepProperty() {
		return rotationAngleStep;
	}
	
	/**
	 * Sets the value for the rotation angle step.
	 * 
	 * @param rotationAngle Rotation angle step.
	 */
	public void setRotationAngleStep(double rotationAngle) {
		this.rotationAngleStep.set(rotationAngle);
	}
		
	/**
	 * Creates a rotation behavior in clockwise direction.
	 * 
	 * @return The created rotation behavior.
	 */
	public static RotationBehavior createClockWiseRotationBehavior() {
		return new RotationBehavior(1.0);
	}
	
	/**
	 * Creates a rotation behavior in counterclockwise direction.
	 * 
	 * @return The created rotation behavior.
	 */
	public static RotationBehavior createCounterClockWiseRotationBehavior() {
		return new RotationBehavior(-1.0);
	}

	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior#onApplyTo(org.sociotech.communitymirror.physics.PhysicsApplicable)
	 */
	@Override
	protected void onApplyTo(PhysicsApplicable component) {
		// simply rotate the component with the current velocity corrected by the speed factor in x and y direction
		component.rotate(getSpeedCorrection() * velocityProperty().get() * rotationAngleStep.get());
	}
}
