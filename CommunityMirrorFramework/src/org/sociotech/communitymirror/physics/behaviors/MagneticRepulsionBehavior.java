package org.sociotech.communitymirror.physics.behaviors;

import javafx.geometry.Bounds;

import org.sociotech.communityinteraction.behaviors.MagneticEffectBehavior;
import org.sociotech.communityinteraction.behaviors.MagneticField;
import org.sociotech.communitymirror.physics.PhysicsApplicable;
import org.sociotech.communitymirror.visualitems.VisualComponent;

public class MagneticRepulsionBehavior extends PhysicsBehavior {

	private DriftBehavior driftBehavior;
	private VisualComponent otherComponent;

	private double deltaMinX = Double.MAX_VALUE;
	private double deltaMaxX = Double.MAX_VALUE;
	private double deltaMinY = Double.MAX_VALUE;
	private double deltaMaxY = Double.MAX_VALUE;

	public MagneticRepulsionBehavior(DriftBehavior driftBehavior,
			VisualComponent otherComponent) {
		this.otherComponent = otherComponent;
		this.driftBehavior = driftBehavior;
	}

	private void calculateDeltas(PhysicsApplicable component) {
		// get bounds of this component
		Bounds compBounds = component.getPositionBounds();
		// calculate bounds of magnetic field
		double componentMinX = compBounds.getMinX();
		double componentMaxX = compBounds.getMaxX();
		double componentMinY = compBounds.getMinY();
		double componentMaxY = compBounds.getMaxY();

		// get bounds of other component
		Bounds otherBounds = otherComponent.getPositionBounds();
		// get range of magnetic field of other component
		double range = this.getMagneticRange();
		// calculate bounds of magnetic field
		double otherMinX = otherBounds.getMinX() - range;
		double otherMaxX = otherBounds.getMaxX() + range;
		double otherMinY = otherBounds.getMinY() - range;
		double otherMaxY = otherBounds.getMaxY() + range;

		// initialize deltas with MAX_VALUE
		this.deltaMinX = Double.MAX_VALUE;
		this.deltaMaxX = Double.MAX_VALUE;
		this.deltaMinY = Double.MAX_VALUE;
		this.deltaMaxY = Double.MAX_VALUE;

		// if left side of component is within bounds
		if (componentMinX > otherMinX && componentMinX < otherMaxX) {
			deltaMinX = otherMaxX - componentMinX;
		}
		// if right side of component is within bounds
		if (componentMaxX > otherMinX && componentMaxX < otherMaxX) {
			deltaMaxX = componentMaxX - otherMinX;
		}
		// if top of component is within bounds
		if (componentMinY > otherMinY && componentMinY < otherMaxY) {
			deltaMinY = otherMaxY - componentMinY;
		}
		// if bottom of component is within bounds
		if (componentMaxY > otherMinY && componentMaxY < otherMaxY) {
			deltaMaxY = componentMaxY - otherMinY;
		}
	}

	private double getMagneticRange() {
		// get magnetic field of other component
		MagneticEffectBehavior magneticEffectBehavior = (MagneticEffectBehavior) otherComponent
				.getFirstInteractionBehavior(new MagneticEffectBehavior(null,
						null).getClass());
		if (magneticEffectBehavior == null) {
			return 0;
		}
		MagneticField magneticField = magneticEffectBehavior.getMagneticField();
		if (magneticField == null) {
			return 0;
		}
		return magneticField.getRange();
	}

	private void moveComponentOutOfBounds(PhysicsApplicable component) {
		// if the shortest way out is to move maxY out
		if (deltaMaxY <= deltaMinY && deltaMaxY <= deltaMinX
				&& deltaMaxY <= deltaMaxX) {
			// move up and out of bounds
			component.move(0, -deltaMaxY);
			// drift up
			driftBehavior.setYDirection(-1);
			driftBehavior.setXDirection(0);
		}
		// if the shortest way out is to move minY out
		else if (deltaMinY <= deltaMaxY && deltaMinY <= deltaMinX
				&& deltaMinY <= deltaMaxX) {
			// move down and out of bounds
			component.move(0, deltaMinY);
			// drift downwards
			driftBehavior.setYDirection(1);
			driftBehavior.setXDirection(0);
		}
		// if the shortest way out is to move minX out
		else if (deltaMinX <= deltaMinY && deltaMinX <= deltaMaxY
				&& deltaMinX <= deltaMaxX) {
			// move component to the right
			component.move(deltaMinX, 0);
			// drift to the right
			driftBehavior.setXDirection(1);
			driftBehavior.setYDirection(0);
		}
		// if the shortest way out is to move maxX out
		else {
			// move component to the left and out of bounds
			component.move(-deltaMaxX, 0);
			// move component to the left
			driftBehavior.setXDirection(-1);
			driftBehavior.setYDirection(0);
		}
	}

	private void resetInteractionTime(PhysicsApplicable component) {
		// reset interaction time
		if (component instanceof VisualComponent) {
			((VisualComponent) component).resetInteractionTime();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior#onApplyTo
	 * (org.sociotech.communitymirror.physics.PhysicsApplicable)
	 */
	@Override
	protected void onApplyTo(PhysicsApplicable component) {
		// calculate deltas
		this.calculateDeltas(component);

		// move component out of bounds and set drift direction
		this.moveComponentOutOfBounds(component);

		// reset interaction time
		this.resetInteractionTime(component);
	}
}
