package org.sociotech.communitymirror.physics.behaviors;

import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;

import org.sociotech.communitymirror.physics.PhysicsApplicable;

/**
 * Behavior that prevents a component from getting out of certain bounds.
 * 
 * @author Peter Lachenmaier
 */
public class StayInBoundsBehavior extends PhysicsBehavior {

	/**
	 * Bounds to keep the Component inside
	 */
	private Bounds bounds;
	
	/**
	 * Creates a behavior that keeps a component inside the given bounds.
	 * 
	 * @param bounds Bounds to keeps the component inside.
	 */
	public StayInBoundsBehavior(Bounds bounds) {
		this.bounds = bounds;
	}
	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior#onApplyTo(org.sociotech.communitymirror.physics.PhysicsApplicable)
	 */
	@Override
	protected void onApplyTo(PhysicsApplicable component) {
		Bounds compBounds = component.getPositionBounds();
		
		// check left side
		if(compBounds.getMinX() < bounds.getMinX()) {
			component.move(bounds.getMinX() - compBounds.getMinX(), 0.0);
		}
		
		// check right side
		if(compBounds.getMaxX() > bounds.getMaxX()) {
			component.move(bounds.getMaxX() - compBounds.getMaxX(), 0.0);
		}
		
		// check top
		if(compBounds.getMinY() < bounds.getMinY()) {
			component.move(0.0, bounds.getMinY() - compBounds.getMinY());
		}

		// check bottom
		if(compBounds.getMaxY() > bounds.getMaxY()) {
			component.move(0.0, bounds.getMaxY() - compBounds.getMaxY());
		}	
	}
	
	/**
	 * Creates a stay in bounds behavior with an offset to all sides.
	 * 
	 * @param bounds Bounds
	 * @param offset Offset to all sides. Positive means inside, negative means outside.
	 * 
	 * @return The newly created stay in bounds behavior 
	 */
	public static StayInBoundsBehavior createStayInBoundsBehaviorWithOffset(Bounds bounds, double offset) {
		// TODO: maybe apply offset in violation calculation to let bounds be dynamically updated from outside
		return new StayInBoundsBehavior(new BoundingBox(bounds.getMinX() + offset,
														bounds.getMinY() + offset,
														bounds.getWidth()  - 2 * offset,
														bounds.getHeight() - 2 * offset));
	}

}
