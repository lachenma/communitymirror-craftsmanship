package org.sociotech.communitymirror.physics.behaviors;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

/**
 * Abstract super class of all acceleratable physics behaviors. 
 * 
 * @author Peter Lachenmaier
 */
public abstract class AcceleratablePhysicsBehavior extends PhysicsBehavior {

	/**
	 * The property for the acceleration.
	 */
	private final DoubleProperty acceleration;

	/**
	 * The property for the current velocity.
	 */
	private final DoubleProperty velocity;

	/**
	 * The property for the velocity that should be reached due to acceleration.
	 */
	private final DoubleProperty targetVelocity;
		
	/**
	 * Creates a acceleratable behavior with the given acceleration and velocity in the given direction. Acceleration will be performed
	 * until the target velocity will be reached.
	 * 
	 * @param velocity Current velocity
	 * @param acceleration Acceleration to apply in every step
	 * @param targetVelocity Velocity to reach due to acceleration
	 */
	public AcceleratablePhysicsBehavior(double velocity, double acceleration, double targetVelocity) {
		this.acceleration   = new SimpleDoubleProperty(acceleration);
		this.velocity       = new SimpleDoubleProperty(velocity);
		this.targetVelocity = new SimpleDoubleProperty(targetVelocity);
	}
	
	/**
	 * Creates a acceleratable behavior with the given velocity. Acceleration will be set to zero and
	 * the target velocity will be set to the initial velocity value. 
	 * 
	 * @param velocity Current velocity
	 */
	public AcceleratablePhysicsBehavior(double velocity) {
		// zero acceleration and target velocity is initial velocity
		this(velocity, 0.0, velocity);
	}
	
	/**
	 * Creates a acceleratable behavior. Acceleration will be set to zero and
	 * the target velocity will be set to the initial velocity value. Default velocity is 1.0. 
	 */
	public AcceleratablePhysicsBehavior() {
		// 1.0 velocity
		this(1.0);
	}
	
	/**
	 * Returns the acceleration property.
	 * 
	 * @return The acceleration property.
	 */
	public DoubleProperty accelerationProperty() {
		return acceleration;
	}
	
	/**
	 * Returns the velocity property.
	 * 
	 * @return The velocity property.
	 */
	public DoubleProperty velocityProperty() {
		return velocity;
	}
	
	/**
	 * Returns the target velocity property.
	 * 
	 * @return The target velocity property.
	 */
	public DoubleProperty targetVelocityProperty() {
		return targetVelocity;
	}
		
	/**
	 * Sets the value for the acceleration.
	 * 
	 * @param targetVelocity Acceleration
	 */
	public void setAcceleration(double acceleration) {
		this.acceleration.set(acceleration);
	}
	
	/**
	 * Sets the value for the velocity.
	 * 
	 * @param velocity Velocity
	 */
	public void setVelocity(double velocity) {
		this.velocity.set(velocity);
	}
	
	/**
	 * Sets the value for the target velocity.
	 * 
	 * @param targetVelocity Target velocity
	 */
	public void setTargetVelocity(double targetVelocity) {
		this.targetVelocity.set(targetVelocity);
	}

	
	/* (non-Javadoc)
	 * @see org.sociotech.communitymirror.physics.behaviors.PhysicsBehavior#onUpdate(double, long)
	 */
	@Override
	protected void onUpdate(double framesPerSecond, long nanoTime) {
		super.onUpdate(framesPerSecond, nanoTime);
			
		if(acceleration.get() != 0.0) {
			double newVel = velocity.get() + acceleration.get();
			// ensure that new velocity is between 0 and target velocity
			if(newVel < 0.0) {
				newVel = 0.0;
			} else if(newVel > targetVelocity.get()) {
				newVel = targetVelocity.get();
			}
			
			// set if different
			if(newVel != velocity.get()) {
				velocity.set(newVel);
			}
		}
	}
}
