	//Einstellungen der Kartenoptionen und Steuerelemente
	function mapInit() {
	var mapOptions = {
        //center: new google.maps.LatLng(52.2931465, 9.1572712),
        //zoom: 11,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		//mapTypeControl: true,
		streetViewControl: false,
		panControl: false,
		zoomControl: false,
		//scaleControl: true,
		//overviewMapControl: true
		};
		
	//Instanziieren der Map
	map = new google.maps.Map(document.getElementById("map_canvas"),
        mapOptions);
	}