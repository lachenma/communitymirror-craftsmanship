function initialize() {	
		//Initialisieren der Map
		mapInit();
		
		//Marker einfügen
		setMarkers(map);
		
		//Clusterer für Marker
		//initClusterer(map);
		
		//suchen()-Funktion aufrufen, um der Ergebnismenge alle Marker zuzuweisen
		suchen(map);
	}
	
	//DomListener, welcher auf Änderungen im HTML und CSS Markup wartet
	google.maps.event.addDomListener(window, "load", initialize);